<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>

<html>

<head>

<title><spring:message code="accueil.home"/></title>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/accueilStyle.css" />"	rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> 

</head>

<body>

<!-- 	************************** IMAGE DE FOND **************************	 -->

			<div id="fond">
				<img src="../static/images/blue-motion-blur-design.jpg"/>
			</div>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG">
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD">
				<img src="../static/images/newLogoAusy.png"/>
			</div>

	<c:import url="header.jsp" /><br>	
	
	<c:if test="${message == 'inscriptionOK'}">
	  <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
      <div class="info">Vous �tes bien inscrit, un email de confirmation vous a �t� envoy�.<br>Veuillez maintenant vous identifier.</div>      
	</c:if>

	 <c:if test="${message == 'erreurIdentification'}">
		  <div class="error">Nous avons detect� une erreur.<br>Votre mot de passe et/ou votre login est incorrect.</div>               
	 </c:if>
	  
	 <c:if test="${message == 'messageMailKo'}">
	   <div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
	   <div class="error">Il n'y a pas de compte enregistr� avec cette adresse Email.<br>V�rifier votre Email ou inscrivez vous sur le site.</div>      
	 </c:if>
	      
	 <c:if test="${message == 'messageMailOk'}">
	   <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	   <div class="info">Un Email contenant un lien permettant la r�initialisation <br>de votre mot de passe est envoy� � l'adresse indiqu�e.</div>      
	 </c:if>
	      
	 <c:if test="${message == 'messageMotDePasseReinitialise'}">
	   <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	   <div class="info">Votre mot de passe a �t�  r�initialis�. </div>     
	 </c:if>
   
    <c:if test="${message == 'erreurConnection'}">
		<div class="error">Vous n'�tes pas connect�.<br> Merci de vous identifier.</div>               
	</c:if>
	
	    <div class="jumbotron">
	         <div class="slogan">
			CV creator vous permet de cr�er vous m�me en toute simplicit� un <b>CV design et moderne</b>, <b>gratuitement</b> et, ce, en <b>quelques minutes</b>.<br>
			� l'aide de nombreux mod�les, vous serez en mesure de cr�er ou modifier CV professionnel en quelques clics.<br>
			Prise en main rapide grace � la rubrique Aide. <br>
			T�l�chargement en PDF. 
			</div>
		</div>
<br>
	<!--SLIDE SOW -->

	<div class="container">
		<div id="slides">
			<div id="myCarousel" class="carousel slide" data-ride="carousel">
				<!-- Indicators -->
				<ol class="carousel-indicators">
					<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
					<li data-target="#myCarousel" data-slide-to="1"></li>
					<li data-target="#myCarousel" data-slide-to="2"></li>
					<li data-target="#myCarousel" data-slide-to="3"></li>
				</ol>

				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">

					<div class="item active">
						<img id="image1" src="<c:url value="/static/images/CV1.png"/>" alt="CV1"
							 />
						<div id="carousel-caption">
							<h2 id="h2">Exemple CV1</h2>
							<p></p>
						</div>
					</div>

					<div class="item">
						<img id="image2" src="<c:url value="/static/images/CV2.png"/>" alt="CV2"
							width="100px" />
						<div id="carousel-caption">
							<h2 id="h2">  Exemple CV2</h2>
							<p></p>
						</div>
					</div>

					<div class="item">
						<img id="image3" src="<c:url value="/static/images/CV3.png"/>" alt="CV3"
							width="100px" />
						<div id="carousel-caption">
							<h2 id="h2">Exemple CV3</h2>
							<p></p>
						</div>
					</div>

					<div class="item">
						<img id="image4" src="<c:url value="/static/images/CV4.png"/>" alt="CV4"
							width="100px" />
						<div id="carousel-caption">
							<h2 id="h2">Exemple CV4</h2>
							<p></p>
						</div>
					</div>

				</div>

				<!-- Left and right controls -->
				<a class="left carousel-control" href="#myCarousel" role="button"
					data-slide="prev"> <span
					class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
					<span class="sr-only">Previous</span>
				</a> <a class="right carousel-control" href="#myCarousel" role="button"
					data-slide="next"> <span
					class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
					<span class="sr-only">Next</span>
				</a>
			</div>
		</div>
	</div>
	
		<!--  Footer -->
	<c:import url="partials/footer.jsp" />
	
	<div id="connexionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
	
</body>
</html>