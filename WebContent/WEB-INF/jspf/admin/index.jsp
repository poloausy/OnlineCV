<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin - Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/accueilStyle.css" />"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>
<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG" >
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD"  >
				<img src="../static/images/newLogoAusy.png"/>
			</div>

	<!--  Header -->
	
	<c:import url="../header.jsp" />
   	  <div class="main">
		<div class="container">
		  		<div class="row">
				<div class="col-lg-2">
				
					<!--  ADMIN SIDE NAV  -->
					
					<c:import url="../partials/admin-sidenav.jsp" />
				</div>
				<div class="col-lg-9 col-lg-push-1"><h3>Welcome <span id="nomLogin" >${utilisateur.credential.nomLogin}</span> </h3> </div>
			</div>
		</div>
 	</div>
	<!--  Footer -->
	<c:import url="../partials/footer.jsp" />
</body>
</html>