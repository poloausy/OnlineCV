<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin - Langues</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>
<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG">
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD">
				<img src="../static/images/newLogoAusy.png"/>
			</div>

	<!--  HEADER -->
	
	<c:import url="../header.jsp" />

	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
				
					<!--  ADMIN SIDENAV  -->
					
					<c:import url="../partials/admin-sidenav.jsp" />
				</div>
				<div class="col-lg-9 col-lg-push-1">
				
				<!--  MESSAGE D'ERREUR  -->
				
				<c:if test="${erreur eq 'impossible'}">
					<div class="jumbotron">
					<div class="error">
					
						Impossible de supprimer la langue <b><c:out value="${langue}" /></b> car les utilisateurs suivants y sont li�s:
						<table class="tableoutil1" align="center">
	 					<c:forEach items="${listeUtilisateur}" var="utilisateur">
							<tr>
	 							<td> ${utilisateur.prenom}&nbsp;${utilisateur.nom}</td>
								<td> ${utilisateur.credential.email}</td>
							</tr>		
						</c:forEach>
						<button type="button" class="btn btn-xs btn-primary" style="position: absolute;top:160px;right:20px;" onclick=window.location.href="<c:url value="/admin/langues" />">Exit</button>
						</table>		
					</div>	
					</div>	
				</c:if>
				<!------------------------>
				
				<!--  MESSAGE DE CONFIRMATION DE SUPPRESSION  -->
				
				<c:if test="${erreur eq 'supprime'}">
					<div class="jumbotron">
					<div class="error">
					La langue <b><c:out value="${langue}" /></b> a �t� supprim�e !
					<a class="close" style="z-index: 3;opacity: 1;color: darkblue;cursor: pointer;font-size: 40px;" href="<c:url value="/admin/langues" />">�</a>
					</div>
					</div>
				</c:if>
								
				<!------------------------>
				
				
					<h2>All Langues</h2>
					<table class="table langues admin-list">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nom Langue</th>
								<th></th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						
							<!--  LISTE DE TOUS LES LANGUES DANS LA BDD -->
							
							<c:forEach items="${listeNomLangue}" var="nomLangue">
								<tr>
									<td>${nomLangue.id}</td>
									<td>${nomLangue.nom}</td>

									<td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#supprimer${nomLangue.id}">Supprimer</button></td>
								</tr>
								
								<!--  ***************POPUP MESSAGE CONFIRMATION SUPPRESSION***************	 -->
														
								    <div id="supprimer${nomLangue.id}" class="modal fade" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
											
						                    <!--MODAL HEADER -->
											
											<div class="modal-header d-flex justify-content-center">
											<h2><b>CONFIRMATION DE SUPPRESSION</b></h2>
											
													<!-- <button type="button" class="close" data-dismiss="modal">x</button> -->							
											</div>
											
						                    <!--MODAL BODY -->
											
											<div class="modal-body">
						 					<h3><strong><font color="red">Etes vous s�r de vouloir supprimer la langue  ${nomLangue.nom} ?</font></strong></h3> 						 
											</div>
											
						                    <!--MODAL FOOTER -->
						                    
											<div class="modal-footer">					
											<button type="button" class="btn btn-danger" onclick=window.location.href="<c:url value="/admin/supprimeNomLangue/${nomLangue.id}"/>">Supprimer</button>					
											<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>					
											
										    </div>
									     </div>
								     </div>								    
							</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--  Footer -->
	<c:import url="../partials/footer.jsp" />
</body>
</html>