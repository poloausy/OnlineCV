<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!DOCTYPE html>

<html>

<head>

<title>ADMIN - ENTREPRISE</title>

<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/accueilStyle.css" />"	rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">

</head>

<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG">
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD">
				<img src="../static/images/newLogoAusy.png"/>
			</div>

	<!--  HEADER -->
	 
	<c:import url="../header.jsp" />
	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
				
					<!--  ADMIN SIDENAV  -->
					
					<c:import url="../partials/admin-sidenav.jsp" />
				</div>
				<div class="col-lg-9 col-lg-push-1">
				
				<!--  MESSAGE D'ERREUR UN UTILISATEUR LIE A UN METIER -->
				
				<c:if test="${erreur eq 'impossible'}">
					<div class="jumbotron">
					<div class="error">
						Impossible de supprimer le m�tier <b><c:out value="${metier}" /></b> car les utilisateurs suivants y sont li�s:
						<table class="tableoutil1" align="center">
	 					<c:forEach items="${listeUtilisateur}" var="utilisateur">
							<tr>
	 							<td> ${utilisateur.prenom}&nbsp;${utilisateur.nom}</td>
								<td> ${utilisateur.credential.email}</td>
							</tr>		
						</c:forEach>
						<button type="button" class="btn btn-xs btn-primary" style="position: absolute;top:160px;right:65px;" onclick=window.location.href="<c:url value="/admin/metiers" />">Exit</button>
						</table>		
					</div>	
					</div>	
				</c:if>
				
				
				<!--  MESSAGE DE CONFIRMATION DE SUPRESSION  -->
				
				<c:if test="${erreur eq 'metier supprime'}">
					<div class="jumbotron">
					<div class="error">
					Le m�tier <b><c:out value="${metier}" /></b> a �t� supprim� !
					<a class="close" style="z-index: 3;opacity: 1;color: darkblue;cursor: pointer;font-size: 40px;" href="<c:url value="/admin/metiers" />">�</a>
					</div>
					</div>
				</c:if>
								
				
					<h2>All Metiers</h2>
					<table class="table metiers admin-list">
						<thead>
							<tr>
								<th>ID</th>
								<th>Nom M�tiers</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
						
							<!--  LISTE DE TOUS LES METIERS DANS LA BDD -->
							
							<c:forEach items="${listeMetiers}" var="metier">
								<tr>
									<td>${metier.id}</td>
									<td>${metier.nom}</td>
									<td><button type="button" class="btn btn-danger" data-toggle="modal" data-target="#supprimer${metier.id}">Supprimer</button></td>
								</tr>
							
		
								<!--  ***************POPUP MESSAGE CONFIRMATION SUPPRESSION***************	 -->
														
								    <div id="supprimer${metier.id}" class="modal fade" role="dialog">
										<div class="modal-dialog">
											<div class="modal-content">
											
						                    <!--MODAL HEADER -->
											
											<div class="modal-header d-flex justify-content-center">
											<h2><b>CONFIRMATION DE SUPPRESSION</b></h2>
											</div>
											
						                    <!--MODAL BODY -->
											
											<div class="modal-body">
						 					<h3><strong><font color="red">Etes vous s�r de vouloir supprimer le metier ${metier.nom} ?</font></strong></h3> 						 
											</div>
											
						                    <!--MODAL FOOTER -->
						                    
											<div class="modal-footer">					
											<button type="button" class="btn btn-danger" onclick=window.location.href="<c:url value="/admin/supprimeMetiers/${metier.id}"/>">Supprimer</button>					
											<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
											</div>					
											
										    </div>
									     </div>
								     </div>	
								    
		     				</c:forEach>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<!--  Footer -->
	<c:import url="../partials/footer.jsp" />
</body>
</html>