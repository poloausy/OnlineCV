<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Admin - Templates</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/accueilStyle.css" />"
	rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>
<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG">
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD">
				<img src="../static/images/newLogoAusy.png"/>
			</div>

	<!--  Header -->
	<c:import url="../header.jsp" />
	<div class="main">
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
					<!--  Admin sidenav  -->
					<c:import url="../partials/admin-sidenav.jsp" />
				</div>
				<div class="col-lg-9 col-lg-push-1">
			
				<h2>All templates</h2>
				
				  <c:if test="${message == 'message'}">
				  <div class="jumbotron">
	                 <div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
	                 <div class="error">Attention un template avec un nom similaire existe d�j� !!<br>Modifier le nom du template pour pouvoir l'ajouter.</div>  
	              </div>	    
	             </c:if>
	            					
				<table class="table templates admin-list">
					<thead>
						<tr>
							<th>ID</th>
							<th>NOM</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					
					<!--  LIST ALL TEMPLATES FROM DATABASE -->
					
					  <c:forEach items="${listeTemplates}" var="template">
						<tr>
						  <td><b>${template.id}</b></td>
						  <td><b>${template.nom}</b></td>
						  <td><button type="button" id="btnSubmit" class="btn btn-danger" data-toggle="modal" data-target="#supprimer${template.id}"> Supprimer</button></td>


			<!--  ***************POPUP MESSAGE CONFIRMATION SUPPRESSION***************	 -->
			
		             <div id="supprimer${template.id}" class="modal fade" role="dialog">
				         <div class="modal-dialog">
					       <div class="modal-content">
					
                           <!--Modal Header -->
						   <div class="modal-header d-flex justify-content-center">
					         <h2><b>CONFIRMATION DE SUPPRESSION</b></h2>													
					       </div>
					
                          <!--Modal body -->
					      <div class="modal-body">
 					         <h3 id ="nomTemplate"><strong><font color="red">ETES VOUS SUR DE VOULOIR SUPPRIMER LE TEMPLATE ${template.nom} !!!</font></strong></h3> 						 
					      </div>
					
                          <!--Modal footer -->
                          <div class="modal-footer">					
					        <button type="button"  id="primaryButton" class="btn btn-danger" data-dismiss="modal" data-toggle="modal" data-target="#templateSupprime${template.id}"
					        data-backdrop="static" data-keyboard="false">Supprimer </button>		
							    	        				
					        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					      </script>
					      </div>			
		     	         </div>
			            </div>
	                  </div>
	               <!--  *************************************************************	 -->  
	               	
	               <!--  ***************MESSAGE TEMPLATE SUPPRIME ***************	 -->
	               
		             <div id="templateSupprime${template.id}" class="modal fade" role="dialog">
				         <div class="modal-dialog">
					       <div class="modal-content">
					                        
					                         
					      <div class="modal-body">
 					         <h3><strong><font color="blue"> Le template ${template.nom} a �t� supprim�.</font></strong></h3> 						 
					      </div>
	                      <div class="modal-footer">	
					        <button type="button" class="btn btn-primary" data-dismiss="modal" onclick=window.location.href="<c:url value='/admin/supprimeTemplate/${template.id}'/>" >Close</button>
					      </div>			
		     	         </div>
			            </div>
		               </div>
		         <!--  *************************************************************	 -->      	
				     </tr>
					</c:forEach>
				  </tbody>
				</table>
			    <div class="wrapperCenter">
		            <button type="button" align="center" class="btn btn-primary" data-toggle="modal" data-target="#ajoutTemplate" >Ajouter <br/>un nouveau template<br/>(fichier jsp)</button>
		        </div> 
		   
		        
		        <!--  ***************POPUP MESSAGE AJOUT TEMPLATE***************	 -->
		        
		           <div id="ajoutTemplate" class="modal fade" role="dialog">
				       <div class="modal-dialog">
					     <div class="modal-content">
					
                         <!--Modal Header -->
						  <div class="modal-header d-flex justify-content-center">
					         <h2><b>Quel template voulez vous ajouter ?</b></h2>													
					      </div>
					
                          <!--Modal body -->
					       <div class="modal-body">
		                     <form  method="POST" action="${pageContext.request.contextPath}/admin/saveTemplate" modelAttribute="template" enctype="multipart/form-data" >			      
 					           <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						       <input id="fileInput" type="file" name="file"  accept=".jsp" class="form-control"  multiple onchange="showname()">			
			                   <br/><br/>
			                   <input type="submit" class="btn btn-primary" value="Sauvegarder " id="submit">
		                    </form>					 
					      </div>
					      <!--Modal footer -->
                          <div class="modal-footer">					
					         <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
					      </script>
					      </div>			
		     	         </div>
			            </div>
	                  </div>
		        <!--  *************************************************************	 --> 	         
			  </div>
			</div>
		  </div>   
	    </div>
	<!--  Footer -->
	<c:import url="../partials/footer.jsp" />
	
  </body>
</html>