<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
</head>


<body>
<!-- LISTE DES CERTIFICATIONS	-->
	<div text-align="center">
	
		<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveCertification" modelAttribute="certification">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<form:hidden path="certification.id" />
					
		<table class="table table-striped">
			<tr>
				<td><b><u>Certification</b></u></td>
				<td><b><u>Date</b></u></td>
				<td><b><u>Score</b></u></td>
				<td></td>
			</tr>

			<c:forEach items="${listeCertification}" var="certification">
				<tr>
					<td><c:out value="${certification.intitule}" /></td>
					<td><c:out value="${certification.date}" /></td>
					<td><c:out value="${certification.score}" /></td>
					<td><input type="button" class="btn btn-primary" align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeCertification/${certification.id}" />">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</c:forEach>
								
			<tr>
					<td><form:input id="certif1" value="" path="certification.intitule" class="form-control" required="required"/></td>
					<td><form:input id="certification-date" type="month" value="" path="certification.date" class="form-control" required="required" autocomplete="off"/></td>
					<td><form:input type="number" min="0" max="10" value="" path="certification.score" class="form-control" required="required"/></td>
		
					<td>
					<input id="s5" type="submit" class="btn btn-primary" value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;
					</td>
			</tr>		
		</table>
		
		<c:if test="${erreur eq 'doublon'}">
				<span class="error"><spring:message code="error.certification.doublon" /></span>
			</c:if>
			
		</form>	
		
		<p style="text-align: center;">
		<input id="fermerAccCertification" type="button" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
		</p>		
	</div>
</body>
</html>