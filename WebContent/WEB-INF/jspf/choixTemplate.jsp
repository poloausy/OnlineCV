<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />"
	rel="stylesheet">
</head>

<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG" >
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD"  >
				<img src="../static/images/newLogoAusy.png"/>
			</div>

<c:import url="header.jsp" />

<div class="row">
  <div class="col-lg-3">

   <!-- Sidenav partial -->
   <sec:authorize
	 access="hasAnyRole('ROLE_UTILISATEUR')">
	 <c:import url="partials/utilisateur-sidenav.jsp" />
   </sec:authorize>

   <sec:authorize
     access="hasAnyRole('ROLE_ADMIN')">
	 <c:import url="partials/admin-sidenav-cv.jsp" />
   </sec:authorize>
   
   <sec:authorize
     access="hasAnyRole('ROLE_MANAGER')">
	 <c:import url="partials/manager-sidenav-cv.jsp" />
   </sec:authorize>
 </div>

 <div class="col-lg-9 col-lg-push-0"> 
   <!-- Main -->
   <div class="main">
     <div class="container">

		<h2 class="textBleuClair"> Choix du template <span class="nomlogin">${utilisateur.prenom} ${utilisateur.nom}</span></h2>
		<br>
		<c:forEach items="${listeTemplates}" var="template">
		<a id = ${template.nom} class="btn btn-primary" href="<c:url value="/visiteur/goToTemplate/${template.nom}" />"> Template ${template.nom} </a><br><br>
		</c:forEach>

     </div>	
  </div>	
 </div>
</div>
</body>
</html>