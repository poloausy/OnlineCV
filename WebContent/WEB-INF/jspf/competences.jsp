<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>CV Online</title>
</head>


<body>
	<p>   Comp�tences � afficher dans le CV</p>
	<div text-align="center">
			    
		<!--  Competences table -->

			<table class="table table-striped">
				<tr>
					<td><b><u>Competence</b></u></td>
					<td><b><u>Niveau</b></u></td>
					<td></td>
					<td></td>
					
				</tr>
				<c:forEach items="${listeCompetence}" var="competence">
				
		<!--  ************************************************************ BOUCLE COMPETENCE RIEN A MODIFIER **************************************************************** -->
		
				<c:if test="${idCompetenceAModifer == null}">
					
					<tr>
						<td><c:out value="${competence.intitule}" /></td>
						<td><c:out value="${competence.niveau.intitule2}" /></td>
						<td><input type="button" class="btn btn-primary" align="middle" value="Modifier" onclick=window.location.href="<c:url value="/donneesVisiteur/goToModifieCompetence/${competence.id}" />">&nbsp;</td>
						<td><input type="button" class="btn btn-primary" align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeCompetence/${competence.id}" />">&nbsp;</td>
					</tr>
				</c:if>
				
	<!-- 		************************************************** BOUCLE MODIFICATION D'UNE COMPETENCE EXISTANTE *************************************************************		 -->
				
				<c:if test="${idCompetenceAModifer != null}">
			
				<c:choose>
				<c:when test="${idCompetenceAModifer eq competence.id}">
				
			    <tr>
				   	<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/competenceModifier" modelAttribute="competence">
				    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
				    <td><form:input value="${competence.intitule}" path="competence.intitule" class="form-control" required="required"/></td>
					
					
					<td>
					<form:hidden path="competence.id" name="id" value="${competence.id}" />
					<form:select path="competence.niveau.id" text-align="center" class="form-control" required="required"> 
 					<form:option value="" label="${competence.niveau.intitule2}" /> 
					<form:options items="${listeNiveau}" itemValue="id" itemLabel="intitule2" /> 
					</form:select>
					</td>
							
					<td><input type="submit" class="btn btn-primary" align="middle" value="Update" /></td>
					<td></td>					
					</form>					
				</tr>
				
				      
				
				</c:when>
				<c:otherwise>	
				<tr>
						<td><c:out value="${competence.intitule}" /></td>
						<td><c:out value="${competence.niveau.intitule2}" /></td>
						<td><input type="button" class="btn btn-primary" align="middle" value="Modifier" onclick=window.location.href="<c:url value="/donneesVisiteur/competenceModifier/${competence.id}" />">&nbsp;</td>
						<td><input type="button" class="btn btn-primary" align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeCompetence/${competence.id}" />">&nbsp;</td>
					</tr>
				</c:otherwise>	
					
				</c:choose>
					
				</c:if>					
				</c:forEach>
				
<!-- 		************************************************** SAUVEGARDE NOUVELLE COMPETENCE *************************************************************		 -->
				
				<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveCompetence" modelAttribute="competence">
			    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

				<tr>
					<td><form:input id="comp1" type="text" name="competence" path="competence.intitule" class="form-control" required="required"></form:input></td>
					<td colspan="2"><form:select id="n1" path="competence.niveau.id" class="form-control" required="required">
					<form:option value="" label="" />
					<form:options items="${listeNiveau}" itemValue="id" itemLabel="intitule2" />
					</form:select></td>
					<td><input id="s4" type="submit" class="btn btn-primary" value="Sauvegarder" align="middle" /></td>
				</tr>
			</form>
			</table>
			
			<c:if test="${erreur eq 'doublon'}">
				<span class="error"><spring:message code="error.competence.doublon" /></span>
			</c:if>

		
	<!-- 		************************************************** FERMETURE FORMULAIRE COMPETENCE *************************************************************		 -->	
			<center>
			<P>
			<input id="fermerAccCompetence" type="button" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
			</P>
			</center>
	</div>
</body>
</html>