<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/fmt" prefix = "fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>EXPERIENCE JSP</title>

<link href="<c:url value="/static/css/accordionStyle.css" />" rel="stylesheet">
</head>


<body>
<!-- LISTE DES EXPERIENCES	-->
	<div class ="experience" text-align="center">
<%-- 	<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveExperience" modelAttribute="experience"> --%>
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

			<c:forEach items="${listeExperience}" var="experience">
				
				<b>De <fmt:formatDate value="${experience.dateDebut}" pattern="MM/yyyy" /> � <fmt:formatDate value="${experience.dateFin}" pattern="MM/yyyy" /> - 
				<c:out value="${experience.metier.nom}" /> - 
				<c:out value="${experience.entreprise.nom}" /></b>				
				<p align="right">

				<input type="button" class="btn btn-primary" align="middle" value="Modifier" onclick=window.location.href="<c:url value="/donneesVisiteur/modifieExperience/${experience.id}" />">&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn btn-primary" align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeExperience/${experience.id}" />">&nbsp;&nbsp;&nbsp;
				</p>
				<b>Projet: <c:out value="${experience.projet}" /></b><br><br>

				Descriptif pour le CV: <br><c:out value="${experience.descriptifCV}" escapeXml="false"/><br><br>
				Descriptif pour le dossier de comp�tences: <c:out value="${experience.descriptif}" escapeXml="false"/>
				<br>Langages: 
				<c:forEach items="${experience.listeLangage}" var="langage" varStatus="loop">
						<c:if test="${loop.index eq 0}"><c:out value="${langage.nom}" /></c:if>
						<c:if test="${loop.index ne 0}">, <c:out value="${langage.nom}" /></c:if>
				</c:forEach>
				<br>Logiciels: 
				<c:forEach items="${experience.listeLogiciel}" var="logiciel" varStatus="loop">
						<c:if test="${loop.index eq 0}"><c:out value="${logiciel.nom}" /></c:if>
						<c:if test="${loop.index ne 0}">, <c:out value="${logiciel.nom}" /></c:if>
				</c:forEach>
				<br>Syst�mes d'exploitation: 
				<c:forEach items="${experience.listeSystemeExploitation}" var="systemeExploitation" varStatus="loop">
						<c:if test="${loop.index eq 0}"><c:out value="${systemeExploitation.nom}" /></c:if>
						<c:if test="${loop.index ne 0}">, <c:out value="${systemeExploitation.nom}" /></c:if>
				</c:forEach>
				<br>Bases de donn�es : 
				<c:forEach items="${experience.listeBaseDeDonnee}" var="baseDeDonnee" varStatus="loop">
						<c:if test="${loop.index eq 0}"><c:out value="${baseDeDonnee.nom}" /></c:if>
						<c:if test="${loop.index ne 0}">, <c:out value="${baseDeDonnee.nom}" /></c:if>
				</c:forEach>

			<hr>
				
			</c:forEach>
			
		<br>

	</div>
	
	
			<p style="text-align: center;">
<%-- 			<a href="<c:url value="/donneesVisiteur/ajouteExperience" />">ajouter une experience</a> --%>
			<input id = "addexperience" type="button" class="btn btn-primary" align="middle" value="Ajouter une exp�rience" onclick=window.location.href="<c:url value="/donneesVisiteur/ajouteExperience" />">&nbsp;&nbsp;&nbsp;
			</p>
			
			<p style="text-align: center;">
			<input type="button" id ="closeAccordeonexpro" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
			</p>

</body>
</html>