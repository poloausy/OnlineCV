<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
	<script src="https://momentjs.com/downloads/moment.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
</head>


<body>
<!-- LISTE DES FORMATIONS	-->
	<div text-align="center">
		<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveFormation" modelAttribute="formation">
			<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			<form:hidden path="formation.id" />
		<table class="table table-striped">
			<tr>
				<td><b><u>Formation</u></b></td>
				<td><b><u>Ann�e d'obtention</u></b></td>
				<td><h4></h4></td> 
			</tr>


			<c:forEach items="${listeFormation}" var="formation">
				<tr>
					<td><c:out value="${formation.intitule}" /></td>
					<td><c:out value="${formation.anneeObtention}" /></td>
					<td><input type="button" class="btn btn-primary" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeFormation/${formation.id}" />">&nbsp;&nbsp;&nbsp;</td>

				</tr>
			</c:forEach>

					<tr>
						<td><form:input value="" path="formation.intitule" class="form-control"  required="required"/></td>
						<form:errors path="formation*" cssClass="error" />
						<%-- <td><form:input type="month" path="formation.anneeObtention" class="form-control" required="required"/></td> --%>
						<td><form:input type="number" min="0000" max="9999" id="formation-datepicker" class="form-control input-group-date hide-spinner" path="formation.anneeObtention" autocomplete="off" required="required"></form:input></td>
						<%-- <form:errors path="formation*" cssClass="error" /> --%>
						<td><input type="submit" class="btn btn-primary" value="Sauvegarder" id="valider" align="middle" />&nbsp;&nbsp;&nbsp;</td>

					</tr>
		</table>
		
		<c:if test="${erreur eq 'doublon'}">
				<span class="error"><spring:message code="error.formation.doublon" /></span>
			</c:if>
		
		</form>	
		<p style="text-align: center">
		<input type="button" class="btn btn-primary" align="middle" value="Ferme Accordeon" id="closeAccFormations" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
		</p>
	</div>
	
<script type="text/javascript">
//Enable datepicker for formation year field
$('#formation-datepicker').datepicker({
    format: 'yyyy',
    viewMode: 'years',
    minViewMode: 'years',
    autoclose: true
  });
</script>

<script type="text/javascript">

var anneeObtention = ${formation.anneeObtention};
document.getElementByAnneObention("demo").innerHTML = anneeObtention;

function myFunction() {
	anneeObtention.sort();
	/* document.getElementByAnneObention("demo").innerHTML = anneeObtention; */
	return ${formation.anneeObtention};
}
</script>





</body>
</html>