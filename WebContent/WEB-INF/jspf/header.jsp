<%@page contentType="text/html; charset=UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>


<!DOCTYPE html>
<html>
<head>
<title><spring:message code="accueil.home" /></title>
<link href="<c:url value="/static/css/headerStyle.css" />"
	rel="stylesheet">
</head>
<body>

	<div class="container">
		<p class="nomSite">

			<!-- 	*************** TITRE DE LA PAGE ACCUEIL + LIENS RESEAUX SOCIAUX *************** -->

			<spring:message code="accueil.titre" />

			<a class="facebook" href="http://www.facebook.com/Groupe.AUSY"><img
				src="../static/images/facebook_25px.png" alt=""></a> <a
				class="youtube"
				href="http://www.youtube.com/user/GROUPEAUSY?feature=watch"><img
				src="../static/images/youtube_25px.png" alt=""></a> <a
				class="linkedIn" href="https://www.linkedin.com/company/ausy"><img
				src="../static/images/linkedIn_25px.png" alt=""></a> <a
				class="live" href="http://www.ausy-live.com"><img
				src="../static/images/live_25px.png" alt=""></a> <a class="viadeo"
				href="http://fr.viadeo.com/v/company/ausy"><img
				src="../static/images/viadeo_25px.png" alt=""></a>

		</p>

		<nav id="header1" class="navbar navbar-inverse">
			<div class="container-fluid">
				<ul class="nav navbar-nav">
					<li><a href="" data-toggle="modal" data-target="#myModal"><spring:message
								code="accueil.regle" /></a></li>

				</ul>
				<ul class="nav navbar-nav navbar-right">
					<sec:authorize
						access="!hasAnyRole('ROLE_ADMIN', 'ROLE_UTILISATEUR', 'ROLE_MANAGER')">
						<li><a href="<c:url value="/visiteur/goToCreer" />"> <span
								id="anim1" class="fa"></span> <spring:message
									code="accueil.inscrire" /></a></li>
						<li><a id="ident" href="" data-toggle="modal"
							data-target="#identification"> <span
								class="glyphicon glyphicon-log-in"></span> <spring:message
									code="accueil.identifier" /></a></li>
					</sec:authorize>

					<sec:authorize
						access="hasAnyRole('ROLE_ADMIN', 'ROLE_UTILISATEUR', 'ROLE_MANAGER')">
					</sec:authorize>

					<sec:authorize access="hasAnyRole('ROLE_UTILISATEUR')">

						<div class="dropdown">
							<li class="dropdown"><a href="#" class="dropdown-toggle"
								data-toggle="dropdown" role="button" aria-haspopup="true"
								aria-expanded="false">Options <span class="caret"></span></a>
								<ul class="dropdown-menu">
									<li><a
										href="<c:url value="/visiteur/goToSupprimerProfil" />">Supprimer
											le profil</a></li>
									<li><a data-toggle="modal"
										data-target="#changerMotDePasse" class="modal-close">Modifier
											le mot de passe</a></li>
								</ul></li>
						</div>
					</sec:authorize>
					<sec:authorize
						access="hasAnyRole('ROLE_ADMIN', 'ROLE_UTILISATEUR', 'ROLE_MANAGER')">
						<li><a href="<c:url value="/logout" />"><span
								id = "logoutuser" class="glyphicon glyphicon-log-out"></span> <spring:message
									code="accueil.deconnecter" /></a></li>
					</sec:authorize>
				</ul>
			</div>
		</nav>

		<!-- MODAL RENSEIGNEMENT -->

		<div id="myModal" class="modal fade" role="dialog">
			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">
							<spring:message code="accueil.reglesite" />
						</h4>
					</div>
					<div class="modal-body">
						<p>

							Avec CV creator, votre nouvel emploi n'est plus qu'a trois
							simples etapes :<br> 1 - Inscrivez-vous avec votre login et
							mail<br> 2 - Remplissez votre profil (informations
							personnelles, langues, formation... ) : créez une image
							professionnelle impeccable qui attirera l'interet des employeurs
							potentiels.<br> 3 - Telechargez votre curriculum vitae et
							commencez a postuler sans plus attendre...

						</p>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>


		<!-- POPUP IDENTIFICATION	-->

		<div id="identification" class="modal fade" role="dialog">
			<form method="POST" action="<c:url value="/login" />"
				modelAttribute="utilisateur">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Veuillez renseigner vos identifiants</h4>
						</div>
						<div class="modal-body">

							<div id="connexionModal" class="form-group">
								<label for="username"><spring:message
										code="utilisateur.credential.nomLogin" /></label>
								<div id="ident" class="input-group">
									<span class="input-group-addon"> <i
										class="glyphicon glyphicon-user"></i>
									</span> <input type="text" name="username" value=""
										placeholder="Login" class="form-control" id="login">
								</div>
							</div>

							<div class="form-group">
								<label for="password"><spring:message
										code="utilisateur.credential.motDePasse" /></label>
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-lock"></i></span> <input type="password"
										name="password" value="" placeholder="Mot de passe"
										class="form-control" id="password">
								</div>
							</div>
							<input type="submit" class="btn btn-danger" id="validateLogin"
								value="<spring:message code="credential.submit"/>" /> <br>
							<br>
							<!--  <a href=""/>Mot de passe oublié</div>
	                   <a id="ident" href="Mot de passe oublié ?"  data-toggle="modal" data-target="#motDePasseOublie">-->
							<a href="" data-toggle="modal" data-target="#motDePasseOublie"
								class="modal-close">Mot de passe oublié ?</a>
						</div>
					</div>
				</div>
			</form>
		</div>



		<!-- POPUP MOT DE PASSE OUBLIE	-->

		<div id="motDePasseOublie" class="modal fade" role="dialog"
			contentType="text/html; charset=ISO-8859-1">
			<form method="POST"
				action="<c:url value="/visiteur/motDePasseOublie" />"
				modelAttribute="credential">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Réinitialiser votre mot de passe</h4>
							Saissiez votre adresse Email et nous vous enverrons un lien pour
							réinitialiser votre mot de passe.
						</div>
						<div class="modal-body">

							<div id="connexionModal" class="form-group">
								<label for="email"><spring:message
										code="credential.email" /></label>
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-user"></i></span> <input type="text"
										name="email" path="credential.email" value=""
										placeholder="Email" class="form-control">
								</div>
							</div>

							<input type="submit" class="btn btn-danger"
								value="<spring:message code="credential.submit" />" /> <br>
							<br>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!-- POPUP MODIFIER LE MOT DE PASSE -->

		<div id="changerMotDePasse" class="modal fade" role="dialog">
			<form method="POST"
				action="<c:url value="/visiteur/changerMotDePasse" />"
				modelAttribute="credential">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />
				<%-- <form:hidden path="credential.id" /> --%>
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">x</button>
							<h4 class="modal-title">Modifier le mot de passe</h4>
						</div>

						<div class="modal-body">

							<div id="connexionModal" class="form-group">
								<label for="email"><spring:message
										code="credential.password.user" /></label>
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-lock"></i></span> <input type="password"
										id="motDePasse" name="motDePasse" path="credential.password"
										placeholder="Mot de passe actuel" class="form-control"
										required="required" value="">
								</div>
							</div>

							<div id="connexionModal" class="form-group">
								<label for="email"><spring:message
										code="credential.password.new" /></label>
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-lock"></i></span> <input type="password"
										id="nouveauMotDePasse" class="form-control"
										name="nouveauMotDePasse" required
										placeholder="Nouveau mot de passe" class="form-control"
										minlength="6" required="required" pattern="[0-9]{6,}">
								</div>
							</div>

							<div id="connexionModal" class="form-group">
								<label for="email"><spring:message
										code="credential.confirmationMotDePasse" /></label>
								<div class="input-group">
									<span class="input-group-addon"><i
										class="glyphicon glyphicon-lock"></i></span> <input type="password"
										id="confirmationMotDePasse" class="form-control"
										name="confirmationMotDePasse" required
										placeholder="Confirmation du mot de passe"
										class="form-control" minlength="6" required="required"
										pattern="[0-9]{6,}">
								</div>
							</div>



							<div align="center">
								<tr>
									<b><FONT color="blue">Format du mot de passe : 6
											chiffres ou plus.</FONT></b>
									</b>
								</tr>
							</div>
							<input type="submit" class="btn btn-danger"
								value="<spring:message code="credential.submit" />" />

							<c:if test="${message == 'messageMotsDePasseNonIdentiques'}">
								<br>
								<div align="center">
									<span class="glyphicon glyphicon-alert" align="center"
										style="font-size: 36px; color: red"></span>
								</div>
								<h3 class="error" align="center">Attention, les deux
									nouveaux mots de passe rentrés ne sont pas identiques !</h3>
								<script>
									$("#changerMotDePasse").modal('show');
									//on conserve la premiere entree du formulaire remplie
									$("#motDePasse")
											.val(
													<c:out value="${motDePasseActuel}"></c:out>);
								</script>
							</c:if>

							<c:if test="${message == 'messageErreurMotDePasse'}">

								<br>
								<div align="center">
									<span class="glyphicon glyphicon-alert" align="center"
										style="font-size: 36px; color: red"></span>
								</div>
								<h3 class="error" align="center">Attention, il y a une
									erreur sur le mot de passe actuel !</h3>
								<script>
									$("#changerMotDePasse").modal('show');
									//on conserve les 2 dernieres entrees du formulaire remplies
									$("#nouveauMotDePasse")
											.val(
													<c:out value="${nouveauMotDePasse}"></c:out>);
									$("#confirmationMotDePasse")
											.val(
													<c:out value="${confirmationMotDePasse}"></c:out>);
								</script>
							</c:if>
							<br> <br>
						</div>
					</div>
				</div>
			</form>
		</div>

		<!-- ICONE INSCRIPTION HEADER ANIMEE -->
		<script>
			function animInscription() {
				var a;
				a = document.getElementById("anim1");
				a.innerHTML = "&#xf007;";
				setTimeout(function() {
					a.innerHTML = "&#xf234;";
				}, 1000);
			}
			animInscription();
			setInterval(animInscription, 3000);
		</script>
</body>
</html>
