<%@page import="com.onlinecv.entities.Utilisateur"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/accordionStyle.css" />" rel="stylesheet">
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>


</head>
<!--  checkURL() permet de v�rifier que le chemin de la photo est reconnu -->
<body onload="checkUrl()">
               <div align="center">
               <br>
				   <button type="button" align="center" class="btn btn-primary" data-toggle="modal" data-target="#ajoutPhoto" >Ajouter ou changer <br> la photo d'identit�</button> 
               </div>
    <!--  ***************POPUP MESSAGE AJOUT PHOTO D'IDENTITE***************	 -->
		        
		           <div id="ajoutPhoto" class="modal fade" role="dialog">
				       <div class="modal-dialog">
					     <div class="modal-content">
					
<!--                          Modal Header -->
						  <div class="modal-header d-flex justify-content-center">
					         <h2><b>Quelle photo d'identit� voulez vous ajouter?</b></h2>													
					      </div>
					
<!--                           Modal body -->
					       <div class="modal-body">
					       <c:choose>
							<c:when test="${not empty utilisateur.photo}">
								
								<img  id="photo2" src="../static/uploads/<c:out value="${utilisateur.photo}" />" alt="photo d'identit�" style="display: block; width: 100px; height: 100px; object-fit: contain" />
							</c:when>
							<c:otherwise>
								<img  id="photo2" src="../static/images/photocv.jpg" alt="photo d'identit� g�n�rique" style="display: block; width: 100px; height: 100px; object-fit: contain" />
							</c:otherwise>
						</c:choose>
						
						<!-- on change sur l'input permet d'avoir une preview de la photo avant l'enregistrement !> -->
		                     <form  method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/savePhoto" modelAttribute="utilisateur" enctype="multipart/form-data" >			      
 					           <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
						       <input type="file" id="newPhoto" name="photo" path="utilisateur.photo" class="form-control"
 					              onchange="document.getElementById('photo2').src = window.URL.createObjectURL(this.files[0]);" accept="image/*"/>	 

                               <form:hidden path="utilisateur.id" />

<%--  					           </form:input>  --%>
<%--  					           <form:errors cssClass="errors" />		  --%>
			                   <br/><br/>
			                   <input type="submit" class="btn btn-primary"  value="Sauvegarder " id="submit">
 		                    </form>					  
 					      </div> 
 				<!--	      Modal footer -->
                          <div class="modal-footer">					 
 					         <button type="button" class="btn btn-primary" data-dismiss="modal" >Close</button> 
					      </script> 
 					      </div>			 
		     	         </div> 
 			            </div> 
	                  </div> 
<!-- 		         *************************************************************	 	 -->

	<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveDonneesPerso"
		modelAttribute="utilisateur" enctype="multipart/form-data">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />
		
		<form:hidden path="utilisateur.id" />
		<div align="center">
			<TABLE class="tableformulaire" BORDER=0>
				<tr>
					<!--  Uploads are saved in /static/uploads -->
										
		<!-- Recupere le nom de la photo -->
					<input id="nomFichierPhoto" type="hidden" value="${utilisateur.photo}" />
					
<!--  					check path : <p id="check"> <p>  -->
				
					<td nowrap><label>Photo d'identit�:</label></td>
					<td >
					<div id="messageEnregistrementPhoto">
					    La nouvelle photo est bien enregistr�e.
					</div>
					<c:choose>
							<c:when test="${not empty utilisateur.photo }">
							         <img id="photo1" src="http://localhost:8080/ProjetOnlineCv/static/uploads/<c:out value="${utilisateur.photo}"/>" alt="photo d'identit�" style="display: block; width: 100px; height: 100px; object-fit: contain" />
							</c:when>
							   
							<c:otherwise>
								<img id="photo0" src="../static/images/photocv.jpg" alt="photo d'identit� g�n�rique" style="display: block; width: 100px; height: 100px; object-fit: contain" />
							</c:otherwise>
						</c:choose>
						  
					</td>
				</tr>
		
				<tr>
					<td nowrap><form:label path="utilisateur.nom">
							<spring:message code="utilisateur.nom" />
							<span class="required">*</span>
						</form:label></td>
					<td><form:input path="utilisateur.nom" class="form-control"
							required="required" /></td>
					<td><form:errors path="utilisateur.nom" cssClass="errors" /></td>					
					
				</tr>
				
					<td nowrap><form:label path="utilisateur.prenom">
							<spring:message code="utilisateur.prenom" />
							<span class="required">*</span>
						</form:label></td>
					<td><form:input path="utilisateur.prenom" class="form-control"
							required="required" /></td>
					<td><form:errors path="utilisateur.prenom" cssClass="errors" /></td>
				</tr>

				<tr>
					<td nowrap><form:label path="utilisateur.dateDeNaissance">
							<spring:message code="utilisateur.dateDeNaissance" />
							<span class="required">*</span>
						</form:label></td>
					<td><form:input id="datefield" type="date" min="1950-01-01" max="today" path="utilisateur.dateDeNaissance"
							class="form-control" required="required" autocomplete="off"/></td>
					<td><form:errors path="utilisateur.dateDeNaissance"
							cssClass="errors" /></td>
				</tr>

				<!--  Nationalit 1 -->				
				
				 <tr>
					<td nowrap><form:label path="utilisateur.nationalite_1">
							<spring:message code="utilisateur.nationalite_1" />
							<span class="required">*</span>
						</form:label></td>
					<td><form:select path="utilisateur.nationalite_1" name="nationalite" class="form-control" required="required">
							<form:option value="" label="choisir" /> 
							 <form:options items="${listeNomNationalite}" itemValue="nom" itemLabel="nom"/>
						</form:select></td>
						<td><form:errors path="error.nationalite_1" cssClass="errors" /></td>
				</tr> 
				
				<!--  Nationalit 2 -->                    
				
				<tr>
					<td nowrap><form:label path="utilisateur.nationalite_2">
							<spring:message code="utilisateur.nationalite_2" />
						</form:label></td>
					<td>

					<form:select path="utilisateur.nationalite_2"
							name="nationalite" class="form-control">
							<form:option value="" label="choisir" /> 
							<form:options items="${listeNomNationalite}" itemValue="nom" itemLabel="nom"/>
						</form:select></td>
						<form:errors path="nationalite.nomNationalite.id" />
						<td>
						
						<!--  TEST DE COMPARAISON ENTRE LES DEUX CHAMPS DE NATIONALITE -->
						
						<c:if test="${erreur eq 'doublon'}">
				              <span class="error"><spring:message code="error.nomNationalite_2.nom.doublon" /></span>
			            </c:if>						
						</td>
			        	
				</tr>				
				
				<tr>
					<td nowrap><form:label path="utilisateur.telephone">
							<spring:message code="utilisateur.telephone" />
							<span class="required">*</span>
						</form:label></td>
					<td><form:input path="utilisateur.telephone" 
							class="form-control hide-spinner" required="required" pattern="[+0-9][0-9]+"
							oninvalid="this.setCustomValidity('Format numro de tlphone : seuls les chiffres, ou le + en premire position, sont autoriss.')"
							oninput="setCustomValidity('')"	/></td>
					<td><form:errors path="utilisateur.telephone"
							cssClass="errors" /></td>
				</tr>

				<tr>
					<td nowrap><form:label path="utilisateur.adresse.rue">
							<spring:message code="utilisateur.adresse.rue" />
						</form:label></td>
					<td><form:input path="utilisateur.adresse.rue"
							class="form-control" /></td>
					<td><form:errors path="utilisateur.adresse.rue"
							cssClass="errors" /></td>
				</tr>

				<tr>
					<td nowrap><form:label path="utilisateur.adresse.codePostal">
							<spring:message code="utilisateur.adresse.codePostal" />
							<span class="required"></span>
						</form:label></td>
					<td><form:input type="number" min="1111" max="99999"
							path="utilisateur.adresse.codePostal"
							class="form-control hide-spinner"  /></td>
					<td><form:errors path="utilisateur.adresse.codePostal"
							cssClass="errors" /></td>
				</tr>
				
				<tr>
					<td nowrap><form:label path="utilisateur.adresse.ville">
							<spring:message code="utilisateur.adresse.ville" />
						</form:label></td>
					<td><form:input path="utilisateur.adresse.ville"
							class="form-control" /></td>
					<td><form:errors path="utilisateur.adresse.ville"
							cssClass="errors" /></td>
				</tr>

				<tr>
					<td nowrap><form:label path="utilisateur.adresse.nomPays">
							<spring:message code="utilisateur.adresse.nomPays.nom" />
						</form:label></td>
					<td><form:select path="utilisateur.adresse.nomPays" name="nomPays" class="form-control">
							<form:option value="" label="choisir" />
							<form:options items="${listeNomPays}" itemValue="nom" itemLabel="nom"/>							
						</form:select></td>
					<td><form:errors path="utilisateur.adresse.nomPays" cssClass="errors" /></td>
				</tr>

			</TABLE>
			<p><input type="submit" class="btn btn-primary" value="Sauvegarder " id="s0"
				align="middle" />&nbsp;&nbsp;&nbsp;</p>		
		</div>
		
		<center>
			<p>
			<input id="closeAccInfoPerso" type="button" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
			</p>
			</center>
		
	</form>

</body>

<!-- *********************************CE SCRIPT PERMET D'AFFICHER LA DATE AU FORMAT "dd/mm/yyyy" : N'EST PAS UTILISER********************************************** -->
<script type="text/javascript">
//Enable datepicker for ddn year field
$('#ddn').datepicker({ 
format: 'dd-mm-yyyy', 
 autoclose: true
})
</script>

<!-- ************* SCRIPT QUI PERMET D'EMPECHER LA VALIDATION D'UNE DATE DE NAISSANCE SOIT SUPERIEUR A AUJOURD'HUI **************** -->

<script type="text/javascript">
var today = new Date();

var dd = today.getDate();
var mm = today.getMonth()+1; //January is 0!
var yyyy = today.getFullYear();

 if(dd<10){
        dd='0'+dd
    } 
    if(mm<10){
        mm='0'+mm
    } 

today = yyyy+'-'+mm+'-'+dd;
document.getElementById("datefield").setAttribute("max", today)
</script>

<script>

//*********FONCTION pour savoir si le chemin de la photo est valide ou non
function UrlExists(url)
{
    var http = new XMLHttpRequest();
    http.open('HEAD', url, false);
    http.send();
     console.log('nom fichier');
     console.log($('#nomFichierPhoto').val());
     console.log('../static/images/'+$('#nomFichierPhoto').val());
//     console.log('nom fichier'+$('#nomFichierPhoto')htlm());
    return http.status!=404
}

//**************** FONCTION qui permet de contourner le probleme de non reconnaissance du chemin de la photo
function checkUrl()
{

//document.getElementById("check").innerHTML = UrlExists('../static/uploads/'+$('#nomFichierPhoto').val());
condition = UrlExists('../static/uploads/'+$('#nomFichierPhoto').val());
console.log("condition = "+condition);
// Si le chemin de la photo est reconnu : on montre la photo sinon on la cache et on montre le message d'enregistrement ok 
  if (condition) {
	$('#photo1').show();
	$('#messageEnregistrementPhoto').hide();
  }
  else {
	   $('#photo1').hide();	
	   $('#messageEnregistrementPhoto').show();
	   console.log("chemin false");
   }
 // gestion photo g�n�rique (condition vrai si nom de photo est null)
 if (!$('#nomFichierPhoto').val()) {
	 $('#photo0').show();
	 $('#messageEnregistrementPhoto').hide();
	 console.log("photo g�n�rique");
   }
}

</script>

</html>
