<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>

<head>
  <title>Mon compte</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="<c:url value="/static/css/inscription.css" />" rel="stylesheet">
</head>

<body>

<div align="center" id="bg">
<img src="<c:url value="/static/images/image ausy.jpg" />" width=50% />
</div>

<div class="container">
	<div align="center">
		<h1>
		<font color="blue"><b><spring:message code="inscription.creation" /></b></font>
		</h1>
	</div>
	<div class="form-group" bgcolor="#E6E6FA">
	<form method="POST" action="creer" modelAttribute="utilisateur">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
	<form:hidden path="utilisateur.id" />
	<form:hidden path="utilisateur.credential.id" />
		<div align="center">
			<TABLE class="tableinscription" BORDER=0>
				<tr>
					<td nowrap><form:label path="utilisateur.credential.nomLogin">
					<spring:message code="utilisateur.credential.nomLogin" /><span class="required">*</span></form:label></td>
					<td><form:input path="utilisateur.credential.nomLogin" class="form-control" placeholder="Saisir un Login" required="required" pattern="[^' ']+" 
					 oninvalid="setCustomValidity('Le caract�re espace n\\'est pas autoris�.')"  
                       oninput="setCustomValidity('')" /> 
					
					</td>
					<td><form:errors path="utilisateur.credential.nomLogin" cssClass="errors" /></td>
				</tr>
				
				<tr>
					<td nowrap><form:label path="utilisateur.credential.email">
					<spring:message code="utilisateur.credential.email" /><span class="required">*</span></form:label></td>
					<td><form:input type="email" id="email" name="email" path="utilisateur.credential.email" class="form-control" placeholder="Saisir un Email" required="required" pattern="^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$" /></td>
					<td><form:errors path="utilisateur.credential.email" cssClass="errors" /></td>
				</tr>
			
				<tr>
					<td nowrap><form:label path="utilisateur.credential.motDePasse">
					<spring:message code="credential.password" /><span class="required">*</span></form:label></td>					
		            <td><form:input type="password" id="motDePasse" name="motDePasse" path="utilisateur.credential.motDePasse" class="form-control" placeholder="Saisir le Mdp" minlength="6" required="required" pattern="^{6,}"/></td>
		            
		            <td><form:errors path="utilisateur.credential.motDePasse" cssClass="errors"/></td>
				</tr>
				
			</TABLE>
			
			<div align="center">
			<tr><b><FONT color="blue">le mot de passe doit contenir au moins de 6 caract�res</FONT></b></b></tr>
			</div>			
			
			</div>
		</div>
		<div align="center">
			<input type="submit" id = "saveInscription" class="btn btn-primary" value="Valider" align="center" />&nbsp;&nbsp;&nbsp;
			<sec:authorize access="!hasRole('ROLE_CLIENT')"><a class="btn btn-danger" href="<c:url value="/welcome/goToAccueil" />" >Retour</a></sec:authorize>
			<sec:authorize access="hasRole('ROLE_CLIENT')"><a class="btn btn-danger" href="goToMenuClient">Retour</a></sec:authorize>
			<sec:authorize access="hasRole('ROLE_ADMIN')"><a class="btn btn-danger" href="goToMenuAdmin">Retour</a></sec:authorize>
			<sec:authorize access="hasRole('ROLE_MANAGER')"><a class="btn btn-danger" href="goToMenuManager">Retour</a></sec:authorize>
			
		</div>
		
		
</div>
</body>
</html>