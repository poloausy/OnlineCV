<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
</head>


<body>
<!-- LISTE DES LANGUES	-->
	<div  text-align="center">
					
		<table class="table table-striped" align="center">
		
			<tr>
				<td><b><u>Langue</b></u></td>
				<td><b><u>Niveau</b></u></td>
				<td></td>				
				<td></td>				
						
			</tr>

			<c:forEach items="${listeLangue}" var="langue">
			
				<!--  Nothing to modify -->
				<c:if test="${idLangueAModifer == null}">
				<tr>
					<td><c:out value="${langue.nomLangue.nom}" /></td>
					<td><c:out value="${langue.niveau.intitule}" /></td>
					<td><a href="<c:url value="/donneesVisiteur/modifieLangue/${langue.id}"/>" class="btn btn-primary">Modifier</a></td>
					<td><input type="button" class="btn btn-primary" align="middle" value="Supprimer" 
					onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeLangue/${langue.id}" />">&nbsp;&nbsp;&nbsp;</td>
				
				</tr>
				</c:if>
				
				<!--  Langue to modify -->
				
				<c:if test="${idLangueAModifer != null}">
				
					<c:choose>					
					<c:when test="${idLangueAModifer eq langue.id}">
					
					<tr>					
		            <form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/modifieLangue" modelAttribute="langue">
		            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		            <td><c:out value="${langue.nomLangue.nom}" /> </td>
		            <form:hidden path="langue.nomLangue.id" name="i" value="${langue.nomLangue.id}" />
		            
 		            <td>
 		            <form:hidden path="langue.id" name="id" value="${langue.id}" />						 
					<form:select path="langue.niveau.id" text-align="center" class="form-control" required="required"> 
 					<form:option value="" label="${langue.niveau.intitule}" /> 
					<form:options items="${listeNiveau}" itemValue="id" itemLabel="intitule" /> 
					</form:select>
					</td>

					<td><input type="submit" class="btn btn-primary" align="middle" value="Update"/></td>
					<td></td>
					
					</form>
					</tr>
					</c:when>
					
					<c:otherwise>
				    <tr>
					<td><c:out value="${langue.nomLangue.nom}" /></td>
					<td><c:out value="${langue.niveau.intitule}" /></td>
					<td><input type="button" class="btn btn-primary" align="middle" value="Modifier" onclick=window.location.href="<c:url value="/donneesVisiteur/modifieLangue/${langue.id}" />">&nbsp;</td>
					<td><input type="button" class="btn btn-primary" align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimeLangue/${langue.id}" />">&nbsp;&nbsp;&nbsp;</td>
										
					</tr>
					
					</c:otherwise>
					</c:choose>
				</c:if>
				
			</c:forEach>
				
				<!--  Save new  -->
				
				<c:if test="${idLangueAModifer == null}">	
				<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveLangue" modelAttribute="Langue">
			    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
			    		
			<tr>
		  			<td>
					<form:select path="langue.nomLangue.id" class="form-control" required="required">
					<form:option value="" label="" />
					<form:options items="${listeNomLangue}" itemValue="id" itemLabel="nom" />
					</form:select><br>
					<form:errors path="langue.nomLangue.id" />
					</td>

		  			<td>
					<form:select path="langue.niveau.id" class="form-control" required="required">
					<form:option value="" label="" />
					<form:options items="${listeNiveau}" itemValue="id" itemLabel="intitule" />
					</form:select></td>
					
					<td>
					<input id="s3" type="submit" class="btn btn-primary" value="Sauvegarder" align="center" />&nbsp;&nbsp;&nbsp;
					</td>
					<td></td>
			</tr>
				</form>
			</c:if>
		
		</table>
		
		
			<c:if test="${erreur eq 'doublon'}">
				<span class="error"><spring:message code="error.nomLangue.nom.doublon" /></span>
			</c:if>
			
			<center>
			<p>
			<input id="closeAccLangues" type="button" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
			</p>
			</center>
					
	</div>
	
</body>
</html>