<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!DOCTYPE html>

<html>

<head>

<title>Manager - Search</title>

<!--  VIDANGE DU CACHE -->

<meta http-equiv="Cache-Control" content="no-cache, must-revalidate" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Cache" content="no store" />
<meta http-equiv="Expires" content="0" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/accueilStyle.css" />"	rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!--  USER SEARCH AJAX SCRIPT -->

<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="<c:url value="/static/js/user-search-filter.js" />"></script>

<script type="text/javascript">
window.onload = function() {
	document.getElementId("loadPreviousFields").hide();
}
</script>

<style>
	input, select{
		width: 100%;
		padding: 5px;
	}
</style>

</head>

<body onload="loadPreviousFields()">

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG" >
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD"  >
				<img src="../static/images/newLogoAusy.png"/>
			</div>

	<!--  HEADER -->
	
	<c:import url="../header.jsp" />
	<div class="main">	
		<div class="container">
			<div class="row">
				<div class="col-lg-2">
				
					<!--  MANAGER SIDE NAV  -->
					
					<c:import url="../partials/manager-sidenav.jsp" />
				</div>
				<div class="col-lg-9 col-lg-push-0">
					<h2>Recherche des utilisateurs</h2>
					<form>
					
					<div class="col-xs-12">
					<div class="col-lg-3 col-xs-12">
					<p>Mot-cl� 1 � rechercher</p>
					
						<!-- KEYWORD 1 INPUT FIELD  -->
						
						<input type="text" class="filterlistener recherche1" id="keyword1" name="keyword1" placeholder="Mot cl�" class="filterlistener"/>												
					</div>
					
					<div class="col-lg-3 col-xs-12">
					<p>Domaine de recherche</p>
					
						<!-- SELECT THE SEARCH DOMAIN -->
						
						<select id="domain_select1" class="filterlistener recherche1" var="domain">
							<option value="all">Tous</option>
							<option value="competences">Competence</option>
							<option value="certifications">Certification</option>
							<option value="experiences">Experience</option>
							<option value="langues">Langue</option>
							<option value="nationalites">Nationalite</option>
							<option value="formations">Formation</option>
							<option value="metiers">Metier</option>
							<option value="entreprises">Entreprise</option>
							<option value="langages">Langage informatique</option>
							<option value="logiciels">Logiciel</option>
							<option value="systemeExploitation">Syst�me d'exploitation</option>
							<option value="baseDeDonnees">Base de donn�es</option>
							<option value="names">Nom / pr�nom</option>
						</select>
					</div>
				
					<div class="col-lg-3 col-xs-12" id="competenceNiveau">
					
					<!--  DETAIL FILTER FOR COMPETENCES NIVEAU -->
					
					<p id= niveauCompetence1>Niveau</p>
						<select id="competences_niveau_select1" class="filterlistener detailfilter1"  >
							<option value="">Tous</option>
							<c:forEach items="${listeNiveau}" var="niveau">
	                             <option value="${niveau.intitule2}" />${niveau.intitule2}</option>
	                        </c:forEach>
						</select>				   		
      		
					<span  id="langueNiveau1">
					
                    <!--  DETAIL FILTER FOR LANGUES NIVEAU -->

                  		<p id= niveauLangue1>Niveau</p>
						<select id="langues_niveau_select1" class="filterlistener detailfilter1"  >
							<option value="">Tous</option>
							<c:forEach items="${listeNiveau}" var="niveau">
	                             <option value="${niveau.intitule}" />${niveau.intitule}</option>
	                        </c:forEach>							
						</select>
					</span>
					
					<span  id="NbAnneeExperience1">
					
                    <!--  DETAIL FILTER FOR NUMBER OF YEARS -->

                  		<p id= "anneeExp1">Ann�es d'exp�rience</p>
						<select id="nombre_annees_select1" class="filterlistener detailfilter1"  >
							<option value="">Indiff�rent</option>
							<option value="Moins1An">Moins d'1 ans</option>
							<option value="1AnA2Ans">De 1 � 2 ans</option>	
							<option value="2AnsA3Ans">De 2 � 3 ans</option>
							<option value="3AnsA5Ans">De 3 � 5 ans</option>	
							<option value="PlusDe5Ans">Plus de 5 ans</option>							
						</select>
					</span>
				  </div>	

                    <!-- FILTER REQUEST BUTTON -->

      <div class="col-xs-3">
            <p><br> </p>
			<button id="filter-btn" type="submit" class="btn btn-success btn-submit"  
			style="width: 100%" disabled ">Voir les r�sultats</button>
		</div>
			</div>
	
                	<!-- CHECKBOX RECHERCHE COMBINEE AVEC 2 MOTS CLES -->
	
       <div class="col-xs-12">  
             <div>&nbsp </div>
             <div class="col-xs-5">
                 Recherche combin�e avec plusieurs mots-cl�s :
             </div>
              <div class="col-xs-3 recherche-combinee">
             <label class="containerCheckbox">
			     <input type="checkbox" id="CheckRechercheCombinee" >
			     <span class="checkmarkCheckbox"></span>
			 </label>
			  </div>
		</div>		  
	
					
	<! POUR LA RECHERCHE COMBINEE AVEC 2 MOTS CLES !>
					
			<div class="col-xs-12">
			         <h5 class="rechercheCombinee bleuClair"><b>ET</b></h5>
					<div class="col-lg-3 col-xs-12">
					<p class="rechercheCombinee">Mot-cl� 2 � rechercher</p>
					
						<!-- KEYWORD 1 INPUT FIELD  -->
						
						<input type="text" id="keyword2" name="keyword2" placeholder="Mot cl�" class="filterlistener"/>
					</div>
					
					<div class="col-lg-3 col-xs-12">
					<p class="rechercheCombinee">Domaine de recherche</p>
					
						<!-- SELECT THE SEARCH DOMAIN -->
						
						<select id="domain_select2" class="filterlistener" var="domain2">
							<option value="all">Tous</option>
							<option value="competences">Competence</option>
							<option value="certifications">Certification</option>
							<option value="experiences">Experience</option>
							<option value="langues">Langue</option>
							<option value="nationalites">Nationalite</option>
							<option value="formations">Formation</option>
							<option value="metiers">Metier</option>
							<option value="entreprises">Entreprise</option>
							<option value="langages">Langage informatique</option>
							<option value="logiciels">Logiciel</option>
							<option value="systemeExploitation">Syst�me d'exploitation</option>
							<option value="baseDeDonnees">Base de donn�es</option>
							<option value="names">Nom / pr�nom</option>
						</select>
					</div>
				
					<div class="col-lg-3 col-xs-12" id="competenceNiveau2">
					
					<!--  DETAIL FILTER FOR COMPETENCES NIVEAU -->
					
					<p id= niveauCompetence2>Niveau</p>
						<select id="competences_niveau_select2" class="filterlistener detailfilter2"  >
							<option value="">Tous</option>
							<c:forEach items="${listeNiveau}" var="niveau">
	                             <option value="${niveau.intitule2}" />${niveau.intitule2}</option>
	                        </c:forEach>
						</select>				   		
      		
					<span  id="langueNiveau2">
					
                    <!--  DETAIL FILTER FOR LANGUES NIVEAU -->

                  		<p id= niveauLangue2>Niveau</p>
						<select id="langues_niveau_select2" class="filterlistener detailfilter2"  >
							<option value="">Tous</option>
							<c:forEach items="${listeNiveau}" var="niveau">
	                             <option value="${niveau.intitule}" />${niveau.intitule}</option>
	                        </c:forEach>							
						</select>
					</span>
					
					<span  id="NbAnneeExperience2">
					
                    <!--  DETAIL FILTER FOR NUMBER OF YEARS -->

                  		<p id= "anneeExp2">Ann�es d'exp�rience</p>
						<select id="nombre_annees_select2" class="filterlistener detailfilter2"  >
							<option value="">Indiff�rent</option>
							<option value="Moins1An">Moins d'1 ans</option>
							<option value="1AnA2Ans">De 1 � 2 ans</option>	
							<option value="2AnsA3Ans">De 2 � 3 ans</option>
							<option value="3AnsA5Ans">De 3 � 5 ans</option>	
							<option value="PlusDe5Ans">Plus de 5 ans</option>							
						</select>
					</span>
					</div>	
       			  </div>
       			  
                	<!-- CHECKBOX RECHERCHE COMBINEE AVEC 3 MOTS CLES -->
  
       <div class="col-xs-12 checkBox3MotsCles">  
              <div>&nbsp </div>
             <div class="col-xs-5">
                 Recherche combin�e avec 3 mots-cl�s :
             </div>
              <div class="col-xs-3 recherche-combinee">
             <label class="containerCheckbox">
			     <input type="checkbox" id="CheckRechercheCombinee2" >
			     <span class="checkmarkCheckbox"></span>
			 </label>
			  </div>
			 
<!-- 			<button id="filter-btn" type="submit" class="btn btn-info btn-submit" >Recherche combin�e avec 2 mots cl�</button> -->
		</div>		       			  
       			  
  <! POUR LA RECHERCHE COMBINEE AVEC 3 MOTS CLES !>				
			<div class="col-xs-12">
			         <h5 class="rechercheCombinee2 bleuClair"><b>ET</b></h5>
					<div class="col-lg-3 col-xs-12">
					<p class="rechercheCombinee2">Mot-cl� 3 � rechercher</p>
						<!-- Keyword 1 input field  -->
						<input type="text" class="filterlistener" id="keyword3" name="keyword3" placeholder="Mot cl�" class="filterlistener"/>
					</div>
					
					<div class="col-lg-3 col-xs-12">
					<p class="rechercheCombinee2">Domaine de recherche</p>
						<!-- Select the search domain -->
						<select id="domain_select3" class="filterlistener" var="domain3">
							<option value="all">Tous</option>
							<option value="competences">Competence</option>
							<option value="certifications">Certification</option>
							<option value="experiences">Experience</option>
							<option value="langues">Langue</option>
							<option value="nationalites">Nationalite</option>
							<option value="formations">Formation</option>
							<option value="metiers">Metier</option>
							<option value="entreprises">Entreprise</option>
							<option value="langages">Langage informatique</option>
							<option value="logiciels">Logiciel</option>
							<option value="systemeExploitation">Syst�me d'exploitation</option>
							<option value="baseDeDonnees">Base de donn�es</option>
							<option value="names">Nom / pr�nom</option>
						</select>
					</div>
					
				
					<div class="col-lg-3 col-xs-12" id="competenceNiveau2">
					<!--  Detail filter for competences niveau -->
					
					<p id= niveauCompetence3>Niveau</p>
						<select id="competences_niveau_select3" class="filterlistener detailfilter3"  >
							<option value="">Tous</option>
							<c:forEach items="${listeNiveau}" var="niveau">
	                             <option value="${niveau.intitule2}" />${niveau.intitule2}</option>
	                        </c:forEach>
						</select>				   		
      		
					<span  id="langueNiveau3">
<!-- 					 Detail filter for langues niveau -->
                  		<p id= niveauLangue3>Niveau</p>
						<select id="langues_niveau_select3" class="filterlistener detailfilter3"  >
							<option value="">Tous</option>
							<c:forEach items="${listeNiveau}" var="niveau">
	                             <option value="${niveau.intitule}" />${niveau.intitule}</option>
	                        </c:forEach>							
						</select>
					</span>
					
					<span  id="NbAnneeExperience3">
<!-- 					 Detail filter for number of years -->
                  		<p id= "anneeExp3">Ann�es d'exp�rience</p>
						<select id="nombre_annees_select3" class="filterlistener detailfilter3"  >
							<option value="">Indiff�rent</option>
							<option value="Moins1An">Moins d'1 ans</option>
							<option value="1AnA2Ans">De 1 � 2 ans</option>	
							<option value="2AnsA3Ans">De 2 � 3 ans</option>
							<option value="3AnsA5Ans">De 3 � 5 ans</option>	
							<option value="PlusDe5Ans">Plus de 5 ans</option>							
						</select>
					</span>
					</div>	
       			  </div>     			  
       			  
   	  
				</form>
		      
	          
				
					<!--  Ajax results will be appended to this div-->
					  &nbsp
					  <h2 style="margin-top: 0px;margin-bottom: 0px;">  R�sulats:</h2>
					  <!--   Message tri par ann�es d'exp�rience avec "paramTri" correspondant au mot-cl� associ� au tri (d�fini dans le fichier js user-search-filter)  -->	
					   <div id="triParExperience" >Les utilisateurs sont tri�s par nombre d'ann�es d'exp�rience d�croissantes  
					         <span style="font-weight:bold" id="paramTri" ></span>
					   </div>
					    				
					<div id="resultDiv">
						<!--  Results will be added here... -->
					</div>		
				</div>
			</div>
		</div>
	</div>
	
	
<!--  Sauvegarde des filtres de recherche et du r�sultat de la recherche (lorsque le bouton visite profil est cliqu�) 
utiliser dans fichier "user-search-filter.js" -->
<script>

//Met en m�moire les donn�es d'entr�e de la recherche. Cette fonction est appel�e dans la feuille user-search-filter.js (quand on clique sur le bouton "voir Profil")
function recupResultatRecherche() {
	
	localStorage.setItem('storageKeywordGetData', $("#keyword1").val());
	localStorage.setItem('storageDomainGetData', $("#domain_select1").val());
	localStorage.setItem('storageNiveauCompGetData', $("#competences_niveau_select1").val());
	localStorage.setItem('storageNiveauLangueGetData', $("#langues_niveau_select1").val());
	localStorage.setItem('storageNiveauNbrAnneeExpGetData', $("#nombre_annees_select1").val());
	localStorage.setItem('storageKeywordGetData2', $("#keyword2").val());
	localStorage.setItem('storageDomainGetData2', $("#domain_select2").val());
	localStorage.setItem('storageNiveauCompGetData2', $("#competences_niveau_select2").val());
	localStorage.setItem('storageNiveauLangueGetData2', $("#langues_niveau_select2").val());
	localStorage.setItem('storageNiveauNbrAnneeExpGetData2', $("#nombre_annees_select2").val());
	localStorage.setItem('storageKeywordGetData3', $("#keyword3").val());
	localStorage.setItem('storageDomainGetData3', $("#domain_select3").val());
	localStorage.setItem('storageNiveauCompGetData3', $("#competences_niveau_select3").val());
	localStorage.setItem('storageNiveauLangueGetData3', $("#langues_niveau_select3").val());
	localStorage.setItem('storageNiveauNbrAnneeExpGetData3', $("#nombre_annees_select3").val());	
	
	localStorage.setItem('storageResultDivGetData', $('#resultDiv').html());	

}

</script>

<!--  Chargement des filtres de recherche et du r�sultat de la recherche pr�c�dente (pour retrouver ces donn�es � l'ouverture de la page) -->

  <script>
     function loadPreviousFields(){
    	 //R�cup�re les entr�es de la recherche pr�c�dente (mise en m�moire par la fonction recupResultatRecherche())
        var storageKeyword1 = localStorage.getItem('storageKeywordGetData');
        var storageDomain = localStorage.getItem('storageDomainGetData');
        var storageNiveauComp = localStorage.getItem('storageNiveauCompGetData');
        var storageNiveauLangue = localStorage.getItem('storageNiveauLangueGetData');
        var storageNbrAnneeExp = localStorage.getItem('storageNiveauNbrAnneeExpGetData');
        
        var storageKeyword2 = localStorage.getItem('storageKeywordGetData2');
        var storageDomain2 = localStorage.getItem('storageDomainGetData2');
        var storageNiveauComp2 = localStorage.getItem('storageNiveauCompGetData2');
        var storageNiveauLangue2 = localStorage.getItem('storageNiveauLangueGetData2');
        var storageNbrAnneeExp2 = localStorage.getItem('storageNiveauNbrAnneeExpGetData2');
        
        var storageKeyword3 = localStorage.getItem('storageKeywordGetData3');
        var storageDomain3 = localStorage.getItem('storageDomainGetData3');
        var storageNiveauComp3 = localStorage.getItem('storageNiveauCompGetData3');
        var storageNiveauLangue3 = localStorage.getItem('storageNiveauLangueGetData3');
        var storageNbrAnneeExp3 = localStorage.getItem('storageNiveauNbrAnneeExpGetData3');
        
        console.log('niveau comp '+ storageNiveauComp3);
        console.log('nbr annees '+ storageNbrAnneeExp3);
        var storageResultDiv = localStorage.getItem('storageResultDivGetData');
        console.log("storageKeyword2= "+storageKeyword2);
 
      //******************************************************************************************
      // Fonction qui R�cup�re les filtres d'une recherche (pour pouvoir les afficher en revenant sur la page recherche apr�s avoir vu le profil d'un candidat)       
  function recupereCritereRecherche (numeroRecherche, storageKeyword1, storageDomain, storageNiveauComp, storageNiveauLangue, storageNbrAnneeExp){      
        $("#keyword"+numeroRecherche).val(storageKeyword1);
        $("#domain_select"+numeroRecherche).val(storageDomain);
        if ($("#domain_select"+numeroRecherche).val()==="competences"){
        	console.log("domain_select ="+$("#domain_select"+numeroRecherche).val());
    	   $("#competences_niveau_select"+numeroRecherche).val(storageNiveauComp);     	   
        }
        if ($("#domain_select"+numeroRecherche).val()==="langues"){
        $("#langues_niveau_select"+numeroRecherche).val(storageNiveauLangue);
        }
        if ($("#domain_select"+numeroRecherche).val() === 'langages' || $("#domain_select"+numeroRecherche).val() === 'logiciels' 
        || $("#domain_select"+numeroRecherche).val() === 'metiers' || $("#domain_select"+numeroRecherche).val() === 'entreprises'){
        $("#nombre_annees_select"+numeroRecherche).val(storageNbrAnneeExp);	
        }
        $('#resultDiv').html(storageResultDiv);
        
        // Mise en page niveau pour langues et comp�tences ou nombre ann�es d'exp cach�es // visibles
        if  ($("#domain_select"+numeroRecherche).val() != 'competences')
                              { $('#niveauCompetence'+numeroRecherche).hide();
                            	$('#competences_niveau_select'+numeroRecherche).hide()
                            	};
        if  ($("#domain_select"+numeroRecherche).val() != 'langues') 
                               { $('#niveauLangue'+numeroRecherche).hide()
        	                    $('#langues_niveau_select'+numeroRecherche).hide()
        	                    };
        if  ($("#domain_select"+numeroRecherche).val() != 'langages' && $("#domain_select"+numeroRecherche).val() != 'logiciels' 
        	&& $("#domain_select"+numeroRecherche).val() != 'metiers' && $("#domain_select"+numeroRecherche).val() != 'entreprises')
                                { $('#anneeExp'+numeroRecherche).hide()
                               $('#nombre_annees_select'+numeroRecherche).hide()
                               };       
  }
//**************************************************************************************************
      
  // R�cup�re les donn�es de la 1�re recherche
  recupereCritereRecherche (1, storageKeyword1, storageDomain, storageNiveauComp, storageNiveauLangue, storageNbrAnneeExp); 
  
  // R�cup�re les donn�es de la 2�me recherche
  recupereCritereRecherche (2, storageKeyword2, storageDomain2, storageNiveauComp2, storageNiveauLangue2, storageNbrAnneeExp2);  
        
                               
  // Rend visibles les crit�res de recherche pour la 2�me recherche si un mot-cl� N�2 est en m�moire (storageKeyword2 non nul)                        
       console.log("storageKeyword2= "+storageKeyword2);
       if (storageKeyword2 != "") {
    	   console.log("ok if "+storageKeyword2);
    	   //Renvoi la checkBox coch�e
    	   $('#CheckRechercheCombinee').prop('checked', true);
    	   	$('#keyword2').show();
        	$('.rechercheCombinee').show();
        	$('#domain_select2').show();
        	$('.checkBox3MotsCles').show();
    		if  ($("#domain_select2").val() != 'competences')
                   { $('#niveauCompetence2').hide();
          	         $('#competences_niveau_select2').hide();
          	       }
            if  ($("#domain_select2").val() != 'langues') 
                    { $('#niveauLangue2').hide();
                      $('#langues_niveau_select2').hide();
                    }
            if  ($("#domain_select2").val() != 'langages' && $("#domain_select2").val() != 'logiciels' 
                && $("#domain_select2").val() != 'metiers' && $("#domain_select2").val() != 'entreprises')
                   { $('#anneeExp2').hide();
                     $('#nombre_annees_select2').hide();  
                   }
       }
    // Cache les crit�res de recherche pour la 2�me recherche si pas de mot cl� N�2 en m�moire 
            else {
            	 $('#keyword2').hide();
            		$('.rechercheCombinee').hide();
            		$('#domain_select2').hide();
            		$('.detailfilter2').hide();
            	   	$('#niveauCompetence2').hide();
            	   	$('#competences_niveau_select2').hide();
            	   	$('#langues_niveau_select2').hide();
            		$('#niveauLangue2').hide();
            		$('#nombre_annees_select2').hide();
            		$('#anneeExp2').hide();
            		$('.checkBox3MotsCles').hide();
            	}  

    // R�cup�re les donn�es de la 3�me recherche
       recupereCritereRecherche (3, storageKeyword3, storageDomain3, storageNiveauComp3, storageNiveauLangue3, storageNbrAnneeExp3);      
                               
   // Rend visibles les crit�res de recherche pour la 3�me recherche si un mot-cl� N�3 est en m�moire (storageKeyword3 non nul)                        
      console.log("storageKeyword3= "+storageKeyword3);
      if (storageKeyword3 != "") {
   	   console.log("ok if "+storageKeyword3);
   	   //Renvoi la checkBox coch�e
   	   $('#CheckRechercheCombinee2').prop('checked', true);
   	   	$('#keyword3').show();
       	$('.rechercheCombinee2').show();
       	$('#domain_select3').show();
   		if  ($("#domain_select3").val() != 'competences')
                  { $('#niveauCompetence3').hide();
         	         $('#competences_niveau_select3').hide();
         	       }
           if  ($("#domain_select3").val() != 'langues') 
                   { $('#niveauLangue3').hide();
                     $('#langues_niveau_select3').hide();
                   }
           if  ($("#domain_select3").val() != 'langages' && $("#domain_select3").val() != 'logiciels' 
        	   && $("#domain_select3").val() != 'metiers' && $("#domain_select3").val() != 'entreprises')
                  { $('#anneeExp3').hide();
                    $('#nombre_annees_select3').hide();  
                  }
      }
   // Cache les crit�res de recherche pour la 3�me recherche si pas de mot cl� N�3 en m�moire 
           else {
           	 $('#keyword3').hide();
           		$('.rechercheCombinee2').hide();
           		$('#domain_select3').hide();
           		$('.detailfilter3').hide();
           	   	$('#niveauCompetence3').hide();
           	   	$('#competences_niveau_select3').hide();
           	   	$('#langues_niveau_select3').hide();
           		$('#niveauLangue3').hide();
           		$('#nombre_annees_select3').hide();
           		$('#anneeExp3').hide(); 
           	    //$('.checkBox3MotsCles').hide(); 
           	}
      
                  
//    Variables de stockage effac�es
        localStorage.setItem('storageKeywordGetData', '');
      	localStorage.setItem('storageDomainGetData', '');
      	localStorage.setItem('storageNiveauCompGetData', '');
      	localStorage.setItem('storageNiveaulangueGetData', '');
      	localStorage.setItem('storageNiveauNbrAnneeExpGetData', '');
      	localStorage.setItem('storageKeywordGetData2', '');
      	localStorage.setItem('storageDomainGetData2', '');
      	localStorage.setItem('storageNiveauCompGetData2', '');
      	localStorage.setItem('storageNiveaulangueGetData2', '');
      	localStorage.setItem('storageNiveauNbrAnneeExpGetData2', '');
      	localStorage.setItem('storageKeywordGetData3', '');
      	localStorage.setItem('storageDomainGetData3', '');
      	localStorage.setItem('storageNiveauCompGetData3', '');
      	localStorage.setItem('storageNiveaulangueGetData3', '');
      	localStorage.setItem('storageNiveauNbrAnneeExpGetData3', '');
      	localStorage.setItem('storageResultDivGetData', '');
      	      	         
         }  
     
     
     
      
  </script> 	
	
 </body>
</html>