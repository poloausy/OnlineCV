<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>


<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG" >
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD"  >
				<img src="../static/images/newLogoAusy.png"/>
			</div>
		
  <div class="container">
  <c:import url="header.jsp" />
     <div class="row">
		<div class="col-lg-3">
		  <!-- Sidenav partial -->
		  <c:import url="partials/utilisateur-sidenav.jsp" />
        </div>
     
        <div class="col-lg-9 col-lg-push-0">
		  <!-- Main -->
		  <div class="main">
	  
			<h2 class="textBlack">Bienvenue <span class="nomlogin">${credential.nomLogin} </span></h2>			

			<h4 class="textBleuBlack">Renseignez votre profil et choisissez un template pour votre CV ou dossier de comp�tence.</h4>
			<br>
			<c:if test="${message == 'messageMotDePasseModifie'}">
	           <div align="center" ><i class="fa fa-check-square-o" style="font-size:36px;color:green"></i></div>
	           <div class="info">Votre mot de passe a �t�  modifi�. </div>     
	        </c:if>				
		 </div>
     </div>
  </div>
 </div>
</body>
</html>