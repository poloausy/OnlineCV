<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/headerStyle.css" />"
	rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>


<body>
<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveOutil/${experienceId}" modelAttribute="nomOutil">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<form:hidden path="nomOutil.id" />
		<table class="tableinscription">
		<div id="connexionModal" class="form-group">
				<tr>
		  			<td><form:label path="nomOutil.id">outil</form:label></td>
					<td><form:select path="nomOutil.id" class="form-control">
					<form:option value="" label="" />
					<form:options items="${listeNomOutil}" itemValue="id" itemLabel="nom" />
					</form:select></td>
				</tr>																									
		</div>
		</table>
		
		<div align="center">
				<input type="submit" class="btn btn-primary" value="Sauvegarder"
				align="center" />&nbsp;&nbsp;&nbsp;
		</div>
</form>
</body>
</html>