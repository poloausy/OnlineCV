<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>NOUVELLE EXPERIENCE JSP</title>

<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<link href="<c:url value="/static/css/headerStyle.css" />"
	rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>
<script>
  tinymce.init({
    selector: '#mytextarea',
    plugins : "autolink,lists,pagebreak,layer,table,save,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,template,wordcount,advlist,autosave,visualblocks",
    // taille disponible
    theme_advanced_font_sizes: "10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px",
    // couleur disponible dans la palette de couleur
    theme_advanced_text_colors : "33FFFF, 007fff, ff7f00",
    // balise html disponible
    theme_advanced_blockformats : "h1, h2,h3,h4,h5,h6",
    // class disponible
    theme_advanced_styles : "Tableau=textTab;TableauSansCadre=textTabSansCadre;",
    style_formats: [
        {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
        {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
        {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
        {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
        {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
        {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
        {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
        {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
        {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
        {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
        {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
        {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
        {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
    ],
  });
  </script>

<body>
<c:import url="header.jsp" />
<div class="container">
<h1 style="text-align: center;">Experience professionnelle</h1>

<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/ajouteOutils" modelAttribute="experience">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
								<form:hidden path="experience.id" name="id"/>
								<div align="center">
								
									<TABLE class="tableformulaire" BORDER=0>
										
							 			<tr>
										<td><form:label path="experience.metier.id" >M�tier</form:label></td>
										<td><form:select id="formMetier" path="experience.metier.id" class="form-control" required="required">
											<form:option value="" label="" /> 
											<option class="editable" value="${experience.metier.nom}">Autre</option>
											<form:options  items="${listeMetier}" itemValue="id" itemLabel="nom" />
																					
										    </form:select>
									<!-- 			propose � l'utilisateur de taper un m�tier personnalis� diff�rent de ceux pr�sent dans la liste d�roulante   -->
									<form:input id="metierPersonnalises" path="experience.metier.nom" type="text" class="editOptionMetier" style="display:none;" placeholder="Tapez un nouveau m�tier"
									pattern="[A-Za-z�-�0-9\s]+" oninvalid="this.setCustomValidity('Les caract�res sp�ciaux ne sont pas autoris�s !')"
									oninput="setCustomValidity('')"></form:input>
									 
						  				</td>

										</tr>
				
									 	<tr>
											<td><form:label path="experience.dateDebut" >Date de d�but</form:label></td>
											<td><form:input id="dateDebut" type="month" path="experience.dateDebut" class="form-control" required="required"/></td>
											
										</tr>
										<tr>
											<td><form:label path="experience.dateFin" >Date de fin</form:label></td>
											<td><form:input id="dateFin" type="month" value="" path="experience.dateFin" class="form-control" required="required"/></td>
										</tr>
										
										<tr>
										<td> </td>
										<td id="messageErreurDate"></td>
										</tr>															 
												 
										<tr>
		  									<td><form:label path="experience.entreprise.id" >Entreprise</form:label></td>
											<td><form:select id="formEntreprise" path="experience.entreprise.id" class="form-control" required="required">
												<form:option value="" label="" />
												<option class="editableEntreprise" value="${experience.entreprise.nom}">Autre</option>
												<form:options items="${listeEntreprise}" itemValue="id" itemLabel="nom" />

											</form:select>
											
							<!-- 		propose � l'utilisateur de taper une entreprise personnalis�e diff�rentes de celles pr�sentes dans la liste d�roulante   -->
							
									       <form:input id="entreprisePersonnalises" path="experience.entreprise.nom" type="text" class="editOptionEntreprise" style="display:none;" placeholder="Tapez une nouvelle entreprise"
									       pattern="[A-Za-z�-�0-9\s]+" oninvalid="this.setCustomValidity('Les caract�res sp�ciaux ne sont pas autoris�s !')"
									       oninput="setCustomValidity('')"></form:input>				
											
											</td>
										</tr>						
										
										<tr>
											<td><form:label path="experience.projet" >Projet</form:label></td>
											<td><form:input value="" path="experience.projet" class="form-control" required="required"/></td>
										</tr>
										
										<tr>
											<td><form:label path="experience.descriptifCV" >Descriptif pour le CV</form:label></td>
											<td><form:textarea value="" path="experience.descriptifCV" rows="5" class="form-control" required="required"/></td>
										</tr>
										
										<tr>
										<div class="form-group row">
										<div class="col-xs-8">
											<td><form:label path="experience.descriptif">Descriptif pour le Dossier de comp�tences</form:label></td> 
											<td><form:textarea id="mytextarea" value="" path="experience.descriptif" rows="10" class="form-control" /></td>
										</div>
										</div>
										</tr>								
			
						</table>
						</div>		 

			<div align="center">
				<input type="submit" class="btn btn-primary" name= "sauve" value="Valider" align="center" />&nbsp;&nbsp;&nbsp;
				<input type="submit" id = "outilsexperience" class="btn btn-primary"  name="ajoute" value="Outils" align="center" />&nbsp;&nbsp;&nbsp;
				<input type="button" class="btn btn-primary" align="center" value="Retour" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeOutil" />">&nbsp;&nbsp;&nbsp;
			</div>
			
</form>	
								
</div>
</body>

<!-- **Script qui permet de rentrer soit les m�tiers � partir d'une liste soit de rentrer un m�tier personalis�**  -->
 <script>									
      var initialText = $('.editable').val();

     $('#formMetier').change(function(){
     var selected = $('option:selected', this).attr('class');
     var optionText = $('.editable').text();

   if(selected == "editable"){
      $('.editOptionMetier').show();
      $('.editOptionMetier').val('');
	  $('.editOptionMetier').html('');
	  console.log($('.editOptionMetier').val()); 
  
  $('.editOptionMetier').keyup(function(){
	 
      var editText = $('.editOptionMetier').val();
       $('.editable').val(editText);
       $('.editable').html(editText);
	 console.log($('.editable').val()); 
	 
     });
   }else{    
	   $('.editOptionMetier').hide();
	   }
 });
 </script>    
     <!-- **Script qui permet de rentrer soit les entreprises � partir d'une liste soit de rentrer une entreprise personalis�e**  -->
     <script>									
          var initialText = $('.editableEntreprise').val();

         $('#formEntreprise').change(function(){
         var selected = $('option:selected', this).attr('class');
         var optionText = $('.editableEntreprise').text();

       if(selected == "editableEntreprise"){
          $('.editOptionEntreprise').show();
          $('.editOptionEntreprise').val('');
    	  $('.editOptionEntreprise').html('');
    	  console.log($('.editOptionEntreprise').val()); 
      
      $('.editOptionEntreprise').keyup(function(){
    	 
          var editText = $('.editOptionEntreprise').val();
           $('.editableEntreprise').val(editText);
           $('.editableEntreprise').html(editText);
    	 console.log($('.editableEntreprise').val()); 
    	 
         });
       }else{    
    	   $('.editOptionEntreprise').hide();
    	   }
     });     
     
 </script>	
 
 	<!-- 	Script pour conditions sur les dates  -->
		<script>
			var arr = [ "dateDebut", "dateFin"];										
			jQuery.each( arr, function( i, val ){
			$( "#"+val ).change(function() {
								
			var dateFinExp = new Date($("#"+val).val());
			var currentDate = new Date();
			var currentDateYYYYMM = currentDate.toISOString().slice(0,7);
			// Condition pas de dates futures
			if (dateFinExp > currentDate) {
																			 
			   $("#"+val).val(currentDateYYYYMM);
		       $("#"+val).html(currentDateYYYYMM);
			   $("#messageErreurDate").html("Les dates post�rieures � la date courante ne sont pas accept�es.");
					}
						else {
							// Condition date de d�but < date de fin
							if ($('#dateDebut').val() > $('#dateFin').val() && $('#dateFin').val() !="" ) {
							$("#messageErreurDate").html("La date de d�but de mission doit �tre ant�rieur � la date de fin de mission.");	
							console.log('test ='+$('#dateFin').val());
							}	
						 else {
						   $("#messageErreurDate").html("");
							}	
				    }
			 });								
		})
 </script>
 
 
</html>