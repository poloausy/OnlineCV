<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script
	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/headerStyle.css" />"
	rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
</head>


<body>
	<c:import url="header.jsp" />

	<div class="container">

		<h4>
			<b>Recapitulatif de l'experience</b>
		</h4>
		<b>Projet: <c:out value="${experience.projet}" /></b><br> <br>
		<b> <c:out value="${experience.metier.nom}" /> - <c:out
				value="${experience.entreprise.nom}" /> - De <fmt:formatDate
				value="${experience.dateDebut}" pattern="MM/yyyy" /> � <fmt:formatDate
				value="${experience.dateFin}" pattern="MM/yyyy" /><br>
		</b> <br> Descriptif pour le CV: <br>
		<c:out value="${experience.descriptifCV}" escapeXml="false" />
		<br> <br> Descriptif pour le dossier de comptences: <br>
		<c:out value="${experience.descriptif}" escapeXml="false" />
		<br> <br>


		<!-- 		------------------Langages------------------------------------- -->

		<h4>
			<b>Langages informatiques</b>
		</h4>
		<p>Veuillez renseigner les langages utilises au cours de cette
			experience.</p>
		<table class="tableoutil" BORDER=0>
			<c:forEach items="${listeLangage}" var="langage">
				<tr>

					<td><c:out value="${langage.nom}" /></td>
					<td><input type="button" class="btn btn-primary"
						align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/removeLangage/${experience.id}/${langage.id}" />">&nbsp;&nbsp;&nbsp;</td>

				</tr>
			</c:forEach>

			<form method="POST"
				action="${pageContext.request.contextPath}/donneesVisiteur/saveLangage/${experience.id}"
				modelAttribute="Langage">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<tr>
					<td><form:select id="formOutilLangage" class="form-control"
							path="langage.id" required="required">
							<form:option value="" label="" />
							<option class="editableLangage" value="${langage.nom}">Autre</option>
							<form:options items="${listeGlobaleLangage}" itemValue="id"
								itemLabel="nom" />
						</form:select> <!-- propose � l'utilisateur de taper un langage personnalis� diff�rent de ceux pr�sent dans la liste d�roulante   -->
						<form:input id="editOptionOutilLangage" path="langage.nom"
							type="text" class="editOptionOutil" style="display:none;"
							placeholder="Tapez un nouveau langage"
							oninput="setCustomValidity('')"></form:input></td>


					<td><input type="submit" id = "saveltoutillangage" class="btn btn-primary"
						value="Sauvegarder" align="center" />&nbsp;&nbsp;&nbsp;</td>
				</tr>

			</form>
		</table>
		<c:if test="${erreurLangage eq 'doublon'}">
			<span class="error"><spring:message
					code="error.langage.nom.doublon" /></span>
		</c:if>

		<!-- 		------------------Logiciel------------------------------------- -->

		<h4>
			<b>Logiciel</b>
		</h4>
		<p>Veuillez renseigner les logiciels utilises au cours de cette
			experience.</p>
		<table class="tableoutil" BORDER=0>
			<c:forEach items="${listeLogiciel}" var="logiciel">
				<tr>
					<td><c:out value="${logiciel.nom}" /></td>
					<td><input type="button" class="btn btn-primary"
						align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/removeLogiciel/${experience.id}/${logiciel.id}" />">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</c:forEach>

			<form method="POST"
				action="${pageContext.request.contextPath}/donneesVisiteur/saveLogiciel/${experience.id}"
				modelAttribute="logiciel">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<tr>
					<td><form:select id="formOutilLogiciel" class="form-control"
							path="logiciel.id" required="required">
							<form:option value="" label="" />
							<option class="editableLogiciel" value="${logiciel.nom}">Autre</option>
							<form:options items="${listeGlobaleLogiciel}" itemValue="id"
								itemLabel="nom" />
						</form:select> <!--  	 propose  l'utilisateur de taper un logiciel different de ceux present dans la liste deroulante   -->

						<form:input id="editOptionOutilLogiciel" path="logiciel.nom"
							type="text" class="editOptionOutil" style="display:none;"
							placeholder="Tapez un nouveau logiciel" pattern="[A-Za-z-0-9\s]+"
							oninvalid="this.setCustomValidity('Les caracteres speciaux ne sont pas autorises !')"
							oninput="setCustomValidity('')"></form:input></td>

					<td><input type="submit" id = "saveltoutilogiciel" class="btn btn-primary"
						value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
				</tr>

			</form>
		</table>
		<c:if test="${erreurLogiciel eq 'doublon'}">
			<span class="error"><spring:message
					code="error.logiciel.nom.doublon" /></span>
		</c:if>

		<!-- 		------------------Systemes d'exploitation------------------------------------ -->

		<h4>
			<b>Syst�mes d'exploitation</b>
		</h4>
		<p>Veuillez renseigner les syst�mes d'exploitation utilises au
			cours de cette experience.</p>
		<table class="tableoutil" BORDER=0>
			<c:forEach items="${listeSystemeExploitation}"
				var="systemeExploitation">
				<tr>
					<td><c:out value="${systemeExploitation.nom}" /></td>
					<td><input type="button" class="btn btn-primary"
						align="center" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/removeSystemeExploitation/${experience.id}/${systemeExploitation.id}" />">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</c:forEach>

			<form method="POST"
				action="${pageContext.request.contextPath}/donneesVisiteur/saveSystemeExploitation/${experience.id}"
				modelAttribute="systemeExploitation">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<tr>
					<td><form:select id="formOutilSystemeExploitation"
							class="form-control" path="systemeExploitation.id"
							required="required">
							<form:option value="" label="" />
							<option class="editableSystemeExploitation"
								value="${systemeExploitation.nom}">Autre</option>
							<form:options items="${listeGlobaleSystemeExploitation}"
								itemValue="id" itemLabel="nom" />
						</form:select> <!-- propose  l'utilisateur de taper un langage different de ceux present dans la liste droulante   -->

						<form:input id="editOptionOutilSystemeExploitation"
							path="systemeExploitation.nom" type="text"
							class="editOptionOutil" style="display:none;"
							placeholder="Tapez un nouveau systeme d'exploitation"
							pattern="[A-Za-z-0-9\s]+"
							oninvalid="this.setCustomValidity('Les caracteres speciaux ne sont pas autorises !')"
							oninput="setCustomValidity('')"></form:input></td>

					<td><input type="submit" id ="saveltoutilsyst" class="btn btn-primary"
						value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
				</tr>

			</form>
		</table>
		<c:if test="${erreurSystemeExploitation eq 'doublon'}">
			<span class="error"><spring:message
					code="error.systemeExploitation.nom.doublon" /></span>
		</c:if>

		<!-- 		------------------Bases de donn�es------------------------------------- -->

		<h4>
			<b>Bases de donn�es</b>
		</h4>
		<p>Veuillez renseigner les bases de donnes utilises au cours de
			cette experience.</p>
		<table class="tableoutil" BORDER=0>
			<c:forEach items="${listeBaseDeDonnee}" var="baseDeDonnee">
				<tr>
					<td><c:out value="${baseDeDonnee.nom}" /></td>
					<td><input type="button" class="btn btn-primary"
						align="center" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/removeBaseDeDonnee/${experience.id}/${baseDeDonnee.id}" />">&nbsp;&nbsp;&nbsp;</td>
				</tr>
			</c:forEach>

			<form method="POST"
				action="${pageContext.request.contextPath}/donneesVisiteur/saveBaseDeDonnee/${experience.id}"
				modelAttribute="baseDeDonnee">
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<tr>
					<td><form:select id="formOutilBaseDeDonnee"
							class="form-control" path="baseDeDonnee.id" required="required">
							<form:option value="" label="" />
							<option class="editableBaseDeDonnee" value="${baseDeDonnee.nom}">Autre</option>
							<form:options items="${listeGlobaleBaseDeDonnee}" itemValue="id"
								itemLabel="nom" />
						</form:select> <!-- propose  l'utilisateur de taper un langage different de ceux present dans la liste droulante   -->

						<form:input id="editOptionOutilBaseDeDonnee"
							path="baseDeDonnee.nom" type="text" class="editOptionOutil"
							style="display:none;" placeholder="Tapez une base de donnee"
							pattern="[A-Za-z-0-9\s]+"
							oninvalid="this.setCustomValidity('Les caracteres speciaux ne sont pas autorises !')"
							oninput="setCustomValidity('')"></form:input></td>

					<td><input type="submit" id ="saveltoutilbd" class="btn btn-primary"
						value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
				</tr>

			</form>
		</table>
		<c:if test="${erreurBaseDeDonnee eq 'doublon'}">
			<span class="error"><spring:message
					code="error.baseDeDonnee.nom.doublon" /></span>
		</c:if>

		<input type="button" id ="saveltoutil" class="btn btn-primary" align="center"
			value="Valider" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeOutil" />">&nbsp;&nbsp;&nbsp;
	</div>

</body>

<!-- **Script qui permet de rentrer soit les Outils  partir d'une liste soit de rentrer un Outil personalis**  -->
<script>									
     
// JScript pour Language

$('#formOutilLangage').change(function(){
	var selected = $('option:selected', this).attr('class');
    //var optionText = $('.editable').text();

    if(selected == "editableLangage"){
       $('#editOptionOutilLangage').show();
       $('#editOptionOutilLangage').val('');
	   //$('.editOptionOutil').html('');
	   //console.log($('.editOptionOutil').val()); 
  
       $('#editOptionOutilLangage').keyup(function(){
	       var editText = $('#editOptionOutilLangage').val();
	       $('.editableLangage').val(editText);
	       $('.editableLangage').html(editText);
       });
       
   }else{
	   $('#editOptionOutilLangage').hide();
   }
 })
 
 // JScript pour Logiciel
 
 $('#formOutilLogiciel').change(function(){
	var selected = $('option:selected', this).attr('class');
    
    if(selected == "editableLogiciel"){
       $('#editOptionOutilLogiciel').show();
       $('#editOptionOutilLogiciel').val('');
	   
       $('#editOptionOutilLogiciel').keyup(function(){
	       var editText = $('#editOptionOutilLogiciel').val();
	       $('.editableLogiciel').val(editText);
	       $('.editableLogiciel').html(editText);
       });
       
   }else{
	   $('#editOptionOutilLogiciel').hide();
   }
 })
 
 // JScript pour Systeme d'Exploitation
 
 $('#formOutilSystemeExploitation').change(function(){
	var selected = $('option:selected', this).attr('class');
    
    if(selected == "editableSystemeExploitation"){
       $('#editOptionOutilSystemeExploitation').show();
       $('#editOptionOutilSystemeExploitation').val('');
	  
       $('#editOptionOutilSystemeExploitation').keyup(function(){
	       var editText = $('#editOptionOutilSystemeExploitation').val();
	       $('.editableSystemeExploitation').val(editText);
	       $('.editableSystemeExploitation').html(editText);
       });
       
   }else{
	   $('#editOptionOutilSystemeExploitation').hide();
   }
 })
 
 // JScript pour Base de donn�es
 
 $('#formOutilBaseDeDonnee').change(function(){
	var selected = $('option:selected', this).attr('class');
   
    if(selected == "editableBaseDeDonnee"){
       $('#editOptionOutilBaseDeDonnee').show();
       $('#editOptionOutilBaseDeDonnee').val('');	  
  
       $('#editOptionOutilBaseDeDonnee').keyup(function(){
	       var editText = $('#editOptionOutilBaseDeDonnee').val();
	       $('.editableBaseDeDonnee').val(editText);
	       $('.editableBaseDeDonnee').html(editText);
       });
       
   }else{
	   $('#editOptionOutilBaseDeDonnee').hide();
   }
 })
     
 </script>
</html>