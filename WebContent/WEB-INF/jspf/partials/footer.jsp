<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!-- Footer -->
<footer class="page-footer font-small pt-4">
	<div class="container">
		<div class="row">
			<!-- <div class="col-lg-4 col-xs-12">
				<ul class="footer-list">
					<li><a>Test</a></li>
					<li><a>Test</a></li>
				</ul>
			</div>

			<div class="col-lg-4 col-xs-12">
				<ul class="footer-list">
					<li><a>Test</a></li>
					<li><a>Test</a></li>
				</ul>
			</div>
			
			<div class="col-lg-4 col-xs-12">
				<ul class="footer-list">
					<li><a>Test</a></li>
					<li><a>Test</a></li>
				</ul>
			</div> -->
			
			<div class="copyright">
				<p>&copy; 2018 copyright Ausy. All rights reserved.</p>
			</div>
		</div>
	</div>
</footer>
<!-- Footer -->