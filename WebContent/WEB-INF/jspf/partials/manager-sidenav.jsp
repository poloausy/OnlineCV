<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!-- MANAGER SIDE NAVIGATION -->

<div class="manager-sidenav sidenav ">
	<ul>
		<li><a href="<c:url value="/manager/goToMenuManager" />">Home</a></li>
		<li><a href="<c:url value="/manager/search" />">Rechercher</a></li>
		<%-- <li><a href="<c:url value="/admin/utilisateurs" />">Utilisateurs</a></li>
		<li><a href="<c:url value="/admin/templates" />">Templates</a></li>
		<li><a href="<c:url value="/admin/langues" />">Langues</a></li>
		<li><a href="<c:url value="/admin/entreprises" />">Entreprises</a></li>
		<li><a href="<c:url value="/admin/metiers" />">Metiers</a></li> --%>		
	</ul>
</div>