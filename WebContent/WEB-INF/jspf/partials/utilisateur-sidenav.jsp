<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>

<!-- SIDE NAVIGATION -->

<div class="sidenav">


	<a id ="profil" href="<c:url value="/donneesVisiteur/goToExperience" />">Profil</a>

	<a id = "selectdatatoshow" href="<c:url value="/visiteur/goToSelection" />">Selection des informations � afficher</a>

	<a id = "choixtemplate" href="<c:url value="/visiteur/goToChoixTemplate" />">Choix du template</a> 
	

</div>
