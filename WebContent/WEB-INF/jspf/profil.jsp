<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/headerStyle.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/accordionStyle.css" />" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>

<!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG" >
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD"  >
				<img src="../static/images/newLogoAusy.png"/>
			</div>

 <!-- Main -->
	<div class="main">
		<c:import url="header.jsp" />
 
 <div class="row">
		<div class="col-lg-2">
	      <!-- Sidenav partial -->
		  <c:import url="partials/utilisateur-sidenav.jsp" />
        </div>

	

	<!-- Navbar -->
	  <div class="col-lg-9 col-lg-push-0">
		<div class="container">

			<h2 class="textBleuClair">Profil <span class="nomlogin">${utilisateur.credential.nomLogin} </span></h2>
			<h3 class="textBleuClair">Renseignez votre profil dans le formulaire ci-dessous.</h3>
			

			<button class="accordion" id="infoPersonnellesFirstSelenium">Informations Personnelles</button>
			<div class="panel">
				<c:import url="infosPersonnelles.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="titresFirstSelenium">Titres</button>
			<div class="panel">
				<c:import url="titres.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="languesFirstSelenium">Langues</button>
			<div class="panel">
				<c:import url="langues.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="formationsFirstSelenium">Formation</button>
			<div class="panel">
				<c:import url="formations.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="experiencesProfessionnellesFirstSelenium">Experience Professionnelle</button>
			<div class="panel">
				<c:import url="experiences.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="competencesFirstSelenium">Competences</button>
			<div class="panel">
				<c:import url="competences.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="certificationsFirstSelenium">Certifications</button>
			<div class="panel">
				<c:import url="certifications.jsp" />
			</div>
			<br><br>
			<button class="accordion" id="resumeTechniqueEtFonctionnelFirstSelenium">Resum� Technique et Fonctionnel</button>
			<div class="panel">
				<c:import url="resumeTechnicalAndFunctionalSkills.jsp" />
			</div>

		 </div>
      </div>
    </div>  
</div>

	<!-- Mise en forme de l'accord�on (block masqu� ou non) -->
	<!-- � refactoriser !!!!! -->
	<c:if test="${champActif eq 'infoPersonnelle'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[0].classList.toggle("active");
			acc[0].nextElementSibling.classList.toggle("show");

		</script>
	</c:if>
		<c:if test="${champActif eq 'titre'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[1].classList.toggle("active");
			acc[1].nextElementSibling.classList.toggle("show");

		</script>
	</c:if>
	<c:if test="${champActif eq 'langue'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[2].classList.toggle("active");
			acc[2].nextElementSibling.classList.toggle("show");
		</script>
	</c:if>
	<c:if test="${champActif eq 'formation'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[3].classList.toggle("active");
			acc[3].nextElementSibling.classList.toggle("show");
		</script>
	</c:if>
	<c:if test="${champActif eq 'experience'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[4].classList.toggle("active");
			acc[4].nextElementSibling.classList.toggle("show");
		</script>
	</c:if>
		<c:if test="${champActif eq 'competence'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[5].classList.toggle("active");
			acc[5].nextElementSibling.classList.toggle("show");
		</script>
	</c:if>
	<c:if test="${champActif eq 'certification'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[6].classList.toggle("active");
			acc[6].nextElementSibling.classList.toggle("show");
		</script>
	</c:if>
	<c:if test="${champActif eq 'resumeTechnicalAndFunctionalSkills'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[7].classList.toggle("active");
			acc[7].nextElementSibling.classList.toggle("show");
		
		</script>
	</c:if>
	<c:if test="${champActif eq 'resumeTechnicalAndFunctionalSkills'}">
		<script>
			var acc = document.getElementsByClassName("accordion")
			acc[8].classList.toggle("active");
			acc[8].nextElementSibling.classList.toggle("show");
		</script>
	</c:if>

<!-- Ouverture des accord�ons avec au pr�alable fermeture des accord�ons deja ouverts -->

 <script>	
 $(document).ready(function(){
	var acc = document.getElementsByClassName("accordion");
    var panel = document.getElementsByClassName('panel');

    for (var i = 0; i < acc.length; i++) {
      acc[i].onclick = function() {
    	var setClasses = !this.classList.contains('active');
        setClass(acc, 'active', 'remove');
        setClass(panel, 'show', 'remove');
                
       	if (setClasses) {
       		//bascule en classe active
            this.classList.toggle("active");
            this.nextElementSibling.classList.toggle("show");           
        }
      }
   }

function setClass(els, className, fnName) {
    for (var i = 0; i < els.length; i++) {
        els[i].classList[fnName](className);
    }
  }
 });
</script>	
  	
</body>
</html>