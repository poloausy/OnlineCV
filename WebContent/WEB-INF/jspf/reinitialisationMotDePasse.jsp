<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>

<!DOCTYPE html>
<html>

<head>
  <title>Mon compte</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <link href="<c:url value="/static/css/inscription.css" />" rel="stylesheet">
</head>

<body>

<div align="center" id="bg" >
<img src="<c:url value="/static/images/blue-motion-blur-design.jpg" />" width=100% />
</div>

<div class="container" >
	<div align="center">
		<h1>
		<font color="blue"><b><spring:message code="reinitialisationMotDePasse" /></b></font>
		</h1>
	</div>
	<div class="form-group" bgcolor="#E6E6FA">
	<form method="POST" action="reinitialiserMotDePasse" modelAttribute="credential">
	<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
<%-- 	R�cup�re id dans 'URL --%>
<input type="hidden" name="id" value="${param.id}"/>
<%-- 	<c:out value="${param.id}" /> --%>
	<% String id = "${param.id}";%>
	<div align="center">
			<TABLE class="tableinscription" BORDER=0>
				<tr>
					<td nowrap><form:label path="credential.nomLogin">
					<spring:message code="utilisateur.credential.nomLogin" /><span class="required">*</span></form:label></td>
					<td><form:input path="credential.nomLogin" class="form-control" required="required"/></td>
					<td><form:errors path="utilisateur.credential.nomLogin" cssClass="errors" /></td>
				</tr>
			    <tr>
					<td nowrap><form:label path="credential.motDePasse">
					<spring:message code="credential.password" /><span class="required">*</span></form:label></td>					
		            <td><form:input type="password" id="motDePasse" name="motDePasse" path="credential.motDePasse" class="form-control" minlength="6" required="required" pattern="[0-9]{6,}"/></td>
		            <td><form:errors path="credential.motDePasse" cssClass="errors"/></td>
				</tr>
				<tr>
				<td><b><spring:message code="credential.confirmationMotDePasse" /><span class="required">*</span></b></td>
 				<td><input type="password" class="form-control"  name="confirmationMotDePasse" required>
 				 </td>
				</tr> 
				
			</TABLE>
			
			<div align="center">
			<tr><b><FONT color="blue">Format du mot de passe : 6 chiffres ou plus.</FONT></b></b></tr>
			</div>			
			
			</div>
		</div>
		<div align="center">
			<input type="submit" class="btn btn-primary" value="Valider" align="center" />&nbsp;&nbsp;&nbsp;
			<a class="btn btn-primary" href="<c:url value="/welcome/goToAccueil" />" >Retour</a>
		</div>
		</form>
		<c:if test="${message == 'messageMotsDePasseNonIdentiques'}">
		<br>
		<div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
		     <h3 class="error" align="center">Attention les deux mots de passe rentr�s ne sont pas identiques !</h3>      
	      </c:if>
	      <c:if test="${message == 'messageErreurLogin'}">
	      <br>
	      <div align="center" ><span class="glyphicon glyphicon-alert" align="center" style="font-size:36px;color:red"></span> </div>
		     <h3 class="error" align="center">Attention, il y a une erreur sur votre login</h3>      
	      </c:if>
		
</div>
</body>
</html>