<%@page import="com.onlinecv.entities.Utilisateur"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
<title>CV Online</title>
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
<script src="https://momentjs.com/downloads/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src='https://cloud.tinymce.com/stable/tinymce.min.js'></script>
</head>
<script>
  tinymce.init({
    selector: '#mytextarea',
    plugins : "autolink,lists,pagebreak,layer,table,save,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,template,wordcount,advlist,autosave,visualblocks",
    // taille disponible
    theme_advanced_font_sizes: "10px,11px,12px,13px,14px,15px,16px,17px,18px,19px,20px,21px,22px,23px,24px,25px",
    // couleur disponible dans la palette de couleur
    theme_advanced_text_colors : "33FFFF, 007fff, ff7f00",
    // balise html disponible
    theme_advanced_blockformats : "h1, h2,h3,h4,h5,h6",
    // class disponible
    theme_advanced_styles : "Tableau=textTab;TableauSansCadre=textTabSansCadre;",
    style_formats: [
        {title: 'Open Sans', inline: 'span', styles: { 'font-family':'Open Sans'}},
        {title: 'Arial', inline: 'span', styles: { 'font-family':'arial'}},
        {title: 'Book Antiqua', inline: 'span', styles: { 'font-family':'book antiqua'}},
        {title: 'Comic Sans MS', inline: 'span', styles: { 'font-family':'comic sans ms,sans-serif'}},
        {title: 'Courier New', inline: 'span', styles: { 'font-family':'courier new,courier'}},
        {title: 'Georgia', inline: 'span', styles: { 'font-family':'georgia,palatino'}},
        {title: 'Helvetica', inline: 'span', styles: { 'font-family':'helvetica'}},
        {title: 'Impact', inline: 'span', styles: { 'font-family':'impact,chicago'}},
        {title: 'Symbol', inline: 'span', styles: { 'font-family':'symbol'}},
        {title: 'Tahoma', inline: 'span', styles: { 'font-family':'tahoma'}},
        {title: 'Terminal', inline: 'span', styles: { 'font-family':'terminal,monaco'}},
        {title: 'Times New Roman', inline: 'span', styles: { 'font-family':'times new roman,times'}},
        {title: 'Verdana', inline: 'span', styles: { 'font-family':'Verdana'}}
    ],
  });
  </script>

<body>
	<p>Comp�tences � afficher dans le dossier de comp�tences</p>
	
	<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveResumeTechniqueEtFonctionnel"

		modelAttribute="resumeTechniqueEtFonctionnel">
		<input type="hidden" name="${_csrf.parameterName}"
			value="${_csrf.token}" />

		<form:hidden path="resumeTechniqueEtFonctionnel.id" name="idResume"/>
		
		<div class="mainArea" class="quickFade delayFive" align="center">
			<table class="tableformulaire" BORDER=0>
								
				<tr>
					<td nowrap><form:label path="resumeTechniqueEtFonctionnel.principalCompetences">
					<spring:message code="resumeTechniqueEtFonctionnel.principalCompetences" />
					</form:label></td>
					<td><form:textarea id="mytextarea"  path="resumeTechniqueEtFonctionnel.principalCompetences" class="form-control"/></td>
			    </tr>			    
			
				<tr>
					<td nowrap><form:label path="resumeTechniqueEtFonctionnel.methodes">
					<spring:message code="resumeTechniqueEtFonctionnel.methodes" />
					</form:label></td>
					<td><form:input  path="resumeTechniqueEtFonctionnel.methodes" class="form-control"/></td>
					
				</tr>
								
				<tr>
					<td nowrap><form:label path="resumeTechniqueEtFonctionnel.industries">
					<spring:message code="resumeTechniqueEtFonctionnel.industries" /><span class="required">*</span>
					</form:label></td>
					<td><form:input  path="resumeTechniqueEtFonctionnel.industries" class="form-control hide-spinner" required="required" /></td>
				</tr>
				
				</table>
				
			<p><input type="submit" id = "saveresume" class="btn btn-primary" value="Sauvegarder"	align="middle" />&nbsp;&nbsp;&nbsp;</p>
			</form>
				
			
			<c:if test="${resumeTechniqueEtFonctionnel.id > 0}">
			<p>Pour r�initialiser les langages, logiciels, bases de donn�es et syst�mes d'exploitation � partir de vos exp�riences : <a href=<c:url value="/donneesVisiteur/rappatrieOutils" />> ici + </a> </p>
				<table class="tableformulaire" BORDER=0>
				<tr>	
					    <td><b>Langages informatiques</b></td>
					    <c:if test="${empty listeLangage}">N/A</c:if>
						<td>						
						<c:forEach items="${listeLangage}" var="outil" varStatus="loop">
								<c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
								<c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
						</c:forEach>
						</td>
						
						<td><input type="button" id="resumeaddlangage" class="btn btn-primary" data-toggle="modal" data-target="#nouveauLangage-${resumeTechniqueEtFonctionnel.id}" value="Ajouter"></button>
						<td><input type="button" class="btn btn-danger" data-toggle="modal" data-target="#retirerLangage-${resumeTechniqueEtFonctionnel.id}" value="Modifier"></button>
						</td>
				</tr>
				<tr>
						<td><b>Logiciels </b></td>
						
						<td><c:forEach items="${listeLogiciel}" var="outil" varStatus="loop">						        
								<c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
								<c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
						</c:forEach></td>
						
						<td><input type="button" id="resumeaddlogiciel" class="btn btn-primary" data-toggle="modal" data-target="#nouveauLogiciel-${resumeTechniqueEtFonctionnel.id}" value="Ajouter"></button></td>
				        <td><input type="button" class="btn btn-danger" data-toggle="modal" data-target="#retirerLogiciel-${resumeTechniqueEtFonctionnel.id}" value="Modifier"></button>
				</tr>
				<tr>
						<td><b>Bases de donn�es</b> </td>
						<td><c:forEach items="${listeBaseDeDonnee}" var="outil" varStatus="loop">
								<c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
								<c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
						</c:forEach></td>
						<td><input type="button" id="resumeaddBD" class="btn btn-primary" data-toggle="modal" data-target="#nouvelleBaseDeDonnee-${resumeTechniqueEtFonctionnel.id}" value="Ajouter"></button></td>
						<td><input type="button" class="btn btn-danger" data-toggle="modal" data-target="#retirerBaseDeDonnee-${resumeTechniqueEtFonctionnel.id}" value="Modifier"></button>
				</tr>
				<tr>
						<td><b>Syst�mes d'exploitation </b></td>
						<td><c:forEach items="${listeSystemeExploitation}" var="outil" varStatus="loop">
								<c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
								<c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
						</c:forEach></td>
						<td><input type="button" id="resumeaddSE" class="btn btn-primary" data-toggle="modal" data-target="#nouveauSystemeExploitation-${resumeTechniqueEtFonctionnel.id}" value="Ajouter"></button></td>
						<td><input type="button" class="btn btn-danger" data-toggle="modal" data-target="#retirerSystemeExploitation-${resumeTechniqueEtFonctionnel.id}" value="Modifier"></button>
				</tr>					
				</table>
			</c:if>	
			
			<center>
			<p>
			<input type="button" id="closeresumeaccordeon" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">&nbsp;&nbsp;&nbsp;
			</p>
			</center>		
		  		    	
		</div>
		
						
				<!--  ***************POPUP NOUVEAU LANGAGE***************	 -->
								
		    <div id="nouveauLangage-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Langage informatique � ajouter</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveLangageResume/${resumeTechniqueEtFonctionnel.id}" modelAttribute="langage">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>						
										<tr>										
											<td>Langage informatique</td>
 											<td><form:select id="formOutilLangage" class="form-control" path="langage.id" required="required">
													<form:option value="" label="" /> 
													<option class="editableLangage" value="${langage.nom}">Autre</option>
													<form:options  items="${listeGlobaleLangage}" itemValue="id" itemLabel="nom" />									
										    	</form:select>
										    	
													<!-- propose � l'utilisateur de taper un langage personnalis� diff�rent de ceux pr�sent dans la liste d�roulante   -->
													
									<form:input id="editOptionOutilLangage" path="langage.nom" type="text" class="editOptionOutil" style="display:none;" placeholder="Tapez un nouveau langage"
									pattern="[A-Za-z�-�0-9\s]+" oninvalid="this.setCustomValidity('Les caract�res sp�ciaux ne sont pas autoris�s !')"
									oninput="setCustomValidity('')"></form:input>

											<td><input type="submit" id ="savenewresumelangage" class="btn btn-primary" value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
										</tr>
									</table>	
								</div>		
							</form>	 
						</div>
					
                    <!--Modal footer -->
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>
		  
		  
		  <!--  ***************POPUP RETIRER LANGAGE***************	 -->
								
		    <div id="retirerLangage-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Langages informatique � retirer</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						
								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>											
 									  <c:forEach items="${resumeTechniqueEtFonctionnel.listeLangage}" var="langage">				                                 
				                       <tr>
				                        <td><c:out value="${langage.nom}" /></td>
				                                  
				                        <td><a href="<c:url value="/donneesVisiteur/retirerLangageResume/${resumeTechniqueEtFonctionnel.id}/${langage.id}"/>" class="btn btn-danger">Retirer</a></td>
				                       </tr>  
			                          </c:forEach>		
									</table>	
								</div>
										
						</div>
					
                    <!--Modal footer -->
                    
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>
				  
		  <!--  ***************POPUP NOUVEAU LOGICIEL***************	 -->
								
		    <div id="nouveauLogiciel-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Logiciel � ajouter</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveLogicielResume/${resumeTechniqueEtFonctionnel.id}"	modelAttribute="logiciel">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>						
										<tr>
											<td><form:select id="formOutilLogiciel" class="form-control"  path="logiciel.id" required="required">
						                        <form:option value="" label="" /> 
						                        <option class="editableLogiciel" value="${logiciel.nom}">Autre</option>
						                        <form:options  items="${listeGlobaleLogiciel}" itemValue="id" itemLabel="nom" />									
						                        </form:select>
							
                                     <!--  	 propose  l'utilisateur de taper un logiciel different de ceux present dans la liste droulante   -->
 								
									           <form:input id="editOptionOutilLogiciel" path="logiciel.nom" type="text" class="editOptionOutil" style="display:none;" placeholder="Tapez un nouveau logiciel"
									           pattern="[A-Za-z-0-9\s]+" oninvalid="this.setCustomValidity('Les caracteres speciaux ne sont pas autorises !')"
									           oninput="setCustomValidity('')"></form:input></td>
						
					                       <td><input type="submit" id = "savenewresumelogiciel" class="btn btn-primary" value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
									  </tr>	
									</table>	
								</div>		
							</form>	 
						</div>
					
<!--                     Modal footer -->
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>
		  
		  <!--  ***************POPUP RETIRER LOGICIEL***************	 -->
								
		    <div id="retirerLogiciel-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Logiciels � retirer</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						
								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>											
 									  <c:forEach items="${resumeTechniqueEtFonctionnel.listeLogiciel}" var="logiciel">				                                 
				                       <tr>
				                        <td><c:out value="${logiciel.nom}" /></td>
				                                  
				                        <td><a href="<c:url value="/donneesVisiteur/retirerLogicielResume/${resumeTechniqueEtFonctionnel.id}/${logiciel.id}"/>" class="btn btn-danger">Retirer</a></td>
				                       </tr>  
			                          </c:forEach>		
									</table>	
								</div>
										
						</div>
					
                    <!--Modal footer -->
                    
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>	
		  
		  <!--  ***************POPUP NOUVELLE BASE DE DONNEE***************	 -->
								
		    <div id="nouvelleBaseDeDonnee-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Base de donn�es � ajouter</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveBaseDeDonneeResume/${resumeTechniqueEtFonctionnel.id}"	modelAttribute="baseDeDonnee">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>						
									<tr>
											<td><form:select id="formOutilBaseDeDonnee" class="form-control"  path="baseDeDonnee.id" required="required">
								            <form:option value="" label="" /> 
								            <option class="editableBaseDeDonnee" value="${baseDeDonnee.nom}">Autre</option>
								            <form:options  items="${listeGlobaleBaseDeDonnee}" itemValue="id" itemLabel="nom" />									
								            </form:select>
								
 								<!-- propose  l'utilisateur de taper un langage different de ceux present dans la liste droulante   -->
 								
									        <form:input id="editOptionOutilBaseDeDonnee" path="baseDeDonnee.nom" type="text" class="editOptionOutil" style="display:none;" placeholder="Tapez une base de donnee"
									        pattern="[A-Za-z-0-9\s]+" oninvalid="this.setCustomValidity('Les caracteres speciaux ne sont pas autorises !')"
									        oninput="setCustomValidity('')"></form:input></td>
						
						                    <td><input type="submit" id = "savenewresumeBD" class="btn btn-primary" value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
									</tr>
									</table>	
								</div>		
							</form>	 
						</div>
					
<!--                     Modal footer -->
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>
		  
		   <!--  ***************POPUP RETIRER BASE DE DONNEE***************	 -->
								
		    <div id="retirerBaseDeDonnee-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Bases de donn�es � retirer</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						
								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>											
 									  <c:forEach items="${resumeTechniqueEtFonctionnel.listeBaseDeDonnee}" var="baseDeDonnee">				                                 
				                       <tr>
				                        <td><c:out value="${baseDeDonnee.nom}" /></td>
				                                  
				                        <td><a href="<c:url value="/donneesVisiteur/retirerBaseDeDonneeResume/${resumeTechniqueEtFonctionnel.id}/${baseDeDonnee.id}"/>" class="btn btn-danger">Retirer</a></td>
				                       </tr>  
			                          </c:forEach>		
									</table>	
								</div>
										
						</div>
					
                    <!--Modal footer -->
                    
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>	
		  
		  <!--  ***************POPUP NOUVEAU SYSTEME D'EXPLOITATION***************	 -->
								
		    <div id="nouveauSystemeExploitation-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Syst�me d'exploitation �  ajouter</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveSystemeExploitationResume/${resumeTechniqueEtFonctionnel.id}"	modelAttribute="systemeExploitation">
								<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>						
									<tr>
											<td><form:select id="formOutilSystemeExploitation" class="form-control"  path="systemeExploitation.id" required="required">
								            <form:option value="" label="" /> 
								            <option class="editableSystemeExploitation" value="${systemeExploitation.nom}">Autre</option>
								            <form:options  items="${listeGlobaleSystemeExploitation}" itemValue="id" itemLabel="nom" />									
								            </form:select>
								
 								<!-- propose  l'utilisateur de taper un langage different de ceux present dans la liste droulante   -->
 								
									        <form:input id="editOptionOutilSystemeExploitation" path="systemeExploitation.nom" type="text" class="editOptionOutil" style="display:none;" placeholder="Tapez un nouveau systeme d'exploitation"
									        pattern="[A-Za-z-0-9\s]+" oninvalid="this.setCustomValidity('Les caracteres speciaux ne sont pas autorises !')"
									        oninput="setCustomValidity('')"></form:input></td>
						
						                    <td><input type="submit" id = "savenewresumeSE" class="btn btn-primary" value="Sauvegarder" align="middle" />&nbsp;&nbsp;&nbsp;</td>
									</tr>
									</table>	
								</div>		
							</form>	 
						</div>
					
<!--                     Modal footer -->
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>
		  
		  <!--  ***************POPUP RETIRER SYSTEME D'EXPLOITATION***************	 -->
								
		    <div id="retirerSystemeExploitation-${resumeTechniqueEtFonctionnel.id}" class="modal fade" role="dialog">
				<div class="modal-dialog">
					<div class="modal-content">
					
                    <!--Modal Header -->
					
						<div class="modal-header d-flex justify-content-center">
						<h2><b>Syst�me d'exploitation � retirer</b></h2>
						<button type="button" class="close" data-dismiss="modal">x</button>							
						</div>
					
                    <!--Modal body -->
					
						<div class="modal-body">
	 						
								<div class="quickFade delayFive" align="center">
									<table class="tableformulaire" BORDER=0>											
 									  <c:forEach items="${resumeTechniqueEtFonctionnel.listeSystemeExploitation}" var="systemeExploitation">				                                 
				                       <tr>
				                        <td><c:out value="${systemeExploitation.nom}" /></td>
				                                  
				                        <td><a href="<c:url value="/donneesVisiteur/retirerSystemeExploitationResume/${resumeTechniqueEtFonctionnel.id}/${systemeExploitation.id}"/>" class="btn btn-danger">Retirer</a></td>
				                       </tr>  
			                          </c:forEach>		
									</table>	
								</div>
										
						</div>
					
                    <!--Modal footer -->
                    
						<div class="modal-footer">					
						<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
						</div>					
					
				    </div>
			   </div>
		  </div>
		  	
</body>

<!-- **Script qui permet de rentrer soit les Outils � partir d'une liste soit de rentrer un Outil personalis�**  -->

 <script>
 
   // JScript pour Language
 
 $('#formOutilLangage').change(function(){
		var selected = $('option:selected', this).attr('class');
	    //var optionText = $('.editable').text();

	    if(selected == "editableLangage"){
	       $('#editOptionOutilLangage').show();
	       $('#editOptionOutilLangage').val('');
		   //$('.editOptionOutil').html('');
		   //console.log($('.editOptionOutil').val()); 
	  
	       $('#editOptionOutilLangage').keyup(function(){
		       var editText = $('#editOptionOutilLangage').val();
		       $('.editableLangage').val(editText);
		       $('.editableLangage').html(editText);
	       });
	       
	   }else{
		   $('#editOptionOutilLangage').hide();
	   }
	 })
	 
	 // JScript pour Logiciel
 
 $('#formOutilLogiciel').change(function(){
	var selected = $('option:selected', this).attr('class');
    
    if(selected == "editableLogiciel"){
       $('#editOptionOutilLogiciel').show();
       $('#editOptionOutilLogiciel').val('');
	   
       $('#editOptionOutilLogiciel').keyup(function(){
	       var editText = $('#editOptionOutilLogiciel').val();
	       $('.editableLogiciel').val(editText);
	       $('.editableLogiciel').html(editText);
       });
       
   }else{
	   $('#editOptionOutilLogiciel').hide();
   }
 })
 
 // JScript pour Systeme d'Exploitation
 
 $('#formOutilSystemeExploitation').change(function(){
	var selected = $('option:selected', this).attr('class');
    
    if(selected == "editableSystemeExploitation"){
       $('#editOptionOutilSystemeExploitation').show();
       $('#editOptionOutilSystemeExploitation').val('');
	  
       $('#editOptionOutilSystemeExploitation').keyup(function(){
	       var editText = $('#editOptionOutilSystemeExploitation').val();
	       $('.editableSystemeExploitation').val(editText);
	       $('.editableSystemeExploitation').html(editText);
       });
       
   }else{
	   $('#editOptionOutilSystemeExploitation').hide();
   }
 })
 
 // JScript pour Base de donn�es
 
 $('#formOutilBaseDeDonnee').change(function(){
	var selected = $('option:selected', this).attr('class');
   
    if(selected == "editableBaseDeDonnee"){
       $('#editOptionOutilBaseDeDonnee').show();
       $('#editOptionOutilBaseDeDonnee').val('');	  
  
       $('#editOptionOutilBaseDeDonnee').keyup(function(){
	       var editText = $('#editOptionOutilBaseDeDonnee').val();
	       $('.editableBaseDeDonnee').val(editText);
	       $('.editableBaseDeDonnee').html(editText);
       });
       
   }else{
	   $('#editOptionOutilBaseDeDonnee').hide();
   }
 })
     
 </script>
</html>