<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/security/tags"	prefix="sec"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>


<head>
<title>CV Online</title>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script	src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/headerStyle.css" />" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="<c:url value="/static/css/selection.css" />" rel="stylesheet">
</head>



<body>

 <!-- 	************************** IMAGE COTE GAUCHE **************************	 -->

			<div id="bandeauG">
				<img src="../static/images/newLogoAusy.png"/>
			</div>
 <!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="bandeauD">
				<img src="../static/images/newLogoAusy.png"/>
			</div>				


<c:import url="header.jsp" />

<div class="row">
  <div class="col-lg-3">
	<!-- Sidenav partial -->
   <sec:authorize
		access="hasAnyRole('ROLE_UTILISATEUR')">
	<c:import url="partials/utilisateur-sidenav.jsp" />
   </sec:authorize>

   <sec:authorize
     access="hasAnyRole('ROLE_ADMIN')">
	 <c:import url="partials/admin-sidenav-cv.jsp" />
   </sec:authorize>
   
   <sec:authorize
     access="hasAnyRole('ROLE_MANAGER')">
	 <c:import url="partials/manager-sidenav-cv.jsp" />
   </sec:authorize>
  </div>
	
 <div class="col-lg-9 col-lg-push-0">

	<h2 class="textBleuClair">Selection des informations - <span class="nomlogin">${utilisateur.credential.nomLogin} </span></h2>
	
	<div text-align="center">
   
	<form:form modelAttribute="affichage" method="POST" action="${pageContext.request.contextPath}/welcome/sauveAffichage"> 
	
		
	<h3 class="textBleuClair">Informations personnelles</h3>
 	<strong>${utilisateur.prenom} ${utilisateur.nom}</strong><br>
	Date de naissance: ${utilisateur.dateDeNaissance}<br>
	${utilisateur.nationalite_1} <c:if test="${!empty utilisateur.nationalite_2}">, <c:out value="${utilisateur.nationalite_2}" /></c:if><br>	
	Tel: ${utilisateur.telephone}<br>
	Adresse: ${utilisateur.adresse.rue} - ${utilisateur.adresse.codePostal} ${utilisateur.adresse.ville} - ${utilisateur.adresse.nomPays}<br>
	<br>
	<input type="button" class="btn btn-primary" align="middle" value="Selectionner tout" onclick=window.location.href="<c:url value="/visiteur/ReinitialisationAffichage/${utilisateur.id}" />">
	<br>
	
	<div class="panel-heading" data-toggle='collapse' href='#titre'>
    <h6 class="first-title"><strong>TITRES </strong>
    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>
		
	</h6>
    </div>	
	
	<div id="titre" class='panel-collapse collapse'>
      <div class="panel-body">
	
		<c:if test="${empty listeTitre}">N/A</c:if>
		<c:if test="${!empty listeTitre}">
		 	<c:forEach items="${listeTitre}" var="titre">
		            <form:checkbox path="listeTitreId" value="${titre.id}" label="${titre.titre}" /><br>
		     </c:forEach>
		</c:if>
		
		</div>
	</div>
	
	<div class="panel-heading" data-toggle='collapse' href='#langue'>
    <h2 class="first-title"><strong >LANGUES</strong>
    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>
			
	</h2>
    </div>
    
    <div id="langue" class='panel-collapse collapse'>
      <div class="panel-body">	

		<c:if test="${empty listeLangue}">N/A</c:if>
		<c:if test="${!empty listeLangue}">
			<c:forEach items="${listeLangue}" var="langue">
				  <form:checkbox path="listeLangueId" value="${langue.id}" label="${langue.nomLangue.nom}" /> - niveau: ${langue.niveau.intitule} <br>
			</c:forEach> 
		</c:if>
		
		</div>
	</div>
	
	<div class="panel-heading" data-toggle='collapse' href='#formation'>
    <h2 class="first-title"><strong >FORMATIONS</strong>
    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>
			
	</h2>
    </div>
    
    <div id="formation" class='panel-collapse collapse'>
      <div class="panel-body">
		
		<c:if test="${empty listeFormation}">N/A</c:if>
		<c:if test="${!empty listeFormation}">
			<c:forEach items="${listeFormation}" var="formation">
				  <form:checkbox path="listeFormationId" value="${formation.id}" label="${formation.intitule}" /> - ${formation.anneeObtention} <br>
			</c:forEach>
		</c:if>
		
	</div>
	</div>			
				
	<div class="panel-heading" data-toggle='collapse' href='#experience'>
	    <h2 class="first-title"><strong >EXPERIENCES</strong>
	    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>
				
		</h2>
    </div>
    
    <div id="experience" class='panel-collapse collapse'>
      <div class="panel-body">
       
       	<c:if test="${empty listeExperience}">N/A</c:if>
		<c:if test="${!empty listeExperience}">
		<input type="hidden" id="listeExpLength" value="${fn:length(listeExperience)}"/>
         <c:forEach items="${listeExperience}" var="experience" varStatus="loop">
  			<form:checkbox path="listeExperienceId" value="${experience.id}" label="${experience.projet}" /><br>
           	<div class="listeACocher">
             	<p>${experience.metier.nom} - ${experience.projet}	- ${experience.entreprise.nom}&nbsp;						
					de <fmt:formatDate value="${experience.dateDebut}" pattern="MM/yyyy" /> � <fmt:formatDate value="${experience.dateFin}" pattern="MM/yyyy" />
				</p>
     			
     			<!--  Liste des D�tails  -->
     			
     			<div class="panel-heading" data-toggle='collapse' href='#details_${loop.index}'>
                   <h4 class="panel-title"><strong >D�tails</strong>
                   <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		              <!-- <span class="if-collapsed"><i class="drop1 glyphicon glyphicon-chevron-right" style="color:red;"></i></span>
		              <span class="if-not-collapsed" ><i class="drop1 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
              		</h4>
                </div>
                
     			<div id="details_${loop.index}" class="panel-collapse collapse">
	     			<div class="panel-body">
		     			<p><b>Descriptif pour le CV :</b>&nbsp;${experience.descriptifCV}</p>
		     			<p><b>Descriptif pour le Dossier de comp�tences :</b>&nbsp;${experience.descriptif}</p>
	     			</div>
     			</div>
     			
     			<!--  Liste des Outils  -->
     			
     			<div class="panel-heading" data-toggle='collapse' href='#outils_${loop.index}'>
                    <h4 class="panel-title"><strong >Outils</strong>
                    <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		              <!-- <span class="if-collapsed"><i class="drop2 glyphicon glyphicon-chevron-right" style="color:red" ></i></span>
		              <span class="if-not-collapsed"><i class="drop2 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
                 	</h4>
                </div>
                
     			<div id="outils_${loop.index}" class='panel-collapse collapse'>
     			<div class="panel-body">
     			
     			
     			<p><c:if test="${experience.listeLangage.size() > 0}"> <!-- *********PERMET DE NE RIEN AFFICHER SI RIEN N'EST RENSEIGNE********* -->
     			<b>Langages :</b> 
		        <c:forEach items="${experience.listeLangage}" var="outil" varStatus="loop">		           
				   <c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
				   <c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
		        </c:forEach>		        
		        </c:if>
		        </p>
		        
		        <p><c:if test="${experience.listeLogiciel.size() > 0}"> <!-- *********PERMET DE NE RIEN AFFICHER SI RIEN N'EST RENSEIGNE********* -->
		        <b>Logiciels :</b> 
		        <c:forEach items="${experience.listeLogiciel}" var="outil" varStatus="loop">
				   <c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
				   <c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
		        </c:forEach>
		        </c:if>
		        </p>
		        
		        <p><c:if test="${experience.listeBaseDeDonnee.size() > 0}"> <!-- *********PERMET DE NE RIEN AFFICHER SI RIEN N'EST RENSEIGNE********* -->
		        <b>Base de donn�es :</b> 
		        <c:forEach items="${experience.listeBaseDeDonnee}" var="outil" varStatus="loop">
				   <c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
				   <c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
		        </c:forEach>
		        </c:if>
		        </p>
		
		        <p><c:if test="${experience.listeSystemeExploitation.size() > 0}"> <!-- *********PERMET DE NE RIEN AFFICHER SI RIEN N'EST RENSEIGNE********* -->
		        <b>Syst�me d'exploitation :</b> 
		        <c:forEach items="${experience.listeSystemeExploitation}" var="outil" varStatus="loop">
				   <c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
				   <c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
		        </c:forEach>
		        </c:if>
		        </p>
		
		        </div>
     			</div>
		
		        </div>
        	</c:forEach>
  		 </c:if>
	
	  </div>
	</div>
	
	<div class="panel-heading" data-toggle='collapse' href='#competence'>
    <h2 class="first-title"><strong >COMPETENCES</strong>
    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>
		
	</h2>
    </div>	
	
	<div id="competence" class='panel-collapse collapse'>
      <div class="panel-body">

		<c:if test="${empty listeCompetence}">N/A</c:if>
		<c:if test="${!empty listeCompetence}">
			<c:forEach items="${listeCompetence}" var="competence">
				  <form:checkbox path="listeCompetenceId" value="${competence.id}" label="${competence.intitule}" /> - niveau: ${competence.niveau.intitule2} <br>
			</c:forEach>
		</c:if>
		
		</div>
		</div>
	
	<div class="panel-heading" data-toggle='collapse' href='#certification'>
    <h2 class="first-title"><strong >CERTIFICATIONS</strong>
    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>
			
	</h2>
    </div>	
	
	<div id="certification" class='panel-collapse collapse'>
      <div class="panel-body">

			<c:if test="${empty listeCertification}">N/A</c:if>
			<c:if test="${!empty listeCertification}">
				<c:forEach items="${listeCertification}" var="certification">
					  <form:checkbox path="listeCertificationId" value="${certification.id}" label="${certification.intitule}" /> - date: ${certification.date} - date: ${certification.score} <br>
				</c:forEach>
			</c:if>
			
			</div>
			</div>
			
	<div class="panel-heading" data-toggle='collapse' href='#resume'>
    <h2 class="first-title"><strong >RESUME TECHNIQUES ET FONCTIONNELS</strong>
    <a href="#"><div class="fa fa-chevron-right rotate" style="color:maroon"></div></a>		
				
	</h2>
    </div>	
	
	<div id="resume" class='panel-collapse collapse'>
          
			<c:if test="${empty resumeTechniqueEtFonctionnel}">N/A</c:if>
			<c:if test="${!empty resumeTechniqueEtFonctionnel}">
			
			<div class="panel-heading" data-toggle='collapse' href="#principalCompetences">
                <h4 class="panel-title"><strong >Comp�tences Principales</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		        <!-- <span class="if-collapsed"><i class="drop3 glyphicon glyphicon-chevron-right" style="color:red;"></i></span>
		        <span class="if-not-collapsed"><i class="drop3 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
	            </h4>
            </div>
	        <div id="principalCompetences" class='panel-collapse collapse'>
	          <div class="panel-body">			
			       <c:if test="${empty resumeTechniqueEtFonctionnel.principalCompetences}">N/A</c:if>
			       ${resumeTechniqueEtFonctionnel.principalCompetences}
			  </div>
			</div>
				
			<div class="panel-heading" data-toggle='collapse' href='#langageInformatiques'>
                <h4 class="panel-title"><strong >Langages Informatiques</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		        <!-- <span class="if-collapsed"><i class="drop4 glyphicon glyphicon-chevron-right" style="color:red;"></i></span>
		        <span class="if-not-collapsed"><i class="drop4 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
	            </h4>
            </div>
            
	        <div id="langageInformatiques" class='panel-collapse collapse'>
	          <div class="panel-body">
				
				<c:if test="${empty listeLangage}">N/A</c:if>
					<c:if test="${!empty listeLangage}">
					  <c:forEach items="${listeLangage}" var="langage">
						<form:checkbox path="listeLangageId" value="${langage.id}" label="${langage.nom}" /> <br>
					  </c:forEach>
					</c:if>
			  </div>
			</div>		
							
			<div class="panel-heading" data-toggle='collapse' href='#logiciels'>
                <h4 class="panel-title"><strong >Logiciels</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		        <!-- <span class="if-collapsed"><i class="drop5 glyphicon glyphicon-chevron-right" style="color:red;"></i></span>
		        <span class="if-not-collapsed"><i class="drop5 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
	            </h4>
            </div>
            
	        <div id="logiciels" class='panel-collapse collapse'>
	          <div class="panel-body">
				
					<c:if test="${empty listeLogiciel}">N/A</c:if>
					  <c:if test="${!empty listeLogiciel}">
						<c:forEach items="${listeLogiciel}" var="logiciel">
						  <form:checkbox path="listeLogicielId" value="${logiciel.id}" label="${logiciel.nom}" /> <br>
						</c:forEach>
					  </c:if>
			  </div>
			</div>		
							
			<div class="panel-heading" data-toggle='collapse' href='#baseDeDonnees'>
                <h4 class="panel-title"><strong >Base de Donn�es</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		        <!-- <span class="if-collapsed"><i class="drop6 glyphicon glyphicon-chevron-right" style="color:red;"></i></span>
		        <span class="if-not-collapsed"><i class="drop6 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
	            </h4>
            </div>
            
	        <div id="baseDeDonnees" class='panel-collapse collapse'>
	          <div class="panel-body">
			
					<c:if test="${empty listeBaseDeDonnee}">N/A</c:if>
					  <c:if test="${!empty listeBaseDeDonnee}">
						<c:forEach items="${listeBaseDeDonnee}" var="baseDeDonnee">
						  <form:checkbox path="listeBaseDeDonneeId" value="${baseDeDonnee.id}" label="${baseDeDonnee.nom}" /> <br>
						</c:forEach>
					  </c:if>
			  </div>
			</div>		
							
			<div class="panel-heading" data-toggle='collapse' href='#systemeExploitations'>
                <h4 class="panel-title"><strong >Syst�mes D'exploitations</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		        <!-- <span class="if-collapsed"><i class="drop7 glyphicon glyphicon-chevron-right" style="color:red;"></i></span>
		        <span class="if-not-collapsed"><i class="drop7 glyphicon glyphicon-chevron-up" style="color:red"></i></span> -->		
	            </h4>
            </div>
            
	        <div id="systemeExploitations" class='panel-collapse collapse'>
	          <div class="panel-body">
			
					<c:if test="${empty listeSystemeExploitation}">N/A</c:if>
					  <c:if test="${!empty listeSystemeExploitation}">
						<c:forEach items="${listeSystemeExploitation}" var="sys">
						  <form:checkbox path="listeSystemeExploitationId" value="${sys.id}" label="${sys.nom}" /> <br>
						</c:forEach>
					  </c:if>
				 </div>
			</div>  
					  
			<div class="panel-heading" data-toggle='collapse' href='#methodes'>
                <h4 class="panel-title"><strong >M�thodes</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
		        </h4>
            </div>
            
	        <div id="methodes" class='panel-collapse collapse'>
	        <div class="panel-body">
	          <c:if test="${empty resumeTechniqueEtFonctionnel.methodes}">N/A</c:if>
			  ${resumeTechniqueEtFonctionnel.methodes}				
			</div>
			</div>  
				
			<div class="panel-heading" data-toggle='collapse' href='#industries'>
			
                <h4 class="panel-title"><strong >Industries</strong>
                <a href="#"><div class="fa fa-chevron-right rotate" style="color:red"></div></a>
	            </h4>
            </div>
            
	        <div id="industries" class='panel-collapse collapse'>
	          <div class="panel-body">	          
			  ${resumeTechniqueEtFonctionnel.industries}				
			  </div>
			</div>
				
			</c:if>			
			</div>			
			
	<br><br>		
 	<input type="submit" id="play_button" class="btn btn-primary" value="Valider" align="center" />&nbsp;&nbsp;&nbsp;
	</form:form>
	</div>
  </div>
 
</div>

<!-- JAVASCRIPT POUR OUVERTURE PARENTS + EXPERIENCE.DETAILS + EXPERIENCE.OUTILS + TOUS LES SOUS CHAPITRES DANS RESUME TECH. ET FONCT.-->

<script>
    $(".rotate").click(function(){
	 $(this).toggleClass("down")  ; 
	})
</script>

<!-- JAVASCRIPT POUR OUVERTURE DETAILS -->

<!-- <script language="javascript" type="text/javascript">
function persoToggle("details_0"){
	$("#details_0").toggle();
}
</script> -->


<!-- <script>                              ******************PAS UTILISE*********************

	$('#details_${loop.index}').on('shown.bs.collapse', function() {
    $(".drop1").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#details_${loop.index}').on('hidden.bs.collapse', function() {
    $(".drop1").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
    
}
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE OUTILS -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#outils').on('shown.bs.collapse', function() {
    $(".drop2").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#outils').on('hidden.bs.collapse', function() {
    $(".drop2").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE COMPETENCES PRINCIPALES -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#principalCompetences').on('shown.bs.collapse', function() {
    $(".drop3").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#principalCompetences').on('hidden.bs.collapse', function() {
    $(".drop3").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE LANGAGES INFORMATIQUES -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#langageInformatiques').on('shown.bs.collapse', function() {
    $(".drop4").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#langageInformatiques').on('hidden.bs.collapse', function() {
    $(".drop4").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE LOGICIELS -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#logiciels').on('shown.bs.collapse', function() {
    $(".drop5").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#logiciels').on('hidden.bs.collapse', function() {
    $(".drop5").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE BASES DE DONNEES -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#baseDeDonnees').on('shown.bs.collapse', function() {
    $(".drop6").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#baseDeDonnees').on('hidden.bs.collapse', function() {
    $(".drop6").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE SYSTEMES D'EXPLOITATIONS -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#systemeExploitations').on('shown.bs.collapse', function() {
    $(".drop7").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#systemeExploitations').on('hidden.bs.collapse', function() {
    $(".drop7").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- JAVASCRIPT POUR OUVERTURE METHODES -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#methodes').on('shown.bs.collapse', function() {
    $(".drop8").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#methodes').on('hidden.bs.collapse', function() {
    $(".drop8").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
    });
</script> -->

<!-- <!-- JAVASCRIPT POUR OUVERTURE INDUSTRIES --> -->

<!-- <script>                              ******************PAS UTILISE*********************
    $('#industries').on('shown.bs.collapse', function() {
    $(".drop9").addClass('glyphicon-chevron-up').removeClass('glyphicon-chevron-right');
    });

    $('#industries').on('hidden.bs.collapse', function() {
    $(".drop9").addClass('glyphicon-chevron-right').removeClass('glyphicon-chevron-up');
  });
</script> -->

</body>
</html>