<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Profil supprim�</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/headerStyle.css" />"	rel="stylesheet">
</head>
<body>
	<div class="main">
		<div class="container">
			<div class="supprime">
				<h3>Votre profil est supprim�...</h3>
				</br>
				<a class="btn btn-danger" href="<c:url value="/welcome/goToAccueil" />" >Acceuil</a>
			</div>
		</div>
	</div>
</body>
</html>