<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>Supprimer profil</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/sideNavStyle.css" />" rel="stylesheet">
<link href="<c:url value="/static/css/styles.css" />" rel="stylesheet">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<body>
		
<div class="container">	
	
	<c:import url="header.jsp" />
	<div class="row">
		<div class="col-lg-3">
	       <!-- Sidenav partial -->
		    <c:import url="partials/utilisateur-sidenav.jsp" />
		</div>
		
		<div class="col-lg-9 col-lg-push-0">
		   <div align="center">
			   <h1>Supprimer le profil "<sec:authentication property="principal.credential.nomLogin"/>"  </h1>
		    </div>
		    <center>
		    <c:if test="${message == 'erreurIdentification'}">
		          <div class="error">Nous avons detect� une erreur.<br>Votre mot de passe est incorrect.</div>               
	         </c:if>
	        </center>
	      
		<div class="form-group" bgcolor="#E6E6FA">
			<form method="POST" action="supprimerProfil">
				<input type="hidden" name="${_csrf.parameterName}"value="${_csrf.token}" />
				<div align="center">
					<TABLE class="tableinscription" BORDER=0>
						<tr>
							<td nowrap>
								<label>
								<span class="required">*</span> Mot de passe
								</label>
							</td>
							<td>
							<td><input type="password" class="form-control"  name="confirmationMotDePasse" required></td>
						</tr>
					</TABLE>
				</div>
		   </div>
		   <div align="center">
			    <input type="submit" class="btn btn-primary" value="Supprimer mon profil"
				align="center" />
		    </div>
		   </form>
	</div>
	</div>
</div>
	
</body>
</html>