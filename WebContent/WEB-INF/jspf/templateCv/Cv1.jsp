<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<!DOCTYPE html>

<html lang="fr">

<head>

<title>TEMPLATE CV1</title>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="The Curriculum Vitae of Joe Bloggs." />
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link href="<c:url value="/static/css/styleCv1.css" />" rel="stylesheet" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.5.0/css/all.css'
	integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU'
	crossorigin='anonymous'>

<!-- Scripts for generating the PDF  -->

<script src="../../static/js/html2canvas.min.js"></script>
<script src="../../static/js/html2pdf.bundle.min.js"></script>
<script src="../../static/js/generatePdf.js"></script>

<style>
#content {
	overflow: visible; !important
}
</style>

</head>

<body class="corpsCV">

	<table>
		<p>
			<sec:authorize access="hasAnyRole('ROLE_UTILISATEUR')">
				<a class="btn btn-primary"
					href="<c:url value="/visiteur/goToChoixTemplate" />"> Retour à
					la page template </a>
			</sec:authorize>
			<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
				<a class="btn btn-primary"
					href="<c:url value="/admin/goToChoixTemplate" />"> Retour à la
					page template </a>
			</sec:authorize>
			<sec:authorize access="hasAnyRole('ROLE_MANAGER')">
				<a class="btn btn-primary"
					href="<c:url value="/manager/goToChoixTemplate" />"> Retour à la
					page template </a>
			</sec:authorize>
		</p>
		<!--  GENERATE A PDF FILE -->

		<p>
			<a id="gen-pdfcv1" class="btn btn-primary pull-left">PDF</a>
		</p>

	</table>
	<!--  everything in the #content div will be printed to PDF -->
	<div id="content" class="cv" class="instaFade" style="margin: 0 auto">
		<div class="element">
			<div class="mainDetails">

				<!-- 	**************************HEADER**************************	 -->

				<div class="name">
					<h1>${utilisateur.prenom} ${utilisateur.nom}</h1>
				</div>

				<div class="contactDetails">
					<ul>
						<p>
						<li class='fas fa-home' style='font-size: 20px'> ${utilisateur.adresse.codePostal} ${utilisateur.adresse.ville}</li>
						</p>
						<p>
						<li class='fas fa-globe' style='font-size: 20px'> ${utilisateur.adresse.nomPays}</li>
						</p>
						<p>
						<li class="fas fa-mobile-alt" style='font-size: 20px'> ${utilisateur.telephone}</li>
						</p>
						<p>
						<li class='fas fa-at' style='font-size: 20px'> ${utilisateur.credential.email}</li>
						</p>
					</ul>
				</div>
				<div class="clear"></div>
			</div>

			<!-- 	**************************TITRES**************************	 -->

			<div class="section">
			<c:if test="${!empty affichage.listeTitreId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
				<div class="sectionTitle">
					<h1>Titres</h1>
				</div>

				<div class="sectionContent">

					<c:forEach items="${listeTitre}" var="titre">
						<c:forEach items="${affichage.listeTitreId}" var="titreId">
							<c:if test="${titreId eq titre.id}">
								<article>
									<h2>
										<p>
											<strong>${titre.titre}</strong>
										</p>
									</h2>
								</article>
							</c:if>
						</c:forEach>
					</c:forEach>

				</div>
				<div class="clear"></div>
				</c:if>
			</div>
		</div>


		<!-- 	**************************EXPERIENCES**************************	 -->

        <c:if test="${!empty affichage.listeExperienceId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->

		<div class="sectionTitlexp" id="sectionelt1">
			<h1>Experiences professionnelles</h1>
		</div>

		<c:forEach items="${listeExperience}" var="experience">
			<c:forEach items="${affichage.listeExperienceId}" var="experienceId">
				<c:if test="${experienceId eq experience.id}">
					<div class="experience">
						<h2>
							<strong>${experience.metier.nom} - ${experience.projet}
								- ${experience.entreprise.nom} </strong>
						</h2>
						<p class="subDetails">
							<fmt:formatDate value="${experience.dateDebut}" pattern="MM/yyyy" />

							<fmt:formatDate value="${experience.dateFin}" pattern="MM/yyyy" />
						</p>
						<p>${experience.descriptifCV}</p>
					</div>
				</c:if>
			</c:forEach>
		</c:forEach>
		<br>
		<div class="clear"></div>
        </c:if>

		<!--  SAUT DE PAGE -->
		<!-- <div class="html2pdf__page-break"></div> -->

		<!-- 	**************************COMPETENCES**************************	 -->
		<br>
		<div class=eltscv>
			<div class="sectionelt" id="sectionelt2">
			<c:if test="${!empty affichage.listeCompetenceId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
				<div class="sectionTitle">
					<b><h1>Compétences</h1> <b>
				</div>
				<div class="sectionContent">
					<ul style="padding-left: 0">
						<table>
							<c:forEach items="${listeCompetence}" var="competence">
								<c:forEach items="${affichage.listeCompetenceId}"
									var="competenceId">
									<c:if test="${competenceId eq competence.id}">

										<tr>
											<td>${competence.intitule}&nbsp&nbsp&nbsp&nbsp</td>
											<td><c:if test="${competence.niveau.valeur == '1'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
												</c:if> <c:if test="${competence.niveau.valeur == '2'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
												</c:if> <c:if test="${competence.niveau.valeur == '3'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
												</c:if> <c:if test="${competence.niveau.valeur == '4'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
												</c:if> <c:if test="${competence.niveau.valeur == '5'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
												</c:if></td>
										</tr>

									</c:if>
								</c:forEach>

							</c:forEach>
						</table>
					</ul>
				</div>
				<div class="clear"></div>
				</c:if>
			</div>

		</div>

		<!-- 	**************************LANGUES**************************	 -->
		<div class=eltscv>
			<div class="sectionelt" id="sectionelt3">
			<c:if test="${!empty affichage.listeLangueId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
				<div class="sectionTitle">
					<h1>Langues</h1>
				</div>
				<div class="sectionContent">
					<ul style="padding-left: 0">

						<c:forEach items="${listeLangue}" var="langue">
							<c:forEach items="${affichage.listeLangueId}" var="langueId">
								<c:if test="${langueId eq langue.id}">
									<td>${langue.nomLangue.nom}</td>
									<p>
										<c:if test="${langue.niveau.intitule == 'Notions'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>

										<c:if test="${langue.niveau.intitule == 'Scolaire'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>

										<c:if test="${langue.niveau.intitule == 'Professionnel'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>

										<c:if test="${langue.niveau.intitule == 'Bilingue'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
										</c:if>

										<c:if test="${langue.niveau.intitule == 'Langue Maternelle'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
										</c:if>
									</p>
								</c:if>
							</c:forEach>
						</c:forEach>
					</ul>
				</div>


				<div class="clear"></div>
				</c:if>
			</div>
		</div>
		<!-- 	**************************FORMATIONS**************************	 -->
		<div class=eltscv>
			<div class="sectionelt" id="sectionelt4">
			<c:if test="${!empty affichage.listeFormationId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
				<div class="sectionTitle">
					<h1>Formations</h1>
				</div>

				<div class="sectionContent">

					<c:forEach items="${listeFormation}" var="formation">
						<c:forEach items="${affichage.listeFormationId}" var="formationId">
							<c:if test="${formationId eq formation.id}">
								<article>
									<h2
										style="font-family: 'Rokkitt', Helvetica, Arial, sans-serif; font-size: 1.5em; margin-bottom: -2px; margin-top: 0;">
										<strong style="font-size: 24px;">${formation.intitule}</strong>
									</h2>
									<p class="subDetails">Année : ${formation.anneeObtention}</p>
								</article>
							</c:if>
						</c:forEach>
					</c:forEach>
					<br>

				</div>
				<div class="clear"></div>
				</c:if>
			</div>
		</div>

		<!-- 	**************************CERTIFICATIONS**************************	 -->

		
			<div class=eltscv>
				<div class="sectionelt" id="sectionelt5">
				<c:if test="${!empty affichage.listeCertificationId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
					<div class="sectionTitle">
						<h1>Certifications</h1>
					</div>

					<div class="sectionContent">

						<c:forEach items="${listeCertification}" var="certification">
							<c:forEach items="${affichage.listeCertificationId}"
								var="certificationId">
								<c:if test="${certificationId eq certification.id}">
									<article>
										<h2
											style="font-family: 'Rokkitt', Helvetica, Arial, sans-serif; font-size: 1.5em; margin-bottom: -2px; margin-top: 0;">
											<strong>${certification.intitule}</strong>
										</h2>
										<p class="subDetails">Date : ${certification.date} - Score
											: ${certification.score}</p>
									</article>
								</c:if>
							</c:forEach>
						</c:forEach>
						<br>

					</div>
					<div class="clear"></div>
					</c:if>
				</div>
			</div>
		

		<!--  SAUT DE PAGE -->

		<div class="html2pdf__page-break"></div>

	</div>

</body>

<!-- SCRIPT POUR SUPPRIMER LA LIGNE DE SEPARATION SUPERIEURE QUAND L'INFORMATION N'Y EST PAS -->

<script type="text/javascript">			
<c:if test="${empty affichage.listeExperienceId}">
$('#sectionelt1').hide(); /* QUAND LE BLOC EXPERIENCE EST VIDE ENLEVER LA LIGNE DE SEPARATION */
</c:if>
</script>

<script type="text/javascript">			
<c:if test="${empty affichage.listeCompetenceId}">
$('#sectionelt2').hide(); /* QUAND LE BLOC COMPETENCE EST VIDE ENLEVER LA LIGNE DE SEPARATION DU BLOC */ 
</c:if>
</script>

<script type="text/javascript">			
<c:if test="${empty affichage.listeLangueId}">
$('#sectionelt3').hide(); /* QUAND LE BLOC LANGUE EST VIDE ENLEVER LA LIGNE DE SEPARATION DU BLOC */
</c:if>
</script>

<script type="text/javascript">			
<c:if test="${empty affichage.listeFormationId}">
$('#sectionelt4').hide(); /* QUAND LE BLOC FORMATION EST VIDE ENLEVER LA LIGNE DE SEPARATION DU BLOC */
</c:if>
</script>

<script type="text/javascript">			
<c:if test="${empty affichage.listeCertificationId}">
$('#sectionelt5').hide(); /* QUAND LE BLOC CERTIFICATION EST VIDE ENLEVER LA LIGNE DE SEPARATION DU BLOC */
</c:if>
</script>
</html>
