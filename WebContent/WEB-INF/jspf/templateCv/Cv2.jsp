<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html lang="fr">
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link rel='stylesheet'
	href='https://use.fontawesome.com/releases/v5.5.0/css/all.css'
	integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU'
	crossorigin='anonymous'>
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
	rel="stylesheet">
<script
	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<title>TEMPLATE CV2</title>

<!-- CSS -->

<link rel="stylesheet"
	href="Sheldon%20COOPER%20-%20Physicien%20surdou%C3%A9%20et%20Geek%20qualifi%C3%A9_files/reset.css">

<link rel="stylesheet" media="screen and (max-width:480px)"
	href="Sheldon%20COOPER%20-%20Physicien%20surdou%C3%A9%20et%20Geek%20qualifi%C3%A9_files/mobile.css">
<link href="<c:url value="/static/css/styleCv2.css" />" rel="stylesheet">
<link rel="stylesheet" media="print"
	href="Sheldon%20COOPER%20-%20Physicien%20surdou%C3%A9%20et%20Geek%20qualifi%C3%A9_files/print.css">

<!-- SCRIPTS POUR LA GENERATION DES FICHIER PDF -->

<script src="../../static/js/html2pdf.bundle.min.js"></script>
<script src="../../static/js/generatePdf.js"></script>
<script src="../../static/js/html2canvas.min.js"></script>
</head>

<body>

	<!-- REDIRECTION EN FONCTION DES ROLES -->
	<p>
		<sec:authorize access="hasAnyRole('ROLE_UTILISATEUR')">
			<a id="backtotemplate" class="btn btn-primary"
				href="<c:url value="/visiteur/goToChoixTemplate" />"> Retour à
				la page template </a>
		</sec:authorize>

		<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
			<a class="btn btn-primary"
				href="<c:url value="/admin/goToChoixTemplate" />"> Retour à la
				page template </a>
		</sec:authorize>
		
		<sec:authorize access="hasAnyRole('ROLE_MANAGER')">
				<a class="btn btn-primary"
					href="<c:url value="/manager/goToChoixTemplate" />"> Retour à la
					page template </a>
			</sec:authorize>
	</p>

	<!--  GENERATE A PDF FILE -->

	<p>
		<a id="gen-pdfcv2" class="btn btn-primary pull-left">PDF</a>
	</p>

	<div id="content">

		<!-- HEADER -->

		<div id="corps_cv">
			<div class="head">
				<header role="banner">

					<div class="container_16">

						<hgroup>
							<h1>${utilisateur.prenom}${utilisateur.nom}</h1>

						</hgroup>

						<figure>
							<c:choose>
								<c:when test="${not empty utilisateur.photo}">
									<img id="photoId" src="../../static/uploads/<c:out value="${utilisateur.photo}" />"	alt="photo identité" />
								</c:when>
								<c:otherwise>
									<img src="../../static/images/photocv.jpg"
										alt="photo identité par défaut" />
								</c:otherwise>
							</c:choose>

						</figure>

					</div>
				</header>

				<!-- CONTACT -->
				
				<section class="contactform clearfix">
					<div class="container_16">
						<h3>Contactez-moi</h3>
						<p>
							Remplissez le formulaire ci-dessous afin de m'envoyer un message.
							Je vous répondrais dans les plus bref délai. <br> <em>Tous
								les champs sont requis.</em>
						</p>
						<form novalidate="novalidate" method="post" action="#"
							name="contact" class="grid_16">
							<p class="grid_10">
								<textarea name="message" placeholder="Votre message"
									class="required"></textarea>
							</p>
							<p class="grid_6">
								<input name="nom" placeholder="Nom - Prénom" class="required"
									type="text"> <input name="email"
									placeholder="Adresse email" class="required" type="email">
								<input name="envoi" value="Envoyer le message" class="required"
									type="submit"> <span class="messageform"></span>
							</p>
						</form>
					</div>
				</section>
			</div>
			
			<!-- CORPS -->
			
			<div class="page1">
				<section role="main" class="container_16 clearfix">
					<div class="grid_16">

						<!-- TITRES -->

						<div class="grid_8 apropos">
						<c:if test="${!empty affichage.listeTitreId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
							<h3>
								<strong>TITRES</strong>
							</h3>
							
							<c:forEach items="${listeTitre}" var="titre">
								<c:forEach items="${affichage.listeTitreId}" var="titreId">
									<c:if test="${titreId eq titre.id}">
										<article>
											<h2>
											 <p>${titre.titre}</p>
											</h2>
										</article>
									</c:if>
								</c:forEach>
							</c:forEach>
							</c:if>
						</div>

						<!-- COMPETENCES -->

						<div class="grid_8 competences">
						<c:if test="${!empty affichage.listeCompetenceId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
							<h3>
								<strong>COMPETENCES</strong>
							</h3>
							<ul class="barres">							  
								<c:forEach items="${listeCompetence}" var="competence">
									<c:forEach items="${affichage.listeCompetenceId}"
										var="competenceId">
										<c:if test="${competenceId eq competence.id}">
											<c:if test="${competence.niveau.valeur == 1}">
												<li data-skills="20">${competence.intitule}<span
													style="width: 20%;"></span></li>
											</c:if>
											<c:if test="${competence.niveau.valeur == 2}">
												<li data-skills="40">${competence.intitule}<span
													style="width: 40%;"></span></li>
											</c:if>
											<c:if test="${competence.niveau.valeur == 3}">
												<li data-skills="60">${competence.intitule}<span
													style="width: 60%;"></span></li>
											</c:if>
											<c:if test="${competence.niveau.valeur == 4}">
												<li data-skills="80">${competence.intitule}<span
													style="width: 80%;"></span></li>

											</c:if>
											<c:if test="${competence.niveau.valeur == 5}">
												<li data-skills="100">${competence.intitule}<span
													style="width: 100%;"></span></li>
											</c:if>
										</c:if>
									</c:forEach>
								</c:forEach>
							</ul>
						</c:if>
						</div>
					</div>

					<!-- FORMATIONS -->

					<div class="grid_16 formations">
					<c:if test="${!empty affichage.listeFormationId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
						<h3>
							<strong>FORMATIONS</strong>
						</h3>
						<ul>
						 <c:if test="${empty listeFormation}">N/A</c:if>
							<c:forEach items="${listeFormation}" var="formation">
								<c:forEach items="${affichage.listeFormationId}"
									var="formationId">
									<c:if test="${formationId eq formation.id}">
										<li><h4>
												<strong>${formation.intitule}</strong> - Année :
												${formation.anneeObtention}
											</h4></li>
									</c:if>
								</c:forEach>
							</c:forEach>
						</ul>
					</c:if>
					</div>

					<!--  PDF PAGE BREAK -->

					<div class="html2pdf__page-break"></div>
				</section>
			</div>
			
			<!-- EXPERIENCES -->
			
			<section role="main" class="container_16 clearfix">
				
             <c:if test="${!empty affichage.listeExperienceId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
				<div class="grid_16 experiences">
					<h3>
						<strong>EXPERIENCES</strong>
					</h3>
					<ul>
					 <c:forEach items="${listeExperience}" var="experience">
						<c:forEach items="${affichage.listeExperienceId}" var="experienceId">
							<c:if test="${experienceId eq experience.id}">
								<li>
								  <h4>
									<strong>${experience.metier.nom} - ${experience.projet} - ${experience.entreprise.nom} :</strong>
								  </h4>
								  <h6>de <fmt:formatDate value="${experience.dateDebut}" pattern="MM/yyyy" /> à
											<fmt:formatDate value="${experience.dateFin}" pattern="MM/yyyy" />
								  </h6>
							    </li>
								  <td>${experience.descriptifCV}
							</c:if>
						</c:forEach>
					</c:forEach>
				   </ul>
				</div>
             </c:if>
			</section>
			
			<!-- CERTIFICATIONS -->
			
			<section role="main" id="certif" class="container_16 clearfix">
				
             <c:if test="${!empty affichage.listeCertificationId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
				<div class="grid_16 certifications">
					<h3>
					 <strong>CERTIFICATIONS</strong>
					</h3>
					<ul>
					  <c:if test="${empty listeCertification}">N/A</c:if>
						<c:forEach items="${listeCertification}" var="certification">
							<c:forEach items="${affichage.listeCertificationId}"
								var="certificationId">
								<c:if test="${certificationId eq certification.id}">
									<li>
										<h4>
											<strong>${certification.intitule}</strong> - Date :
											${certification.date} - Score : ${certification.score}
										</h4>
									</li>
								</c:if>
							</c:forEach>
						</c:forEach>

					</ul>
				</div>
				</c:if>
			</section>
			
			<!-- LANGUES & NIVEAU -->

			<section role="main" class="container_16 clearfix">			

				<div class="grid_8 langue & Niveau">
				 <c:if test="${!empty affichage.listeLangueId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
					<h3>
						<strong>LANGUES</strong>
					</h3>
					<c:forEach items="${listeLangue}" var="langue">
						<c:forEach items="${affichage.listeLangueId}" var="langueId">
							<c:if test="${langueId eq langue.id}">
								<td><h4>
										<strong>${langue.nomLangue.nom}</td>
								</strong>
								</h4>
								</td>

								<c:if test="${langue.niveau.intitule == 'Notions'}">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star "></span>
									<span class="fa fa-star "></span>
									<span class="fa fa-star "></span>
									<span class="fa fa-star "></span>
								</c:if>

								<c:if test="${langue.niveau.intitule == 'Scolaire'}">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star "></span>
									<span class="fa fa-star "></span>
									<span class="fa fa-star "></span>
								</c:if>

								<c:if test="${langue.niveau.intitule == 'Professionnel'}">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star "></span>
									<span class="fa fa-star "></span>
								</c:if>

								<c:if test="${langue.niveau.intitule == 'Bilingue'}">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star "></span>
								</c:if>

								<c:if test="${langue.niveau.intitule == 'Langue Maternelle'}">
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
									<span class="fa fa-star checked"></span>
								</c:if>
							</c:if>
						</c:forEach>
					</c:forEach>
				 </c:if>
				</div>

				<!-- CONTACT -->
				
				<div class="grid_8 contact">
					<h3>
						<strong>CONTACT</strong>
					</h3>
					<p>Si mon profil vous intéresse, n'hésitez pas à  me contacter
						:</p>
					<ul>
						<li class='fas  fa-user'> ${utilisateur.prenom}
							${utilisateur.nom}</li>
						<p>
						<li class='fas fa-home' style='font-size: 20px'>
							${utilisateur.adresse.codePostal} ${utilisateur.adresse.ville}</li>
						</p>
						<p>
						<li class='fas fa-globe' style='font-size: 20px'>
							${utilisateur.adresse.nomPays}</li>
						</p>
						<p>
						<li class='fas fa-mobile-alt' style='font-size: 20px'>
							${utilisateur.telephone}</li>
						</p>
						<p>
						<li class='fas fa-at' style='font-size: 20px'>
							${utilisateur.credential.email}</li>
						</p>


					</ul>

				</div>
			</section>

			<!--  PDF PAGE BREAK -->
			
			<div class="html2pdf__page-break"></div>

		</div>
	</div>

	<!-- SCRIPTS JAVASCRIPTS -->

	<script
		src="Sheldon%20COOPER%20-%20Physicien%20surdou%C3%A9%20et%20Geek%20qualifi%C3%A9_files/jquery-1.js"></script>
	<script
		src="Sheldon%20COOPER%20-%20Physicien%20surdou%C3%A9%20et%20Geek%20qualifi%C3%A9_files/validate.js"></script>

	<script
		src="Sheldon%20COOPER%20-%20Physicien%20surdou%C3%A9%20et%20Geek%20qualifi%C3%A9_files/plugins.js"></script>


	<!-- <! refresh automatique au bout de 2.5 sec car dans certains cas la photo d'identité ne se charge pas sans refresh !> -->

	<script type="text/javascript">
		if (location.search.indexOf('r') < 0) {
			var hash = window.location.hash;
			var loc = window.location.href.replace(hash, '');
			loc += (loc.indexOf('?') < 0 ? '?' : '&') + 'r';
			setTimeout(function() {
				window.location.href = loc + hash;
			}, 2500);
		}
	</script>

</body>
</html>