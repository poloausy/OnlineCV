<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>

<!DOCTYPE html>

<html>
<head>

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>TEMPLATE CV3</title>
<link href="<c:url value="/static/css/styleCv3.css" />" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css'
	integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU'
	crossorigin='anonymous'>
<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Scripts for generating the PDF  -->

<script src="../../static/js/html2canvas.min.js"></script>
<script src="../../static/js/html2pdf.bundle.min.js"></script>
<script src="../../static/js/generatePdf.js"></script>

</head>
<body>

<!-- REDIRECTION EN FONCTION DES ROLES -->

<div style="float: left;">
		<ul>
		<sec:authorize access="hasAnyRole('ROLE_UTILISATEUR')">
			<a id="backtotemplate" class="btn btn-primary"
				href="<c:url value="/visiteur/goToChoixTemplate" />"> Retour à
				la page template </a>
		</sec:authorize>

		<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
			<a class="btn btn-primary"
				href="<c:url value="/admin/goToChoixTemplate" />"> Retour à la
				page template </a>
		</sec:authorize>
		
		<sec:authorize access="hasAnyRole('ROLE_MANAGER')">
				<a class="btn btn-primary"
					href="<c:url value="/manager/goToChoixTemplate" />"> Retour à la
					page template </a>
		</sec:authorize>
		</ul>
		
	<!--  GENERATE A PDF FILE -->
    <ul style="margin-top: 40px;">
	<a id="gen-pdfcv3" class="btn btn-primary pull-left">PDF</a>	
    </ul>
</div>
	
<!-- Begin Wrapper -->
<div id="content" style="margin: auto;">
<div id="wrapper">
  

    <div id="paper">
      <div class="paper-top"></div>
      <div id="paper-mid">
      
        <div class="entry">

          <img class="portrait" src="../../static/images/images.jpeg" alt="">
          
          <div class="self" style="margin-left: 100px; margin-top: 0;">
            <h2> ${utilisateur.prenom} ${utilisateur.nom} </h2>
            <ul>
              
				<li class='fas fa-home' style='font-size: 12px; padding-bottom: 5px;'> ${utilisateur.adresse.codePostal} ${utilisateur.adresse.ville}</li>
				<li class='fas fa-globe' style='font-size: 12px; padding-bottom: 5px;'> ${utilisateur.adresse.nomPays}</li>
				<li class="fas fa-mobile-alt" style='font-size: 12px; padding-bottom: 5px;'> ${utilisateur.telephone}</li>
				<li class='fas fa-at' style='font-size: 12px; padding-bottom: 5px;'> ${utilisateur.credential.email}</li>
			             
            </ul>
          </div>
          
        </div>
        
            <!-- 	**************************TITRES**************************	 -->        
        
        <div class="title" style="margin-left: 100px;">
        <ul>
    		<c:if test="${!empty affichage.listeTitreId}">
        		<c:forEach items="${listeTitre}" var="titre">
		   		<c:forEach items="${affichage.listeTitreId}" var="titreId">
	      	<c:if test="${titreId eq titre.id}">
								
				<h5>										
				<strong>${titre.titre}</strong>										
				</h5>
								
		  	</c:if>
		   		</c:forEach>
        		</c:forEach>
			</c:if>
			</ul>
 		</div>
        
            <!-- 	**************************FORMATIONS**************************	 -->        
        
        <div class="entry" style="margin-left: 50px; margin-top: 0;">
          <h4 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-graduation-cap"></i> FORMATIONS</h4>
                    
          <c:forEach items="${listeFormation}" var="formation">
						<c:forEach items="${affichage.listeFormationId}" var="formationId">
							<c:if test="${formationId eq formation.id}">
								<article style="float: left; text-align: left;">
									<h5>
										<b>${formation.anneeObtention} : ${formation.intitule}</b>
									</h5>									
								</article>
							</c:if>
						</c:forEach>
					</c:forEach>
					<br>
          
        </div>
        
            <!-- 	**************************EXPERIENCES**************************	 -->        
        
        <div class="entry" style="margin-left: 50px; margin-top: 0;">
          <h4 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-briefcase"></i> EXPERIENCES PROFESSIONNELLES</h4>
          <!-- <div class="content"> -->


        <c:if test="${!empty affichage.listeExperienceId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->

		<p><c:forEach items="${listeExperience}" var="experience">
			<c:forEach items="${affichage.listeExperienceId}" var="experienceId">
				<c:if test="${experienceId eq experience.id}">
					<div class="entry">
						<h5>
						<b>${experience.metier.nom} - ${experience.projet}	- ${experience.entreprise.nom}</b>
						</h5>
						<p>
						<fmt:formatDate value="${experience.dateDebut}" pattern="MM/yyyy" /> à 
						<fmt:formatDate value="${experience.dateFin}" pattern="MM/yyyy" />
						</p>
						<p>${experience.descriptifCV}</p>
					</div>
				</c:if>
			</c:forEach>
		</c:forEach></p>		
        </c:if>          
        </div>
        
            <!-- 	**************************COMPETENCES**************************	 -->        
        
        <div class="entry" style="margin-left: 50px;">
          <h4 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-thumbs-up"></i> COMPETENCES</h4>
          <!-- <div class="content"> -->
            
            <ul class="skills">
              <table>
							<c:forEach items="${listeCompetence}" var="competence">
								<c:forEach items="${affichage.listeCompetenceId}"
									var="competenceId">
									<c:if test="${competenceId eq competence.id}">

										<tr>
											<td><b>${competence.intitule}</b></td>
											<td><c:if test="${competence.niveau.valeur == '1'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
												</c:if> 
												<c:if test="${competence.niveau.valeur == '2'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
												</c:if> 
												<c:if test="${competence.niveau.valeur == '3'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
												</c:if> 
												<c:if test="${competence.niveau.valeur == '4'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
												</c:if> 
												<c:if test="${competence.niveau.valeur == '5'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
												</c:if></td>
										</tr>

									</c:if>
								</c:forEach>

							</c:forEach>
						</table>
            </ul>
          </div>
          
		<!-- 	**************************LANGUES**************************	 -->
		          
         <div class="entry" style="margin-left: 50px; margin-top: 10px;"> 
          <!-- <div class="content"> -->
           <h4 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-microphone"></i> LANGUES</h4>
            <ul class="skills">
            <table>
              <c:forEach items="${listeLangue}" var="langue">
							<c:forEach items="${affichage.listeLangueId}" var="langueId">
								<c:if test="${langueId eq langue.id}">
								<tr>
									<td><b>${langue.nomLangue.nom}</b></td>
									
										<td><c:if test="${langue.niveau.intitule == 'Notions'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>
										<c:if test="${langue.niveau.intitule == 'Scolaire'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>
										<c:if test="${langue.niveau.intitule == 'Professionnel'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>
										<c:if test="${langue.niveau.intitule == 'Bilingue'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
										</c:if>
										<c:if test="${langue.niveau.intitule == 'Langue Maternelle'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
										</c:if></td>									
									</tr>
								</c:if>
							</c:forEach>
						</c:forEach>
			</table>			
            </ul>
          </div>
        
        
		<!-- 	**************************CERTIFICATIONS**************************	 -->        
    
    <div class="entry" style="margin-left: 50px; margin-top: 0;">    
      <!-- <div class="content"> -->
        <c:if test="${!empty affichage.listeCertificationId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
          <h4 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-graduation-cap"></i> CERTIFICATIONS</h4>
           <ul>
             <c:forEach items="${listeCertification}" var="certification">
			   <c:forEach items="${affichage.listeCertificationId}"
								var="certificationId">
								
								<c:if test="${certificationId eq certification.id}">
									<article style="text-align: center;">
										<p style="margin-bottom: -2px; margin-top: 0;">
											<b>${certification.intitule}</b>
										</p>
										<p style="font-size: 12px;">Date : ${certification.date} - Score
											: ${certification.score}</p>
									</article>
								</c:if>
								
			  </c:forEach>
			</c:forEach>
      </ul>
     </c:if>
        
    </div>
    <div class="clear"></div>
    </div>
   
  </div>
  
</div>
</div>
</body>
</html>