<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html>

<html>

<title>TEMPLATE CV4</title>
    
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet"	href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="<c:url value="/static/css/styleCv4.css" />" rel="stylesheet" />
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
<link href="https://fonts.googleapis.com/css?family=Share+Tech|Share+Tech+Mono" rel="stylesheet" />
<link rel="stylesheet"	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<!-- Scripts for generating the PDF  -->

<script src="../../static/js/html2canvas.min.js"></script>
<script src="../../static/js/html2pdf.bundle.min.js"></script>
<script src="../../static/js/generatePdf.js"></script>

</head>

<body>

<table>
		<p>
			<sec:authorize access="hasAnyRole('ROLE_UTILISATEUR')">
				<a class="btn btn-primary" href="<c:url value="/visiteur/goToChoixTemplate" />"> Retour à la page template </a>
			</sec:authorize>
			<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
				<a class="btn btn-primary" href="<c:url value="/admin/goToChoixTemplate" />"> Retour à la page template </a>
			</sec:authorize>
			<sec:authorize access="hasAnyRole('ROLE_MANAGER')">
				<a class="btn btn-primary"
					href="<c:url value="/manager/goToChoixTemplate" />"> Retour à la
					page template </a>
			</sec:authorize>
		</p>
		<!--  GENERATE A PDF FILE -->
		<p>
			<a id="gen-pdfcv4" class="btn btn-primary pull-left">PDF</a>
		</p>

</table>

<div class="content" id="content">
<div class="resume">

 <div class="base">
  
    <div class="profile">
      <div class="photo">
        <c:choose>
			<c:when test="${not empty utilisateur.photo}">
				<img id="photoId" src="../../static/uploads/<c:out value="${utilisateur.photo}" />"	alt="photo identité" />
			</c:when>
			
			<c:otherwise>
				<img src="../../static/images/photocv.jpg" alt="photo identité par défaut" />
			</c:otherwise>
		</c:choose>
      </div>
      
      <div>
        <h1>${utilisateur.prenom} ${utilisateur.nom}</h1>        
      </div>
            
    </div>
    
    <div class="title">
    <c:if test="${!empty affichage.listeTitreId}">
    <%-- <c:if test="${empty listeTitre}">N/A</c:if> --%>
        <c:forEach items="${listeTitre}" var="titre">
		   <c:forEach items="${affichage.listeTitreId}" var="titreId">
		      <c:if test="${titreId eq titre.id}">
								
				<h2>										
				<strong>${titre.titre}</strong>										
				</h2>
								
			  </c:if>
		   </c:forEach>
        </c:forEach>
	</c:if>
 </div>&nbsp&nbsp
    
    <div class="contact" style="padding: 0,0,10px,0 ! important;"><br><br><br><br><br>
      <h2 style="text-decoration: underline; font-weight: bold;">CONTACT</h2>&nbsp
      <p><li class="fas fa-mobile-alt" style='font-size: 25px'> ${utilisateur.telephone}</li></p>
      <p><li class='fas fa-home' style='font-size: 25px'> ${utilisateur.adresse.codePostal} ${utilisateur.adresse.ville}</li></p>
      <p><li class='fas fa-globe' style='font-size: 25px'> ${utilisateur.adresse.nomPays}</li></p>
      <p><li class='fas fa-at' style='font-size: 25px'> ${utilisateur.credential.email}</li></p>
    </div>
       
  </div>
  
  <div class="func" style="margin-bottom: 0;">
  
    <div class="work">
    <c:if test="${!empty affichage.listeExperienceId}">
      <h2 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-briefcase"></i> EXPERIENCES</h2>
      <ul style="text-align: left;">
      <c:if test="${empty listeExperience}">N/A</c:if>
        <c:forEach items="${listeExperience}" var="experience">
			<c:forEach items="${affichage.listeExperienceId}" var="experienceId">
				<c:if test="${experienceId eq experience.id}">
					<div>
						<p style="font-size: 24px;">
						<b>${experience.metier.nom}</b> - ${experience.projet} - ${experience.entreprise.nom}>
						</p>
						<p  style="font-size: 16px;">
							<fmt:formatDate value="${experience.dateDebut}" pattern="MM/yyyy" /> à 

							<fmt:formatDate value="${experience.dateFin}" pattern="MM/yyyy" />
						</p>
						<p style="font-size: 16px;">${experience.descriptifCV}</p>
					</div>
				</c:if>
			</c:forEach>
		</c:forEach>
      </ul>
      </c:if>
    </div>
    
    <div class="edu">
    <c:if test="${!empty affichage.listeFormationId}">
      <h2 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-graduation-cap"></i> FORMATIONS</h2>
      <ul>
      <c:if test="${empty listeFormation}">N/A</c:if>
        <c:forEach items="${listeFormation}" var="formation">
				<c:forEach items="${affichage.listeFormationId}" var="formationId">
					<c:if test="${formationId eq formation.id}"> 
					 <p style="font-size: 24px;"><b>${formation.anneeObtention}</b> : ${formation.intitule}</p>												
					</c:if>
				</c:forEach>
		</c:forEach>
      </ul>
      </c:if>
    </div>
    
    <div class="skills-prog">
    <c:if test="${!empty affichage.listeCompetenceId}">
      <h2 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-thumbs-up"></i> COMPETENCES</h2>
       <ul style="text-align: center; style=font-size: 20px;">              
        <c:forEach items="${listeCompetence}" var="competence">
								<c:forEach items="${affichage.listeCompetenceId}" var="competenceId">
									<c:if test="${competenceId eq competence.id}">

										<p style="font-size: 16px;">
											${competence.intitule}&nbsp
											<c:if test="${competence.niveau.valeur == '1'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
											</c:if>		
																						
											<c:if test="${competence.niveau.valeur == '2'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
											</c:if>		
																								
													
											<c:if test="${competence.niveau.valeur == '3'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
													<span class="fa fa-star "></span>
											</c:if>		
																								
													
											<c:if test="${competence.niveau.valeur == '4'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star "></span>
											</c:if> 		
																								
													
											<c:if test="${competence.niveau.valeur == '5'}">
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
													<span class="fa fa-star checked"></span>
											</c:if>		
																								
										</p>			
									</c:if>
									
								</c:forEach>
							</c:forEach>
							      
     </ul>
     </c:if> 
    </div>    
    
    <div class="langues">
    <c:if test="${!empty affichage.listeLangueId}">
      <h2 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-microphone"></i> LANGUES</h2>      
       <ul style="text-align: center;">
          <c:if test="${empty listeLangue}">N/A</c:if>
						<c:forEach items="${listeLangue}" var="langue">
							<c:forEach items="${affichage.listeLangueId}" var="langueId">
								<c:if test="${langueId eq langue.id}">
								
								 <p style="font-size: 16px;">${langue.nomLangue.nom}&nbsp
									
										<c:if test="${langue.niveau.intitule == 'Notions'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>
                                    
										<c:if test="${langue.niveau.intitule == 'Scolaire'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>
									
										<c:if test="${langue.niveau.intitule == 'Professionnel'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
											<span class="fa fa-star "></span>
										</c:if>

										<c:if test="${langue.niveau.intitule == 'Bilingue'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star "></span>
										</c:if>

										<c:if test="${langue.niveau.intitule == 'Langue Maternelle'}">
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
											<span class="fa fa-star checked"></span>
										</c:if>
									</p>
								</c:if>
							</c:forEach>
						</c:forEach>
       </ul>
       </c:if>      
    </div>
    
    <div class="edu">
     <c:if test="${!empty affichage.listeCertificationId}"> <!-- condition d'affichage, si il n'y a pas de certification alors le bloc n'apparait pas -->
      <h2 style="text-decoration: underline;font-weight: bold;"><i class="fa fa-graduation-cap"></i> CERTIFICATIONS</h2>
      <ul>
        <c:forEach items="${listeCertification}" var="certification">
							<c:forEach items="${affichage.listeCertificationId}"
								var="certificationId">
								
								<c:if test="${certificationId eq certification.id}">
									<article>
										<p style="font-family: 'Rokkitt', Helvetica, Arial, sans-serif; font-size: 18px; margin-bottom: -2px; margin-top: 0;">
											${certification.intitule}
										</p>
										<p style="font-size: 14px;">Date : ${certification.date} - Score
											: ${certification.score}</p>
									</article>
								</c:if>
								
							</c:forEach>
						</c:forEach>
      </ul>
     </c:if>
    </div>        
  </div>  
</div>
</div>    
</body>
</html>