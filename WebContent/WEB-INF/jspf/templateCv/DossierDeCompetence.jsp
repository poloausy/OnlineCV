<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"	pageEncoding="UTF-8"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html lang="fr">

<head>

<title>DOSSIER DE COMPETENCE</title>

<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no, height=device-height, viewport-fit=cover">
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="UTF-8"/>
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

<link href="<c:url value="/static/css/stylePdf.css" />" rel="stylesheet" />
<link href="<c:url value="/static/css/dossierDeCompetence.css"/>" rel="stylesheet" />
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href='http://fonts.googleapis.com/css?family=Rokkitt:400,700|Lato:400,300' rel='stylesheet' type='text/css'/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<!-- SCRIPTS FOR GENERATING THE PDF -->

<script	src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="../../static/js/html2canvas.min.js"></script>
<script src="../../static/js/html2pdf.bundle.min.js"></script>
<script src="../../static/js/generatePdf.js"></script>

</head>

<body>

	<!--  BOUTONS RETOUR VERS LES TEMPLATES CV -->
	<p>
		<sec:authorize access="hasAnyRole('ROLE_UTILISATEUR')">
			<a class="btn btn-primary"
				href="<c:url value="/visiteur/goToChoixTemplate" />"> Retour à
				la page template </a>
		</sec:authorize>
		<sec:authorize access="hasAnyRole('ROLE_ADMIN')">
			<a class="btn btn-primary"
				href="<c:url value="/admin/goToChoixTemplate" />"> Retour à la
				page template </a>
		</sec:authorize>
		<sec:authorize access="hasAnyRole('ROLE_MANAGER')">
				<a class="btn btn-primary"
					href="<c:url value="/manager/goToChoixTemplate" />"> Retour à la
					page template </a>
			</sec:authorize>
	</p>

	<!--  GENERATION D'UN FICHIER PDF -->

	<a id="gen-pdfdc" class="btn btn-primary">PDF</a>

	<!-- 	************************** PAGE 1 : PAGE D'INTRODUCTION DOSSIER DE COMPETENCE **************************	 -->

	<!-- 	************************** HEADER **************************	 -->

	<div id="content">
		<div class="dossiercompetence">

			<!-- 	************************** IMAGE COTE DROIT **************************	 -->

			<div id="image-holder">
				<img src="../../static/images/dossier de competence bande droite.png" title="bande verticale" />
			</div>

			<!-- 	**************************LOGO AUSY**************************	 -->

			<div>
				<img src="../../static/images/newLogoAusy.png" title="logo Ausy" width=30% />
			</div>

			<div>
				<h2>
					<strong>${utilisateur.prenom} ${utilisateur.nom}</strong>
				</h2>
			</div>

			<!-- 	************************** CORPS DOSSIER 1 PAGE D'INTRODUCTION DOSSIER DE COMPETENCE **************************	 -->

		<div class="mainArea" align="center">

			<div class="sectionTitle">
				<h1>
					<b>DOSSIER DE COMPETENCE<b>
				</h1>
			</div>

		</div>

			<!-- 	************************** CORPS DOSSIER 2 PAGE D'INTRODUCTION DOSSIER DE COMPETENCE **************************	 -->


		<div class="sectionContent">

				<c:forEach items="${listeTitre}" var="titre">
					<c:forEach items="${affichage.listeTitreId}" var="titreId">
						<c:if test="${titreId eq titre.id}">
							<h2>
								<p>
									<strong>${titre.titre}</strong>
								</p>
							</h2>
						</c:if>
					</c:forEach>
				</c:forEach>

		</div>

			<!-- 	************************** FOOTER **************************	 -->

		<div class="footer">Controlled Distribution</div>
		</div>

		<!-- 	************************** PAGE2 : RESUME DES COMPETENCES TECHNIQUES ET FONCTIONNELLES **************************	 -->


		<!-- 	**************************HEADER**************************	 -->

		<div class="dossiercompetence">


		<div class="boxed" align="center">
			<strong><h3 >
			   <b>COMPETENCES TECHNIQUES ET FONCTIONNELLES<b>
			</h3></strong>
		</div>

			<!-- 	************************** COMPETENCES PRINCIPALES **************************	 -->

		<div class="subDetails" style = "padding: 10px;">

			<h4>
				<b>COMPETENCES PRINCIPALES: </b>
			</h4>
				${resumeTechniqueEtFonctionnel.principalCompetences}
			<c:if test="${empty resumeTechniqueEtFonctionnel.principalCompetences}">N/A</c:if>	
		</div>



			<!-- 	**************************LANGAGES**************************	 -->

        <section>

		<div class="subDetails" style = "padding: 10px;">

			<h4>
				<b>LANGAGES: </b>							            
			</h4>
				<c:forEach items="${listeLangage}" var="outil">
					<c:forEach items="${affichage.listeLangageId}" var="langageId"
						varStatus="loop">
						<c:if test="${langageId eq outil.id}">						
						<c:if test="${loop.index eq 0}"><c:out value="${outil.nom}" /></c:if>
						<c:if test="${loop.index ne 0}">, <c:out value="${outil.nom}" /></c:if>
						</c:if>
					</c:forEach>
					
				</c:forEach>
                <c:if test="${empty listeLangage}">N/A</c:if>
		</div>
		</section>

			<!-- 	************************** LOGICIELS **************************	 -->

		<section>

		<div class="subDetails" style = "padding: 10px;">

			<h4>
				<b>LOGICIELS: </b>
			</h4>

				<c:forEach items="${listeLogiciel}" var="outil">
					<c:forEach items="${affichage.listeLogicielId}" var="logicielId"
						varStatus="loop">
						<c:if test="${logicielId eq outil.id}">
							<c:if test="${loop.index eq 0}">
								<c:out value="${outil.nom}" />
							</c:if>
							<c:if test="${loop.index ne 0}">, <c:out
									value="${outil.nom}" />
							</c:if>
						</c:if>
					</c:forEach>
				</c:forEach>
                <c:if test="${empty listeLogiciel}">N/A</c:if>
		</div>

		<div class="clear"></div>
		</section>

			<!-- 	************************** SYSTEMES D'EXPLOITATION **************************	 -->

		<section>
		<div class="subDetails" style = "padding: 10px;">

			<h4>
				<b>SYSTEMES D'EXPLOITATIONS: </b>
			</h4>
				<c:forEach items="${listeSystemeExploitation}" var="outil">
					<c:forEach items="${affichage.listeSystemeExploitationId}"
						var="systemeExploitationId" varStatus="loop">
						<c:if test="${systemeExploitationId eq outil.id}">
							<c:if test="${loop.index eq 0}">
								<c:out value="${outil.nom}" />
							</c:if>
							<c:if test="${loop.index ne 0}">, <c:out
									value="${outil.nom}" />
							</c:if>
						</c:if>
					</c:forEach>
				</c:forEach>
                <c:if test="${empty listeSystemeExploitation}">N/A</c:if>
		</div>

		<div class="clear"></div>
		</section>


			<!-- 	************************** BASES DE DONNEES **************************	 -->

		<section>

		<div class="subDetails" style = "padding: 10px;">

			<h4>
				<b>BASES DE DONNEES: </b>
			</h4>
				<c:forEach items="${listeBaseDeDonnee}" var="outil">
					<c:forEach items="${affichage.listeBaseDeDonneeId}"
						var="baseDeDonneeId" varStatus="loop">
						<c:if test="${baseDeDonneeId eq outil.id}">
							<c:if test="${loop.index eq 0}">
								<c:out value="${outil.nom}" />
							</c:if>
							<c:if test="${loop.index ne 0}">, <c:out
									value="${outil.nom}" />
							</c:if>
						</c:if>
					</c:forEach>
				</c:forEach>
                <c:if test="${empty listeBaseDeDonnee}">N/A</c:if>
		</div>

		<div class="clear"></div>
		</section>

			<!-- 	************************** METHODES **************************	 -->

		<section>

		<div class="subDetails" style = "padding: 10px;">

			<h4>
				<b>METHODES: </b>
			</h4>
			<h5>${resumeTechniqueEtFonctionnel.methodes}</h5>
            <c:if test="${empty resumeTechniqueEtFonctionnel.methodes}">N/A</c:if>
		</div>
            
		<div class="clear"></div>
		</section>

			<!-- 	************************** INDUSTRIES **************************	 -->

		<section>

		<div class="subDetails " style = "padding: 10px;">

			<h4>
				<b>SECTEURS D'ACTIVITES: </b>
			</h4>			
			<h5>${resumeTechniqueEtFonctionnel.industries}</h5>
			<c:if test="${empty resumeTechniqueEtFonctionnel.industries}">N/A</c:if>
		</div>

		</section>
		
			<!-- 	************************** FOOTER **************************	 -->

		<div class="footer">Controlled Distribution</div>

		</div>

		<!-- 	************************** AUTRES PAGES **************************	 -->

		<!-- 	************************** PAGE 3 & + : CORPS DOSSIER DE COMPETENCE AUTRES PAGES **************************	 -->

		<c:forEach items="${listeExperience}" var="experience">
			<c:forEach items="${affichage.listeExperienceId}" var="experienceId">
				<c:if test="${experienceId eq experience.id}">
					<div class="dossiercompetencepage">
						<div class="boxed" style ="text-align: center;">

							<strong><h3>
									<b><fmt:formatDate
											value="${experience.dateDebut}" pattern="MM/yyyy" /> à <fmt:formatDate
											value="${experience.dateFin}" pattern="MM/yyyy" /> -
										${experience.entreprise.nom} - ${experience.metier.nom}
									</b>
								</h3></strong>
						</div>
						<h4>
							<b>PROJET: ${experience.projet}<b>
						</h4>

						<h6 style = "padding: 10px;">
							<i>${experience.descriptif}</i>
						</h6>

						<div class="subDetails">
							<h4>
								<b>OUTILS: </b>
							</h4>

							<c:forEach items="${experience.listeLogiciel}" var="outil"
								varStatus="loop">
								<c:if test="${loop.index eq 0}">
									<c:out value="${outil.nom}" />
								</c:if>
								<c:if test="${loop.index ne 0}">, <c:out
										value="${outil.nom}" />
								</c:if>
							</c:forEach>
							<c:forEach items="${experience.listeLangage}" var="outil"
								varStatus="loop">
								, <c:out value="${outil.nom}" />
							</c:forEach>
							<c:forEach items="${experience.listeBaseDeDonnee}" var="outil"
								varStatus="loop">
								, <c:out value="${outil.nom}" />
							</c:forEach>
							<c:forEach items="${experience.listeSystemeExploitation}"
								var="outil" varStatus="loop">
								, <c:out value="${outil.nom}" />
							</c:forEach>
						</div>
						<hr>
					</div>
				</c:if>
			</c:forEach>
		</c:forEach>

	</div>
</body>
</html>