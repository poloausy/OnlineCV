<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>

<head>
</head>

<body>

<div class="container accodion-content">
		<form method="POST" action="${pageContext.request.contextPath}/donneesVisiteur/saveTitre/${titre.id}" modelAttribute="titre">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<form:hidden path="titre.id" />
				
		
			
			<table  class="table" BORDER=0>
			<tr>
				<th>Titre</th>
				<th></th>
			</tr>
				<c:forEach items="${listeTitre}" var="titre">
					<tr>
						<td><c:out value="${titre.titre}" /></td>			
						<td><input type="button" class="btn btn-primary" align="middle" value="Supprimer" onclick=window.location.href="<c:url value="/donneesVisiteur/supprimerTitre/${titre.id}" />">&nbsp;&nbsp;&nbsp;</td>		
					</tr>
				</c:forEach>
				
				<tr>
					<td><form:input value="" path="titre.titre" class="form-control" required="required"/></td>
					<td><input type="submit" class="btn btn-primary" value="Sauvegarder"  id="s1" align="middle" />&nbsp;&nbsp;&nbsp;</td>
				</tr>			
				
				
				
				
			</table>
			
			<c:if test="${erreur eq 'doublon'}">
				<span class="error"><spring:message code="error.titre.doublon" /></span>
			</c:if>
			
			<p style="text-align: center;">
			<input id="closeAccTitre" type="button" class="btn btn-primary" align="middle" value="Ferme Accordeon" onclick=window.location.href="<c:url value="/donneesVisiteur/fermeAccordeon" />">
			</p>
		</form>					
</div>

</body>
</html>