//Implemented by Arno for generating pdf files
$(document).ready(function(){
	$("#pdf").on("click", genPdf);
	$("#print").on("click", printPdf);
	$("#gen-pdfdc").on("click", generatePdf1);
	$("#gen-pdfcv4").on("click", generatePdf2);
	$("#gen-pdfcv1").on("click", generatePdfCV1);
	$("#gen-pdfcv2").on("click", generatePdfCV2);	
	$("#gen-pdfcv3").on("click", generatePdf3);	
});

//FONCTION GENERATION DOSSIER DE COMPETENCE

function generatePdf1(){	

	var valeurpage = parseInt(0);
	var numexp = parseInt(0);
	var ind = 10;
	var page = parseInt(1)
	var contentdiv = 0;
	var i=parseInt(0);


	//METHODE DE PAGINATION DU DOSSIER DE COMPETENCE
	
	$('.dossiercompetencepage').each(function (index) {
		var exp = $(this).html();
		//si valeurpage = 0 on est sur la premiere page à expériences
		if(valeurpage == 0){
			console.log('on créé la page');
			$("#content").append('<div class="dossiercompetencepagetoleave"></div>');
			valeurpage = parseInt(1);
		}

		//on est sur la page créée ci-haut
		if((valeurpage != 0) && (280 > (valeurpage + parseInt(parseInt($(this).height())* 0.2645833)))){

			//l'élément peut-il être inséré dans la page ?
			console.log('on insère lexpérience '+numexp+ ' dans la page '+page);
			numexp++;
			//on insère l'expérience dans la page A4 en cours de remplissage
			$('#content div.dossiercompetencepagetoleave:last').append(exp);

			//on recalcule la taille après insertion de l'expérience
			valeurpage = valeurpage + parseInt(parseInt($(this).height())* 0.2645833);
			console.log(valeurpage);
		}//l'expérience ne peut pas renter dans la page

		else {

			$(".dossiercompetencepagetoleave:last").append('<div class="footer">Controlled Distribution</div>');
			console.log('on insère le footer');			

			//on créé la nouvelle page
			$("#content .dossiercompetencepagetoleave:last").after(function(){
				$('#content').append('<div class="dossiercompetencepagetoleave"></div>');
			});

			$('#content div.dossiercompetencepagetoleave:last').append(exp);
			contentdiv = 0;
			valeurpage = parseInt(1) + parseInt(parseInt($(this).height())* 0.2645833);
			page++;
		}
	});

	$(".dossiercompetencepagetoleave:last").append('<div class="footer">Controlled Distribution</div>');
	console.log('on insère le footer de fin');	
	$( "div.dossiercompetencepage" ).remove();

	//ELEMENT TO BE PRINTED TO PDF
	
	var element = document.getElementById('content');
	console.log('getting element...');

	//GET CURRENT TIME
	
	var currentTime = new Date();

	var output = {
			margin : [ 0, 0, -1, 0 ],
			filename : 'DC_' + currentTime + '.pdf',
			image : {
				type : 'jpeg',
				quality : 1
			},
			html2canvas : {
				scale : 2
			},
			enableLinks : true,
			jsPDF : {
				unit : 'mm',
				format : 'a4',
				orientation : 'p'
			}
	};

	// CREATION OF PDF FROM <<#content>> & PDF SAVE
	
	html2pdf().from(element).set(output).toPdf().save();
	
	/* plugin: https://github.com/eKoopmans/html2pdf */
}



//FONCTION GENERATION CV1

function generatePdfCV1(){	
	$("#contenttoleave").remove();
	var nbexponlastpage = parseInt(0);
	var valexponlastpage = parseInt(0);
	var valeurpage = parseInt(0);
	var eltt = parseInt(0);
	var page = parseInt(1);
	var finexperience = parseInt(0);
	var nbexperience = parseInt(0);

	//ON CREEE LA DIV QUI VA CONTENIR LES ELEMENTS PAGINES
	
	$("body").append('<div id="contenttoleave" class="cv" class="instaFade" style="margin: 0 auto"></div>');

	//ON CREE LA PREMIERE PAGE ET ON INSERE L'ENTETE
	
	console.log('on cree la premiere page');	
	$("#contenttoleave").append('<div class="dossiercvtoleave"></div>');
	$('#contenttoleave div.dossiercvtoleave:last').append($('.element'));	
	valeurpage = 1 + valeurpage + parseInt(parseInt($('.element').height())* 0.2645833);
	console.log('valeurpage apres entete: '+valeurpage);

	//ON INSERE LE TITRE <<experience professionnelle>>
	
	var titrexp = $('#content div.sectionTitlexp');

	$('#contenttoleave div.dossiercvtoleave:last').append(titrexp);
	valeurpage = 1 + valeurpage + parseInt(parseInt($('.sectionTitlexp').innerHeight())* 0.2645833);
	console.log($('.sectionTitlexp').outerHeight(true));
	console.log('valeur page apres titre: '+valeurpage);

	//ON INSERE LES EXPERIENCES
	
	$('#content div.experience').each(function (index) {
		console.log('page :' +page);
		var experience = $(this);

	//ON RECUPERE LA TAILLE DE L'EXPERIENCE
		
	var taillexp = 1 +  parseInt( $(this).innerHeight()*0.2645833);
	console.log('taille experience ' + taillexp);

	//LA TAILLE DE L'EXPERIENCE PEUT ELLE S'INSERER DANS LA PAGE ?
	
	if(280 > (valeurpage + parseInt(taillexp))){
		console.log("on peut inserer lexperience");
		$('#contenttoleave div.dossiercvtoleave:last').append(experience); 
		valeurpage = 1 + valeurpage + parseInt(taillexp);
		console.log(valeurpage);
		nbexponlastpage++;
		valexponlastpage = valexponlastpage + taillexp;
		nbexperience++;
	}else{
		console.log("il faut sauter de page");
		
			//on créé une nouvelle page et on insère l'expérience
			$("#contenttoleave div.dossiercvtoleave:last").after(function(){
				$('#contenttoleave').append('<div class="dossiercvtoleave"></div>');
			});

			$('#contenttoleave div.dossiercvtoleave:last').append(experience);
			valeurpage = 1 + parseInt(taillexp);
			page++;
			console.log(valeurpage);
			nbexponlastpage = parseInt(1);
			valexponlastpage = parseInt(1);
			nbexperience++;
		}
	});
	//on insère les competences, langues et formations
	$('.eltscv').each(function (index) {

		console.log('nbre exp on page: ' + nbexponlastpage);
		console.log('taille exp on page: ' + parseInt(valexponlastpage/0.2645833));

		var elt = $(this);
		console.log("taille elt: " + parseInt($(this).innerHeight()/* * 0.2645833*/));

		if(finexperience == 0){

			console.log('on est juste apres la fin d experience');
			//peut-on inserer l'element ?

			if(280 > (valeurpage + parseInt(parseInt($(this).innerHeight())* 0.2645833))){

				console.log("on peut inserer lelement");
				console.log(valeurpage)
				$('#contenttoleave div.dossiercvtoleave:last').append('<br>');
				//on insere l'element 

				$('#contenttoleave div.dossiercvtoleave:last').append(elt);
				valeurpage = 1 + valeurpage + parseInt(parseInt($(this).innerHeight())* 0.2645833);
				console.log(valeurpage);
				finexperience++;
				console.log(finexperience);
			}
			else{
				console.log("il faut sauter de page");
				//on créé une nouvelle page et on insère l'element
				$("#contenttoleave div.dossiercvtoleave:last").after(function(){
					$('#contenttoleave').append('<div class="dossiercvtoleave"></div>');
				});
				$('#contenttoleave div.dossiercvtoleave:last').append(elt);
				valeurpage = 1 + parseInt(0) + parseInt(parseInt($(this).innerHeight())* 0.2645833);
				page++;
				console.log(valeurpage);
				finexperience++;
				console.log(finexperience);
			}

		}else{
			//peut-on inserer l'element ?
			if(280 > (valeurpage + parseInt(parseInt($(this).innerHeight())* 0.2645833))){
				console.log("on peut inserer lelement");

				//on insere l'element 
				$('#contenttoleave div.dossiercvtoleave:last').children().last().after(function(){
					$('#contenttoleave div.dossiercvtoleave:last').append(elt); 
				});
				valeurpage = 1 + valeurpage + parseInt(parseInt($(this).innerHeight())* 0.2645833);
				console.log(valeurpage);
			}
			else{
				console.log("il faut sauter de page");
				//on créé une nouvelle page et on insère l'element
				$("#contenttoleave div.dossiercvtoleave:last").after(function(){
					$('#contenttoleave').append('<div class="dossiercvtoleave"></div>');
				});
				$('#contenttoleave div.dossiercvtoleave:last').append(elt);
				valeurpage = 1 + parseInt(0) + parseInt(parseInt($(this).innerHeight())* 0.2645833);
				page++;
				console.log(valeurpage);
			}
		}
	});


	console.log('fin insertion');
	$("#content").remove();

	console.log('generate');
	console.log('clickkk');
	//element to be printed to pdf
	var element = document.getElementById('contenttoleave');
	console.log('getting element...');

	//get current time
	var currentTime = new Date();

	var opt = {
			margin : [ -3, -3, -3, -3 ],
			filename : 'CV1_' + currentTime + '.pdf',
			image : {
				type : 'jpeg',
				quality : 1
			},
			html2canvas : {
				scale : 2
			},
			enableLinks : true,
			jsPDF : {
				unit : 'mm',
				format : 'a4',
				orientation : 'p'
			}
	};

	// Promise: create pdf from #content and save to pdf 
	html2pdf().from(element).set(opt).toPdf().save();
	/* plugin: https://github.com/eKoopmans/html2pdf */
	setTimeout(
			function() 
			{
				window.location.reload(true);
			}, 500);
}


//fonction génération CV2
function generatePdfCV2(){	
	var valeurpage = parseInt(0);

	//on créé la div qui va contenir les éléments paginés
	$("body").append('<div id="contenttoleave"></div>')
	//on créé la premiere page
	console.log('on cree la premiere page');	
	$("#contenttoleave").append('<div class="pagecv2toleave"></div>');
	$('#contenttoleave div.pagecv2toleave:last').append($('.head').html());	
	valeurpage = 1 + parseInt(parseInt($('.head').height())* 0.2645833);
	console.log('valeurpage apres entete: '+valeurpage);
	$('#contenttoleave div.pagecv2toleave:last').append($('.page1').html());
	valeurpage = valeurpage + parseInt(1 + parseInt($('.page1').height())* 0.2645833);
	console.log('valeurpage apres premiere page: '+valeurpage);

	//creation page pour inserer les autres element
	$("#contenttoleave").append('<div class="pagecv2toleave"></div>');

	//on insere le titre "experience"
	$("#contenttoleave div.pagecv2toleave:last").append('<section role="main" class="container_16 clearfix"></section>');
	$(".pagecv2toleave:last section:last").append('<div class="grid_16 experiences"></div>');
	var titreexp = $("h3",".grid_16.experiences");
	$("#contenttoleave  .grid_16.experiences").append(titreexp);
	console.log('valeur page avec titre experience: ' + parseInt($(".pagecv2toleave:last section:last").innerHeight()* 0.2645833));
	valeurpage = parseInt($(".pagecv2toleave:last section:last").innerHeight()* 0.2645833);


	//on insere les experiences
	$('#content .grid_16.experiences li').each(function (index) {
		if(280 > parseInt(valeurpage + 1 + parseInt($(this).height()* 0.2645833))){
			//on insere l'experience
			console.log("on insere lexperience");
			$(".grid_16.experiences:last").append($(this));
			valeurpage = valeurpage + 1 + parseInt($(this).height()* 0.2645833);
			console.log('valeur page apres insertion exp: ' + valeurpage);
		}
		else{
			$("#contenttoleave").append('<div class="pagecv2toleave"></div>');
			$("#contenttoleave div.pagecv2toleave:last").append('<section role="main" class="container_16 clearfix"></section>');
			$(".pagecv2toleave:last section:last").append('<div class="grid_16 experiences"></div>');
			$(".grid_16.experiences:last").append($(this));
			valeurpage = parseInt($(this).height()* 0.2645833);
			console.log('valeur page apres insertion exp: ' + valeurpage);
		}
	});
	console.log('fin insertion exp');
	console.log(valeurpage);
	console.log(1 + parseInt($('#certif').innerHeight() * 0.2645833));
	var countcertifications = parseInt(0);

	$('#certif .grid_16.certifications li').each(function (index){
		countcertifications++;
	});
	console.log("nbre certifs :" + countcertifications);
	//insertion du bloc certifications
	if(280 > (parseInt(valeurpage) + parseInt($('#certif').innerHeight()* 0.2645833))){
		//on insere les certifications dans la page actuelle
		console.log('on insere la certification');

		if(countcertifications >= 1){
			$(".pagecv2toleave:last section:last").after('<section role="main" class="container_16 clearfix"></section>');
			$(".pagecv2toleave:last section:last").append($('#certif'));
			valeurpage = valeurpage + 1 + parseInt($('#certif').innerHeight()* 0.2645833);
		}
		console.log("pas de certifications")
	}
	else{
		if(countcertifications >= 1){
			//on cree une nouvelle page et on insere le bloc certifications
			console.log('on cree une nvelle page et on insere la certification');	
			$("#contenttoleave").append('<div class="pagecv2toleave"></div>');
			$("#contenttoleave div.pagecv2toleave:last").append('<section role="main" class="container_16 clearfix"></section>');
			$(".pagecv2toleave:last section:last").append($('#certif'));
			valeurpage = 1 + parseInt($("#certif").innerHeight()* 0.2645833);
		}
	}	
	$(".pagecv2toleave:last section:last")
	console.log('fin insertion certif. valeur page apres: ' + valeurpage);
	//insertion du bloc langues et contact
	if(280 > parseInt(valeurpage + 1 + parseInt($('#corps_cv section:last').innerHeight()* 0.2645833))){
		//on insere les langues et contact dans la page actuelle
		console.log('on insere les langues et contact');
		console.log(1 + parseInt($('#corps_cv section:last').innerHeight()* 0.2645833));
		$(".pagecv2toleave:last").append($('#corps_cv section:last'));
		valeurpage = valeurpage + 1 + parseInt($('.pagecv2toleave:last section:last').innerHeight()* 0.2645833);
	}
	else{
		//on cree une nouvelle page et on insere le bloc langues et contact
		console.log('on cree une nvelle page et on insere les langues et contact');	
		$("#contenttoleave").append('<div class="pagecv2toleave"></div>');
		$("#contenttoleave div.pagecv2toleave:last").append($('#corps_cv section:last'));
		valeurpage = 1 + parseInt($('.pagecv2toleave:last section:last').innerHeight()* 0.2645833);
	}
	console.log('fin insertion langues et contact. valeur page apres: ' + valeurpage);

	console.log('fin mise en page cv2');
	$("#content").remove();
	console.log('generate');
	console.log('clickkk');
	//element to be printed to pdf
	var element = document.getElementById('contenttoleave');
	console.log('getting element...');

	//get current time
	var currentTime = new Date();

	var opt = {
			margin : [ -3, -3, -3, -3 ],
			filename : 'CV2_' + currentTime + '.pdf',
			image : {
				type : 'jpeg',
				quality : 1
			},
			html2canvas : {
				scale : 2
			},
			enableLinks : true,
			jsPDF : {
				unit : 'mm',
				format : 'a4',
				orientation : 'p'
			}
	};

	// Promise: create pdf from #content and save to pdf 
	html2pdf().from(element).set(opt).toPdf().save();
	/* plugin: https://github.com/eKoopmans/html2pdf */
	setTimeout(
			function() 
			{
				window.location.reload(true);
			}, 500);	
}


//GENERATE pdf function
function genPdf() {
	console.log("pdf gen click");

	//create a new html canvas element + execute canvas function
	html2canvas(document.getElementById("content")).then(function(canvas) {

		console.log("après le html2canvas");

		//create a new pdf document A4 size (jsPdf)
		var doc = new jsPDF("p", "mm", "a4");

		var imgWidth = 210;
		var pageHeight = 295;
		var imgHeight = canvas.height * imgWidth / canvas.width;
		var heightLeft = imgHeight;
		var position = 0;

		//let the html canvas capture a screenshot of the web page
		var img = canvas.toDataURL('image/png');

		//captures the width & height of the cv div
		var divHeight = $('#content').height();
		var divWidth = $('#content').width();
		var ratio = divHeight / divWidth;

		//page width & height
		var width = doc.internal.pageSize.getWidth();
		var height = doc.internal.pageSize.getHeight();

		//adjust height so it fits the width/height ration
		height = ratio * width;

		//add the canvas image element to the pdf
		doc.addImage(img, 'JPEG', 0, 0, imgWidth, imgHeight);
		heightLeft -= (pageHeight + 50);

		//split the pages
		while (heightLeft >= 0) {
			position = heightLeft - imgHeight;
			doc.addPage();
			doc.addImage(img, 'PNG', 0, position, imgWidth, imgHeight);
			heightLeft -= pageHeight;
		}

		//get current time
		var currentTime = new Date();

		//save the pdf with time
		doc.save('CV_' + currentTime + '.pdf');
	});
	
	

}

//PRINT pdf function
function printPdf() {
	console.log("pdf gen click");

	//create a new html canvas element + execute canvas function
	html2canvas(document.getElementById("content")).then(function(canvas) {

		//create a new pdf document A4 size (jsPdf)
		var doc = new jsPDF('p', 'mm', 'a4');

		var imgWidth = 210;
		var pageHeight = 295;
		var imgHeight = canvas.height * imgWidth / canvas.width;
		var heightLeft = imgHeight;
		var position = 0;

		//let the html canvas capture a screenshot of the web page
		var img = canvas.toDataURL('image/png');

		//captures the width & height of the cv div
		var divHeight = $('#content').height();
		var divWidth = $('#content').width();
		var ratio = divHeight / divWidth;

		//page width & height
		var width = doc.internal.pageSize.getWidth();
		var height = doc.internal.pageSize.getHeight();

		//adjust height so it fits the width/height ration
		height = ratio * width;

		//add the canvas image element to the pdf
		doc.addImage(img, 'JPEG', 0, 0, imgWidth, imgHeight);

		//get current time
		var currentTime = new Date();

		//save the pdf with time
		doc.autoPrint();
		window.open(doc.output('bloburl'), '_blank');
	});
}

//fonction génération CV4
function generatePdf2(){	

	//element to be printed to pdf
	var element = document.getElementById('content');

	//get current time
	var currentTime = new Date();

	var output = {
			margin : [ -3, -3, -3, -3 ],
			filename : 'CV4_' + currentTime + '.pdf',
			image : {
				type : 'jpeg',
				quality : 1
			},
			html2canvas : {
				scale : 2
			},
			enableLinks : true,
			jsPDF : {
				unit : 'mm',
				format : 'a4',
				orientation : 'p'
			},
	};
	// Promise: create pdf from #content and save to pdf 
	html2pdf().from(element).set(output).toPdf().save();
	/* plugin: https://github.com/eKoopmans/html2pdf */
}

//fonction génération CV3
function generatePdf3(){	

	//element to be printed to pdf
	var element = document.getElementById('content');

	//get current time
	var currentTime = new Date();

	var output = {
			margin : [ -3, 0, -3, 0 ],
			filename : 'CV3_' + currentTime + '.pdf',
			image : {
				type : 'jpeg',
				quality : 1
			},
			html2canvas : {
				scale : 2
			},
			enableLinks : true,
			jsPDF : {
				unit : 'mm',
				format : 'a4',
				orientation : 'p'
			},
	};
	// Promise: create pdf from #content and save to pdf 
	html2pdf().from(element).set(output).toPdf().save();
	/* plugin: https://github.com/eKoopmans/html2pdf */
}
