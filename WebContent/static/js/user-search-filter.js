//Only execute script when document is loaded/ready
$( document ).ready(function() {
	
	//GLOBAL VARS
	var keyword1, domain, detailCompetence, detailLangue, newHtml, CheckRechercheCombinee ;
	detailCompetence ="";
	detailLangue="";
	nombreAnneeExp ="";
	CheckRechercheCombinee = false;
	if ($('#CheckRechercheCombinee').is(':checked') ) {
		CheckRechercheCombinee = true;
	}
	var keyword2, domain2, detailCompetence2, detailLangue2;
	detailCompetence2 ="";
	detailLangue2="";
	nombreAnneeExp2 ="";
	CheckRechercheCombinee2 = false;
	if ($('#CheckRechercheCombinee2').is(':checked') ) {
		CheckRechercheCombinee2 = true;
	}
	var keyword3,  domain3, detailCompetence3, detailLangue3;
	detailCompetence3 ="";
	detailLangue3="";
	nombreAnneeExp3 ="";
	
	$('#triParExperience').hide()

	//*********************************************************************************** 
 	 //FUNCTION TOGGLE DETAIL FILTER
	function AfficheCacheDetailRecherche (domain, numeroRecherche){
 	 
 	 switch(domain){
	case "competences":
		console.log('lala competences');
		$('.detailfilter'+numeroRecherche).hide();
		$('#competences_niveau_select'+numeroRecherche).show();
		$('#niveauCompetence'+numeroRecherche).show();
		$('#langues_niveau_select'+numeroRecherche).hide();
		$('#niveauLangue'+numeroRecherche).hide();
		$('#nombre_annees_select'+numeroRecherche).hide();
		$('#anneeExp'+numeroRecherche).hide();
		break;
	case "langues":
		console.log('lala langue');
		$('.detailfilter'+numeroRecherche).hide();
		$('#langues_niveau_select'+numeroRecherche).show();
		$('#niveauLangue'+numeroRecherche).show();
		$('#niveauCompetence'+numeroRecherche).hide();
       	$('#competences_niveau_select'+numeroRecherche).hide();
       	$('#nombre_annees_select'+numeroRecherche).hide();
		$('#anneeExp'+numeroRecherche).hide();
		break;
	case "langages":
	case "logiciels":	
	case "metiers":
	case "entreprises":
		console.log('lala formation');
		$('.detailfilter'+numeroRecherche).hide();
		$('#nombre_annees_select'+numeroRecherche).show();
		$('#anneeExp'+numeroRecherche).show();
		$('#niveauCompetence'+numeroRecherche).hide();
       	$('#competences_niveau_select'+numeroRecherche).hide();
       	$('#langues_niveau_select'+numeroRecherche).hide();
		$('#niveauLangue'+numeroRecherche).hide();
		break;
   	default:
		$('.detailfilter'+numeroRecherche).hide();
       	$('#niveauCompetence'+numeroRecherche).hide();
       	$('#competences_niveau_select'+numeroRecherche).hide();
       	$('#langues_niveau_select'+numeroRecherche).hide();
		$('#niveauLangue'+numeroRecherche).hide();
		$('#nombre_annees_select'+numeroRecherche).hide();
		$('#anneeExp'+numeroRecherche).hide();
		break;
	}
} 
	
	  //*********************************************************************************** 
	  //Fonction REINITIALISATION RECHERCHE permet de réinitialiser le mot clé de la recherche et de cacher les filtres associés à cette recherche  
	 function ReinitialisationRecherche (numeroRecherche){
		//$('#keyword'+numeroRecherche).val("");
		$('#keyword'+numeroRecherche).hide();
	   	$('#domain_select'+numeroRecherche).hide();
		//Réinitialise le domaine quand la checkbox est décochée 
		//$('#domain_select'+numeroRecherche).val("");
	   	$('.detailfilter'+numeroRecherche).hide();
	   	//$('#niveauCompetence'+numeroRecherche).val("");
	   	$('#niveauCompetence'+numeroRecherche).hide();
	   	//$('#competences_niveau_select'+numeroRecherche).val("");
	   	$('#competences_niveau_select'+numeroRecherche).hide();
	   	//$('#langues_niveau_select'+numeroRecherche).val("");
	   	$('#langues_niveau_select'+numeroRecherche).hide();
		$('#niveauLangue'+numeroRecherche).hide();
		//$('#nombre_annees_select'+numeroRecherche).val("");
		$('#nombre_annees_select'+numeroRecherche).hide();
		$('#anneeExp'+numeroRecherche).hide(); 
		console.log('reinitialisation ok');
	 }  	
	
	
    // SUBMIT FORM
    $("#filter-btn").click(function(event) {
    	// Prevent the form from submitting via the browser.
    	event.preventDefault();
    	//show only competences instead of users
    	console.log("newHtml 1: ");
    	user_filter_ajax();
    	//newHtml = "check variable";
    	//Rend visible les résulats
    	$('#resultDiv').show();
    	console.log("newHtml 2: "+newHtml);
    	//detailCompetence ="";
    	//detailLangue="";
    });
    
     //ENABLE FORM SUBMIT BTNS
    $(".filterlistener").on('change', function(){
    	
    	//get value of keyword 1
    	if($('#keyword1').val() != null){
    		keyword1 = $('#keyword1').val();
    		console.log('keyword1: ' + keyword1);
    	}
    	else{
    		keyword1 = " ";
    	}
    	
    	//get value for domain
    	if($('#domain_select1').val() != null){
    		domain = $('#domain_select1').val();
    		console.log('domain: ' + domain);
    	}
    	else{
    		domain = " ";
    	}

    	//get value for detail filter for competence
    	if($('#domain_select1').val() === 'competences' && $('#competences_niveau_select1').val() != ""){
    		 
    		detailCompetence = $('#competences_niveau_select1').val();
    		console.log('detail competence: ' + detailCompetence);    		
    	}
    	
    	else{
    		detailCompetence = "";
    		
    		console.log('detail competence null: ' + detailCompetence);
    	}
    	 
    	//get value for detail filter for langue
    	 if($('#domain_select1').val() === 'langues' && $('#langues_niveau_select1').val() != ""){
    		 
    			detailLangue = $('#langues_niveau_select1').val(); 
    	 		console.log('detail langue: ' + detailLangue);
        		}
    		
    		else{
    			detailLangue = "";
        		console.log('detail langue null: ' + detailLangue);
        	}
    	
    	//get value for detail filter years number for langage info, logiciel, métier 
    	 if($('#domain_select1').val() === 'langages' && $('#nombre_annees_select1').val() != "" || $('#domain_select1').val() === 'logiciels' && $('#nombre_annees_select1').val() != ""
    		 || $('#domain_select1').val() === 'metiers' && $('#nombre_annees_select1').val() != "" || $('#domain_select1').val() === 'entreprises' && $('#nombre_annees_select1').val() != "")  {
    		 
    			nombreAnneeExp = $('#nombre_annees_select1').val(); 
    	 		console.log('Nombre années d exp: ' + nombreAnneeExp);
        		}
    		
    		else{
    			nombreAnneeExp = "";
        		console.log('Nombre années d exp: null: ' + nombreAnneeExp);
        	}
    	 
    //*********************************************************************************************
    //get value of keyword2 2EME RECHERCHE (RECHERCHE COMBINEE)
     	if($('#keyword2').val() != null){
     		keyword2 = $('#keyword2').val();
     		console.log('keyword2: ' + keyword2);
     	}
     	else{
     		keyword2 = " ";
     	}
     	
     	//get value for domain
     	if($('#domain_select2').val() != null){
     		domain2 = $('#domain_select2').val();
     		console.log('domain2: ' + domain2);
     	}
     	else{
     		domain2 = " ";
     	}

     	//get value for detail filter for competence
     	if($('#domain_select2').val() === 'competences' && $('#competences_niveau_select2').val() != ""){
     		 
     		detailCompetence2 = $('#competences_niveau_select2').val();
     		console.log('detail competence2: ' + detailCompetence2);    		
     	}
     	
     	else{
     		detailCompetence2 = "";
     		
     		console.log('detail competence2 null: ' + detailCompetence2);
     	}
     	 
     	//get value for detail filter for langue
     	 if($('#domain_select2').val() === 'langues' && $('#langues_niveau_select2').val() != ""){
     		 
     			detailLangue2 = $('#langues_niveau_select2').val(); 
     	 		console.log('detail langue2: ' + detailLangue2);
         		}
     		
     		else{
     			detailLangue2 = "";
         		console.log('detail langue null: ' + detailLangue2);
         	}
     	
     	//get value for detail filter years number for langage info, logiciel, métier 
     	 if($('#domain_select2').val() === 'langages' && $('#nombre_annees_select2').val() != "" || $('#domain_select2').val() === 'logiciels' && $('#nombre_annees_select2').val() != ""
     		 || $('#domain_select2').val() === 'metiers' && $('#nombre_annees_select2').val() != "" || $('#domain_select2').val() === 'entreprises' && $('#nombre_annees_select2').val() != "")  {
     		 
     			nombreAnneeExp2 = $('#nombre_annees_select2').val(); 
     	 		console.log('Nombre années d ex2p: ' + nombreAnneeExp2);
         		}
     		
     		else{
     			nombreAnneeExp2 = "";
         		console.log('Nombre années d exp2: null: ' + nombreAnneeExp2);
         	}
     	
        //*********************************************************************************************
     	//get value of keyword3 3EME RECHERCHE (RECHERCHE COMBINEE)
      	if($('#keyword3').val() != null){
      		keyword3 = $('#keyword3').val();
      		console.log('keyword3: ' + keyword3);
      	}
      	else{
      		keyword3 = " ";
      	}
      	
      	//get value for domain
      	if($('#domain_select3').val() != null){
      		domain3 = $('#domain_select3').val();
      		console.log('domain3: ' + domain2);
      	}
      	else{
      		domain3 = " ";
      	}

      	//get value for detail filter for competence
      	if($('#domain_select3').val() === 'competences' && $('#competences_niveau_select3').val() != ""){
      		 
      		detailCompetence3 = $('#competences_niveau_select3').val();
      		console.log('detail competence3: ' + detailCompetence3);    		
      	}
      	
      	else{
      		detailCompetence3 = "";
      		
      		console.log('detail competence3 null: ' + detailCompetence3);
      	}
      	 
      	//get value for detail filter for langue
      	 if($('#domain_select3').val() === 'langues' && $('#langues_niveau_select3').val() != ""){
      		 
      			detailLangue3 = $('#langues_niveau_select3').val(); 
      	 		console.log('detail langue3: ' + detailLangue3);
          		}
      		
      		else{
      			detailLangue3 = "";
          		console.log('detail langue null: ' + detailLangue3);
          	}
      	
      	//get value for detail filter years number for langage info, logiciel, métier 
      	 if($('#domain_select3').val() === 'langages' && $('#nombre_annees_select3').val() != "" || $('#domain_select3').val() === 'logiciels' && $('#nombre_annees_select3').val() != ""
      		 || $('#domain_select3').val() === 'metiers' && $('#nombre_annees_select3').val() != "" || $('#domain_select3').val() === 'entreprises' && $('#nombre_annees_select3').val() != "")  {
      		 
      			nombreAnneeExp3 = $('#nombre_annees_select3').val(); 
      	 		console.log('Nombre années d exp3: ' + nombreAnneeExp2);
          		}
      		
      		else{
      			nombreAnneeExp3 = "";
          		console.log('Nombre années d exp3: null: ' + nombreAnneeExp3);
          	} 
    	 
     	//TOGGLE DETAIL FILTER RECHERCHE 1
     	AfficheCacheDetailRecherche (domain, 1);      	 
      	 
     	//TOGGLE DETAIL FILTER RECHERCHE 2 
     	AfficheCacheDetailRecherche (domain2, 2);  
      	 
     	//TOGGLE DETAIL FILTER RECHERCHE 3 
     	AfficheCacheDetailRecherche (domain3, 3);   	 
     	
    	//enable search buttons when value is selected
    	$(".btn-submit").attr('disabled', false);
    	
    	// Cache les niveaux et les années d'experience 2 si la checkbox 1 n'est pas activée
    	console.log('condition checkbox ='+!$('#CheckRechercheCombinee').is(':checked'));
    	  if (!$('#CheckRechercheCombinee').is(':checked') ) {
    		  $('#langues_niveau_select2').hide();
    		  $('#niveauCompetence2').hide();
    	      $('#competences_niveau_select2').hide();
    		  $('#niveauLangue2').hide();
    		  $('#anneeExp2').hide();
    		  $('#nombre_annees_select2').hide();
    	  }
    	  
    	// Cache les niveaux et les années d'experience 3 si la checkbox 2 n'est pas activée
      	console.log('condition checkbox ='+!$('#CheckRechercheCombinee').is(':checked'));
      	  if (!$('#CheckRechercheCombinee2').is(':checked') ) {
      		  $('#langues_niveau_select3').hide();
      		  $('#niveauCompetence3').hide();
      	      $('#competences_niveau_select3').hide();
      		  $('#niveauLangue3').hide();
      		  $('#anneeExp3').hide();
      		  $('#nombre_annees_select3').hide();
      	  }
    });

  //*********************************************************************************************
  //Condition affichage recherche combinée avec 2 mots-clés, divs cachéees si la checkbox n'est pas activée 
    $("#CheckRechercheCombinee").click(function(event) { 
    	console.log("condition recherche combinée = " +$('#CheckRechercheCombinee').is(':checked'));
    	//Réinitialise le résultat de la recherche lorsque la checkbox est cochée/décochée
     	$('#resultDiv').html("");
    if ($('#CheckRechercheCombinee').is(':checked') ) {
    	$('#keyword2').show();
     	$('.rechercheCombinee').show();
    	$('#domain_select2').show();

     	
        // Montre l'option 3 mots-clés         
        $('.checkBox3MotsCles').show();        
         
       //CheckRechercheCombinee va etre envoyé vers le controlleur pour vérifier s'il faut lancer la deuxième recherche ou non
       CheckRechercheCombinee = true;
       AfficheCacheDetailRecherche (domain2, 2);           
    }
    else {

		
    	//Réinitialise la recherche mot clé 2 quand la checkbox est décochée 
    	ReinitialisationRecherche (2);		
    	$('.rechercheCombinee').hide();
    	
		//CheckRechercheCombinee va être envoyé vers le controlleur pour vérifier s'il faut lancer la deuxième recherche ou non
	       CheckRechercheCombinee = false;
	    // Cache l'option 3 mots-clés         
	        $('.checkBox3MotsCles').hide();  
	        
	      //Réinitialise la recherche mot clé 3 quand la checkbox est décochée 
	    	ReinitialisationRecherche (3); 
	    	$('.rechercheCombinee2').hide();
		  //CheckRechercheCombinee2 va etre envoyé vers le controlleur pour vérifier s'il faut lancer la troisième recherche ou non
		    CheckRechercheCombinee2 = false;
		 // Décoche automatiquement la 2ème checkbox
		 $("#CheckRechercheCombinee2").prop("checked", false);
   }
 })
 
 // Montre l'option 3 mots-clés à l'ouverture de la page si la checkbox pour une recherche combinée est checkée   
  if ($('#CheckRechercheCombinee').is(':checked') ) {
	  $('.checkBox3MotsCles').show(); 
  }
 
 
  //*********************************************************************************************
    //Condition affichage recherche combinée avec 3 mots-clés, divs cachéees si la checkbox n'est pas activée 
    $("#CheckRechercheCombinee2").click(function(event) { 
    	console.log("condition recherche combinée = " +$('#CheckRechercheCombinee2').is(':checked'));
    	//Réinitialise le résultat de la recherche lorsque la checkbox est cochée/décochée
     	$('#resultDiv').html("");
    if ($('#CheckRechercheCombinee2').is(':checked') ) {
    	$('#keyword3').show();
    	$('.rechercheCombinee2').show();
    	$('#domain_select3').show();

       //CheckRechercheCombinee va etre envoyé vers le controlleur pour vérifier s'il faut lancer la troisième recherche ou non
       CheckRechercheCombinee2 = true;
       AfficheCacheDetailRecherche (domain3, 3);  
   }
    else {
 		
		//Réinitialise la recherche mot clé 3 quand la checkbox est décochée 
    	ReinitialisationRecherche (3);
    	$('.rechercheCombinee2').hide();
    	
		//CheckRechercheCombinee2 va etre envoyé vers le controlleur pour vérifier s'il faut lancer la troisième recherche ou non
	       CheckRechercheCombinee2 = false;
   }
 })
 
   //Condition affichage info sur le tri en fonction du nombre d'années d'expérience
 $(".recherche1").change(function(event) {
	 if($('#domain_select1').val() === 'langages' || $('#domain_select1').val() === 'logiciels' || 
	 $('#domain_select1').val() === 'metiers' || $('#domain_select1').val() === 'entreprises' ){
		 $('#triParExperience').show();
		 $('#paramTri').html('en '+$('#keyword1').val()+'.');
		 if($('#domain_select1').val() === 'metiers'){
			 $('#paramTri').html('comme '+$('#keyword1').val()+'.')
		 }
		 if($('#domain_select1').val() === 'entreprises'){
			 $('#paramTri').html('chez '+$('#keyword1').val()+'.')
		 }
	 }
	 else {
		 $('#triParExperience').hide();
	 }
//	 if($('#domain_select1').val() === 'metiers'){
//		 $('#triParExperience').show();
//		 $('#paramTri').html('comme '+$('#keyword1').val()+'.');		 
//	 } 
//	 else {
//		 $('#triParExperience').hide();
//	 }
 })
 
 
	// AJAX GET function - show competences by selected competence
 
	function user_filter_ajax(){
		//ajax get function
		$.ajax({
			//GET request
			type : "GET",
			//JSON format
			contentType: 'application/json',
			
			//url to ManagerController /manager/searchByCompetence
			
			url : "searchUsers",
			
			//send input data to controller as an array
			//user @RequestParam in controller to get this value
			
			data: 
			{
				'keyword1': keyword1,
				'domain': domain,
				'detailCompetence': detailCompetence,
				'detailLangue': detailLangue,
				'nombreAnneeExp': nombreAnneeExp,
				'keyword2': keyword2,
				'domain2': domain2,
				'detailCompetence2': detailCompetence2,
				'detailLangue2': detailLangue2,
				'nombreAnneeExp2': nombreAnneeExp2,
				'CheckRechercheCombinee': CheckRechercheCombinee,
				'keyword3': keyword3,
				'domain3': domain3,
				'detailCompetence3': detailCompetence3,
				'detailLangue3': detailLangue3,
				'nombreAnneeExp3': nombreAnneeExp3,
				'CheckRechercheCombinee2': CheckRechercheCombinee2
			},
			//on success
			success:	function(result){
				newHtml = "";							
				//if there is a JSON result
				if(result.length > 0 ){
					//log the JSON result, you can parse this json on: https://jsonformatter.curiousconcept.com/
					console.log('JSON result ' + result);
					//clear the result div html
					$("#resultDiv").html('');
					//create table
					newHtml += '<table class="table-striped col-xs-12" cellpadding="15">';
					//create table head
					newHtml += 
						'<tr>' +
						'<th>Prenom</th>' +
						'<th>Nom</th>' +
						'<th>Email</th>' +
						'<th>ROLE</th>' +
						'<th></th>' +
						'</tr>';
					//parse the json result to object and loop over result List
					$.each(JSON.parse(result), function(index, value){
					//create table row for each user
					var utilisateurId = value.id;
					// url avec la valeur de l'utilisateur ciblé par la recherche
					var url='goToChoixTemplate/'+utilisateurId;
					newHtml += 
						'<tr>' +
						'<td>' + value.prenom +'</td>' +
						'<td>' + value.nom +'</td>' +
						'<td>' + value.credential.email +'</td>' +
						'<td>' + value.credential.role +'</td>' +
						'<td>' + '<a href="'+url+'"' + 'class="btn btn-info" onclick="recupResultatRecherche()">Voir profil</a>' + '</td>' +
						'</tr>'+					
						'<tr>' +	
						'<td>' +  '<br>' + '</td>' +
						'</tr>';
						
					//end of loop
					});
					newHtml += '</table>';
					//paste the new html in the results div
					$("#resultDiv").html(newHtml);
					//$("#resultDivPrecedent").html(newHtml);
					console.log("newHtml : ",newHtml);
				}
				//if no results available
				if(result.length == 2 || result.length == 0){
					$("#resultDiv").html('D&#233sol&#233, il n y a aucun r&#233sutat pour ce crit&#232re de recherche.');
				}
			},
			//on error
			error : function(e) {
				$("#resultDiv").html("<strong>Error when fetching results...</strong>");
				console.log("ERROR: ", e);
			}
		});	
	}
});




