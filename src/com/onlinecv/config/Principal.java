package com.onlinecv.config;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.onlinecv.entities.Admin;
import com.onlinecv.entities.Credential;
import com.onlinecv.entities.Manager;
import com.onlinecv.entities.Utilisateur;

public class Principal implements UserDetails {

	private static final long serialVersionUID = -5270061614463868043L;

	private Credential credential;

	private Admin admin;

	private Utilisateur utilisateur;

	private Manager manager;

	public Principal(Admin admin) {
		this.credential = admin.getCredential();
		this.admin = admin;
	}

	public Principal(Utilisateur utilisateur) {
		this.credential = utilisateur.getCredential();
		this.utilisateur = utilisateur;
	}

	public Principal(Manager manager) {
		this.credential = manager.getCredential();
		this.manager = manager;
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> authorities = new ArrayList<>();
		authorities.add(new SimpleGrantedAuthority(credential.getRole().name()));
		return authorities;
	}

	@Override
	public String getPassword() {
		return credential.getMotDePasse();
	}

	@Override
	public String getUsername() {
		return credential.getNomLogin();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}

	public boolean isAdmin() {
		return null != admin;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public Admin getAdmin() {
		return admin;
	}

	public Manager getManager() {
		return manager;
	}

	public Credential getCredential() {
		return credential;
	}
}
