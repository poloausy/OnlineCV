package com.onlinecv.controllers;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import com.onlinecv.entities.Admin;
import com.onlinecv.entities.ERole;
import com.onlinecv.entities.Manager;
import com.onlinecv.entities.Utilisateur;

// UNE CLASSE QUI FACILITE L'ACCES AUX INFORMATIONS D'AUTHENTIFICATION. AIDE A RECUPERER L'UTILISATEUR CONNECTE ET SON ROLE.

public class AuthHelper {

	// RETOURNE LE ROLE DE L'UTILISATEUR CONNECTE.

	public static ERole getRole() {
		return getPrincipal().getCredential().getRole();
	}

	// RETOURNE LE ROLE DE L'UTILISATEUR CONNECTE OU NULL.

	public static Utilisateur getUtilisateur() {
		return getPrincipal().getUtilisateur();
	}

	// RETOURNE LE ROLE DE L'ADMINISTRATEUR CONNECTE OU NULL.

	public static Admin getAdmin() {
		return getPrincipal().getAdmin();
	}

	// RETOURNE LE ROLE DU MANAGER CONNECTE OU NULL.

	public static Manager getManager() {
		return getPrincipal().getManager();
	}

	public static boolean isAuthenticated() {
		boolean authenticated = false;
		Collection<? extends GrantedAuthority> authorities = getAuthorities();
		ERole[] roles = ERole.values();
		List<String> stringRoles = new ArrayList<>();
		for (ERole role : roles) {
			stringRoles.add(role.name());
		}
		for (GrantedAuthority authority : authorities) {
			if (stringRoles.contains(authority.getAuthority())) {
				authenticated = true;
				break;
			}
		}
		return authenticated;
	}

	public static Authentication getAuthentication() {
		return SecurityContextHolder.getContext().getAuthentication();
	}

	public static com.onlinecv.config.Principal getPrincipal() {
		return (com.onlinecv.config.Principal) getAuthentication().getPrincipal();
	}

	public static Collection<? extends GrantedAuthority> getAuthorities() {
		return getAuthentication().getAuthorities();
	}

}
