package com.onlinecv.controllers;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.itextpdf.text.DocumentException;
import com.onlinecv.dao.INiveauJpaRepository;
import com.onlinecv.dao.ITemplateJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Affichage;
import com.onlinecv.entities.BaseDeDonnee;
import com.onlinecv.entities.Certification;
import com.onlinecv.entities.CompareUtilisateurExperience;
import com.onlinecv.entities.Competence;
import com.onlinecv.entities.Experience;
import com.onlinecv.entities.Formation;
import com.onlinecv.entities.Langage;
import com.onlinecv.entities.Langue;
import com.onlinecv.entities.Logiciel;
import com.onlinecv.entities.Niveau;
import com.onlinecv.entities.Parcours;
import com.onlinecv.entities.ResumeTechniqueEtFonctionnel;
import com.onlinecv.entities.SystemeExploitation;
import com.onlinecv.entities.Template;
import com.onlinecv.entities.Titre;
import com.onlinecv.entities.Utilisateur;

@Controller
@RequestMapping("/manager")
@Secured("ROLE_MANAGER")
public class ManagerController {

	// STATIC STRING ROOTVAL = NEW STRING();

	// id de l'utilisateur résultat de la recherche

	private Long utilisateurIdPass;

	// IDENTIFICATION DE LA PAGE

	private String pageId;

	static String rootPathFinal = new String();

	@Autowired
	private IUtilisateurJpaRepository utilisateurRepo;

	@Autowired
	private ITemplateJpaRepository templateRepo;

	@Autowired
	private INiveauJpaRepository niveauRepo;

	@Secured("ROLE_MANAGER")
	@RequestMapping("/goToMenuManager")
	public String gotoMenuManager(Model model) {
		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		model.addAttribute("utilisateur", utilisateur);
		return "manager/indexM";
	}

	@Secured("ROLE_MANAGER")
	@RequestMapping("/search")
	public String search(Model model) {

		List<Niveau> listeNiveau = niveauRepo.findAll();
		model.addAttribute("listeNiveau", listeNiveau);

		// IDENTIFICATION DE LA PAGE POUR LIEN DE RETOUR

		pageId = "pageRecherche";
		return "manager/search";
	}

	@RequestMapping("/goToSelection")
	public String goToSelection(Model model, HttpServletRequest request) {
		prepareCVManager(model, request);
		return "selection";
	}

	@RequestMapping("/goToChoixTemplate")
	public String goToChoixTemplate(Model model) {
		Utilisateur utilisateur;
		utilisateur = utilisateurRepo.getOne(utilisateurIdPass);
		model.addAttribute("utilisateur", utilisateur);
		model.addAttribute("pageRecherche", pageId);

		// IDENTIFICATION DE LA PAGE POUR LIEN DE RETOUR

		model.addAttribute("pageId", pageId);

		// RECUPERATION DE LA LISTE DES DES TEMPLATES SITUES DANS LE REPERTOIRE DEDIE

		model.addAttribute("listeTemplates", listTemplatesForFolder());

		return "manager/choixTemplateManager";
	}

	@RequestMapping("/goToChoixTemplate/{utilisateurId}")
	public String goToChoixTemplate(@PathVariable(value = "utilisateurId", required = true) Long utilisateurId,
			Model model, HttpServletRequest request) {
		Utilisateur utilisateur;
		utilisateurIdPass = utilisateurId;
		utilisateur = utilisateurRepo.getOne(utilisateurIdPass);
		model.addAttribute("utilisateur", utilisateur);

		// RECUPERATION DE LA LISTE DES DES TEMPLATES SITUES DANS LE REPERTOIRE DEDIE

		model.addAttribute("listeTemplates", listTemplatesForFolder());

		// INITIALISATION DE L'ATTRIBUT DE SESSION AFFICHAGE POUR SHOW/NO SHOW

		HttpSession session = request.getSession(true);

		// AFFICHAGE AFFICHAGE

		session.setAttribute("affichage", null);

		// IDENTIFICATION DE LA PAGE POUR LIEN DE RETOUR

		model.addAttribute("pageId", pageId);

		return "manager/choixTemplateManager";
	}

	@RequestMapping("/goToTemplate/{templateNom}")
	public String goToTemplateNomTemplate(@PathVariable(value = "templateNom", required = true) String templateNom,
			Model model, HttpServletRequest request) throws DocumentException, IOException {

		prepareCVManager(model, request);

		return "templateCv/" + templateNom;

	}

	public List<Template> listTemplatesForFolder() {
		// Recherche de la racine du chemin utilisateur;
		// RootPath rootPath = new RootPath();
		// rootPath.RecupRootPath();
		// Répertoire où sont situés les fichiers template
		File folder = new File(File.separator + "opt" + File.separator + "tomcat" + File.separator + "webapps"
				+ File.separator + "ProjetOnlineCv" + File.separator + "WEB-INF" + File.separator + "jspf"
				+ File.separator + "templateCv");
		System.out.println("path cv1 = " + folder);

		File[] Files = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File file, String name) {
				if (name.endsWith("jsp")) {
					// filters files whose extension is .jsp
					return true;
				} else {
					return false;
				}
			}
		});

		// Récupère la listes des noms de fichiers dans le répertoire
		for (final File fileEntry : Files) {
			Template template = new Template();
			String nomFileTemplate = fileEntry.getName();
			// Enlève l'extension des noms de fichiers
			String nomTemplate = nomFileTemplate.substring(0, nomFileTemplate.length() - 4);
			// Si le nom du template n'est pas déjà dans la base de données, il est
			// rajouté
			if (templateRepo.findTemplateByNom(nomTemplate) == null) {
				template.setNom(nomTemplate);
				System.out.println("template1: " + template);
				templateRepo.save(template);
			}
		}
		List<Template> listeTemplates = templateRepo.findAll();
		// Trie la liste par ordre alphabétique des noms de template
		Collections.sort(listeTemplates);
		return listeTemplates;
	}

	// RESPONSE BODY TO PASS THE RESULT TO AJAX

	@Secured("ROLE_MANAGER")
	@ResponseBody
	@GetMapping(value = "/searchUsers", produces = "application/json")
	public String searchAjax(Model model, @RequestParam(value = "keyword1") String keyword,
			@RequestParam(value = "domain") String domain,
			@RequestParam(value = "detailCompetence") String detailCompetence,
			@RequestParam(value = "detailLangue") String detailLangue,
			@RequestParam(value = "nombreAnneeExp") String nombreAnneeExp,
			@RequestParam(value = "keyword2") String keyword2, @RequestParam(value = "domain2") String domain2,
			@RequestParam(value = "detailCompetence2") String detailCompetence2,
			@RequestParam(value = "detailLangue2") String detailLangue2,
			@RequestParam(value = "nombreAnneeExp2") String nombreAnneeExp2,
			@RequestParam(value = "CheckRechercheCombinee") Boolean CheckRechercheCombinee,
			@RequestParam(value = "keyword3") String keyword3, @RequestParam(value = "domain3") String domain3,
			@RequestParam(value = "detailCompetence3") String detailCompetence3,
			@RequestParam(value = "detailLangue3") String detailLangue3,
			@RequestParam(value = "nombreAnneeExp3") String nombreAnneeExp3,
			@RequestParam(value = "CheckRechercheCombinee2") Boolean CheckRechercheCombinee2,

			HttpServletRequest request) {

		// CREATION DU'UN STRING JSON VIDE

		String response = "";

		// INITIALISATION DE LA LISTE USER VIDE

		List<Utilisateur> ut = new ArrayList();
		Set<Utilisateur> ut0 = new HashSet();
		Set<Utilisateur> ut1 = new HashSet();

		// KEYWORD, DOMAIN ARE PASSED BY AJAX DATA ATTRIBUTE & RETRIEVED BY REQUESTPARAM
		// IN THE CONTROLLER

		// PREMIERE RECHERCHE

		ut = rechercheUtilisateur(keyword, domain, detailCompetence, detailLangue, nombreAnneeExp);

		// RECHERCHE 2 REQUETES COMBINEES SI LA CHECKBOX CORRESPONDANTE EST COCHEE DANS
		// L'APPLICATION (booléen CheckRechercheCombinee)

		if (CheckRechercheCombinee) {
			List<Utilisateur> utCombine = new ArrayList();

			// DEUXIEME RECHERCHE

			utCombine = rechercheUtilisateur(keyword2, domain2, detailCompetence2, detailLangue2, nombreAnneeExp2);

			// RETOURNE LA LISTE DES UTILISATEURS PRESENTS DANS LES DEUX LISTES A LA FOIS

			ut = intersectionListeUtilisateur(ut, utCombine);

		}

		// RECHERCHE 3 REQUETES COMBINEES SI LES CHECKBOXES CORRESPONDANTES SONT COCHEES
		// DANS L'APPLICATION (booléen CheckRechercheCombinee ET
		// CheckRechercheCombinee2)

		if (CheckRechercheCombinee2) {
			List<Utilisateur> utCombine = new ArrayList();
			List<Utilisateur> utCombine2 = new ArrayList();

			// DEUXIEME RECHERCHE

			utCombine = rechercheUtilisateur(keyword2, domain2, detailCompetence2, detailLangue2, nombreAnneeExp2);

			// RETOURNE LA LISTE DES UTILISATEURS PRESENTS DANS LES DEUX PREMIERES LISTES A
			// LA FOIS

			ut = intersectionListeUtilisateur(ut, utCombine);
			utCombine2 = rechercheUtilisateur(keyword3, domain3, detailCompetence3, detailLangue3, nombreAnneeExp3);

			// RETOURNE LA LISTE DES UTILISATEURS PRESENTS DANS LES TROIS LISTES A LA FOIS

			ut = intersectionListeUtilisateur(ut, utCombine2);

		}
		// CONDITION POUR CLASSER LES UTILISATEURS SELON LE NOMBRE D'ANNEES
		// D'EXPERIENCES SI LES CHAMP LANGAGE, METIER OU LOGICIEL SONT ACTIFS
		// EN CAS CONTRAIRE, CLASSEMENT SELON LE NOM D'UTILISATEUR

		if (domain.equals("langages") || domain.equals("logiciels") || domain.equals("metiers")
				|| domain.equals("entreprises")) {

			Collections.sort(ut, new CompareUtilisateurExperience().reversed());

		} else {
			Collections.sort(ut);
		}

		// CREATE A NEW JACKSON MAPPER TO SERIALIZE THE ARRAYLIST

		ObjectMapper mapper = new ObjectMapper();
		try {

			// PARSE/SERIALIZE THE RESULT ARRAYLIST TO A JSON STRING WITH JACKSON

			response = mapper.writeValueAsString(ut);

		} catch (JsonGenerationException | JsonMappingException e) {

		} catch (IOException e) {

		}
		// TRANSFERER LA REPONSE JSON (STRING) A LA JSP

		HttpSession session = request.getSession();
		session.setAttribute("response", response);

		return response;
	}

	// TRAITEMENT DE LA RECHERCHE D'UN UTILISATEUR, RETOURNE UNE LISTE
	// D'UTILISATEURS

	public List<Utilisateur> rechercheUtilisateur(String keyword, String domain, String detailCompetence,
			String detailLangue, String nombreAnneeExp) {
		List<Utilisateur> ut = new ArrayList();
		Set<Utilisateur> ut0 = new HashSet();
		Set<Utilisateur> ut1 = new HashSet();
		switch (domain) {
		case "competences":

			if (detailCompetence != null && !detailCompetence.equals("")) {

				// QUERY WITH KEYWORD AND DETAIL

				ut = utilisateurRepo.searchByCompetenceLikeAndDetail(keyword, detailCompetence);

			} else {

				// QUERY WITH KEYWORD ONLY

				ut = utilisateurRepo.searchByCompetenceLike(keyword);
				List<Utilisateur> utl;
				String keyword1 = "anglais";

			}
			break;

		case "langues":

			if (detailLangue != null && !detailLangue.equals("")) {

				// QUERY WITH KEYWORD AND DETAIL

				ut = utilisateurRepo.searchByLangueLikeAndDetail(keyword, detailLangue);
			} else {

				// QUERY WITH KEYWORD ONLY

				ut = utilisateurRepo.searchByLangueLike(keyword);

			}
			break;
		case "experiences":

			ut = utilisateurRepo.searchByExperienceLike(keyword);
			break;
		case "nationalites":

			ut = utilisateurRepo.searchByNationaliteLike(keyword);
			break;
		case "formations":

			ut = utilisateurRepo.searchByFormationLike(keyword);
			break;
		case "certifications":

			ut = utilisateurRepo.searchByCertificationLike(keyword);
			break;
		case "names":

			ut = utilisateurRepo.searchByName(keyword);
			break;
		case "metiers":

			// PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE POUR LE METIER

			if (nombreAnneeExp != null) {
				ut1.addAll(utilisateurRepo.searchByMetierLike(keyword));
				ut = listeUtilisateurParNombreAnneesExperience(ut1, keyword, nombreAnneeExp);
			} else {

				// SANS PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE

				ut = utilisateurRepo.searchByMetierLike(keyword);
			}
			break;
		case "entreprises":

			// PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE POUR ENTREPRISE

			if (nombreAnneeExp != null) {
				ut1.addAll(utilisateurRepo.searchByEntrepriseLike(keyword));
				ut = listeUtilisateurParNombreAnneesExperience(ut1, keyword, nombreAnneeExp);
			} else {

				// SANS PRISE EN COMPTE DU NOMBRE D'ANNEES D'ENTREPRISE

				ut = utilisateurRepo.searchByEntrepriseLike(keyword);
			}
			break;
		case "langages":

			// PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE POUR LE LANGAGE

			if (nombreAnneeExp != null) {
				ut1.addAll(utilisateurRepo.searchByLangageExperienceLike(keyword));
				ut = listeUtilisateurParNombreAnneesExperience(ut1, keyword, nombreAnneeExp);
			} else {

				// SANS PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE, RECHERCHE DANS
				// "EXPERIENCE & RESUME" TABLES DANS HASHSET POUR EVITER DOUBLONS

				ut0.addAll(utilisateurRepo.searchByLangageResumeLike(keyword));
				ut0.addAll(utilisateurRepo.searchByLangageExperienceLike(keyword));
				ut = new ArrayList<>(ut0);
			}
			break;
		case "logiciels":

			// PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE POUR LE LOGICIEL

			if (nombreAnneeExp != null) {
				ut1.addAll(utilisateurRepo.searchByLogicielExperienceLike(keyword));

				ut = listeUtilisateurParNombreAnneesExperience(ut1, keyword, nombreAnneeExp);

			} else {
				// SANS PRISE EN COMPTE DU NOMBRE D'ANNEES D'EXPERIENCE, RECHERCHE DANS
				// "EXPERIENCE & RESUME" TABLES DANS HASHSET POUR EVITER DOUBLONS

				ut0.addAll(utilisateurRepo.searchByLogicielResumeLike(keyword));
				ut0.addAll(utilisateurRepo.searchByLogicielExperienceLike(keyword));
				ut = new ArrayList<>(ut0);
			}
			break;
		case "baseDeDonnees":

			// RECHERCHE DANS "EXPERIENCE & RESUME" TABLES DANS HASHSET POUR EVITER DOUBLONS

			ut0.addAll(utilisateurRepo.searchByBaseDeDonneeResumeLike(keyword));
			ut0.addAll(utilisateurRepo.searchByBaseDeDonneeExperienceLike(keyword));
			ut = new ArrayList<>(ut0);
			break;
		case "systemeExploitation":
			// RECHERCHE DANS "EXPERIENCE & RESUME" TABLES DANS HASHSET POUR EVITER DOUBLONS

			ut0.addAll(utilisateurRepo.searchBySystemeExploitationExperienceLike(keyword));
			ut0.addAll(utilisateurRepo.searchBySystemeExploitationResumeLike(keyword));
			ut = new ArrayList<>(ut0);
			break;
		case "all":

			// RECHERCHE DANS TOUS LES TABLES DANS HASHSET POUR EVITER DOUBLONS

			ut0.addAll(utilisateurRepo.searchByBaseDeDonneeExperienceLike(keyword));
			ut0.addAll(utilisateurRepo.searchByBaseDeDonneeResumeLike(keyword));
			ut0.addAll(utilisateurRepo.searchByCertificationLike(keyword));
			ut0.addAll(utilisateurRepo.searchByCompetenceLike(keyword));
			ut0.addAll(utilisateurRepo.searchByEntrepriseLike(keyword));
			ut0.addAll(utilisateurRepo.searchByExperienceLike(keyword));
			ut0.addAll(utilisateurRepo.searchByFormationLike(keyword));
			ut0.addAll(utilisateurRepo.searchByLangageExperienceLike(keyword));
			ut0.addAll(utilisateurRepo.searchByLangageResumeLike(keyword));
			ut0.addAll(utilisateurRepo.searchByLangueLike(keyword));
			ut0.addAll(utilisateurRepo.searchByLogicielExperienceLike(keyword));
			ut0.addAll(utilisateurRepo.searchByLogicielResumeLike(keyword));
			ut0.addAll(utilisateurRepo.searchByMetierLike(keyword));
			ut0.addAll(utilisateurRepo.searchByName(keyword));
			ut0.addAll(utilisateurRepo.searchByNationaliteLike(keyword));
			ut0.addAll(utilisateurRepo.searchBySystemeExploitationExperienceLike(keyword));
			ut0.addAll(utilisateurRepo.searchBySystemeExploitationResumeLike(keyword));
			ut = new ArrayList<>(ut0);
			break;
		case "default":

			ut = utilisateurRepo.findAll();
			break;
		}

		return ut;
	}

	// METHODE QUI RETOURNE UNE LISTE D'UTILISATEURS POSSEDANT UNE APTITUDE
	// (keyword) SELON UN CERTAIN NOMBRE D'ANNEES (nombreAnneeExp).

	// CETTE METHODE PREND EN ENTREE UNE LISTE D'UTILISATEURS A PARTIR DE LA QUELLE
	// LA RECHERCHE SELON L'APTITUDE ET LE NOMBRE D'ANNEES VA ETRE REALISEE.

	public List<Utilisateur> listeUtilisateurParNombreAnneesExperience(Set<Utilisateur> listeUtilisateurs,
			String keyword, String nombreAnneeExp) {
		List<Utilisateur> ut = new ArrayList();
		for (Utilisateur u : listeUtilisateurs) {
			List<Experience> listExp = u.getParcours().getListeExperience();
			int dureeExperienceTot = 0;
			for (Experience exp : listExp) {

				// CONDITION POUR SAVOIR SI L'EXPERIENCE POSSEDE LE LANGAGE, LE LOGICIEL OU LE
				// METIER CHERCHE
				// CONDITION SUR LA LISTE DE LANGAGES OU DE LOGICIELS ASSOCIEE A L'EXPERIENCE
				// QUI EN CONTIENT UN.

				// NOM DE LANGAGE, LOGICIEL OU METIER EGAL AU MOT CLE DE LA RECHERCHE
				// LA CASSE N'EST PAS PRISE EN COMPTE (utilisation de (?i) dans le regex) ET SI
				// LE KEYWORD EST CONTENU DANS LA CHAINE DE CARACTERE
				// DU NOM DE LANGAGE, METIER..., L'EXPERIENCE EST PRISE EN COMPTE.

				if ((exp.getListeLangage().stream().anyMatch(o -> o.getNom().matches("(?i).*" + keyword + ".*")))
						|| (exp.getListeLogiciel().stream()
								.anyMatch(o -> o.getNom().matches("(?i).*" + keyword + ".*")))
						|| (exp.getMetier().getNom().matches("(?i).*" + keyword + ".*"))
						|| (exp.getEntreprise().getNom().matches("(?i).*" + keyword + ".*"))) {

					// CONVERSION DES OBJETS "DATE" (DateDebut et DateFin) EN OBJET LOCALDATE POUR
					// POUVOIR FACILLEMENT DETERMINER LA DUREE D'UNE EXPERIENCE EN ANNEES.

					int dureeExperience;
					if (exp.getDateDebut() != null || exp.getDateFin() != null) {
						LocalDate localDateDebut = exp.getDateDebut().toInstant().atZone(ZoneId.systemDefault())
								.toLocalDate();
						LocalDate localDateFin = exp.getDateFin().toInstant().atZone(ZoneId.systemDefault())
								.toLocalDate();

						// DUREE D'UNE EXPERIENCE EN MOIS.

						Period period = Period.between(localDateDebut, localDateFin);
						dureeExperience = period.getYears() * 12 + period.getMonths();
					} else {
						dureeExperience = 0;
					}

					dureeExperienceTot += dureeExperience;

				}
			}

			// DUREE D'UNE EXPERIENCE EN ANNEES.

			double dureeExperienceTotAnnee = (double) dureeExperienceTot / 12;

			// POUR POUVOIR TRIER EN FONCTION DE L'EXPERIENCE, ON ASSOCIE POUR CHAQUE
			// UTILISATEUR DE LA LISTE UNE DUREE D'EXPERIENCE EN ANNEE

			u.setNbAnneeExperience(dureeExperienceTotAnnee);

			switch (nombreAnneeExp) {
			case "Moins1An":
				if (dureeExperienceTotAnnee < 1 && dureeExperienceTotAnnee != 0) {

					ut.add(u);

				}
				break;
			case "1AnA2Ans":
				if (1 <= dureeExperienceTotAnnee && dureeExperienceTotAnnee < 2) {
					ut.add(u);
				}
				break;
			case "2AnsA3Ans":
				if (2 <= dureeExperienceTotAnnee && dureeExperienceTotAnnee < 3) {
					ut.add(u);
				}
				break;
			case "3AnsA5Ans":
				if (3 <= dureeExperienceTotAnnee && dureeExperienceTotAnnee < 5) {
					ut.add(u);
				}
				break;
			case "PlusDe5Ans":
				if (5 <= dureeExperienceTotAnnee) {
					ut.add(u);
				}
				break;
			default:
				ut.add(u);
			}
		}

		return ut;
	}

	// INTERSECTION DE LISTES. RETOURNE LA LISTE DES UTILISATEURS PRESENT DANS LES 2
	// LISTES.

	public <Utilisateur> List<Utilisateur> intersectionListeUtilisateur(List<Utilisateur> list1,
			List<Utilisateur> list2) {
		List<Utilisateur> list = new ArrayList<Utilisateur>();

		for (Utilisateur ut : list1) {
			if (list2.contains(ut)) {
				list.add(ut);
			}
		}
		return list;
	}

	@Transactional
	public void prepareCVManager(Model model, HttpServletRequest request) {

		/// GET "UTILISATEUR" DE LA BDD. ///

		Utilisateur utilisateur = utilisateurRepo.getOne(utilisateurIdPass);
		model.addAttribute("utilisateur", utilisateur);

		/// GET "PARCOURS" ///

		Parcours parcours = utilisateur.getParcours();
		List<Langue> listeLangue;
		List<Formation> listeFormation;
		List<Experience> listeExperience;
		List<Competence> listeCompetence;
		List<Certification> listeCertification;
		List<Titre> listeTitre;
		List<Langage> listeLangage;
		List<Logiciel> listeLogiciel;
		List<BaseDeDonnee> listeBaseDeDonnee;
		List<SystemeExploitation> listeSystemeExploitation;
		ResumeTechniqueEtFonctionnel resumeTechniqueEtFonctionnel;

		if (parcours != null) {

			/// GET "LISTE LANGUE" ///

			listeLangue = parcours.getListeLangue();
			if (listeLangue == null)
				listeLangue = new ArrayList();

			/// GET "LISTE FORMATION" ///

			listeFormation = parcours.getListeFormation();
			if (listeFormation == null)
				listeFormation = new ArrayList();

			/// GET LISTE EXPERIENCE ///

			listeExperience = parcours.getListeExperience();
			if (listeExperience == null)
				listeExperience = new ArrayList();

			/// GET LISTE COMPETENCE ///

			listeCompetence = parcours.getListeCompetence();
			if (listeCompetence == null)
				listeCompetence = new ArrayList();

			/// GET LISTE CERTIFICATION ///

			listeCertification = parcours.getListeCertification();
			if (listeCertification == null)
				listeCertification = new ArrayList();

			/// GET LISTE TITRE ///

			listeTitre = parcours.getListeTitre();
			if (listeTitre == null)
				listeTitre = new ArrayList();

			/// GET RESUME TECHNIQUE ET FONCTIONNEL ///

			resumeTechniqueEtFonctionnel = parcours.getResumeTechniqueEtFonctionnel();
			if (resumeTechniqueEtFonctionnel == null)
				resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();

			/// GET LISTE LANGAGE ///

			listeLangage = resumeTechniqueEtFonctionnel.getListeLangage();
			if (listeLangage == null) {
				listeLangage = new ArrayList<Langage>();
			}
			/// GET LISTE LOGICIEL ///

			listeLogiciel = resumeTechniqueEtFonctionnel.getListeLogiciel();
			if (listeLogiciel == null) {
				listeLogiciel = new ArrayList<Logiciel>();
			}
			/// GET LISTE BDD ///

			listeBaseDeDonnee = resumeTechniqueEtFonctionnel.getListeBaseDeDonnee();
			if (listeBaseDeDonnee == null) {
				listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			}
			/// GET LISTE SYSTEME D'EXPLOITATION ///

			listeSystemeExploitation = resumeTechniqueEtFonctionnel.getListeSystemeExploitation();
			if (listeSystemeExploitation == null) {
				listeSystemeExploitation = new ArrayList<SystemeExploitation>();
			}
		} else {
			parcours = new Parcours();
			listeLangue = new ArrayList();
			listeFormation = new ArrayList();
			listeExperience = new ArrayList();
			listeCompetence = new ArrayList();
			listeCertification = new ArrayList();
			listeTitre = new ArrayList();
			resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
			listeLangage = new ArrayList<Langage>();
			listeLogiciel = new ArrayList<Logiciel>();
			listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			listeSystemeExploitation = new ArrayList<SystemeExploitation>();
		}
		model.addAttribute("listeLangue", listeLangue);
		model.addAttribute("listeFormation", listeFormation);
		Collections.sort(listeExperience, Collections.reverseOrder());
		model.addAttribute("listeExperience", listeExperience);
		model.addAttribute("listeCompetence", listeCompetence);
		model.addAttribute("listeCertification", listeCertification);
		model.addAttribute("listeTitre", listeTitre);
		model.addAttribute("resumeTechniqueEtFonctionnel", resumeTechniqueEtFonctionnel);
		model.addAttribute("listeLangage", listeLangage);
		model.addAttribute("listeLogiciel", listeLogiciel);
		model.addAttribute("listeBaseDeDonnee", listeBaseDeDonnee);
		model.addAttribute("listeSystemeExploitation", listeSystemeExploitation);

		//// INITIALISATION DE L'ATTRIBUT DE SESSION AFFICHAGE

		HttpSession session = request.getSession(true);
		Affichage affichage;

		if ((Affichage) session.getAttribute("affichage") == null) {
			affichage = new Affichage();

			// INITIALISATION DE LA LISTE DES EXPERIENCES

			List<Long> listeExperienceId = new ArrayList<Long>();
			for (int i = 0; i < listeExperience.size(); i++) {
				listeExperienceId.add(listeExperience.get(i).getId());
			}
			affichage.setListeExperienceId(listeExperienceId);

			// INITIALISATION DE LA LISTE DES TITRES

			List<Long> listeTitreId = new ArrayList<Long>();
			for (int i = 0; i < listeTitre.size(); i++) {
				listeTitreId.add(listeTitre.get(i).getId());
			}
			affichage.setListeTitreId(listeTitreId);

			// INITIALISATION DE LA LISTE DES COMPETENCES

			List<Long> listeCompetenceId = new ArrayList<Long>();
			for (int i = 0; i < listeCompetence.size(); i++) {
				listeCompetenceId.add(listeCompetence.get(i).getId());
			}
			affichage.setListeCompetenceId(listeCompetenceId);

			// INITIALISATION DE LA LISTE DES FORMATIONS

			List<Long> listeFormationId = new ArrayList<Long>();
			for (int i = 0; i < listeFormation.size(); i++) {
				listeFormationId.add(listeFormation.get(i).getId());
			}
			affichage.setListeFormationId(listeFormationId);

			// INITIALISATION DE LA LISTE DES LANGUES

			List<Long> listeLangueId = new ArrayList<Long>();
			for (int i = 0; i < listeLangue.size(); i++) {
				listeLangueId.add(listeLangue.get(i).getId());
			}
			affichage.setListeLangueId(listeLangueId);

			// INITIALISATION DE LA LISTE DES CERTIFICATIONS

			List<Long> listeCertificationId = new ArrayList<Long>();
			for (int i = 0; i < listeCertification.size(); i++) {
				listeCertificationId.add(listeCertification.get(i).getId());
			}
			affichage.setListeCertificationId(listeCertificationId);

			// INITIALISATION DE LA LISTE DES LANGAGES

			List<Long> listeLangageId = new ArrayList<Long>();
			for (int i = 0; i < listeLangage.size(); i++) {
				listeLangageId.add(listeLangage.get(i).getId());
			}
			affichage.setListeLangageId(listeLangageId);

			// INITIALISATION DE LA LISTE DES LOGICIELS

			List<Long> listeLogicielId = new ArrayList<Long>();
			for (int i = 0; i < listeLogiciel.size(); i++) {
				listeLogicielId.add(listeLogiciel.get(i).getId());
			}
			affichage.setListeLogicielId(listeLogicielId);

			// INITIALISATION DE LA LISTE DES BASES DE DONNEES

			List<Long> listeBaseDeDonneeId = new ArrayList<Long>();
			for (int i = 0; i < listeBaseDeDonnee.size(); i++) {
				listeBaseDeDonneeId.add(listeBaseDeDonnee.get(i).getId());
			}
			affichage.setListeBaseDeDonneeId(listeBaseDeDonneeId);

			// INITIALISATION DE LA LISTE DES SYSTEMES D'EXPLOITATIONS

			List<Long> listeSystemeExploitationId = new ArrayList<Long>();
			for (int i = 0; i < listeSystemeExploitation.size(); i++) {
				listeSystemeExploitationId.add(listeSystemeExploitation.get(i).getId());
			}
			affichage.setListeSystemeExploitationId(listeSystemeExploitationId);

			session.setAttribute("affichage", affichage);
		} else
			affichage = (Affichage) session.getAttribute("affichage");
		model.addAttribute("affichage", affichage);

	}

	@RequestMapping("/sauveAffichage")
	public String sauveAffichage(Model model, HttpServletRequest request,
			@ModelAttribute(value = "affichage") Affichage affichage, BindingResult result) {
		prepareCVManager(model, request);

		// RECUPERE LA LISTE DES TEMPLATES SITUES DANS LE REPERTOIRE DEDIE

		model.addAttribute("listeTemplates", listTemplatesForFolder());
		return "manager/choixTemplateManager";
	}
}
