package com.onlinecv.controllers;

import java.io.FileOutputStream;
import java.io.IOException;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

public class PdfConverter {
	public static void main(String[] args) throws IOException {

		// PDF CREATION IS WORKING AND SAVING INTO PROJECT DIRECTORY. CREATE EMPTY A4
		// DOCUMENT.

		Document document = new Document(PageSize.A4);
		String filename = "test.pdf";

		// DOCUMENT META

		document.addAuthor("Ausy FR");
		document.addTitle("Ausy CV");
		System.out.println("Document created");
		try {
			PdfWriter.getInstance(document, new FileOutputStream(filename));
			System.out.println("writer instance created...");

			// OPEN DOCUMENT

			document.open();
			System.out.println("Document open...");

			// NEW "PARAGRAPH"

			Paragraph p = new Paragraph("Hello world");

			// ADD "PARAGRAPH" TO DOC

			document.add(p);
			System.out.println("P added to document...");

		} catch (Exception e) {
			System.out.println(e);
		}
		// CLOSE DOCUMENT

		document.close();

		// STEP 1

	}

}