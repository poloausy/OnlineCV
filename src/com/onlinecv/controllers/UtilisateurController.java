package com.onlinecv.controllers;

import java.io.File;
import java.io.FilenameFilter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.hibernate.Session;
import org.hibernate.Transaction;
//import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.mail.MailSender;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.itextpdf.io.IOException;
import com.itextpdf.text.DocumentException;
import com.onlinecv.dao.ICertificationJpaRepository;
import com.onlinecv.dao.ICompetenceJpaRepository;
import com.onlinecv.dao.ICredentialJpaRepository;
import com.onlinecv.dao.IExperienceJpaRepository;
import com.onlinecv.dao.IFormationJpaRepository;
import com.onlinecv.dao.ILangueJpaRepository;
import com.onlinecv.dao.INationaliteJpaRepository;
import com.onlinecv.dao.IParcoursJpaRepository;
import com.onlinecv.dao.ITemplateJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Affichage;
import com.onlinecv.entities.BaseDeDonnee;
import com.onlinecv.entities.Certification;
import com.onlinecv.entities.Competence;
import com.onlinecv.entities.Credential;
import com.onlinecv.entities.ERole;
import com.onlinecv.entities.Experience;
import com.onlinecv.entities.Formation;
import com.onlinecv.entities.Langage;
import com.onlinecv.entities.Langue;
import com.onlinecv.entities.Logiciel;
import com.onlinecv.entities.MailMail;
import com.onlinecv.entities.Parcours;
import com.onlinecv.entities.ResumeTechniqueEtFonctionnel;
import com.onlinecv.entities.SystemeExploitation;
import com.onlinecv.entities.Template;
import com.onlinecv.entities.Titre;
import com.onlinecv.entities.Utilisateur;

@Controller
@RequestMapping("/visiteur")
public class UtilisateurController {

	private MailSender mailSender;

	@Autowired
	private IUtilisateurJpaRepository utilisateurRepo;

	@Autowired
	private ICredentialJpaRepository credentialRepo;

	@Autowired
	private IParcoursJpaRepository parcoursRepo;

	@Autowired
	private ILangueJpaRepository langueRepo;

	@Autowired
	private INationaliteJpaRepository nationaliteRepo;

	@Autowired
	private ICompetenceJpaRepository competenceRepo;

	@Autowired
	private ICertificationJpaRepository certificationRepo;

	@Autowired
	private IExperienceJpaRepository experienceRepo;

	@Autowired
	private IFormationJpaRepository formationRepo;

	@Autowired
	private ITemplateJpaRepository templateRepo;

	Session session = null;
	Transaction transaction = null;

	@Secured("ROLE_UTILISATEUR")
	@RequestMapping("/goToMenuUtilisateur")
	public String gotoMenuUtilisateur(Model model) {
		Credential credential = credentialRepo.getOne(AuthHelper.getPrincipal().getCredential().getId());
		model.addAttribute("credential", credential);
		return "menuUtilisateur";
	}

	@GetMapping("/goToCreer")
	public String goToCreer(Model model) {
		model.addAttribute("utilisateur", new Utilisateur());
		return "inscription";
	}

	@PostMapping("/creer")
	public String creer(@Valid @ModelAttribute(value = "utilisateur") Utilisateur utilisateur, BindingResult result,
			Model model) {

		if (!result.hasErrors()) {

			String nomLogin = utilisateur.getCredential().getNomLogin();
			String email = utilisateur.getCredential().getEmail();
			Credential other = null;
			Credential other1 = null;
			Utilisateur otherUtilisateur = null;
			Long id = utilisateur.getId();

			if (id == null) { // creation d'un compte

				other = credentialRepo.findByNomLogin(nomLogin);
				other1 = credentialRepo.findByEmail(email);

			} else { // modification d'un compte
				otherUtilisateur = utilisateurRepo.findUtilisateurByIdNotAndCredentialNomLogin(id, nomLogin);
			}

			if (other != null) {
				result.rejectValue("credential.nomLogin", "error.credential.nomLogin.doublon");
			}
			if (other1 != null) {
				result.rejectValue("credential.email", "error.credential.email.doublon");
			}
		}

		if (!result.hasErrors()) {

			encodePassword(utilisateur.getCredential());
			utilisateur.getCredential().setRole(ERole.ROLE_UTILISATEUR);

			// Envoi d'un mail de confirmation de l'inscription
			ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");
			MailMail mm = (MailMail) context.getBean("mailMail");
			String urlPageAccueil = "http://onlinecv.ausy.fr:8080/ProjetOnlineCv";

			mm.sendMail("gestioncvausy@gmail.com", utilisateur.getCredential().getEmail(),
					"Inscription au site CV création",

					"Bonjour " + utilisateur.getCredential().getNomLogin()
							+ ", \n bienvenue sur notre site CV Créator. \n Votre inscription a bien été prise en compte."
							+ "\n Vous pouvez maintenant vous identifier avec votre Login et votre Mot de passe, à partir du lien suivant. \n"
							+ urlPageAccueil + "\n\n Cordialement \n L'équipe CV Création");

			utilisateurRepo.save(utilisateur);

			model.addAttribute("utilisateur", new Utilisateur());
			if (AuthHelper.isAuthenticated())
				return "menuUtilisateur";
			else
				model.addAttribute("message", "inscriptionOK");
			return "accueil";
		} else {
			return "inscription";
		}
	}

	private static void encodePassword(Credential credential) {
		String rawPassword = credential.getMotDePasse();
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodedPassword = encoder.encode(rawPassword);
		credential.setMotDePasse(encodedPassword);
	}

	@RequestMapping("/motDePasseOublie")
	public String motDePasseOublie(Model model, @ModelAttribute(value = "credential") Credential credential) {

		// V�rification que le mail renseigne corresponds a un mail utilisateur en
		// base
		if (credentialRepo.findByEmail(credential.getEmail()) == null) {
			model.addAttribute("message", "messageMailKo");
			return "accueil";
		}
		// Envoi d'un mail contenant le lien pour la modification du mot de passe
		else {
			model.addAttribute("message", "messageMailOk");// message d'information a utilisateur de l'envoi d'un mail
															// avec lien pour reinitialiser le mot de passe
			ApplicationContext context = new ClassPathXmlApplicationContext("Spring-Mail.xml");
			MailMail mm = (MailMail) context.getBean("mailMail");
			Long id = credentialRepo.findByEmail(credential.getEmail()).getId();

			// login correspondant a l'Email renseigne dans le formulaire
			String login = (credentialRepo.findByEmail(credential.getEmail())).getNomLogin();
			String urlModifMotDePasse = "http://onlinecv.ausy.fr:8080/ProjetOnlineCv/visiteur/goToReinitialiserMotDePasse?id="
					+ id;

			mm.sendMail("gestioncvausy@gmail.com", credential.getEmail(), "Réinitialisation du mot de passe CV créator",
					"Bonjour " + login + ", \n vous avez demandé de réinitialiser votre mot de passe."
							+ "\n Cliquez sur le  lien suivant pour la réinitialisation votre mot de passe. \n"
							+ urlModifMotDePasse + "\n \n Cordialement \n L'équipe CV Création");

			return "accueil";
		}
	}

	// MOT DE PASSE OUBLIE
	@RequestMapping("/goToReinitialiserMotDePasse")
	public String goToReinitialiserMotDePasse(Model model, @RequestParam Long id) {
		model.addAttribute("credential", new Credential());

		return "reinitialisationMotDePasse";
	}

	@RequestMapping("/goToSupprimerProfil")
	public String goToSupprimerProfil(Model model) {
		model.addAttribute("credential", new Credential());

		return "supprimer-profil";
	}

	// MOT DE PASSE OUBLIE
	@RequestMapping("/reinitialiserMotDePasse")
	public String reinitialiserMotDePasse(@Valid @ModelAttribute(value = "credential") Credential credential,
			BindingResult result, Model model,
			@RequestParam(value = "confirmationMotDePasse") String confirmationMotDePasse, @RequestParam Long id) {

		String nomLogin = credential.getNomLogin();
		String motDePasse = credential.getMotDePasse();
		model.addAttribute("message", "messageMailKo");

		Long idFormulaire = null;
		// IdFormulaire est renseign� que si un login existant dans la base est
		// rentr�
		// par l'utilisateur
		if (credentialRepo.findByNomLogin(nomLogin) != null) {
			idFormulaire = (credentialRepo.findByNomLogin(nomLogin)).getId();
		}

		// Verification que le login renseigne existe dans la base de donnee
		if (idFormulaire == null) {
			model.addAttribute("message", "messageErreurLogin");
			return "reinitialisationMotDePasse";
		}
		// Verification que l'id correspondant au login renseign� dans le formulaire
		// correspond a l'id du mail envoye a l'utilisateur pour la reinitialisation
		// de son mot de passe
		if (id != idFormulaire) {
			model.addAttribute("message", "messageErreurLogin");
			return "reinitialisationMotDePasse";
		}

		// Vérification que le mot de passe et la confirmation du mot de passe sont
		// identiques.
		if (!motDePasse.equals(confirmationMotDePasse)) {
			model.addAttribute("message", "messageMotsDePasseNonIdentiques");
			return "reinitialisationMotDePasse";
		} else

		{
			// Modification du mot de passe pour le credential de l'utilisateur
			Credential credentialAModifier = credentialRepo.findByNomLogin(credential.getNomLogin());
			credentialAModifier.setMotDePasse(motDePasse);
			// Mot de passe encode
			encodePassword(credentialAModifier);
			// nouveau mot de passe sauve
			credentialRepo.save(credentialAModifier);
			model.addAttribute("message", "messageMotDePasseReinitialise");
			return "accueil";
		}
	}

	// MOT DE PASSE OUBLIE
	@RequestMapping("/goToChangerMotDePasse")
	public String goToChangerMotDePasse(Model model, @RequestParam Long id) {
		model.addAttribute("credential", new Credential());

		return "reinitialisationMotDePasse";
	}

	// CHANGEMENT MOT DE PASSE EN ETANT CONNECTE
	@RequestMapping("/changerMotDePasse")
	public String changerMotDePasse(@Valid @ModelAttribute(value = "credential") Credential credential,
			BindingResult result, @RequestParam(value = "nouveauMotDePasse") String nouveauMotDePasse,
			@RequestParam(value = "confirmationMotDePasse") String confirmationMotDePasse, Model model) {

		String motDePasseActuel = credential.getMotDePasse();
		// R�cup�ration du credential de l'utilisateur connecte
		Credential credentialConnecte = (AuthHelper.getUtilisateur().getCredential());
		String motDePasseDB = credentialConnecte.getMotDePasse();
		// Classe pour cryptage du mot de passe
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

		model.addAttribute("motDePasseActuel", motDePasseActuel);

		model.addAttribute("nouveauMotDePasse", nouveauMotDePasse);

		model.addAttribute("confirmationMotDePasse", confirmationMotDePasse);

		model.addAttribute("credential", credential);

		// V�rification que le mot de passe tape par l'utilisateur et celui de la
		// session (encode dans la base de donnees) sont identiques
		if (!encoder.matches(motDePasseActuel, motDePasseDB)) {
			model.addAttribute("message", "messageErreurMotDePasse");
			return "menuUtilisateur";
		} else {
			// V�rification que le nouveau mot de passe et la confirmation du mot de passe
			// sont identiques.
			if (!nouveauMotDePasse.equals(confirmationMotDePasse)) {
				model.addAttribute("message", "messageMotsDePasseNonIdentiques");
				return "menuUtilisateur";

			} else {
				// Modification du mot de passe pour le credential de l'utilisateur
				credentialConnecte.setMotDePasse(nouveauMotDePasse);
				// Mot de passe encode
				encodePassword(credentialConnecte);
				// nouveau mot de passe sauve
				credentialRepo.save(credentialConnecte);
				model.addAttribute("message", "messageMotDePasseModifie");
				return "menuUtilisateur";
			}
		}
	}

	@Transactional
	@RequestMapping("/supprimerProfil")
	public String supprimerProfil(Model model,
			@RequestParam(value = "confirmationMotDePasse") String confirmationMotDePasse, HttpServletRequest request,
			HttpServletResponse response) {

		// Get current user
		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());

		// Get current user credential password
		String password = utilisateur.getCredential().getMotDePasse();

		// get current parcours
		Parcours parcours = utilisateur.getParcours();

		// get current cred
		Credential credential = utilisateur.getCredential();

		// password match
		if (BCrypt.checkpw(confirmationMotDePasse, password)) {

			// profile has languages, competences,...
			if (parcours != null) {

				deleteUserAndFK(utilisateur, credential, parcours);
			}

			// no profile settings (languages, competences,..) -> delete user
			else {

				deleteUser(utilisateur, credential);
			}
			// logout user, call to logout URL (this is the default Spring Security method)
			// String redirectUrl = "/logout";
			// return "redirect:" + redirectUrl;

			// logout user
			Authentication auth = SecurityContextHolder.getContext().getAuthentication();
			new SecurityContextLogoutHandler().logout(request, response, auth);

			// show supprimé page
			return "supprime";
		}

		// no password match -> return to previous page
		else {

			// user error message
			model.addAttribute("message", "erreurIdentification");
			return "supprimer-profil";
		}
	}

	// delete user without parcours
	public void deleteUser(Utilisateur utilisateur, Credential credential) {
		// Delete current user
		utilisateurRepo.delete(utilisateur);

	}

	// delete user and all Foreign Keys
	public void deleteUserAndFK(Utilisateur utilisateur, Credential credential, Parcours parcours) {
		System.out.println("Delete user and profile...");
		// everything is cascaded, when you delete the user, parcours, languages are
		// deleted too.
		utilisateurRepo.delete(utilisateur);
	}

	@RequestMapping("/goToChoixTemplate")
	public String goToChoixTemplate(Model model) {
		Utilisateur utilisateur = AuthHelper.getUtilisateur();
		model.addAttribute("utilisateur", utilisateur);
		// Récupère la liste des templates situés dans le répertoire dédié
		model.addAttribute("listeTemplates", listTemplatesForFolder());
		return "choixTemplate";
	}

	// Retourne la liste des templates situés dans le répertoire dédié
	public List<Template> listTemplatesForFolder() {
		// Recherche de la racine du chemin utilisateur;
		// RootPath rootPath = new RootPath();
		// rootPath.RecupRootPath();
		// Répertoire où sont situés les fichiers template
		File folder = new File(File.separator + "opt" + File.separator + "tomcat" + File.separator + "webapps"
				+ File.separator + "ProjetOnlineCv" + File.separator + "WEB-INF" + File.separator + "jspf"
				+ File.separator + "templateCv");
		System.out.println("path cv1 = " + folder);

		File[] Files = folder.listFiles(new FilenameFilter() {
			@Override
			public boolean accept(File file, String name) {
				if (name.endsWith("jsp")) {
					// filters files whose extension is .jsp
					return true;
				} else {
					return false;
				}
			}
		});

		// Récupère la listes des noms de fichiers dans le répertoire
		for (final File fileEntry : Files) {
			Template template = new Template();
			String nomFileTemplate = fileEntry.getName();
			// Enlève l'extension des noms de fichiers
			String nomTemplate = nomFileTemplate.substring(0, nomFileTemplate.length() - 4);
			// Si le nom du template n'est pas déjà dans la base de données, il est
			// rajouté
			if (templateRepo.findTemplateByNom(nomTemplate) == null) {
				template.setNom(nomTemplate);
				System.out.println("template1: " + template);
				templateRepo.save(template);
			}
		}
		List<Template> listeTemplates = templateRepo.findAll();
		// Trie la liste par ordre alphabétique des noms de template
		Collections.sort(listeTemplates);
		return listeTemplates;
	}

	@RequestMapping("/goToTemplate/{templateNom}")
	public String goToTemplateNomTemplate(@PathVariable(value = "templateNom", required = true) String templateNom,
			Model model, HttpServletRequest request) throws DocumentException, IOException {
		System.out.println("template nom = " + templateNom);
		prepareCV(model, request);

		return "templateCv/" + templateNom;

	}

	@RequestMapping("/goToSelection")
	public String goToSelection(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		prepareCV(model, request);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		session.setAttribute("affichage", affichage);
		model.addAttribute("affichage", affichage);
		model.addAttribute("champActif", "infoPersonnel");
		return "selection";
	}

	@RequestMapping("/sauveAffichage")
	public String sauveAffichage(Model model, HttpServletRequest request,
			@ModelAttribute(value = "affichage") Affichage affichage, BindingResult result) {
		// request.getSession().setAttribute("affichage", affichage);
		prepareCV(model, request);

		// Récupère la liste des templates situés dans le répertoire dédié
		model.addAttribute("listeTemplates", listTemplatesForFolder());
		return "choixTemplate";
	}

	/**
	 * Verifier la validite d'un mail
	 */
	public static Boolean VerifMail(String email) {
		Boolean bValide = false;
		Pattern patTest = Pattern.compile("^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,4}$");
		Matcher matchTest = patTest.matcher(email.toUpperCase());
		bValide = matchTest.matches();
		return bValide;
	}

	@Transactional
	public void prepareCV(Model model, HttpServletRequest request) {
		/// get utilisateur from database ///
		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		model.addAttribute("utilisateur", utilisateur);

		/// get parcours ///
		Parcours parcours = utilisateur.getParcours();
		List<Langue> listeLangue;
		List<Formation> listeFormation;
		List<Experience> listeExperience;
		List<Competence> listeCompetence;
		List<Certification> listeCertification;
		List<Titre> listeTitre;
		List<Langage> listeLangage;
		List<Logiciel> listeLogiciel;
		List<BaseDeDonnee> listeBaseDeDonnee;
		List<SystemeExploitation> listeSystemeExploitation;
		ResumeTechniqueEtFonctionnel resumeTechniqueEtFonctionnel;

		if (parcours != null) {
			/// get listeLangue ///
			listeLangue = parcours.getListeLangue();
			if (listeLangue == null)
				listeLangue = new ArrayList();
			/// get listeFormation ///
			listeFormation = parcours.getListeFormation();
			if (listeFormation == null)
				listeFormation = new ArrayList();
			/// get listeExperience ///
			listeExperience = parcours.getListeExperience();
			if (listeExperience == null)
				listeExperience = new ArrayList();
			/// get listeCompetence ///
			listeCompetence = parcours.getListeCompetence();
			if (listeCompetence == null)
				listeCompetence = new ArrayList();
			/// get listeCertification ///
			listeCertification = parcours.getListeCertification();
			if (listeCertification == null)
				listeCertification = new ArrayList();
			/// get listeTitre ///
			listeTitre = parcours.getListeTitre();
			if (listeTitre == null)
				listeTitre = new ArrayList();
			/// get resumeTechniqueEtFonctionnel ///
			resumeTechniqueEtFonctionnel = parcours.getResumeTechniqueEtFonctionnel();
			if (resumeTechniqueEtFonctionnel == null)
				resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
			/// get listelangage ///
			listeLangage = resumeTechniqueEtFonctionnel.getListeLangage();
			if (listeLangage == null) {
				listeLangage = new ArrayList<Langage>();
			}
			/// get listelogiciel ///
			listeLogiciel = resumeTechniqueEtFonctionnel.getListeLogiciel();
			if (listeLogiciel == null) {
				listeLogiciel = new ArrayList<Logiciel>();
			}
			/// get listeBaseDeDonnee ///
			listeBaseDeDonnee = resumeTechniqueEtFonctionnel.getListeBaseDeDonnee();
			if (listeBaseDeDonnee == null) {
				listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			}
			/// get listeSystemeExploitation ///
			listeSystemeExploitation = resumeTechniqueEtFonctionnel.getListeSystemeExploitation();
			if (listeSystemeExploitation == null) {
				listeSystemeExploitation = new ArrayList<SystemeExploitation>();
			}
		} else {
			parcours = new Parcours();
			listeLangue = new ArrayList<Langue>();
			listeFormation = new ArrayList<Formation>();
			listeExperience = new ArrayList<Experience>();
			listeCompetence = new ArrayList<Competence>();
			listeCertification = new ArrayList<Certification>();
			listeTitre = new ArrayList<Titre>();
			resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
			listeLangage = new ArrayList<Langage>();
			listeLogiciel = new ArrayList<Logiciel>();
			listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			listeSystemeExploitation = new ArrayList<SystemeExploitation>();
		}

		model.addAttribute("listeLangue", listeLangue);
		Collections.sort(listeFormation);
		model.addAttribute("listeFormation", listeFormation);
		Collections.sort(listeExperience, Collections.reverseOrder());
		model.addAttribute("listeExperience", listeExperience);
		model.addAttribute("listeCompetence", listeCompetence);
		model.addAttribute("listeCertification", listeCertification);
		model.addAttribute("listeTitre", listeTitre);
		model.addAttribute("resumeTechniqueEtFonctionnel", resumeTechniqueEtFonctionnel);
		model.addAttribute("listeLangage", listeLangage);
		model.addAttribute("listeLogiciel", listeLogiciel);
		model.addAttribute("listeBaseDeDonnee", listeBaseDeDonnee);
		model.addAttribute("listeSystemeExploitation", listeSystemeExploitation);

		// mise � jour de la variable affichage
		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null) {
			affichage = new Affichage();
			functionReinitialisationAffichage(utilisateur.getId(), model, request);
		} else
			model.addAttribute("affichage", affichage);
	}

	@RequestMapping("/ReinitialisationAffichage/{utilisateurId}")
	public String ReinitialisationAffichage(@PathVariable(value = "utilisateurId", required = true) Long utilisateurId,
			Model model, HttpServletRequest request) {

		functionReinitialisationAffichage(utilisateurId, model, request);
		return "selection";

	}

	public void functionReinitialisationAffichage(Long utilisateurId, Model model, HttpServletRequest request) {
		/// get utilisateur from database ///
		Utilisateur utilisateur = utilisateurRepo.getOne(utilisateurId);
		// utilisateurIdPass = utilisateurId;
		// Utilisateur utilisateur =
		// utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		model.addAttribute("utilisateur", utilisateur);

		/// get parcours ///
		Parcours parcours = utilisateur.getParcours();
		List<Langue> listeLangue;
		List<Formation> listeFormation;
		List<Experience> listeExperience;
		List<Competence> listeCompetence;
		List<Certification> listeCertification;
		List<Titre> listeTitre;
		List<Langage> listeLangage;
		List<Logiciel> listeLogiciel;
		List<BaseDeDonnee> listeBaseDeDonnee;
		List<SystemeExploitation> listeSystemeExploitation;
		ResumeTechniqueEtFonctionnel resumeTechniqueEtFonctionnel;

		if (parcours != null) {
			/// get listeLangue ///
			listeLangue = parcours.getListeLangue();
			if (listeLangue == null)
				listeLangue = new ArrayList();
			/// get listeFormation ///
			listeFormation = parcours.getListeFormation();
			if (listeFormation == null)
				listeFormation = new ArrayList();
			/// get listeExperience ///
			listeExperience = parcours.getListeExperience();
			if (listeExperience == null)
				listeExperience = new ArrayList();
			/// get listeCompetence ///
			listeCompetence = parcours.getListeCompetence();
			if (listeCompetence == null)
				listeCompetence = new ArrayList();
			/// get listeCompetence ///
			listeCertification = parcours.getListeCertification();
			if (listeCertification == null)
				listeCertification = new ArrayList();
			/// get listeTitre ///
			listeTitre = parcours.getListeTitre();
			if (listeTitre == null)
				listeTitre = new ArrayList();
			resumeTechniqueEtFonctionnel = parcours.getResumeTechniqueEtFonctionnel();
			if (resumeTechniqueEtFonctionnel == null)
				resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
			/// get listelangage ///
			listeLangage = resumeTechniqueEtFonctionnel.getListeLangage();
			if (listeLangage == null) {
				listeLangage = new ArrayList<Langage>();
			}
			/// get listelogiciel ///
			listeLogiciel = resumeTechniqueEtFonctionnel.getListeLogiciel();
			if (listeLogiciel == null) {
				listeLogiciel = new ArrayList<Logiciel>();
			}
			/// get listeBaseDeDonnee ///
			listeBaseDeDonnee = resumeTechniqueEtFonctionnel.getListeBaseDeDonnee();
			if (listeBaseDeDonnee == null) {
				listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			}
			/// get listeSystemeExploitation ///
			listeSystemeExploitation = resumeTechniqueEtFonctionnel.getListeSystemeExploitation();
			if (listeSystemeExploitation == null) {
				listeSystemeExploitation = new ArrayList<SystemeExploitation>();
			}
		} else {
			parcours = new Parcours();
			listeLangue = new ArrayList<Langue>();
			listeFormation = new ArrayList<Formation>();
			listeExperience = new ArrayList<Experience>();
			listeCompetence = new ArrayList<Competence>();
			listeCertification = new ArrayList<Certification>();
			listeTitre = new ArrayList<Titre>();
			resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
			listeLangage = new ArrayList<Langage>();
			listeLogiciel = new ArrayList<Logiciel>();
			listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			listeSystemeExploitation = new ArrayList<SystemeExploitation>();
		}

		model.addAttribute("listeLangue", listeLangue);
		Collections.sort(listeFormation);
		model.addAttribute("listeFormation", listeFormation);
		Collections.sort(listeExperience, Collections.reverseOrder());
		model.addAttribute("listeExperience", listeExperience);
		model.addAttribute("listeCompetence", listeCompetence);
		model.addAttribute("listeCertification", listeCertification);
		model.addAttribute("listeTitre", listeTitre);
		model.addAttribute("resumeTechniqueEtFonctionnel", resumeTechniqueEtFonctionnel);
		model.addAttribute("listeLangage", listeLangage);
		model.addAttribute("listeLogiciel", listeLogiciel);
		model.addAttribute("listeBaseDeDonnee", listeBaseDeDonnee);
		model.addAttribute("listeSystemeExploitation", listeSystemeExploitation);

		///// Initialisation de l'attribut de Session Affichage
		HttpSession session = request.getSession(true);
		Affichage affichage = new Affichage();

		// initialisation de la liste des experiences
		List<Long> listeExperienceId = new ArrayList<Long>();
		for (int i = 0; i < listeExperience.size(); i++)
			listeExperienceId.add(listeExperience.get(i).getId());
		affichage.setListeExperienceId(listeExperienceId);

		// initialisation de la liste des titres
		List<Long> listeTitreId = new ArrayList<Long>();
		for (int i = 0; i < listeTitre.size(); i++) {
			listeTitreId.add(listeTitre.get(i).getId());
		}
		affichage.setListeTitreId(listeTitreId);

		// initialisation de la liste des competences
		List<Long> listeCompetenceId = new ArrayList<Long>();
		for (int i = 0; i < listeCompetence.size(); i++) {
			listeCompetenceId.add(listeCompetence.get(i).getId());
		}
		affichage.setListeCompetenceId(listeCompetenceId);

		// initialisation de la liste des formations
		List<Long> listeFormationId = new ArrayList<Long>();
		for (int i = 0; i < listeFormation.size(); i++) {
			listeFormationId.add(listeFormation.get(i).getId());
		}
		affichage.setListeFormationId(listeFormationId);

		// initialisation de la liste des langues
		List<Long> listeLangueId = new ArrayList<Long>();
		for (int i = 0; i < listeLangue.size(); i++) {
			listeLangueId.add(listeLangue.get(i).getId());
		}
		affichage.setListeLangueId(listeLangueId);

		// initialisation de la liste des certifications
		List<Long> listeCertificationId = new ArrayList<Long>();
		for (int i = 0; i < listeCertification.size(); i++) {
			listeCertificationId.add(listeCertification.get(i).getId());
		}
		affichage.setListeCertificationId(listeCertificationId);

		// initialisation de la liste des langages
		List<Long> listeLangageId = new ArrayList<Long>();
		for (int i = 0; i < listeLangage.size(); i++) {
			listeLangageId.add(listeLangage.get(i).getId());
		}

		affichage.setListeLangageId(listeLangageId);

		// initialisation de la liste des logiciels
		List<Long> listeLogicielId = new ArrayList<Long>();
		for (int i = 0; i < listeLogiciel.size(); i++) {
			listeLogicielId.add(listeLogiciel.get(i).getId());
		}
		affichage.setListeLogicielId(listeLogicielId);

		// initialisation de la liste des bases de donnees
		List<Long> listeBaseDeDonneeId = new ArrayList<Long>();
		for (int i = 0; i < listeBaseDeDonnee.size(); i++) {
			listeBaseDeDonneeId.add(listeBaseDeDonnee.get(i).getId());
		}
		affichage.setListeBaseDeDonneeId(listeBaseDeDonneeId);

		// initialisation de la liste des systemes d'exploitation
		List<Long> listeSystemeExploitationId = new ArrayList<Long>();
		for (int i = 0; i < listeSystemeExploitation.size(); i++) {
			listeSystemeExploitationId.add(listeSystemeExploitation.get(i).getId());
		}
		affichage.setListeSystemeExploitationId(listeSystemeExploitationId);

		session.setAttribute("affichage", affichage);
		model.addAttribute("affichage", affichage);

	}
}