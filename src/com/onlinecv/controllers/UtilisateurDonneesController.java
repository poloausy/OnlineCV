package com.onlinecv.controllers;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.transaction.Transactional;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.onlinecv.dao.IBaseDeDonneeJpaRepository;
import com.onlinecv.dao.ICertificationJpaRepository;
import com.onlinecv.dao.ICompetenceJpaRepository;
import com.onlinecv.dao.ICredentialJpaRepository;
import com.onlinecv.dao.IEntrepriseJpaRepository;
import com.onlinecv.dao.IExperienceJpaRepository;
import com.onlinecv.dao.IFormationJpaRepository;
import com.onlinecv.dao.ILangageJpaRepository;
import com.onlinecv.dao.ILangueJpaRepository;
import com.onlinecv.dao.ILogicielJpaRepository;
import com.onlinecv.dao.IMetierJpaRepository;
import com.onlinecv.dao.INiveauJpaRepository;
import com.onlinecv.dao.INomLangueJpaRepository;
import com.onlinecv.dao.INomNationaliteJpaRepository;
import com.onlinecv.dao.INomPaysJpaRepository;
import com.onlinecv.dao.IParcoursJpaRepository;
import com.onlinecv.dao.IResumeTechniqueEtFonctionnelJpaRepository;
import com.onlinecv.dao.ISystemeExploitationJpaRepository;
import com.onlinecv.dao.ITitreJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Affichage;
import com.onlinecv.entities.BaseDeDonnee;
import com.onlinecv.entities.Certification;
import com.onlinecv.entities.Competence;
import com.onlinecv.entities.Entreprise;
import com.onlinecv.entities.Experience;
import com.onlinecv.entities.Formation;
import com.onlinecv.entities.Langage;
import com.onlinecv.entities.Langue;
import com.onlinecv.entities.Logiciel;
import com.onlinecv.entities.Metier;
import com.onlinecv.entities.Niveau;
import com.onlinecv.entities.NomLangue;
import com.onlinecv.entities.NomNationalite;
import com.onlinecv.entities.NomPays;
import com.onlinecv.entities.Parcours;
import com.onlinecv.entities.ResumeTechniqueEtFonctionnel;
import com.onlinecv.entities.SystemeExploitation;
import com.onlinecv.entities.Titre;
import com.onlinecv.entities.Utilisateur;

@Controller
@RequestMapping("/donneesVisiteur")
public class UtilisateurDonneesController {

	@Autowired
	private IUtilisateurJpaRepository utilisateurRepo;

	@Autowired
	private ICredentialJpaRepository credentialRepo;

	@Autowired
	private IParcoursJpaRepository parcoursRepo;

	@Autowired
	private ILangueJpaRepository langueRepo;

	@Autowired
	private INiveauJpaRepository niveauRepo;

	@Autowired
	private INomLangueJpaRepository nomLangueRepo;

	@Autowired
	private INomPaysJpaRepository nomPaysRepo;

	@Autowired
	private INomNationaliteJpaRepository nomNationaliteRepo;

	@Autowired
	private IFormationJpaRepository formationRepo;

	@Autowired
	private IExperienceJpaRepository experienceRepo;

	@Autowired
	private IMetierJpaRepository metierRepo;

	@Autowired
	private IEntrepriseJpaRepository entrepriseRepo;

	@Autowired
	private ICompetenceJpaRepository competenceRepo;

	@Autowired
	private ICertificationJpaRepository certificationRepo;

	@Autowired
	private ITitreJpaRepository titreRepo;

	@Autowired
	private IResumeTechniqueEtFonctionnelJpaRepository resumeRepo;

	@Autowired
	private ILangageJpaRepository langageRepo;

	@Autowired
	private ILogicielJpaRepository logicielRepo;

	@Autowired
	private ISystemeExploitationJpaRepository systemeExploitationRepo;

	@Autowired
	private IBaseDeDonneeJpaRepository baseDeDonneeRepo;

	@RequestMapping("/goToExperience")
	public String goToExperience(Model model, HttpServletRequest request) {
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "0");
		return "profil";
	}

	@RequestMapping("/fermeAccordeon")
	public String fermeAccordeon(Model model, HttpServletRequest request) {
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "");
		return "profil";
	}

	@RequestMapping("/fermeOutil")
	public String fermeOutil(Model model, HttpServletRequest request) {

		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "experience");
		return "profil";
	}

	// La photo d'identité est sauvée à part pour pouvoir conservée celle-ci lorsque
	// l'on modifie une autre donnée utilisateur
	// @Transactional
	@PostMapping("/savePhoto")

	public String savePhoto(@Valid @ModelAttribute(value = "utilisateur") Utilisateur utilisateur, BindingResult result,
			Model model, @RequestParam("photo") MultipartFile photo, HttpServletRequest request) {
		// Récupération des données de l'utilisateur connecté
		utilisateur.setCredential(AuthHelper.getUtilisateur().getCredential());
		utilisateur.setParcours(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getParcours());
		utilisateur.setNom(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getNom());
		utilisateur.setPrenom(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getPrenom());
		utilisateur.setTelephone(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getTelephone());
		utilisateur
				.setDateDeNaissance(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getDateDeNaissance());
		utilisateur.setNationalite_1(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getNationalite_1());
		utilisateur.setNationalite_2(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getNationalite_2());
		utilisateur.setAdresse(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getAdresse());

		System.out.println("utilisateur1 photo =" + utilisateur);
		// photo uploaded in form
		if (photo.getSize() > 0) {
			System.out.println("photo is not null");
			// picture upload with MultipartFile
			// String nameImage = uploadFile(photo, request);
			// model.addAttribute("nameImage", nameImage);
			// // If picture, save picture to user
			// if (nameImage != null) {
			// System.out.println("image uploaded! " + nameImage);
			// utilisateur.setPhoto(nameImage);
			// }
		}
		// no photo uploaded in form
		if (photo.getSize() <= 0) {
			System.out.println("photo: " + photo);
			System.out.println("ut photo: " + utilisateur.getPhoto());
			System.out.println("utilisateur " + utilisateur.toString());
			// TODO Don't update picture if user already has one. This doesn't work yet...
			// user has no picture yet
			if (utilisateur.getPhoto() == null || utilisateur.getPhoto().isEmpty()
					|| utilisateur.getPhoto().length() == 0 || utilisateur.getPhoto().equals("")) {
				System.out.println("no picture in form and no picture in general");
			}
			// user already has picture
			else {
				System.out.println("no picture in form, but user has a picture already: " + utilisateur.getPhoto());
				// save the current picture to the user
			}
		}
		System.out.println("ok sauve");
		System.out.println("nom photo =" + utilisateur.getPhoto());
		System.out.println("utilisateur photo =" + utilisateur);
		utilisateurRepo.save(utilisateur);
		model.addAttribute("champActif", "infoPersonnelle");
		prepareProfil(model, null, (long) 0, request);

		return "profil";

		// return "redirect:/donneesVisiteur/goToExperience";

	}

	@Transactional
	@PostMapping("/saveDonneesPerso")
	public String saveDonneesPerso(@Valid @ModelAttribute(value = "utilisateur") Utilisateur utilisateur,
			BindingResult result, Model model, HttpServletRequest request) {
		utilisateur.setCredential(AuthHelper.getUtilisateur().getCredential());
		utilisateur.setParcours(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getParcours());
		// Récupération photo identité
		utilisateur.setPhoto(utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId()).getPhoto());
		System.out.println("utilisateur2 photo =" + utilisateur);

		// *********************************COMPARAISON ENTRE LES DEUX CHAMPS
		// NATIONALITES****************************************

		if (utilisateur.getNationalite_1().equals(utilisateur.getNationalite_2())) { // si nationalité 1 = nationalité
																						// 2
			result.rejectValue("nationalite_2", "message d'erreur doublon nationalite"); // alors rejeter la valeur car
																							// doublon (identique)
			model.addAttribute("erreur", "doublon"); // model attribute injecté dans .jsp pour lien message errors
			utilisateur.setNationalite_2(utilisateur.getNationalite_1()); // si erreur alors garder les valeurs dans la
																			// page pour que l'utilisateur le remarque

		} else {
			utilisateur.setNom(utilisateur.getNom().toUpperCase()); // Les Noms des utilisateurs seront enregistrés en
																	// Majuscule.
			utilisateur.setPrenom(utilisateur.getPrenom().toUpperCase()); // Les Prénoms des utilisateurs seront
																			// enregistrés en Majuscule.

			utilisateurRepo.save(utilisateur);
		}

		// utilisateurRepo.save(utilisateur);
		System.out.println("nom photo =" + utilisateur.getPhoto());
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "infoPersonnelle");

		return "profil";
	}

	// private String uploadFile(MultipartFile photo, HttpServletRequest request) {
	// System.out.println("Uploading file...");
	// String result = "";
	// // If photo input was not empty
	// if (!photo.isEmpty()) {
	// System.out.println("file not empty");
	// Long fileSize = photo.getSize();
	// if (fileSize > 2097152) {
	// System.out.println("File too big.." + photo.getSize());
	// }
	// try {
	// byte[] bytes = photo.getBytes();
	// // TODO change root path to relative project/server path
	//
	// // Recherche de la racine du chemin utilisateur;
	//// RootPath rootPath = new RootPath();
	//// rootPath.RecupRootPath();
	// File folder = new File(File.separator + "opt" + File.separator + "tomcat" +
	// File.separator
	// + "webapps" + File.separator + "ProjetOnliveCv" + File.separator + "WEB-INF"
	// + File.separator
	// + "jspf" + File.separator + "templateCv");
	// // String rootPath = "https:\\\\gitlab.com/poloausy/OnlineCV.git";
	// File dir = new File(folder + File.separator + "WebContent" + File.separator +
	// "static" + File.separator
	// + "uploads");
	// // File dir = new File(rootPath);
	// System.out.println("dir: " + dir);
	// // If uploads folder doesn't exist yet, create the folder
	// if (!dir.exists()) {
	// dir.mkdirs();
	// System.out.println("dir created:" + dir);
	// }
	// // use the time + date for the upload name
	// String name = String.valueOf(new Date().getTime() + ".jpg");
	// // creates new file with upload dir and upload name
	// File serverFile = new File(dir.getAbsolutePath() + File.separator + name);
	// System.out.println("serverFile path =" + serverFile.getPath());
	// // writes the bytes to the new upload/image1
	// BufferedOutputStream stream = new BufferedOutputStream(new
	// FileOutputStream(serverFile));
	// stream.write(bytes);
	// stream.close();
	// result = name;
	// } catch (Exception e) {
	// System.out.println("Error: " + e.getMessage());
	// }
	// }
	// listPhotoForFolder();
	// return result;
	// }

	// public String listPhotoForFolder() {
	//
	// // Recherche de la racine du chemin utilisateur;
	// RootPath rootPath = new RootPath();
	// rootPath.RecupRootPath();
	// // Répertoire où sont situés les fichiers template
	// File folderphoto = new File(rootPath.getRootName() +
	// "\\WebContent\\static\\uploads");
	// System.out.println("path photo = " + folderphoto);
	// List<String> listeFiles = new ArrayList<String>();
	// // Récupère la listes des noms de fichiers dans le répertoire
	// String nomFile = "";
	// for (final File fileEntry : folderphoto.listFiles()) {
	// nomFile = fileEntry.getName();
	// System.out.println("nom fichier photo =" + nomFile);
	// listeFiles.add(nomFile);
	// }
	// System.out.println("dernier fichier photo =" + nomFile);
	// return nomFile;
	// }

	@Transactional
	@RequestMapping("/saveLangue")
	public String saveLangue(Model model, @Valid @ModelAttribute(value = "langue") Langue langue, BindingResult result,
			HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();
		HttpSession session = request.getSession(true);

		if (parcours == null) {
			parcours = new Parcours();
			Affichage affichage = new Affichage();
			session.setAttribute("affichage", affichage);
		}

		// check for update
		if (!result.hasErrors()) {
			List<Langue> listeLangue = parcours.getListeLangue();
			if (listeLangue != null) {
				for (int i = 0; i < listeLangue.size(); i++) {

					if (langue.getNomLangue().getId() == listeLangue.get(i).getNomLangue().getId()) {

						model.addAttribute("erreur", "doublon");
						result.rejectValue("nomLangue.id", "message d'erreur doublon langue");
						break;
					}
				}
			}
		}

		if (!result.hasErrors()) {
			langue.setNiveau(niveauRepo.getOne(langue.getNiveau().getId()));
			langue.setNomLangue(nomLangueRepo.getOne(langue.getNomLangue().getId()));
			prepareProfil(model, langue, (long) 0, request);
		} else {
			prepareProfil(model, null, (long) 0, request);
		}

		// mise à jour de la variable affichage
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeLangueId();
		if (affichage.getListeLangueId() == null)
			liste = new ArrayList();
		liste.add(langue.getId());
		affichage.setListeLangueId(liste);
		session.setAttribute("affichage", affichage);

		model.addAttribute("champActif", "langue");
		return "profil";
	}

	/// modifie langue GET
	@RequestMapping("/modifieLangue/{langueId}")
	public String modifieLangue(@PathVariable(value = "langueId", required = true) Long id, Model model,
			HttpServletRequest request) {
		model.addAttribute("idLangueAModifer", id);
		model.addAttribute("champActif", "langue");
		prepareProfil(model, null, null, request);
		return "profil";
	}

	/// modifie langue POST
	@Transactional
	@PostMapping("/modifieLangue")
	public String modifieLangue(Model model, @Valid @ModelAttribute(value = "langue") Langue langue,
			BindingResult result, HttpServletRequest request) {

		prepareProfil(model, langue, langue.getId(), request);
		model.addAttribute("champActif", "langue");
		return "profil";
	}

	@RequestMapping("/supprimeLangue/{langueId}")
	public String supprimeLangue(@PathVariable(value = "langueId", required = true) Long id, Model model,
			HttpServletRequest request) {
		langueRepo.deleteById(id);
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "langue");
		return "profil";
	}

	// @Transactional
	@RequestMapping("/saveFormation")
	public String saveFormation(Model model, @Valid @ModelAttribute(value = "formation") Formation formation,
			BindingResult result, HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();
		HttpSession session = request.getSession(true);

		if (parcours == null) {
			parcours = new Parcours();
			Affichage affichage = new Affichage();
			session.setAttribute("affichage", affichage);
		}

		// check for update
		if (!result.hasErrors()) {
			List<Formation> listeFormation = parcours.getListeFormation();
			if (listeFormation != null) {
				for (int i = 0; i < listeFormation.size(); i++) {

					if (formation.getIntitule().equals(listeFormation.get(i).getIntitule())
							&& (formation.getAnneeObtention().equals(listeFormation.get(i).getAnneeObtention()))) {

						model.addAttribute("erreur", "doublon");
						result.rejectValue("intitule", "message d'erreur doublon formation");
						break;
					}
				}
			}
		}

		if (!result.hasErrors()) {

			prepareProfil(model, formation, (long) 0, request);
		} else {
			prepareProfil(model, null, (long) 0, request);
		}

		// mise à jour de la variable affichage

		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeFormationId();
		if (affichage.getListeFormationId() == null)
			liste = new ArrayList();
		liste.add(formation.getId());
		affichage.setListeFormationId(liste);
		session.setAttribute("affichage", affichage);

		model.addAttribute("champActif", "formation");
		return "profil";
	}

	// @Transactional
	@RequestMapping("/supprimeFormation/{formationId}")
	public String supprimeFormation(@PathVariable(value = "formationId", required = true) Long id, Model model,
			HttpServletRequest request) {
		formationRepo.deleteById(id);
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "formation");
		return "profil";
	}

	@Transactional
	@PostMapping(value = "/ajouteOutils", params = "sauve")
	public String saveExperience(Model model, @Valid @ModelAttribute(value = "experience") Experience experience,
			BindingResult result, HttpServletRequest request) {

		// Empeche d'avoir des doublons dans la liste métier :
		// Si le nouveau métier tapé par l'utilisateur correspond à un métier déjà
		// dans
		// la liste,
		// le métier tapé est remplacer par le métier correspondant issu de la liste

		if (experience.getMetier().getId() == null) {
			Metier metierNomList = metierRepo.findByNom(experience.getMetier().getNom());
			if (metierNomList != null) {
				experience.setMetier(metierNomList);
			}
		} else
			experience.setMetier(metierRepo.getOne(experience.getMetier().getId()));

		// Empeche d'avoir des doublons dans la liste entreprise :
		// Si la nouvelle entreprise tapée par l'utilisateur correspond à une
		// entreprise
		// déjà dans
		// la liste, l'entreprise tapée est remplacée par l'entreprise correspondante
		// issue de la liste
		if (experience.getEntreprise().getId() == null) {
			Entreprise entrepriseNomList = entrepriseRepo.findByNom(experience.getEntreprise().getNom());
			if (entrepriseNomList != null) {
				experience.setEntreprise(entrepriseNomList);
			}
		} else
			experience.setEntreprise(entrepriseRepo.getOne(experience.getEntreprise().getId()));

		// sauvegarde de l'expérience
		if (experience.getId() == null)
			prepareProfil(model, experience, (long) 0, request);
		else
			prepareProfil(model, experience, experience.getId(), request);

		// mise à jour de la variable affichage
		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeExperienceId();
		if (affichage.getListeExperienceId() == null)
			liste = new ArrayList();
		liste.add(experience.getId());
		affichage.setListeExperienceId(liste);
		session.setAttribute("affichage", affichage);

		model.addAttribute("champActif", "experience");
		return "profil";
	}

	@Transactional
	@RequestMapping(value = "/ajouteOutils", params = "ajoute")
	public String ajouteOutils(Model model, @Valid @ModelAttribute(value = "experience") Experience experience,
			BindingResult result, HttpServletRequest request) {

		/// SAUVEGARDE DE L'EXPERIENCE ///

		if (experience.getId() == null) {
			prepareProfil(model, experience, (long) 0, request);
		} else {
			prepareProfil(model, experience, experience.getId(), request);
		}

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeExperienceId();
		if (affichage.getListeExperienceId() == null)
			liste = new ArrayList();
		liste.add(experience.getId());
		affichage.setListeExperienceId(liste);
		session.setAttribute("affichage", affichage);

		/// LISTE DES NOMS DES OUTILS DE L'EXPERIENCE ///

		List<Langage> listeLangage = experience.getListeLangage();
		model.addAttribute("listeLangage", listeLangage);
		List<Logiciel> listeLogiciel = experience.getListeLogiciel();
		model.addAttribute("listeLogiciel", listeLogiciel);
		List<BaseDeDonnee> listeBaseDeDonnee = experience.getListeBaseDeDonnee();
		model.addAttribute("listeBaseDeDonnee", listeBaseDeDonnee);
		List<SystemeExploitation> listeSystemeExploitation = experience.getListeSystemeExploitation();
		model.addAttribute("listeSystemeExploitation", listeSystemeExploitation);

		/// NOUVEAUX OUTILS ///

		model.addAttribute("langage", new Langage());
		model.addAttribute("logiciel", new Langage());
		model.addAttribute("baseDeDonnee", new Langage());
		model.addAttribute("systemeExploitation", new Langage());

		experience.setMetier(metierRepo.getOne(experience.getMetier().getId()));
		experience.setEntreprise(entrepriseRepo.getOne(experience.getEntreprise().getId()));
		model.addAttribute("experience", experience);

		return "outils";
	}

	@RequestMapping("/ajouteExperience")
	public String ajouteExperience(Model model) {

		/// NOUVELLE EXPERIENCE ///

		Experience nouvelleExperience = new Experience();
		model.addAttribute("experience", nouvelleExperience);

		/// LISTE DES METIERS ///

		List<Metier> metiers = metierRepo.findAll();
		Collections.sort(metiers);
		model.addAttribute("listeMetier", metiers);

		/// LISTE DES ENTREPRISES ///

		List<Entreprise> entreprises = entrepriseRepo.findAll();
		Collections.sort(entreprises);
		model.addAttribute("listeEntreprise", entreprises);

		return "nouvelleExperience";
	}

	@RequestMapping("/supprimeExperience/{experienceId}")
	public String supprimeExperience(@PathVariable(value = "experienceId", required = true) Long id, Model model,
			HttpServletRequest request) {
		experienceRepo.deleteById(id);
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "experience");
		return "profil";
	}

	@Transactional
	@RequestMapping("/modifieExperience/{experienceId}")
	public String modifieExperience(@PathVariable(value = "experienceId", required = true) Long id, Model model) {

		/// RECUPERATION DE L'ID DE L'EXPERIENCE DEJA EN BDD, AVEC GETONE, L'ID DU
		/// PARCOURS SE PERD ///

		Experience experience = experienceRepo.findById(id).orElseThrow(null);
		model.addAttribute("experience", experience);

		/// LISTE DES METIERS ///

		List<Metier> metiers = metierRepo.findAll();
		Collections.sort(metiers);
		model.addAttribute("listeMetier", metiers);

		/// LISTE DES ENTREPRISES ///

		List<Entreprise> entreprises = entrepriseRepo.findAll();
		Collections.sort(entreprises);
		model.addAttribute("listeEntreprise", entreprises);

		model.addAttribute("experience", experience);

		return "nouvelleExperience";
	}

	@RequestMapping("/modifieCompetence/{competenceId}")
	public String modifieCompetence(@PathVariable(value = "competenceId", required = true) Long id, Model model,
			HttpServletRequest request) {

		model.addAttribute("idCompetenceAModifer", id);

		model.addAttribute("champActif", "competence");
		prepareProfil(model, null, null, request);

		return "profil";
	}

	@Transactional
	@RequestMapping("/saveLangage/{experienceId}")
	public String saveLangage(Model model, @Valid @ModelAttribute(value = "Langage") Langage langage,
			BindingResult result, @PathVariable(value = "experienceId", required = true) Long experienceId,
			HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();

		Experience experience = experienceRepo.getOne(experienceId);
		List<Langage> listeLangage = experience.getListeLangage();
		boolean erreur = false;

		if (langage.getId() != null) {
			for (Langage i : listeLangage) {
				if (i.getId() == langage.getId()) {
					erreur = true;
					break;
				}
			}
			langage.setNom(langageRepo.getOne(langage.getId()).getNom());
		} else {
			if (langageRepo.findByNom(langage.getNom()) == null) {
				langageRepo.save(langage);
			} else {
				erreur = true;
			}
		}
		if (erreur == false) {
			listeLangage.add(langage);
			experience.setListeLangage(listeLangage);
			experience.setParcours(parcours);
			utilisateur.setParcours(parcours);
			utilisateurRepo.save(utilisateur);

		} else {
			result.rejectValue("id", "error.langage.nom.doublon");
			model.addAttribute("erreurLangage", "doublon");
		}

		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/saveLogiciel/{experienceId}")
	public String saveLogiciel(Model model, @Valid @ModelAttribute(value = "Logiciel") Logiciel logiciel,
			BindingResult result, @PathVariable(value = "experienceId", required = true) Long experienceId,
			HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();

		Experience experience = experienceRepo.getOne(experienceId);
		List<Logiciel> listeLogiciel = experience.getListeLogiciel();
		boolean erreur = false;

		if (logiciel.getId() != null) {
			for (Logiciel i : listeLogiciel) {
				if (i.getId() == logiciel.getId()) {
					erreur = true;
					break;
				}
			}
			logiciel.setNom(logicielRepo.getOne(logiciel.getId()).getNom());
		} else {
			if (logicielRepo.findByNom(logiciel.getNom()) == null) {
				logicielRepo.save(logiciel);
			} else {
				erreur = true;
			}
		}
		if (erreur == false) {
			listeLogiciel.add(logiciel);
			experience.setListeLogiciel(listeLogiciel);
			experience.setParcours(parcours);
			utilisateur.setParcours(parcours);
			utilisateurRepo.save(utilisateur);

		} else {
			result.rejectValue("id", "error.logiciel.nom.doublon");
			model.addAttribute("erreurLogiciel", "doublon");
		}

		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/saveBaseDeDonnee/{experienceId}")
	public String saveBaseDeDonnee(Model model,
			@Valid @ModelAttribute(value = "baseDeDonnee") BaseDeDonnee baseDeDonnee, BindingResult result,
			@PathVariable(value = "experienceId", required = true) Long experienceId, HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();

		Experience experience = experienceRepo.getOne(experienceId);
		List<BaseDeDonnee> listeBaseDeDonnee = experience.getListeBaseDeDonnee();
		boolean erreur = false;

		if (baseDeDonnee.getId() != null) {
			for (BaseDeDonnee i : listeBaseDeDonnee) {
				if (i.getId() == baseDeDonnee.getId()) {
					erreur = true;
					break;
				}
			}
			baseDeDonnee.setNom(baseDeDonneeRepo.getOne(baseDeDonnee.getId()).getNom());
		} else {
			if (baseDeDonneeRepo.findByNom(baseDeDonnee.getNom()) == null) {
				baseDeDonneeRepo.save(baseDeDonnee);
			} else {
				erreur = true;
			}
		}
		if (erreur == false) {
			listeBaseDeDonnee.add(baseDeDonnee);
			experience.setListeBaseDeDonnee(listeBaseDeDonnee);
			experience.setParcours(parcours);
			utilisateur.setParcours(parcours);
			utilisateurRepo.save(utilisateur);

		} else {
			result.rejectValue("id", "error.baseDeDonnee.nom.doublon");
			model.addAttribute("erreurBaseDeDonnee", "doublon");
		}
		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/saveSystemeExploitation/{experienceId}")
	public String saveSystemeExploitation(Model model,
			@Valid @ModelAttribute(value = "SystemeExploitation") SystemeExploitation systemeExploitation,
			BindingResult result, @PathVariable(value = "experienceId", required = true) Long experienceId,
			HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();

		Experience experience = experienceRepo.getOne(experienceId);
		List<SystemeExploitation> listeSystemeExploitation = experience.getListeSystemeExploitation();
		boolean erreur = false;

		if (systemeExploitation.getId() != null) {
			for (SystemeExploitation i : listeSystemeExploitation) {
				if (i.getId() == systemeExploitation.getId()) {
					erreur = true;
					break;
				}
			}
			systemeExploitation.setNom(systemeExploitationRepo.getOne(systemeExploitation.getId()).getNom());
		} else {
			if (systemeExploitationRepo.findByNom(systemeExploitation.getNom()) == null) {
				systemeExploitationRepo.save(systemeExploitation);
			} else {
				erreur = true;
			}
		}
		if (erreur == false) {
			listeSystemeExploitation.add(systemeExploitation);
			experience.setListeSystemeExploitation(listeSystemeExploitation);
			experience.setParcours(parcours);
			utilisateur.setParcours(parcours);
			utilisateurRepo.save(utilisateur);

		} else {
			result.rejectValue("id", "error.systemeExploitation.nom.doublon");
			model.addAttribute("erreurSystemeExploitation", "doublon");
		}
		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/saveLangageResume/{resumeId}")
	public String saveLangageResume(Model model, @Valid @ModelAttribute(value = "Langage") Langage langage,
			BindingResult result, @PathVariable(value = "resumeId", required = true) Long resumeId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeId);

		List<Langage> listeLangage = resume.getListeLangage();
		if (langage.getId() != null)
			langage.setNom(langageRepo.getOne(langage.getId()).getNom());
		else {
			langageRepo.save(langage);
		}
		listeLangage.add(langage);

		resume.setListeLangage(listeLangage);
		resumeRepo.save(resume);

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeLangageId();
		if (affichage.getListeLangageId() == null)
			liste = new ArrayList();
		liste.add(langage.getId());
		affichage.setListeLangageId(liste);
		session.setAttribute("affichage", affichage);

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("retirerLangageResume/{resumeTechniqueEtFonctionnelId}/{langageId}")
	public String retirererLangageResume(Model model,
			@PathVariable(value = "langageId", required = true) Long langageId,
			@PathVariable(value = "resumeTechniqueEtFonctionnelId", required = true) Long resumeTechniqueEtFonctionnelId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeTechniqueEtFonctionnelId);
		List<Langage> listeLangage = resume.getListeLangage();
		for (Langage i : listeLangage) {
			if (i.getId() == langageId) {
				listeLangage.remove(i);
				break;
			}
		}

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("/saveLogicielResume/{resumeId}")
	public String saveLogicielResume(Model model, @Valid @ModelAttribute(value = "Logiciel") Logiciel logiciel,
			BindingResult result, @PathVariable(value = "resumeId", required = true) Long resumeId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeId);

		List<Logiciel> listeLogiciel = resume.getListeLogiciel();
		if (logiciel.getId() != null)
			logiciel.setNom(logicielRepo.getOne(logiciel.getId()).getNom());
		else {
			logicielRepo.save(logiciel);
		}
		listeLogiciel.add(logiciel);

		resume.setListeLogiciel(listeLogiciel);
		resumeRepo.save(resume);

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeLogicielId();
		if (affichage.getListeLogicielId() == null)
			liste = new ArrayList();
		liste.add(logiciel.getId());
		affichage.setListeLogicielId(liste);
		session.setAttribute("affichage", affichage);
		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("retirerLogicielResume/{resumeTechniqueEtFonctionnelId}/{logicielId}")
	public String retirerLogicielResume(Model model,
			@PathVariable(value = "logicielId", required = true) Long logicielId,
			@PathVariable(value = "resumeTechniqueEtFonctionnelId", required = true) Long resumeTechniqueEtFonctionnelId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeTechniqueEtFonctionnelId);
		List<Logiciel> listeLogiciel = resume.getListeLogiciel();
		for (Logiciel i : listeLogiciel) {
			if (i.getId() == logicielId) {
				listeLogiciel.remove(i);
				break;
			}
		}

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("/saveBaseDeDonneeResume/{resumeId}")
	public String saveBaseDeDonneeResume(Model model,
			@Valid @ModelAttribute(value = "BaseDeDonnee") BaseDeDonnee baseDeDonnee, BindingResult result,
			@PathVariable(value = "resumeId", required = true) Long resumeId, HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeId);

		List<BaseDeDonnee> listeBaseDeDonnee = resume.getListeBaseDeDonnee();
		if (baseDeDonnee.getId() != null)
			baseDeDonnee.setNom(baseDeDonneeRepo.getOne(baseDeDonnee.getId()).getNom());
		else {
			baseDeDonneeRepo.save(baseDeDonnee);
		}
		listeBaseDeDonnee.add(baseDeDonnee);

		resume.setListeBaseDeDonnee(listeBaseDeDonnee);
		resumeRepo.save(resume);

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeBaseDeDonneeId();
		if (affichage.getListeBaseDeDonneeId() == null)
			liste = new ArrayList();
		liste.add(baseDeDonnee.getId());
		affichage.setListeBaseDeDonneeId(liste);
		session.setAttribute("affichage", affichage);

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("retirerBaseDeDonneeResume/{resumeTechniqueEtFonctionnelId}/{baseDeDonneeId}")
	public String retirerBaseDeDonneeResume(Model model,
			@PathVariable(value = "baseDeDonneeId", required = true) Long baseDeDonneeId,
			@PathVariable(value = "resumeTechniqueEtFonctionnelId", required = true) Long resumeTechniqueEtFonctionnelId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeTechniqueEtFonctionnelId);
		List<BaseDeDonnee> listeBaseDeDonnee = resume.getListeBaseDeDonnee();
		for (BaseDeDonnee i : listeBaseDeDonnee) {
			if (i.getId() == baseDeDonneeId) {
				listeBaseDeDonnee.remove(i);
				break;
			}
		}

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("/saveSystemeExploitationResume/{resumeId}")
	public String saveSystemeExploitationResume(Model model,
			@Valid @ModelAttribute(value = "SystemeExploitation") SystemeExploitation systemeExploitation,
			BindingResult result, @PathVariable(value = "resumeId", required = true) Long resumeId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeId);

		List<SystemeExploitation> listeSystemeExploitation = resume.getListeSystemeExploitation();
		if (systemeExploitation.getId() != null)
			systemeExploitation.setNom(systemeExploitationRepo.getOne(systemeExploitation.getId()).getNom());
		else {
			systemeExploitationRepo.save(systemeExploitation);
		}
		listeSystemeExploitation.add(systemeExploitation);

		resume.setListeSystemeExploitation(listeSystemeExploitation);
		resumeRepo.save(resume);

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeSystemeExploitationId();
		if (affichage.getListeSystemeExploitationId() == null)
			liste = new ArrayList();
		liste.add(systemeExploitation.getId());
		affichage.setListeSystemeExploitationId(liste);
		session.setAttribute("affichage", affichage);

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	@Transactional
	@RequestMapping("retirerSystemeExploitationResume/{resumeTechniqueEtFonctionnelId}/{systemeExploitationId}")
	public String retirerSystemeExploitationResume(Model model,
			@PathVariable(value = "systemeExploitationId", required = true) Long systemeExploitationId,
			@PathVariable(value = "resumeTechniqueEtFonctionnelId", required = true) Long resumeTechniqueEtFonctionnelId,
			HttpServletRequest request) {

		ResumeTechniqueEtFonctionnel resume = resumeRepo.getOne(resumeTechniqueEtFonctionnelId);
		List<SystemeExploitation> listeSystemeExploitation = resume.getListeSystemeExploitation();
		for (SystemeExploitation i : listeSystemeExploitation) {
			if (i.getId() == systemeExploitationId) {
				listeSystemeExploitation.remove(i);
				break;
			}
		}

		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");

		return "profil";
	}

	private void prepareOutil(Model model, Experience experience) {

		List<Langage> listeLangage;
		List<Logiciel> listeLogiciel;
		List<BaseDeDonnee> listeBaseDeDonnee;
		List<SystemeExploitation> listeSystemeExploitation;

		/// GET LISTE LANGAGE ///

		listeLangage = experience.getListeLangage();
		if (listeLangage == null)
			listeLangage = new ArrayList<Langage>();

		/// GET LISTE LOGICIEL ///

		listeLogiciel = experience.getListeLogiciel();
		if (listeLogiciel == null)
			listeLogiciel = new ArrayList<Logiciel>();

		/// GET LISTE BDD ///

		listeBaseDeDonnee = experience.getListeBaseDeDonnee();
		if (listeBaseDeDonnee == null)
			listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();

		/// GET LISTE SYSTEME D'EXPLOITATION ///

		listeSystemeExploitation = experience.getListeSystemeExploitation();
		if (listeSystemeExploitation == null)
			listeSystemeExploitation = new ArrayList<SystemeExploitation>();

		model.addAttribute("listeLangage", listeLangage);
		model.addAttribute("listeLogiciel", listeLogiciel);
		model.addAttribute("listeBaseDeDonnee", listeBaseDeDonnee);
		model.addAttribute("listeSystemeExploitation", listeSystemeExploitation);

		model.addAttribute("experience", experience);

		model.addAttribute("langage", new Langage());
		model.addAttribute("logiciel", new Logiciel());
		model.addAttribute("baseDeDonnee", new BaseDeDonnee());
		model.addAttribute("systemeExploitation", new SystemeExploitation());

		model.addAttribute("listeGlobaleLangage", langageRepo.findAll());
		model.addAttribute("listeGlobaleLogiciel", logicielRepo.findAll());
		model.addAttribute("listeGlobaleSystemeExploitation", systemeExploitationRepo.findAll());
		model.addAttribute("listeGlobaleBaseDeDonnee", baseDeDonneeRepo.findAll());

	}

	@Transactional
	@RequestMapping("/removeLangage/{experienceId}/{outilId}")
	public String removeLangage(Model model, @PathVariable(value = "outilId", required = true) Long outilId,
			@PathVariable(value = "experienceId", required = true) Long experienceId) {

		Experience experience = experienceRepo.getOne(experienceId);
		List<Langage> listeLangage = experience.getListeLangage();
		for (Langage i : listeLangage) {
			if (i.getId() == outilId) {
				listeLangage.remove(i);
				break;
			}
		}
		experience.setListeLangage(listeLangage);
		experienceRepo.save(experience);

		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/removeLogiciel/{experienceId}/{outilId}")
	public String removeLogiciel(Model model, @PathVariable(value = "outilId", required = true) Long outilId,
			@PathVariable(value = "experienceId", required = true) Long experienceId) {

		Experience experience = experienceRepo.getOne(experienceId);
		List<Logiciel> listeLogiciel = experience.getListeLogiciel();
		for (Logiciel i : listeLogiciel) {
			if (i.getId() == outilId) {
				listeLogiciel.remove(i);
				break;
			}
		}
		experience.setListeLogiciel(listeLogiciel);
		experienceRepo.save(experience);

		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/removeSystemeExploitation/{experienceId}/{outilId}")
	public String removeSystemeExploitation(Model model, @PathVariable(value = "outilId", required = true) Long outilId,
			@PathVariable(value = "experienceId", required = true) Long experienceId) {

		Experience experience = experienceRepo.getOne(experienceId);
		List<SystemeExploitation> listeSystemeExploitation = experience.getListeSystemeExploitation();
		for (SystemeExploitation i : listeSystemeExploitation) {
			if (i.getId() == outilId) {
				listeSystemeExploitation.remove(i);
				break;
			}
		}
		experience.setListeSystemeExploitation(listeSystemeExploitation);
		experienceRepo.save(experience);

		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/removeBaseDeDonnee/{experienceId}/{outilId}")
	public String removeBaseDeDonnee(Model model, @PathVariable(value = "outilId", required = true) Long outilId,
			@PathVariable(value = "experienceId", required = true) Long experienceId) {

		Experience experience = experienceRepo.getOne(experienceId);
		List<BaseDeDonnee> listeBaseDeDonnee = experience.getListeBaseDeDonnee();
		for (BaseDeDonnee i : listeBaseDeDonnee) {
			if (i.getId() == outilId) {
				listeBaseDeDonnee.remove(i);
				break;
			}
		}
		experience.setListeBaseDeDonnee(listeBaseDeDonnee);
		experienceRepo.save(experience);

		prepareOutil(model, experience);

		return "outils";
	}

	@Transactional
	@RequestMapping("/saveCompetence")
	public String saveCompetence(Model model, @Valid @ModelAttribute(value = "competence") Competence competence,
			BindingResult result, HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();
		HttpSession session = request.getSession(true);

		if (parcours == null) {
			parcours = new Parcours();
			Affichage affichage = new Affichage();
			session.setAttribute("affichage", affichage);
		}

		/// CHECK FOR UPDATES ///

		if (!result.hasErrors()) {
			List<Competence> listeCompetence = parcours.getListeCompetence();
			if (listeCompetence != null) {
				for (int i = 0; i < listeCompetence.size(); i++) {

					if (competence.getIntitule().equals(listeCompetence.get(i).getIntitule())) {

						model.addAttribute("erreur", "doublon");
						result.rejectValue("intitule", "message d'erreur doublon competence");
						break;
					}
				}
			}
		}

		if (!result.hasErrors()) {
			competence.setNiveau(niveauRepo.getOne(competence.getNiveau().getId()));
			prepareProfil(model, competence, (long) 0, request);
		} else {
			prepareProfil(model, null, (long) 0, request);
		}

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeCompetenceId();
		if (affichage.getListeCompetenceId() == null)
			liste = new ArrayList();
		liste.add(competence.getId());
		affichage.setListeCompetenceId(liste);
		session.setAttribute("affichage", affichage);

		model.addAttribute("champActif", "competence");
		return "profil";

	}

	@GetMapping("/goToModifieCompetence/{competenceId}")
	public String goToModifieCompetence(@PathVariable(value = "competenceId", required = true) Long id, Model model,
			HttpServletRequest request) {

		model.addAttribute("idCompetenceAModifer", id);
		model.addAttribute("champActif", "competence");
		prepareProfil(model, null, null, request);
		return "profil";
	}

	@Transactional
	@PostMapping("/competenceModifier")
	public String competenceModifier(Model model, @Valid @ModelAttribute(value = "competence") Competence competence,
			BindingResult result, HttpServletRequest request) {

		prepareProfil(model, competence, competence.getId(), request);
		model.addAttribute("champActif", "competence");
		return "profil";
	}

	@Transactional
	@RequestMapping("/supprimeCompetence/{competenceId}")
	public String supprimeCompetence(@PathVariable(value = "competenceId", required = true) Long id, Model model,
			HttpServletRequest request) {
		competenceRepo.deleteById(id);
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "competence");
		return "profil";
	}

	@Transactional
	@RequestMapping("/saveTitre")
	public String saveTitre(Model model, @Valid @ModelAttribute(value = "titre") Titre titre, BindingResult result,
			HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();
		HttpSession session = request.getSession(true);

		if (parcours == null) {
			parcours = new Parcours();
			Affichage affichage = new Affichage();
			session.setAttribute("affichage", affichage);
		}

		/// CHECK FOR UPDATES ///

		if (!result.hasErrors()) {
			List<Titre> listeTitre = parcours.getListeTitre();
			if (listeTitre != null) {
				for (int i = 0; i < listeTitre.size(); i++) {

					if (titre.getTitre().equals(listeTitre.get(i).getTitre())) {

						model.addAttribute("erreur", "doublon");
						result.rejectValue("titre", "message d'erreur doublon titre");
						break;
					}
				}
			}
		}

		if (!result.hasErrors()) {

			prepareProfil(model, titre, (long) 0, request);
		} else {
			prepareProfil(model, null, (long) 0, request);
		}

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeTitreId();
		if (affichage.getListeTitreId() == null)
			liste = new ArrayList();
		liste.add(titre.getId());
		affichage.setListeTitreId(liste);
		session.setAttribute("affichage", affichage);

		model.addAttribute("champActif", "titre");
		return "profil";
	}

	@Transactional
	@RequestMapping("/supprimerTitre/{titreId}")
	public String supprimerTitre(@PathVariable(value = "titreId", required = true) Long id, Model model,
			HttpServletRequest request) {
		titreRepo.deleteById(id);
		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "titre");
		return "profil";
	}

	@RequestMapping("/saveCertification")
	public String saveCertification(Model model,
			@Valid @ModelAttribute(value = "certification") Certification certification, BindingResult result,
			HttpServletRequest request) {

		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		Parcours parcours = utilisateur.getParcours();
		HttpSession session = request.getSession(true);

		if (parcours == null) {
			parcours = new Parcours();
			Affichage affichage = new Affichage();
			session.setAttribute("affichage", affichage);
		}

		/// CHECK FOR UPDATES ///

		if (!result.hasErrors()) {
			List<Certification> listeCertification = parcours.getListeCertification();
			if (listeCertification != null) {
				for (int i = 0; i < listeCertification.size(); i++) {

					if (certification.getIntitule().equals(listeCertification.get(i).getIntitule())) {

						model.addAttribute("erreur", "doublon");
						result.rejectValue("intitule", "message d'erreur doublon titre");
						break;
					}
				}
			}
		}

		if (!result.hasErrors()) {

			prepareProfil(model, certification, (long) 0, request);
		} else {
			prepareProfil(model, null, (long) 0, request);
		}

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		Affichage affichage = (Affichage) session.getAttribute("affichage");
		if (affichage == null)
			affichage = new Affichage();
		List<Long> liste = affichage.getListeCertificationId();
		if (affichage.getListeCertificationId() == null)
			liste = new ArrayList();
		liste.add(certification.getId());
		affichage.setListeCertificationId(liste);
		session.setAttribute("affichage", affichage);

		model.addAttribute("champActif", "certification");
		return "profil";
	}

	@Transactional
	@RequestMapping("/supprimeCertification/{certificationId}")
	public String supprimeCertification(@PathVariable(value = "certificationId", required = true) Long id, Model model,
			HttpServletRequest request) {
		certificationRepo.deleteById(id);
		prepareProfil(model, null, (long) 0, request);

		model.addAttribute("champActif", "certification");
		return "profil";
	}

	@RequestMapping("/saveResumeTechniqueEtFonctionnel")
	public String saveResumeTechniqueEtFonctionnel(Model model,
			@Valid @ModelAttribute(value = "resumeTechniqueEtFonctionnel") ResumeTechniqueEtFonctionnel resumeTechniqueEtFonctionnel,
			BindingResult result, HttpServletRequest request) {

		Parcours parcours = AuthHelper.getUtilisateur().getParcours();
		System.out.println("parcours :" + parcours);

		/// RECUPERE LES LISTES OUTILS LIES A CE RESUME DE LA BDD ///

		if (resumeTechniqueEtFonctionnel.getId() != null) {
			ResumeTechniqueEtFonctionnel previousResume = resumeRepo.getOne(resumeTechniqueEtFonctionnel.getId());
			resumeTechniqueEtFonctionnel.setListeLangage(previousResume.getListeLangage());
			resumeTechniqueEtFonctionnel.setListeLogiciel(previousResume.getListeLogiciel());
			resumeTechniqueEtFonctionnel.setListeBaseDeDonnee(previousResume.getListeBaseDeDonnee());
			resumeTechniqueEtFonctionnel.setListeSystemeExploitation(previousResume.getListeSystemeExploitation());
		}

		resumeTechniqueEtFonctionnel.setParcours(parcours);
		resumeRepo.save(resumeTechniqueEtFonctionnel);

		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");
		return "profil";
	}

	@GetMapping("/rappatrieOutils")
	public String rappatrieOutils(Model model, HttpServletRequest request) {
		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());

		Parcours parcours = utilisateur.getParcours();
		Long parcoursId = parcours.getId();
		ResumeTechniqueEtFonctionnel resume = parcours.getResumeTechniqueEtFonctionnel();

		/// RECUPERE LES LISTES ///

		List<Langage> listeLangage = langageRepo.searchByParcoursId(parcoursId);
		List<Logiciel> listeLogiciel = logicielRepo.searchByParcoursId(parcoursId);
		List<BaseDeDonnee> listeBaseDeDonnee = baseDeDonneeRepo.searchByParcoursId(parcoursId);
		List<SystemeExploitation> listeSystemeExploitation = systemeExploitationRepo.searchByParcoursId(parcoursId);

		/// SAUVEGARDE ///

		resume.setListeLangage(listeLangage);
		resume.setListeLogiciel(listeLogiciel);
		resume.setListeBaseDeDonnee(listeBaseDeDonnee);
		resume.setListeSystemeExploitation(listeSystemeExploitation);
		resumeRepo.save(resume);

		/// MISE A JOUR DE LA VARIABLE AFFICHAGE ///

		HttpSession session = request.getSession(true);
		Affichage affichage = (Affichage) session.getAttribute("affichage");

		/// INITIALISATION DE LA LISTE DES LANGAGES ///

		List<Long> listeLangageId = new ArrayList<Long>();
		for (int i = 0; i < listeLangage.size(); i++) {
			listeLangageId.add(listeLangage.get(i).getId());
		}
		if (listeLangageId == null)
			affichage.setListeLangageId(listeLangageId);

		/// INITIALISATION DE LA LISTE DES LOGICIELS ///

		List<Long> listeLogicielId = new ArrayList<Long>();
		for (int i = 0; i < listeLogiciel.size(); i++) {
			listeLogicielId.add(listeLogiciel.get(i).getId());
		}
		if (listeLogicielId == null)
			affichage.setListeLogicielId(listeLogicielId);

		/// INITIALISATION DE LA LISTE DES BDD ///

		List<Long> listeBaseDeDonneeId = new ArrayList<Long>();
		for (int i = 0; i < listeBaseDeDonnee.size(); i++) {
			listeBaseDeDonneeId.add(listeBaseDeDonnee.get(i).getId());
		}
		if (listeBaseDeDonneeId == null)
			affichage.setListeBaseDeDonneeId(listeBaseDeDonneeId);

		/// INITIALISATION DE LA LISTE DES SYSTEMES D'EXPLOITATIONS ///

		List<Long> listeSystemeExploitationId = new ArrayList<Long>();
		for (int i = 0; i < listeSystemeExploitation.size(); i++) {
			listeSystemeExploitationId.add(listeSystemeExploitation.get(i).getId());
		}
		if (listeSystemeExploitationId == null)
			affichage.setListeSystemeExploitationId(listeSystemeExploitationId);

		session.setAttribute("affichage", affichage);

		prepareProfil(model, null, (long) 0, request);
		model.addAttribute("champActif", "resumeTechnicalAndFunctionalSkills");
		return "profil";
	}

	////////////////////////////////////////
	// Fonctionnalit�s de "prepareProfil":
	// 1. envoie tous les modeles attributes n�c�ssaires � l'affichage de la
	//////////////////////////////////////// jsp
	//////////////////////////////////////// profil.
	// 2. sauvegarde l'objet "save" d'id "Id" si cet objet n'est pas nul. (Si Id est
	//////////////////////////////////////// nul, un id est affect� � l'objet
	//////////////////////////////////////// lors
	//////////////////////////////////////// de la sauvegarde.)
	//////////////////////////////////////////
	public void prepareProfil(Model model, Object save, Long Id, HttpServletRequest request) {

		///// RECHERCHE LA VALEUR DES ATTRIBUTS DANS LA BASE DE DONNEE ET MISE EN PLACE
		///// DES LIENS////////
		/// get utilisateur from database ///
		Utilisateur utilisateur = utilisateurRepo.getOne(AuthHelper.getUtilisateur().getId());
		model.addAttribute("utilisateur", utilisateur);
		HttpSession session = request.getSession(true);

		/// GET PARCOURS ///

		Parcours parcours = utilisateur.getParcours();
		List<Langue> listeLangue;
		List<Formation> listeFormation;
		List<Experience> listeExperience;
		List<Competence> listeCompetence;
		List<Certification> listeCertification;
		List<Titre> listeTitre;
		List<Langage> listeLangage;
		List<Logiciel> listeLogiciel;
		List<BaseDeDonnee> listeBaseDeDonnee;
		List<SystemeExploitation> listeSystemeExploitation;
		ResumeTechniqueEtFonctionnel resumeTechniqueEtFonctionnel;

		if (parcours != null) {

			/// GET LISTE LANGUES ///

			listeLangue = parcours.getListeLangue();
			if (listeLangue == null)
				listeLangue = new ArrayList<Langue>();

			/// GET LISTE FORMATIONS ///

			listeFormation = parcours.getListeFormation();
			if (listeFormation == null)
				listeFormation = new ArrayList<Formation>();

			/// GET LISTE EXPERIENCES ///

			listeExperience = parcours.getListeExperience();
			if (listeExperience == null)
				listeExperience = new ArrayList<Experience>();

			/// GET LISTE COMPETENCES ///

			listeCompetence = parcours.getListeCompetence();
			if (listeCompetence == null)
				listeCompetence = new ArrayList<Competence>();

			/// GET LISTE CERTIFICATIONS ///

			listeCertification = parcours.getListeCertification();
			if (listeCertification == null)
				listeCertification = new ArrayList<Certification>();

			/// GET LISTE TITRES ///

			listeTitre = parcours.getListeTitre();
			if (listeTitre == null)
				listeTitre = new ArrayList<Titre>();

			/// GET RESUME TECHNIQUES ET FONCTIONNEL ///

			resumeTechniqueEtFonctionnel = parcours.getResumeTechniqueEtFonctionnel();
			if (resumeTechniqueEtFonctionnel == null)
				resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();

			/// GET LISTE LANGAGES ///

			listeLangage = resumeTechniqueEtFonctionnel.getListeLangage();
			if (listeLangage == null) {
				listeLangage = new ArrayList<Langage>();
			}

			/// GET LISTE LOGICIELS ///

			listeLogiciel = resumeTechniqueEtFonctionnel.getListeLogiciel();
			if (listeLogiciel == null) {
				listeLogiciel = new ArrayList<Logiciel>();
			}

			/// GET LISTE BDD ///

			listeBaseDeDonnee = resumeTechniqueEtFonctionnel.getListeBaseDeDonnee();
			if (listeBaseDeDonnee == null) {
				listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			}

			/// GET LISTE SYSTEMS D'EXPLOITATIONS ///

			listeSystemeExploitation = resumeTechniqueEtFonctionnel.getListeSystemeExploitation();
			if (listeSystemeExploitation == null) {
				listeSystemeExploitation = new ArrayList<SystemeExploitation>();
			}
		} else {
			parcours = new Parcours();
			Affichage affichage = new Affichage();
			session.setAttribute("affichage", affichage);
			utilisateur.setParcours(parcours);
			utilisateurRepo.save(utilisateur);
			listeLangue = new ArrayList<Langue>();
			listeFormation = new ArrayList<Formation>();
			listeExperience = new ArrayList<Experience>();
			listeCompetence = new ArrayList<Competence>();
			listeCertification = new ArrayList<Certification>();
			listeTitre = new ArrayList<Titre>();
			resumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
			listeLangage = new ArrayList<Langage>();
			listeLogiciel = new ArrayList<Logiciel>();
			listeBaseDeDonnee = new ArrayList<BaseDeDonnee>();
			listeSystemeExploitation = new ArrayList<SystemeExploitation>();
		}

		/// SAUVEGARDE ///

		if (save instanceof Langue) {
			Langue langue = (Langue) save;
			if (Id == 0) {
				listeLangue.add(langue);
				parcours.setListeLangue(listeLangue);
			}
			langue.setParcours(parcours);
			langueRepo.save(langue);
		}
		if (save instanceof Formation) {
			Formation formation = (Formation) save;
			if (Id == 0)
				listeFormation.add(formation);
			parcours.setListeFormation(listeFormation);
			formation.setParcours(parcours);
			formationRepo.save(formation);
		}
		if (save instanceof Experience) {
			Experience experience = (Experience) save;
			if (Id == 0) {
				listeExperience.add(experience);
				System.out.println("****1:" + listeExperience);
				parcours.setListeExperience(listeExperience);
				experience.setParcours(parcours);
			} else {
				experience.setListeLangage(experienceRepo.getOne(Id).getListeLangage());
				experience.setListeLogiciel(experienceRepo.getOne(Id).getListeLogiciel());
				experience.setListeBaseDeDonnee(experienceRepo.getOne(Id).getListeBaseDeDonnee());
				experience.setListeSystemeExploitation(experienceRepo.getOne(Id).getListeSystemeExploitation());
			}

			/// METIER SAUVE POUR POUVOIR CREER UN NOUVEAU METIER DANS LA LISTE ///

			if (experience.getMetier().getId() == null) {

				/// SUPPRESSION DES ACCENTS POUR LE NOM DE METIER ///

				String nomMetierNormalise = Normalizer.normalize(experience.getMetier().getNom(), Normalizer.Form.NFD);
				String nomMetierSansAccent = nomMetierNormalise.replaceAll("\\p{M}", "");
				System.out.println("essai accent : " + nomMetierSansAccent);

				/// PASSAGE DU NOM DE METIER EN MAJUSCULE ///

				experience.getMetier().setNom(nomMetierSansAccent.toUpperCase());
				metierRepo.save(experience.getMetier());
			}

			/// ENTREPRISE SAUVEE POUR POUVOIR CREER UNE NOUVELLE ENTREPRISE DANS LA LISTE
			/// ///

			System.out.println("nouveau nom entreprise =" + experience.getEntreprise().getNom());

			if (experience.getEntreprise().getId() == null) {

				/// SUPPRESSION DES ACCENTS POUR LE NOM D'ENTREPRISE ///

				String nomEntrepriseNormalise = Normalizer.normalize(experience.getEntreprise().getNom(),
						Normalizer.Form.NFD);
				String nomEntrepriseSansAccent = nomEntrepriseNormalise.replaceAll("\\p{M}", "");
				System.out.println("essai accent : " + nomEntrepriseSansAccent);

				/// PASSAGE DU NOM D'ENTREPRISE EN MAJUSCULE ///

				experience.getEntreprise().setNom(nomEntrepriseSansAccent.toUpperCase());
				entrepriseRepo.save(experience.getEntreprise());
			}
			experienceRepo.save(experience);
		}

		if (save instanceof Competence) {
			Competence competence = (Competence) save;
			if (Id == 0) {
				listeCompetence.add(competence);
				parcours.setListeCompetence(listeCompetence);
			}
			competence.setParcours(parcours);
			competenceRepo.save(competence);
		}
		if (save instanceof Certification) {
			Certification certification = (Certification) save;
			if (Id == 0)
				listeCertification.add(certification);
			parcours.setListeCertification(listeCertification);
			certification.setParcours(parcours);
			certificationRepo.save(certification);
		}

		if (save instanceof Titre) {
			Titre titre = (Titre) save;
			if (Id == 0)
				listeTitre.add(titre);
			parcours.setListeTitre(listeTitre);
			titre.setParcours(parcours);
			titreRepo.save(titre);
		}

		/// MISE EN PLACE DES MODELS ATTRIBUTES DE LA JSP PROFIL ///

		model.addAttribute("listeLangue", listeLangue);
		Collections.sort(listeFormation);
		model.addAttribute("listeFormation", listeFormation);
		Collections.sort(listeExperience, Collections.reverseOrder());
		model.addAttribute("listeExperience", listeExperience);
		model.addAttribute("listeCompetence", listeCompetence);
		model.addAttribute("listeCertification", listeCertification);
		model.addAttribute("listeTitre", listeTitre);
		model.addAttribute("resumeTechniqueEtFonctionnel", resumeTechniqueEtFonctionnel);
		model.addAttribute("listeLangage", listeLangage);
		model.addAttribute("listeLogiciel", listeLogiciel);
		model.addAttribute("listeBaseDeDonnee", listeBaseDeDonnee);
		model.addAttribute("listeSystemeExploitation", listeSystemeExploitation);

		/// NOUVELLE LANGUE ///

		Langue nouvellelangue = new Langue();
		model.addAttribute("langue", nouvellelangue);

		/// LISTE DES NOMS DES LANGUES ///

		List<NomLangue> nomLangue = nomLangueRepo.findAll();
		model.addAttribute("listeNomLangue", nomLangue);

		/// liste des nom nationalites ///

		List<NomNationalite> nomNationalite = nomNationaliteRepo.findAll();
		model.addAttribute("listeNomNationalite", nomNationalite);

		/// LISTE DES NOMS PAYS ///

		List<NomPays> nomPays = nomPaysRepo.findAll();
		model.addAttribute("listeNomPays", nomPays);

		/// LISTE DES NIVEAUX ///

		List<Niveau> niveaux = niveauRepo.findAll();
		model.addAttribute("listeNiveau", niveaux);

		/// NOUVELLE EXPERIENCE ///

		Experience nouvelleExperience = new Experience();
		model.addAttribute("experience", nouvelleExperience);

		/// NOUVELLE FORMATION ///

		Formation nouvelleFormation = new Formation();
		model.addAttribute("formation", nouvelleFormation);

		/// NOUVELLE COMPETENCE ///

		Competence nouvelleCompetence = new Competence();
		model.addAttribute("competence", nouvelleCompetence);

		/// NOUVELLE CERTIFICATION ///

		Certification nouvelleCertification = new Certification();
		model.addAttribute("certification", nouvelleCertification);

		/// NOUVEAU TITRE ///

		Titre nouveauTitre = new Titre();
		model.addAttribute("titre", nouveauTitre);

		/// NOUVEAU RESUME TECHNIQUE ET FONCTIONNEL ///

		ResumeTechniqueEtFonctionnel nouveauResumeTechniqueEtFonctionnel = new ResumeTechniqueEtFonctionnel();
		model.addAttribute("nouveauResumeTechniqueEtFonctionnel", nouveauResumeTechniqueEtFonctionnel);

		/// NOUVEAUX OUTILS ///

		model.addAttribute("langage", new Langage());
		model.addAttribute("logiciel", new Logiciel());
		model.addAttribute("systemeExploitation", new SystemeExploitation());
		model.addAttribute("baseDeDonnee", new BaseDeDonnee());

		/// GET LISTE LANGAGES ///

		List<Langage> langages = langageRepo.findAll();
		model.addAttribute("listeGlobaleLangage", langages);

		/// GET LISTE LOGICIELS ///

		List<Logiciel> logiciels = logicielRepo.findAll();
		model.addAttribute("listeGlobaleLogiciel", logiciels);

		/// GET LISTE SYSTEMES D'EXPLOITATIONS ///

		List<SystemeExploitation> systemeExploitations = systemeExploitationRepo.findAll();
		model.addAttribute("listeGlobaleSystemeExploitation", systemeExploitations);

		/// GET LISTE BDD ///

		List<BaseDeDonnee> baseDeDonnees = baseDeDonneeRepo.findAll();
		model.addAttribute("listeGlobaleBaseDeDonnee", baseDeDonnees);
	}

}
