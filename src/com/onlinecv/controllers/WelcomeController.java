package com.onlinecv.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import com.onlinecv.dao.IAdminJpaRepository;
import com.onlinecv.entities.Admin;
import com.onlinecv.entities.Affichage;
import com.onlinecv.entities.Credential;
import com.onlinecv.entities.ERole;
import com.onlinecv.entities.Utilisateur;

@Controller
@RequestMapping("/welcome")
public class WelcomeController {

	@Autowired
	private IAdminJpaRepository AdminRepo;

	@RequestMapping("/goToAccueil")
	public String goToAccueil(Model model) {

		// createElementsInDataBase();

		if (!AuthHelper.isAuthenticated()) {
			model.addAttribute("utilisateur", new Utilisateur());
			model.addAttribute("credential", new Credential());
			return "accueil";
		} else if (AuthHelper.getRole().equals(ERole.ROLE_ADMIN)) {
			return "redirect:/admin/goToMenuAdmin";
		}

		else if (AuthHelper.getRole().equals(ERole.ROLE_MANAGER)) {
			return "redirect:/manager/goToMenuManager";
		} else {
			return "redirect:/visiteur/goToMenuUtilisateur";
		}
	}

	@RequestMapping("/sauveAffichage")
	public String goToSelection(Model model, HttpServletRequest request,
			@ModelAttribute(value = "affichage") Affichage affichage, BindingResult result) {
		request.getSession().setAttribute("affichage", affichage);

		if (AuthHelper.getRole().equals(ERole.ROLE_ADMIN)) {
			return "redirect:/admin/sauveAffichage";
		} else if (AuthHelper.getRole().equals(ERole.ROLE_MANAGER)) {
			return "redirect:/manager/sauveAffichage";
		} else {
			return "redirect:/visiteur/sauveAffichage";
		}
	}

	private static void encodePassword(Credential authentification) {
		String rawPassword = authentification.getMotDePasse();
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodedPassword = encoder.encode(rawPassword);
		authentification.setMotDePasse(encodedPassword);
	}

	private void createElementsInDataBase() {
		Admin admin = new Admin();
		Credential credential = new Credential();
		credential.setNomLogin("root");
		credential.setMotDePasse("123456");
		credential.setRole(ERole.ROLE_ADMIN);
		encodePassword(credential);
		admin.setCredential(credential);
		AdminRepo.save(admin);
	}

}
