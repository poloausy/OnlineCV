package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Admin;

public interface IAdminJpaRepository extends JpaRepository<Admin, Long> {

	public Admin findByCredentialNomLogin(String nomLogin);

	@Query("select c from Admin c where c.id<>:id and c.credential.nomLogin=:nomLogin")
	public Admin findAdminByIdNotAndCredentialNomLogin(@Param("id") Long id, @Param("nomLogin") String nomLogin);

}
