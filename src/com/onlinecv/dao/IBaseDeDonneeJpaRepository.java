package com.onlinecv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.BaseDeDonnee;

public interface IBaseDeDonneeJpaRepository extends JpaRepository<BaseDeDonnee, Long> {

	@Query("SELECT DISTINCT l FROM Experience e, IN (e.listeBaseDeDonnee) l WHERE e.parcours.id = :parcoursId")
	public List searchByParcoursId(@Param("parcoursId") Long parcoursId);

	public BaseDeDonnee findByNom(String nom);
}
