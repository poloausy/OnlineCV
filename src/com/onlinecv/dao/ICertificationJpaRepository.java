package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Certification;

public interface ICertificationJpaRepository extends JpaRepository<Certification, Long> {

}
