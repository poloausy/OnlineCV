package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Competence;

public interface ICompetenceJpaRepository extends JpaRepository<Competence, Long> {

}
