package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Credential;

public interface ICredentialJpaRepository extends JpaRepository<Credential, Long> {

	public Credential findByNomLogin(String nomLogin);

	@Query("select c from Utilisateur c where c.id <> :id and c.credential.nomLogin=:nomLogin")
	public Credential findCredentialUtilisateurByIdNotAndCredentialNomLogin(@Param("nomLogin") String nomLogin,
			@Param("id") Long Id);

	public Credential findByEmail(String email);

}
