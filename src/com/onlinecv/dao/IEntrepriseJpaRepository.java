package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Entreprise;

public interface IEntrepriseJpaRepository extends JpaRepository<Entreprise, Long> {

	public Entreprise findByNom(String nom);

}
