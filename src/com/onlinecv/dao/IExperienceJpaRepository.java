package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Experience;

public interface IExperienceJpaRepository extends JpaRepository<Experience, Long> {

	@Query("SELECT e FROM Experience e INNER JOIN e.listeLangage WHERE e.id = (:id)")
	public Experience findByIdAndFetch(@Param("id") Long id);

	// Recherche par keyword langage, utilisé pour la recherche par nombre d'années
	// d'expérience
	@Query("SELECT e FROM Experience e INNER JOIN e.listeLangage ll WHERE ll.nom  LIKE %:outil%")
	public Experience searchByLangageLike(@Param("outil") String outil);

}
