package com.onlinecv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Langage;

public interface ILangageJpaRepository extends JpaRepository<Langage, Long> {

	@Query("SELECT DISTINCT l FROM Experience e, IN (e.listeLangage) l WHERE e.parcours.id = :parcoursId")
	public List searchByParcoursId(@Param("parcoursId") Long parcoursId);

	public Langage findByNom(String nom);
}
