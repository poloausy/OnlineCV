package com.onlinecv.dao;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import com.onlinecv.entities.Langue;

public interface ILangueJpaRepository extends JpaRepository<Langue, Long> {
	@Modifying
	@Transactional
	@Query("delete from Langue l where Parcours_ID = ?1")
	void DeleteByParcoursId(long Parcours_ID);
}
