package com.onlinecv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Logiciel;

public interface ILogicielJpaRepository extends JpaRepository<Logiciel, Long> {

	@Query("SELECT DISTINCT l FROM Experience e, IN (e.listeLogiciel) l WHERE e.parcours.id = :parcoursId")
	public List searchByParcoursId(@Param("parcoursId") Long parcoursId);

	public Logiciel findByNom(String nom);
}
