package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.Manager;

public interface IManagerJpaRepository extends JpaRepository<Manager, Long> {

	public Manager findByCredentialNomLogin(String nomLogin);

	@Query("select c from Manager c where c.id<>:id and c.credential.nomLogin=:nomLogin")
	public Manager findManagerByIdNotAndCredentialNomLogin(@Param("id") Long id, @Param("nomLogin") String nomLogin);

}
