package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Metier;

public interface IMetierJpaRepository extends JpaRepository<Metier, Long> {

	public Metier findByNom(String nom);
}
