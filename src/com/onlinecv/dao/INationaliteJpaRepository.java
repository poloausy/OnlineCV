package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Nationalite;

public interface INationaliteJpaRepository extends JpaRepository<Nationalite, Long> {

}
