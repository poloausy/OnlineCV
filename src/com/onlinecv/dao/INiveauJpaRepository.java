package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Niveau;

public interface INiveauJpaRepository extends JpaRepository<Niveau, Long> {

}
