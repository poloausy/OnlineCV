package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.NomLangue;

public interface INomLangueJpaRepository extends JpaRepository<NomLangue, Long> {

}
