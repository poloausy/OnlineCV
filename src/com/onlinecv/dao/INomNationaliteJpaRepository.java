package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.NomNationalite;

public interface INomNationaliteJpaRepository extends JpaRepository<NomNationalite, Long> {

}
