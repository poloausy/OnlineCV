package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.NomPays;

public interface INomPaysJpaRepository extends JpaRepository<NomPays, Long> {

}
