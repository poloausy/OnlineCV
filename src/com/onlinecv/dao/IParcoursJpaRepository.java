package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Parcours;

public interface IParcoursJpaRepository extends JpaRepository<Parcours, Long> {

}
