package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.ResumeTechniqueEtFonctionnel;

public interface IResumeTechniqueEtFonctionnelJpaRepository extends JpaRepository<ResumeTechniqueEtFonctionnel, Long> {

}
