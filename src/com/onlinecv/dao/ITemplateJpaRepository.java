package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Template;

public interface ITemplateJpaRepository extends JpaRepository<Template, Long> {

	public Template findTemplateByNom(String nom);

}
