package com.onlinecv.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.onlinecv.entities.Titre;

public interface ITitreJpaRepository extends JpaRepository<Titre, Long> {

}
