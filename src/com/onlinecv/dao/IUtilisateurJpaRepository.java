package com.onlinecv.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.onlinecv.entities.ERole;
import com.onlinecv.entities.Utilisateur;

public interface IUtilisateurJpaRepository extends JpaRepository<Utilisateur, Long> {

	public Utilisateur findByCredentialNomLogin(String nomLogin);

	// methode de tri sur la liste de tous les utilisateurs, on affiche UNIQUEMENT
	// les UTILISATEURS avec le role "ROLE_UTILISATEUR"
	public List<Utilisateur> findByCredentialRole(ERole role);

	@Query("select c from Utilisateur c where c.id<>:id and c.credential.nomLogin=:nomLogin")
	public Utilisateur findUtilisateurByIdNotAndCredentialNomLogin(@Param("id") Long id,
			@Param("nomLogin") String nomLogin);

	// Gives a list with all users that have an EXACT specific competence - written
	// by Arno
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Competence c ON u.parcours.id = c.parcours.id where c.intitule=:competence")
	public List searchByCompetence(@Param("competence") String competence);

	// search by competence keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Competence c ON u.parcours.id = c.parcours.id where c.intitule LIKE %:competence%")
	public List searchByCompetenceLike(@Param("competence") String competence);

	// search by competence keyword AND detail
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Competence c ON u.parcours.id = c.parcours.id where c.intitule LIKE %:competence% AND c.niveau.intitule2=:niveau")
	public List searchByCompetenceLikeAndDetail(@Param("competence") String competence, @Param("niveau") String niveau);

	// search by certification keyword
	@Query("SELECT u from Utilisateur u INNER JOIN Certification c ON u.parcours.id = c.parcours.id where c.intitule LIKE %:certification%")
	public List searchByCertificationLike(@Param("certification") String certification);

	// search by formation keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Formation f ON u.parcours.id = f.parcours.id where f.intitule LIKE %:formation%")
	public List searchByFormationLike(@Param("formation") String formation);

	// search by experience keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Experience e ON u.parcours.id = e.parcours.id where e.descriptifCV LIKE %:experience% or e.descriptif LIKE %:experience%")
	public List searchByExperienceLike(@Param("experience") String experience);

	// search by metier keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Experience e ON u.parcours.id = e.parcours.id "
			+ "INNER JOIN Metier m ON e.metier.id = m.id WHERE m.nom LIKE %:metier% ")
	public List searchByMetierLike(@Param("metier") String metier);

	// search by entreprise keyword
	@Query("SELECT DISTINCT u from Utilisateur u " + "INNER JOIN Experience e ON u.parcours.id = e.parcours.id "
			+ "INNER JOIN Entreprise ep ON e.entreprise.id = ep.id WHERE ep.nom LIKE %:entreprise% ")
	public List searchByEntrepriseLike(@Param("entreprise") String entreprise);

	// // search by langage keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Experience e ON u.parcours.id = e.parcours.id "
			+ "INNER JOIN e.listeLangage o WHERE o.nom LIKE %:outil% ")
	public List searchByLangageExperienceLike(@Param("outil") String outil);

	// // search by langage keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN ResumeTechniqueEtFonctionnel r ON u.parcours.id = r.parcours.id "
			+ "INNER JOIN r.listeLangage o WHERE o.nom LIKE %:outil% ")
	public List searchByLangageResumeLike(@Param("outil") String outil);

	// // search by logiciel keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Experience e ON u.parcours.id = e.parcours.id "
			+ "INNER JOIN e.listeLogiciel o WHERE o.nom LIKE %:outil% ")
	public List searchByLogicielExperienceLike(@Param("outil") String outil);

	// // search by logiciel keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN ResumeTechniqueEtFonctionnel r ON u.parcours.id = r.parcours.id "
			+ "INNER JOIN r.listeLogiciel o WHERE o.nom LIKE %:outil% ")
	public List searchByLogicielResumeLike(@Param("outil") String outil);

	// // search by base de donnees keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Experience e ON u.parcours.id = e.parcours.id "
			+ "INNER JOIN e.listeBaseDeDonnee o WHERE o.nom LIKE %:outil% ")
	public List searchByBaseDeDonneeExperienceLike(@Param("outil") String outil);

	// // search by base de donnees keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN ResumeTechniqueEtFonctionnel r ON u.parcours.id = r.parcours.id "
			+ "INNER JOIN r.listeBaseDeDonnee o WHERE o.nom LIKE %:outil% ")
	public List searchByBaseDeDonneeResumeLike(@Param("outil") String outil);

	// // search by syst�me d'exploitation keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Experience e ON u.parcours.id = e.parcours.id "
			+ "INNER JOIN e.listeSystemeExploitation o WHERE o.nom LIKE %:outil% ")
	public List searchBySystemeExploitationExperienceLike(@Param("outil") String outil);

	// // search by syst�me d'exploitation keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN ResumeTechniqueEtFonctionnel r ON u.parcours.id = r.parcours.id "
			+ "INNER JOIN r.listeSystemeExploitation o WHERE o.nom LIKE %:outil% ")
	public List searchBySystemeExploitationResumeLike(@Param("outil") String outil);

	// search by langue keyword
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Langue l ON u.parcours.id = l.parcours.id where l.nomLangue.nom LIKE %:langue%")
	public List searchByLangueLike(@Param("langue") String langue);

	// search by langue keyword AND detail (niveau)
	@Query("SELECT DISTINCT u from Utilisateur u INNER JOIN Langue l ON u.parcours.id = l.parcours.id where l.nomLangue.nom LIKE %:langue% AND l.niveau.intitule=:niveau")
	public List searchByLangueLikeAndDetail(@Param("langue") String langue, @Param("niveau") String niveau);

	// search by nationalite keyword
	@Query("SELECT DISTINCT u from Utilisateur u where u.nationalite_1 LIKE %:nationalite% or u.nationalite_2 LIKE %:nationalite%")
	public List searchByNationaliteLike(@Param("nationalite") String nationalite);

	// search by prenom | nom keyword
	@Query("SELECT DISTINCT u from Utilisateur u where u.nom LIKE %:name% or u.prenom LIKE %:name%")
	public List searchByName(@Param("name") String name);

}
