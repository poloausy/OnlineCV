package com.onlinecv.entities;

import java.util.List;

public class Affichage {

	private List<Long> listeExperienceId;

	private List<Long> listeTitreId;

	private List<Long> listeCompetenceId;

	private List<Long> listeLangueId;

	private List<Long> listeFormationId;

	private List<Long> listeCertificationId;

	private List<Long> listeLangageId;

	private List<Long> listeLogicielId;

	private List<Long> listeBaseDeDonneeId;

	private List<Long> listeSystemeExploitationId;

	private List<Long> listeLangageRId;

	private List<Long> listeLogicielRId;

	private List<Long> listeBaseDeDonneeRId;

	private List<Long> listeSystemeExploitationRId;

	public Affichage() {
		super();
	}

	public List<Long> getListeExperienceId() {
		return listeExperienceId;
	}

	public void setListeExperienceId(List<Long> listeExperienceId) {
		this.listeExperienceId = listeExperienceId;
	}

	public List<Long> getListeTitreId() {
		return listeTitreId;
	}

	public void setListeTitreId(List<Long> listeTitreId) {
		this.listeTitreId = listeTitreId;
	}

	public List<Long> getListeCompetenceId() {
		return listeCompetenceId;
	}

	public void setListeCompetenceId(List<Long> listeCompetenceId) {
		this.listeCompetenceId = listeCompetenceId;
	}

	public List<Long> getListeLangueId() {
		return listeLangueId;
	}

	public void setListeLangueId(List<Long> listeLangueId) {
		this.listeLangueId = listeLangueId;
	}

	public List<Long> getListeFormationId() {
		return listeFormationId;
	}

	public void setListeFormationId(List<Long> listeFormationId) {
		this.listeFormationId = listeFormationId;
	}

	public List<Long> getListeCertificationId() {
		return listeCertificationId;
	}

	public void setListeCertificationId(List<Long> listeCertificationId) {
		this.listeCertificationId = listeCertificationId;
	}

	public List<Long> getListeLangageId() {
		return listeLangageId;
	}

	public void setListeLangageId(List<Long> listeLangageId) {
		this.listeLangageId = listeLangageId;
	}

	public List<Long> getListeLogicielId() {
		return listeLogicielId;
	}

	public void setListeLogicielId(List<Long> listeLogicielId) {
		this.listeLogicielId = listeLogicielId;
	}

	public List<Long> getListeBaseDeDonneeId() {
		return listeBaseDeDonneeId;
	}

	public void setListeBaseDeDonneeId(List<Long> listeBaseDeDonneeId) {
		this.listeBaseDeDonneeId = listeBaseDeDonneeId;
	}

	public List<Long> getListeSystemeExploitationId() {
		return listeSystemeExploitationId;
	}

	public void setListeSystemeExploitationId(List<Long> listeSystemeExploitationId) {
		this.listeSystemeExploitationId = listeSystemeExploitationId;
	}

	public List<Long> getListeLangageRId() {
		return listeLangageRId;
	}

	public void setListeLangageRId(List<Long> listeLangageRId) {
		this.listeLangageRId = listeLangageRId;
	}

	public List<Long> getListeLogicielRId() {
		return listeLogicielRId;
	}

	public void setListeLogicielRId(List<Long> listeLogicielRId) {
		this.listeLogicielRId = listeLogicielRId;
	}

	public List<Long> getListeBaseDeDonneeRId() {
		return listeBaseDeDonneeRId;
	}

	public void setListeBaseDeDonneeRId(List<Long> listeBaseDeDonneeRId) {
		this.listeBaseDeDonneeRId = listeBaseDeDonneeRId;
	}

	public List<Long> getListeSystemeExploitationRId() {
		return listeSystemeExploitationRId;
	}

	public void setListeSystemeExploitationRId(List<Long> listeSystemeExploitationRId) {
		this.listeSystemeExploitationRId = listeSystemeExploitationRId;
	}

	@Override
	public String toString() {
		return "Affichage [listeExperienceId=" + listeExperienceId + ", listeTitreId=" + listeTitreId
				+ ", listeCompetenceId=" + listeCompetenceId + ", listeLangueId=" + listeLangueId
				+ ", listeFormationId=" + listeFormationId + ", listeCertificationId=" + listeCertificationId
				+ ", listeLangageId=" + listeLangageId + ", listeLogicielId=" + listeLogicielId
				+ ", listeBaseDeDonneeId=" + listeBaseDeDonneeId + ", listeSystemeExploitationId="
				+ listeSystemeExploitationId + "]";
	}

}
