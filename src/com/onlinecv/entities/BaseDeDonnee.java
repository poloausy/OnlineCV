package com.onlinecv.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class BaseDeDonnee implements Comparable<BaseDeDonnee> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(unique = true, nullable = false)
	private String nom;

	public BaseDeDonnee() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	@Override
	public String toString() {
		return "BaseDeDonnee [id=" + id + ", nom=" + nom + "]";
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public int compareTo(BaseDeDonnee o) {
		return this.nom.compareTo(o.nom);
	}
}
