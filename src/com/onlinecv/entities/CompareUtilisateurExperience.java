package com.onlinecv.entities;

import java.util.Comparator;

public class CompareUtilisateurExperience implements Comparator<Utilisateur> {

	@Override
	public int compare(Utilisateur u1, Utilisateur u2) {
		// Condition pour éviter une exception si des utilisateurs ont des expérience de
		// valeur null
		if (u1.getNbAnneeExperience() == null)
			u1.setNbAnneeExperience(0.0);
		if (u2.getNbAnneeExperience() == null)
			u2.setNbAnneeExperience(0.0);
		System.out.println("comparaison =" + u1.getNbAnneeExperience().compareTo(u2.getNbAnneeExperience()));
		return u1.getNbAnneeExperience().compareTo(u2.getNbAnneeExperience());
	}
}
