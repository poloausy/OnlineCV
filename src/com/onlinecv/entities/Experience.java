package com.onlinecv.entities;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Experience implements Comparable<Experience> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@DateTimeFormat(pattern = "yyyy-MM")
	private Date dateDebut;

	@DateTimeFormat(pattern = "yyyy-MM")
	private Date dateFin;

	private String projet;

	@Column(columnDefinition = "LONGTEXT")
	private String descriptif;

	private String descriptifCV;

	@ManyToOne
	private Metier metier;

	@ManyToOne
	private Entreprise entreprise;

	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(name = "experience_Langage", joinColumns = @JoinColumn(name = "experience_id"), inverseJoinColumns = @JoinColumn(name = "langage_id"))
	List<Langage> listeLangage;

	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(name = "experience_logiciel", joinColumns = @JoinColumn(name = "experience_id"), inverseJoinColumns = @JoinColumn(name = "logiciel_id"))
	List<Logiciel> listeLogiciel;

	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(name = "experience_systemeExploitation", joinColumns = @JoinColumn(name = "experience_id"), inverseJoinColumns = @JoinColumn(name = "systemeExploitation_id"))
	List<SystemeExploitation> listeSystemeExploitation;

	@ManyToMany(cascade = { CascadeType.MERGE })
	@JoinTable(name = "experience_baseDeDonnee", joinColumns = @JoinColumn(name = "experience_id"), inverseJoinColumns = @JoinColumn(name = "baseDeDonnee_id"))
	List<BaseDeDonnee> listeBaseDeDonnee;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "Parcours_ID")
	private Parcours parcours;

	public Experience() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDateDebut() {
		return dateDebut;
	}

	public void setDateDebut(Date dateDebut) {
		this.dateDebut = dateDebut;
	}

	public Date getDateFin() {
		return dateFin;
	}

	public void setDateFin(Date dateFin) {
		this.dateFin = dateFin;
	}

	public String getDescriptif() {
		return descriptif;
	}

	public void setDescriptif(String descriptif) {
		this.descriptif = descriptif;
	}

	public Metier getMetier() {
		return metier;
	}

	public void setMetier(Metier metier) {
		this.metier = metier;
	}

	public Entreprise getEntreprise() {
		return entreprise;
	}

	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}

	public Parcours getParcours() {
		return parcours;
	}

	public void setParcours(Parcours parcours) {
		this.parcours = parcours;
	}

	public String getProjet() {
		return projet;
	}

	public void setProjet(String projet) {
		this.projet = projet;
	}

	public List<Langage> getListeLangage() {
		return listeLangage;
	}

	public void setListeLangage(List<Langage> listeLangage) {
		this.listeLangage = listeLangage;
	}

	public List<Logiciel> getListeLogiciel() {
		return listeLogiciel;
	}

	public void setListeLogiciel(List<Logiciel> listeLogiciel) {
		this.listeLogiciel = listeLogiciel;
	}

	public List<SystemeExploitation> getListeSystemeExploitation() {
		return listeSystemeExploitation;
	}

	public void setListeSystemeExploitation(List<SystemeExploitation> listeSystemeExploitation) {
		this.listeSystemeExploitation = listeSystemeExploitation;
	}

	public List<BaseDeDonnee> getListeBaseDeDonnee() {
		return listeBaseDeDonnee;
	}

	public void setListeBaseDeDonnee(List<BaseDeDonnee> listeBaseDeDonnee) {
		this.listeBaseDeDonnee = listeBaseDeDonnee;
	}

	public String getDescriptifCV() {
		return descriptifCV;
	}

	public void setDescriptifCV(String descriptifCV) {
		this.descriptifCV = descriptifCV;
	}

	@Override
	public String toString() {
		return "Experience [id=" + id + "]";
	}

	@Override
	public int compareTo(Experience compareExp) {
		Date dateExp = this.dateFin;
		Date dateCompareExp = compareExp.getDateFin();
		return dateExp.compareTo(dateCompareExp);
	}

}
