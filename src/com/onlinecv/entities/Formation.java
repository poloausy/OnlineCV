package com.onlinecv.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Formation implements Comparable<Formation> {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@NotEmpty(message = "champs intitule requis")
	private String intitule;

	@NotEmpty(message = "champs annee requis")
	private String anneeObtention;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "Parcours_ID")
	private Parcours parcours;

	public Formation() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public String getAnneeObtention() {
		return anneeObtention;
	}

	public void setAnneeObtention(String anneeObtention) {
		this.anneeObtention = anneeObtention;
	}

	public Parcours getParcours() {
		return parcours;
	}

	public void setParcours(Parcours parcours) {
		this.parcours = parcours;
	}

	@Override
	public String toString() {
		return "Formation [id=" + id + ", intitule=" + intitule + ", anneeObtention=" + anneeObtention + "]";
	}

	@Override
	public int compareTo(Formation compareFormation) {
		String anneeObtention = this.anneeObtention;
		String anneeObtentionCompare = compareFormation.getAnneeObtention();
		return anneeObtention.compareTo(anneeObtentionCompare);
	}

}
