package com.onlinecv.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Langue {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private Niveau niveau;

	@NotNull(message = "{error.langue.required}")
	@ManyToOne
	private NomLangue nomLangue;

	@ManyToOne
	@JsonIgnore
	@JoinColumn(name = "Parcours_ID")
	private Parcours parcours;

	public Langue() {
	}

	public Long getId() {
		return id;
	}

	public Parcours getParcours() {
		return parcours;
	}

	public void setParcours(Parcours parcours) {
		this.parcours = parcours;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NomLangue getNomLangue() {
		return nomLangue;
	}

	public void setNomLangue(NomLangue nomLangue) {
		this.nomLangue = nomLangue;
	}

	public Niveau getNiveau() {
		return niveau;
	}

	public void setNiveau(Niveau niveau) {
		this.niveau = niveau;
	}

	@Override
	public String toString() {
		return "Langue id=" + id + ", niveau=" + niveau + ", nomLangue=" + nomLangue;
	}

}
