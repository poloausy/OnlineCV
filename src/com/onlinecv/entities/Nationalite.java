package com.onlinecv.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Nationalite {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@ManyToOne
	private NomNationalite nomNationalite;

	@ManyToOne
	@JoinColumn(name = "utilisateur_ID")
	private Utilisateur utilisateur;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public NomNationalite getNomNationalite() {
		return nomNationalite;
	}

	public void setNomNationalite(NomNationalite nomNationalite) {
		this.nomNationalite = nomNationalite;
	}

	public Utilisateur getUtilisateur() {
		return utilisateur;
	}

	public void setUtilisateur(Utilisateur utilisateur) {
		this.utilisateur = utilisateur;
	}

	public Nationalite() {

	}

}
