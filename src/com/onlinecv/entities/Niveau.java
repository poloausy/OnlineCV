package com.onlinecv.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Niveau {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private int valeur;

	private String intitule;

	private String intitule2;

	public int getValeur() {
		return valeur;
	}

	public void setValeur(int valeur) {
		this.valeur = valeur;
	}

	public String getIntitule() {
		return intitule;
	}

	public void setIntitule(String intitule) {
		this.intitule = intitule;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIntitule2() {
		return intitule2;
	}

	public void setIntitule2(String intitule2) {
		this.intitule2 = intitule2;
	}

	@Override
	public String toString() {
		return "Niveau [id=" + id + ", valeur=" + valeur + ", intitule=" + intitule + ", intitule2=" + intitule2 + "]";
	}

}
