package com.onlinecv.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class NomNationalite {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String nom;
	private String continent;

	public NomNationalite() {

	}

	public NomNationalite(Long id, String nom, String continent) {
		this.id = id;
		this.nom = nom;
		this.continent = continent;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getContinent() {
		return continent;
	}

	public void setContinent(String continent) {
		this.continent = continent;
	}

	@Override
	public String toString() {
		return "NomNationalite [id=" + id + ", nom=" + nom + ", continent=" + continent + "]";
	}

}
