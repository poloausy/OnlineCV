package com.onlinecv.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@Entity
public class Parcours {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToMany(mappedBy = "parcours", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<Langue> listeLangue;
	@OneToMany(mappedBy = "parcours", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<Formation> listeFormation;
	@OneToMany(mappedBy = "parcours", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<Experience> listeExperience;
	@OneToMany(mappedBy = "parcours", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<Competence> listeCompetence;
	@OneToMany(mappedBy = "parcours", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<Certification> listeCertification;
	@OneToMany(mappedBy = "parcours", cascade = { CascadeType.PERSIST, CascadeType.MERGE }, orphanRemoval = true)
	private List<Titre> listeTitre;
	@OneToOne(mappedBy = "parcours", cascade = { CascadeType.PERSIST }, orphanRemoval = true)
	private ResumeTechniqueEtFonctionnel ResumeTechniqueEtFonctionnel;

	@Transient
	private Affichage affichage;

	public Parcours() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<Langue> getListeLangue() {
		return listeLangue;
	}

	public void setListeLangue(List<Langue> listeLangue) {
		this.listeLangue = listeLangue;
	}

	public List<Formation> getListeFormation() {
		return listeFormation;
	}

	public void setListeFormation(List<Formation> listeFormation) {
		this.listeFormation = listeFormation;
	}

	public List<Experience> getListeExperience() {
		return listeExperience;
	}

	public void setListeExperience(List<Experience> listeExperience) {
		this.listeExperience = listeExperience;
	}

	public List<Competence> getListeCompetence() {
		return listeCompetence;
	}

	public void setListeCompetence(List<Competence> listeCompetence) {
		this.listeCompetence = listeCompetence;
	}

	public List<Certification> getListeCertification() {
		return listeCertification;
	}

	public void setListeCertification(List<Certification> listeCertification) {
		this.listeCertification = listeCertification;
	}

	public List<Titre> getListeTitre() {
		return listeTitre;
	}

	public void setListeTitre(List<Titre> listeTitre) {
		this.listeTitre = listeTitre;
	}

	public Affichage getAffichage() {
		return affichage;
	}

	public void setAffichage(Affichage affichage) {
		this.affichage = affichage;
	}

	public ResumeTechniqueEtFonctionnel getResumeTechniqueEtFonctionnel() {
		return ResumeTechniqueEtFonctionnel;
	}

	public void setResumeTechniqueEtFonctionnel(ResumeTechniqueEtFonctionnel resumeTechniqueEtFonctionnel) {
		ResumeTechniqueEtFonctionnel = resumeTechniqueEtFonctionnel;

	}

	@Override
	public String toString() {
		return "Parcours [id=" + id + ", listeLangue=" + listeLangue + ", listeFormation=" + listeFormation
				+ ", listeExperience=" + listeExperience + ", listeCompetence=" + listeCompetence
				+ ", listeCertification=" + listeCertification + ", listeTitre=" + listeTitre
				+ ", ResumeTechniqueEtFonctionnel=" + ResumeTechniqueEtFonctionnel + "]";
	}

}
