package com.onlinecv.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class ResumeTechniqueEtFonctionnel {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(columnDefinition = "LONGTEXT")
	private String principalCompetences;

	private String methodes;

	private String industries;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "resume_Langage", joinColumns = @JoinColumn(name = "resume_id"), inverseJoinColumns = @JoinColumn(name = "langage_id"))
	List<Langage> listeLangage;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "resume_logiciel", joinColumns = @JoinColumn(name = "resume_id"), inverseJoinColumns = @JoinColumn(name = "logiciel_id"))
	List<Logiciel> listeLogiciel;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "resume_systemeExploitation", joinColumns = @JoinColumn(name = "resume_id"), inverseJoinColumns = @JoinColumn(name = "systemeExploitation_id"))
	List<SystemeExploitation> listeSystemeExploitation;

	@ManyToMany(cascade = { CascadeType.PERSIST, CascadeType.MERGE })
	@JoinTable(name = "resume_baseDeDonnee", joinColumns = @JoinColumn(name = "resume_id"), inverseJoinColumns = @JoinColumn(name = "baseDeDonnee_id"))
	List<BaseDeDonnee> listeBaseDeDonnee;

	@OneToOne(cascade = CascadeType.MERGE)
	@JsonIgnore
	private Parcours parcours;

	public ResumeTechniqueEtFonctionnel() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getPrincipalCompetences() {
		return principalCompetences;
	}

	public void setPrincipalCompetences(String principalCompetences) {
		this.principalCompetences = principalCompetences;
	}

	public String getIndustries() {
		return industries;
	}

	public void setIndustries(String industries) {
		this.industries = industries;
	}

	public Parcours getParcours() {
		return parcours;
	}

	public void setParcours(Parcours parcours) {
		this.parcours = parcours;
	}

	public String getMethodes() {
		return methodes;
	}

	public void setMethodes(String methodes) {
		this.methodes = methodes;
	}

	public List<Langage> getListeLangage() {
		return listeLangage;
	}

	public void setListeLangage(List<Langage> listeLangage) {
		this.listeLangage = listeLangage;
	}

	public List<Logiciel> getListeLogiciel() {
		return listeLogiciel;
	}

	public void setListeLogiciel(List<Logiciel> listeLogiciel) {
		this.listeLogiciel = listeLogiciel;
	}

	public List<SystemeExploitation> getListeSystemeExploitation() {
		return listeSystemeExploitation;
	}

	public void setListeSystemeExploitation(List<SystemeExploitation> listeSystemeExploitation) {
		this.listeSystemeExploitation = listeSystemeExploitation;
	}

	public List<BaseDeDonnee> getListeBaseDeDonnee() {
		return listeBaseDeDonnee;
	}

	public void setListeBaseDeDonnee(List<BaseDeDonnee> listeBaseDeDonnee) {
		this.listeBaseDeDonnee = listeBaseDeDonnee;
	}

	@Override
	public String toString() {
		return "ResumeTechniqueEtFonctionnel [id=" + id + ", principalCompetences=" + principalCompetences
				+ ", methodes=" + methodes + ", industries=" + industries + "]";
	}

}
