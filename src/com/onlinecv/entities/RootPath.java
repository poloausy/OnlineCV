package com.onlinecv.entities;

//CLASSE PERMETTANT DE DETERMINER LE REPERTOIRE COURANT QUELQUE SOIT LE POSTE DE TRAVAIL
public class RootPath {

	String rootName = new String();

	public String getRootName() {
		return rootName;
	}

	public void setRootName(String rootName) {
		this.rootName = rootName;
	}

	// Recherche du répertoire de travail quelque soit le poste de travail pour
	// pouvoir copier des fichiers sans paramètre pour coder en dur le répertoire de
	// recherche uniquement ici
	// public void RecupRootPath() {
	// try {
	// // // liste de tous les fichiers et répertoires sous le chemin "dir" au 1er
	// // niveau
	// File dir = new File(File.separator + "opt" + File.separator + "tomcat" +
	// File.separator + "webapps"
	// + File.separator + "ProjetOnlineCv");
	// File[] files = dir.listFiles();
	// // // Boucle sur tous les répertoires
	// for (File file : files) {
	// if (file.isDirectory()) {
	// // // Recherche le répertoire avec le pattern suivant
	// WEB-INF\jspf\templateCv.
	// // // On prend comme hypothèse que le résultat de la recherche est unique.
	// Pattern uName = Pattern.compile(
	// File.separator + "WEB-INF" + File.separator + "jspf" + File.separator +
	// "templateCv");
	// String fileName = file.getCanonicalPath();
	// Matcher mUname = uName.matcher(fileName);
	// if (mUname.matches()) {
	// rootName = file.getCanonicalPath().substring(0,
	// file.getCanonicalPath().length() - 35);
	// // System.out.println("-------------");
	// // System.out.println("root directory: " + rootName);
	// // System.out.println("-------------");
	// } else {
	// // // Boucle et recherche sur les sous répertoires
	// RecupRootPath(file);
	// //
	// }
	// }
	// }
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

	// Recherche des répertoire de travail quelque soit le poste de travail pour
	// pouvoir copier des fichiers, méthode avec un paramètre destiné à la
	// recherche dans les sous répertoires
	// public void RecupRootPath(File dir) {
	// try {
	// // liste de tous les fichiers et répertoires sous le chemin "dir" au 1er
	// // niveau
	// File[] files = dir.listFiles();
	// // // Boucle sur tous les répertoires
	// for (File file : files) {
	// if (file.isDirectory()) {
	// // // On prend comme hypothèse que le résultat de la recherche sur le chemin
	// est
	// // // unique
	// Pattern uName = Pattern.compile(
	// File.separator + "WEB-INF" + File.separator + "jspf" + File.separator +
	// "templateCv");
	// String fileName = file.getCanonicalPath();
	// Matcher mUname = uName.matcher(fileName);
	// if (mUname.matches()) {
	// rootName = file.getCanonicalPath().substring(0,
	// file.getCanonicalPath().length() - 35);
	// System.out.println("-------------");
	// System.out.println("root directory retour:" + rootName);
	// System.out.println("-------------");
	// } else {
	// // Boucle et recherche sur les sous répertoires
	// RecupRootPath(file);
	//
	// }
	// }
	// }
	//
	// } catch (IOException e) {
	// e.printStackTrace();
	// }
	// }

}
