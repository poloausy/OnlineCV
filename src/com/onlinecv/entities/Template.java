package com.onlinecv.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Template implements Comparable<Template> { // comparable = comparaison entre 2 templates
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@Column(unique = true, nullable = false)
	private String nom;

	public Template() {

	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Template [id=" + id + ", nom=" + nom + "]";
	}

	// fonction de comparaison, le trie se fait par l'ordre alphabétique du Nom

	@Override
	public int compareTo(Template templateCompare) {
		String nom = this.nom;
		String nomCompare = templateCompare.getNom();
		return nom.toLowerCase().compareTo(nomCompare.toLowerCase());
	}
}
