package com.onlinecv.entities;

import javax.persistence.CascadeType;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Transient;
import javax.validation.Valid;

@Entity
public class Utilisateur implements Comparable<Utilisateur> { // comparable = comparaisson entre 2 utilisateurs

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String nom;

	private String prenom;

	// private String titre;

	@Valid
	@OneToOne(cascade = CascadeType.ALL)
	private Credential credential;

	private String dateDeNaissance;

	@Embedded
	private Adresse adresse;

	private String telephone;

	private String nationalite_1;
	private String nationalite_2;

	// to delete parcours when user is deleted
	@OneToOne(cascade = CascadeType.ALL)
	// @JsonIgnore
	private Parcours parcours;
	private String photo;

	@Transient
	private Double NbAnneeExperience;

	public Utilisateur() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public Credential getCredential() {
		return credential;
	}

	public void setCredential(Credential credential) {
		this.credential = credential;
	}

	public String getDateDeNaissance() {
		return dateDeNaissance;
	}

	public void setDateDeNaissance(String dateDeNaissance) {
		this.dateDeNaissance = dateDeNaissance;
	}

	public Adresse getAdresse() {
		return adresse;
	}

	public void setAdresse(Adresse adresse) {
		this.adresse = adresse;
	}

	public String getNationalite_1() {
		return nationalite_1;
	}

	public void setNationalite_1(String nationalite_1) {
		this.nationalite_1 = nationalite_1;
	}

	public String getNationalite_2() {
		return nationalite_2;
	}

	public void setNationalite_2(String nationalite_2) {
		this.nationalite_2 = nationalite_2;
	}

	public String getTelephone() {
		return telephone;
	}

	public void setTelephone(String telephone) {
		this.telephone = telephone;
	}

	public Parcours getParcours() {
		return parcours;
	}

	public void setParcours(Parcours parcours) {
		this.parcours = parcours;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Double getNbAnneeExperience() {
		return NbAnneeExperience;
	}

	public void setNbAnneeExperience(Double nbAnneeExperience) {
		NbAnneeExperience = nbAnneeExperience;
	}

	@Override
	public boolean equals(Object o) {
		Utilisateur other = (Utilisateur) o;
		return (this.getId() == other.getId());
	}

	@Override
	public int hashCode() {
		Integer hash = (int) (long) id;
		return hash;
	}

	@Override
	public String toString() {
		// return "Utilisateur [id=" + id + ", nom=" + nom + ", prenom=" + prenom + ",
		// credential=" + credential
		// + ", dateDeNaissance=" + dateDeNaissance + ", adresse=" + adresse + ",
		// telephone=" + telephone
		// + ", nationalite_1=" + nationalite_1 + ", nationalite_2=" + nationalite_2 +
		// ", parcours=" + parcours
		// + ", photo=" + photo + "]";
		return "Utilisateur [id=" + id + ", nbAnneeExperience=" + NbAnneeExperience + "]";
	}

	// fonction de comparaisson, le trie se fait d'abord par l'ordre alphabétique du
	// Nom ensuite du Prénom
	@Override
	public int compareTo(Utilisateur utilisateurCompare) {
		String nomPrenom = this.nom + this.prenom;
		String nomPrenomCompare = utilisateurCompare.getNom() + utilisateurCompare.getPrenom();
		return nomPrenom.toLowerCase().compareTo(nomPrenomCompare.toLowerCase());
	}

}
