package com.onlinecv.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.onlinecv.config.Principal;
import com.onlinecv.dao.IAdminJpaRepository;
import com.onlinecv.dao.IManagerJpaRepository;
import com.onlinecv.dao.IUtilisateurJpaRepository;
import com.onlinecv.entities.Admin;
import com.onlinecv.entities.Utilisateur;

@Service
public class LoginService implements UserDetailsService {

	@Autowired
	private IUtilisateurJpaRepository utilisateurRepo;

	@Autowired
	private IAdminJpaRepository adminRepo;

	@Autowired
	private IManagerJpaRepository managerRepo;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		Utilisateur utilisateur = utilisateurRepo.findByCredentialNomLogin(username);
		if (null == utilisateur) {
			Admin admin = adminRepo.findByCredentialNomLogin(username);
			if (null == admin) {
				throw new UsernameNotFoundException("No user found with this Login: " + username);
			}
			return new Principal(admin);
		}
		return new Principal(utilisateur);
	}
}
