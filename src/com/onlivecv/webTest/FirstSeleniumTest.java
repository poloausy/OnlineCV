package com.onlivecv.webTest;

import java.awt.AWTException;
import java.awt.Robot;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

public class FirstSeleniumTest {
	static Robot screenWin;

	public static void main(String[] args) throws InterruptedException, AWTException {
		// initiaisation et ouverture navigateur
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "data\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://localhost:8080/ProjetOnlineCv/securitycontroller/login");
		driver.manage().window().fullscreen();

		// déclaration identifiants utilisateur
		String nom = new String();
		String mail = new String();
		String mdp = "ausy2018";
		// valeur autoincrément
		int val = 0;
		while (true) {
			// curseur souris
			screenWin = new Robot();

			// affectation des variables utilisateur
			nom = "Durand" + val;
			mail = "jdurand" + val + "@ausy.fr";
			TimeUnit.SECONDS.sleep(1);

			CreateUser(driver, nom, mail, mdp);
			ConnectUser(driver, nom, mdp);
			AjoutElementProfil(driver);
			TimeUnit.SECONDS.sleep(1);
			PlacerCurseur(driver, screenWin, "logoutuser");
			TimeUnit.SECONDS.sleep(1);
			driver.findElement(By.id("logoutuser")).click();
			TimeUnit.SECONDS.sleep(1);
			ConnectUser(driver, nom, mdp);
			CreerResumerTechniqueFonctionnel(driver);
			CV(driver);
			TimeUnit.SECONDS.sleep(1);
			PlacerCurseur(driver, screenWin, "backtotemplate");
			TimeUnit.SECONDS.sleep(1);
			driver.findElement(By.id("backtotemplate")).click();
			TimeUnit.SECONDS.sleep(1);
			PlacerCurseur(driver, screenWin, "logoutuser");
			TimeUnit.SECONDS.sleep(1);
			driver.findElement(By.id("logoutuser")).click();
			val++;
		}

	}

	public static void PlacerCurseur(WebDriver driver, Robot screenWin, String nameId) throws InterruptedException {
		WebElement elt = driver.findElement(By.id(nameId));
		Dimension eltSize = elt.getSize();
		// System.out.println("dimenzions: " + eltSize.height + " " + eltSize.width);
		// Used points class to get x and y coordinates of element.
		org.openqa.selenium.Point classname = elt.getLocation();
		int xcordi = classname.getX();
		int ycordi = classname.getY();
		screenWin.mouseMove(xcordi + (int) (eltSize.width / 2), ycordi + (int) (eltSize.height / 2));
		// driver.manage().window().fullscreen();
		TimeUnit.SECONDS.sleep(1);

	}

	public static void PlacerCurseur(WebDriver driver, Robot screenWin, String nameId, int offsetX, int offsetY)
			throws InterruptedException {
		WebElement elt = driver.findElement(By.id(nameId));
		Dimension eltSize = elt.getSize();
		// System.out.println("dimenzions: " + eltSize.height + " " + eltSize.width);
		// Used points class to get x and y coordinates of element.
		org.openqa.selenium.Point classname = elt.getLocation();
		int xcordi = classname.getX();
		int ycordi = classname.getY();
		screenWin.mouseMove(xcordi + (int) (eltSize.width / 2) - offsetX,
				ycordi + (int) (eltSize.height / 2) - offsetY);
		// driver.manage().window().fullscreen();
		TimeUnit.SECONDS.sleep(1);

	}

	public static void AjoutElementProfil(WebDriver driver) throws InterruptedException {

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "profil");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("profil")).click();
		TimeUnit.SECONDS.sleep(1);
		// "INFORMATIONS PERSONNELLES"
		PlacerCurseur(driver, screenWin, "infoPersonnellesFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("infoPersonnellesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "nom");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("nom")).clear();
		driver.findElement(By.id("nom")).sendKeys("Durand");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "prenom");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("prenom")).clear();
		driver.findElement(By.id("prenom")).sendKeys("John");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "datefield");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("datefield")).clear();
		driver.findElement(By.id("datefield")).sendKeys("1980-01-19");
		TimeUnit.SECONDS.sleep(1);

		// dropdown list nationalité
		PlacerCurseur(driver, screenWin, "nationalite_1");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown1 = new Select(driver.findElement(By.id("nationalite_1")));
		dropdown1.selectByVisibleText("FRANCAIS");

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "telephone");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("telephone")).clear();
		driver.findElement(By.id("telephone")).sendKeys("0102030405");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "adresse.rue");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("adresse.rue")).clear();
		driver.findElement(By.id("adresse.rue")).sendKeys("4 rue du Professeur Velas");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "adresse.codePostal");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("adresse.codePostal")).clear();
		driver.findElement(By.id("adresse.codePostal")).sendKeys("31000");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "adresse.ville");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("adresse.ville")).clear();
		driver.findElement(By.id("adresse.ville")).sendKeys("Toulouse");
		TimeUnit.SECONDS.sleep(1);

		// dropdown list nomPays
		PlacerCurseur(driver, screenWin, "adresse.nomPays");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown2 = new Select(driver.findElement(By.id("adresse.nomPays")));
		dropdown2.selectByVisibleText("FRANCE");

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "s0");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s0")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "closeAccInfoPerso");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccInfoPerso")).click();

		// "TITRES"

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "titresFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("titresFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "titre");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("titre")).sendKeys("WEB DEVELOPER");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "s1");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s1")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "titre");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("titre")).sendKeys("CONCEPTEUR BE");
		PlacerCurseur(driver, screenWin, "s1");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s1")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "closeAccTitre");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccTitre")).click();

		// "LANGUES"

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "languesFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("languesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		// dropdown list nomLangue
		PlacerCurseur(driver, screenWin, "nomLangue.id");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown3 = new Select(driver.findElement(By.id("nomLangue.id")));
		dropdown3.selectByVisibleText("ANGLAIS");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "s3");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s3")).click();

		PlacerCurseur(driver, screenWin, "niveau.id");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown4 = new Select(driver.findElement(By.id("niveau.id")));
		dropdown4.selectByVisibleText("Professionnel");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "s3");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s3")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "nomLangue.id");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown5 = new Select(driver.findElement(By.id("nomLangue.id")));
		dropdown5.selectByVisibleText("ESPAGNOL");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "s3");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s3")).click();

		PlacerCurseur(driver, screenWin, "niveau.id");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown6 = new Select(driver.findElement(By.id("niveau.id")));
		dropdown6.selectByVisibleText("Scolaire");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "s3");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s3")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "closeAccLangues");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccLangues")).click();

		// "FORMATIONS"

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formationsFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("formationsFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "intitule");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("intitule")).clear();
		driver.findElement(By.id("intitule")).sendKeys("DUT GENIE MECANIQUE");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formation-datepicker");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("formation-datepicker")).clear();
		driver.findElement(By.id("formation-datepicker")).sendKeys("1998");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "valider");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("valider")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "closeAccFormations");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccFormations")).click();

		CreerExperience(driver);
		// "COMPETENCES"

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "competencesFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("competencesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "comp1");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("comp1")).clear();
		driver.findElement(By.id("comp1")).sendKeys("JAVA JEE");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "n1");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown7 = new Select(driver.findElement(By.id("n1")));
		dropdown7.selectByVisibleText("Confirme");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "s4");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s4")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "comp1");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("comp1")).clear();
		driver.findElement(By.id("comp1")).sendKeys("ANGULAR");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "n1");
		TimeUnit.SECONDS.sleep(1);
		Select dropdown8 = new Select(driver.findElement(By.id("n1")));
		dropdown8.selectByVisibleText("Expert");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "s4");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s4")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "fermerAccCompetence");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("fermerAccCompetence")).click();

		// "CERTIFICATIONS"

		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "certificationsFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("certificationsFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "certif1");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("certif1")).clear();
		driver.findElement(By.id("certif1")).sendKeys("CIMPEC");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "certification-date");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("certification-date")).clear();
		driver.findElement(By.id("certification-date")).sendKeys("Avril 2010");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "score");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("score")).clear();
		driver.findElement(By.id("score")).sendKeys("8");
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "s5");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("s5")).click();
		TimeUnit.SECONDS.sleep(1);

		PlacerCurseur(driver, screenWin, "fermerAccCertification");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("fermerAccCertification")).click();

	}

	// création d'un utilisateur
	public static void CreateUser(WebDriver driver, String nom, String mail, String mdp) throws InterruptedException {
		PlacerCurseur(driver, screenWin, "anim1");
		driver.findElement(By.id("anim1")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "credential.nomLogin");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("credential.nomLogin")).click();
		driver.findElement(By.id("credential.nomLogin")).clear();
		driver.findElement(By.id("credential.nomLogin")).sendKeys(nom);
		TimeUnit.SECONDS.sleep(1); // on insere l'email
		PlacerCurseur(driver, screenWin, "email");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("email")).click();
		driver.findElement(By.id("email")).clear();
		driver.findElement(By.id("email")).sendKeys(mail);
		TimeUnit.SECONDS.sleep(1); // on insere le mdp
		PlacerCurseur(driver, screenWin, "motDePasse");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("motDePasse")).click();
		driver.findElement(By.id("motDePasse")).clear();
		driver.findElement(By.id("motDePasse")).sendKeys(mdp);
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveInscription");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveInscription")).click();
		TimeUnit.SECONDS.sleep(1);
	}

	// connecter l'utilisateur
	public static void ConnectUser(WebDriver driver, String nom, String mdp) throws InterruptedException {
		// on se connecte par la suite
		PlacerCurseur(driver, screenWin, "ident");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("ident")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "login");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys(nom);
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "password");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys(mdp);
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "validateLogin");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("validateLogin")).click();
	}

	// creer experience
	public static void CreerExperience(WebDriver driver) throws InterruptedException {
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "profil");
		driver.findElement(By.id("profil")).click();
		TimeUnit.SECONDS.sleep(1);

		// on insere une experience professionelle
		PlacerCurseur(driver, screenWin, "experiencesProfessionnellesFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("experiencesProfessionnellesFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "addexperience");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("addexperience")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formMetier");
		TimeUnit.SECONDS.sleep(1);
		Select dropdownMetier = new Select(driver.findElement(By.id("formMetier")));
		dropdownMetier.selectByVisibleText("ACTEUR");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "dateDebut");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("dateDebut")).click();
		driver.findElement(By.id("dateDebut")).clear();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("dateDebut")).sendKeys("1999-01-15");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "dateFin");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("dateFin")).click();
		driver.findElement(By.id("dateFin")).clear();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("dateFin")).sendKeys("2002-01-15");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formEntreprise");
		TimeUnit.SECONDS.sleep(1);
		Select dropdownEntreprise = new Select(driver.findElement(By.id("formEntreprise")));
		dropdownEntreprise.selectByVisibleText("ATR");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "projet");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("projet")).click();
		driver.findElement(By.id("projet")).clear();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("projet")).sendKeys("Architecte d'intérieur");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "descriptifCV");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("descriptifCV")).click();
		driver.findElement(By.id("descriptifCV")).clear();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("descriptifCV"))
				.sendKeys("J'ai réalisé la conception de la maquette du nouveau siège principal du client ATR");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "outilsexperience");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("outilsexperience")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilLangage");
		TimeUnit.SECONDS.sleep(1);
		Select dropdownLangueInfo = new Select(driver.findElement(By.id("formOutilLangage")));
		dropdownLangueInfo.selectByVisibleText("Java");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveltoutillangage");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveltoutillangage")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilLogiciel");
		TimeUnit.SECONDS.sleep(1);
		Select dropdownOutilLog = new Select(driver.findElement(By.id("formOutilLogiciel")));
		dropdownOutilLog.selectByVisibleText("git");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveltoutilogiciel");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveltoutilogiciel")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilLogiciel");
		TimeUnit.SECONDS.sleep(1);
		Select dropdownOutilSystExpl = new Select(driver.findElement(By.id("formOutilSystemeExploitation")));
		dropdownOutilSystExpl.selectByVisibleText("Unix");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveltoutilsyst");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveltoutilsyst");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveltoutilsyst")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilBaseDeDonnee");
		TimeUnit.SECONDS.sleep(1);
		Select dropdownOutilBD = new Select(driver.findElement(By.id("formOutilBaseDeDonnee")));
		dropdownOutilBD.selectByVisibleText("SWAP");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveltoutilbd");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveltoutilbd")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveltoutil");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveltoutil")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "closeAccordeonexpro");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeAccordeonexpro")).click();
	}

	// creer resume technique et fonctionnel (les elts entre parentheses dans la
	// méthodes permettent d'ajouter un élement non existant en base de données)
	public static void CreerResumerTechniqueFonctionnel(WebDriver driver) throws InterruptedException {
		// on insere un resume technique et fonctionnel
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "profil");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("profil")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "resumeTechniqueEtFonctionnelFirstSelenium");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("resumeTechniqueEtFonctionnelFirstSelenium")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "methodes");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("methodes")).click();
		driver.findElement(By.id("methodes")).clear();
		driver.findElement(By.id("methodes")).sendKeys("travail en méthode Agile SRUM");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "industries");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("industries")).click();
		driver.findElement(By.id("industries")).clear();
		driver.findElement(By.id("industries")).sendKeys("Aéronautique et spatial secret défense");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "saveresume");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("saveresume")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "resumeaddlangage");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("resumeaddlangage")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilLangage", 0, 425);
		TimeUnit.SECONDS.sleep(1);
		Select dropdownResumeLangage = new Select(driver.findElement(By.id("formOutilLangage")));
		dropdownResumeLangage.selectByVisibleText("KOBOL");
		// TimeUnit.SECONDS.sleep(1);
		// driver.findElement(By.id("editOptionOutilLangage")).sendKeys("Objective C
		// V1");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "savenewresumelangage", 0, 425);
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("savenewresumelangage")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "resumeaddlogiciel");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("resumeaddlogiciel")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilLogiciel", 0, 475);
		TimeUnit.SECONDS.sleep(1);
		Select dropdownResumeLogiciel = new Select(driver.findElement(By.id("formOutilLogiciel")));
		dropdownResumeLogiciel.selectByVisibleText("safe");
		// TimeUnit.SECONDS.sleep(1);
		// driver.findElement(By.id("editOptionOutilLogiciel")).sendKeys("Spring Tool
		// Suite V1");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "savenewresumelogiciel", 0, 475);
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("savenewresumelogiciel")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "resumeaddBD");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("resumeaddBD")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilBaseDeDonnee", 0, 525);
		TimeUnit.SECONDS.sleep(1);
		Select dropdownResumeBD = new Select(driver.findElement(By.id("formOutilBaseDeDonnee")));
		dropdownResumeBD.selectByVisibleText("VPM");
		// TimeUnit.SECONDS.sleep(1);
		// driver.findElement(By.id("editOptionOutilBaseDeDonnee")).sendKeys("SQL Server
		// V1");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "savenewresumeBD", 0, 525);
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("savenewresumeBD")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "resumeaddSE");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("resumeaddSE")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "formOutilSystemeExploitation", 0, 575);
		TimeUnit.SECONDS.sleep(1);
		Select dropdownResumeSE = new Select(driver.findElement(By.id("formOutilSystemeExploitation")));
		dropdownResumeSE.selectByVisibleText("Pack office windows");
		// TimeUnit.SECONDS.sleep(1);
		// driver.findElement(By.id("editOptionOutilSystemeExploitation")).sendKeys("Mandriva
		// V1");
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "savenewresumeSE", 0, 575);
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("savenewresumeSE")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "closeresumeaccordeon");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("closeresumeaccordeon")).click();
	}

	// méthode de choix du cv2
	public static void choixcv2() throws InterruptedException {
		WebDriver driver;
		System.setProperty("webdriver.gecko.driver", "data\\geckodriver.exe");
		driver = new FirefoxDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("http://localhost:8080/ProjetOnlineCv/securitycontroller/login");
		driver.findElement(By.id("ident")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("login")).click();
		driver.findElement(By.id("login")).clear();
		driver.findElement(By.id("login")).sendKeys("Ausy_User_0");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("password")).clear();
		driver.findElement(By.id("password")).sendKeys("ausy2018");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("validateLogin")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("profil")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("choixtemplate")).click();
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("Cv2")).click();
		driver.findElement(By.id("gen-pdfcv2")).click();
	}

	public static void CV(WebDriver driver) throws InterruptedException {

		PlacerCurseur(driver, screenWin, "selectdatatoshow");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("selectdatatoshow")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "selectalldata");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("selectalldata")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "play_button");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("play_button")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "choixtemplate");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("choixtemplate")).click();
		TimeUnit.SECONDS.sleep(1);
		PlacerCurseur(driver, screenWin, "Cv2");
		TimeUnit.SECONDS.sleep(1);
		driver.findElement(By.id("Cv2")).click();
		TimeUnit.SECONDS.sleep(3);
		// PlacerCurseur(driver, screenWin, "gen-pdfcv2");
		// TimeUnit.SECONDS.sleep(1);
		// driver.findElement(By.id("gen-pdfcv2")).click();
	}
}
